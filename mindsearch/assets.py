# -*- coding: utf-8 -*-
from flask.ext.assets import Bundle, Environment

css = Bundle(
    # Libraries:

    # Custom files:
    "css/style.css",
    "css/playground.css",
    "css/form.css",
    "css/about.css",

    filters="cssmin",
    output="final/css/packed.css"
)

js = Bundle(
    # Libraries:
    "libs/jquery/dist/jquery.js",
    # (Use min b/c normal has bug: "Uncaught SyntaxError: Unexpected token if".)
    "libs/bootstrap/dist/js/bootstrap.min.js",
    "libs/react/react.js",
    "libs/react/JSXTransformer.js",

    "libs/jquery-color/jquery.color.js",
    "libs/jquery-color/jquery.color.svg-names.js",
    "libs/css-element-queries/src/ResizeSensor.js",
    "libs/css-element-queries/src/ElementQueries.js",
    "libs/date.js",  # For Date.parseString().
    "libs/FileSaver.js",

    # Helpers:
    "js/helpers/inline-styles.js",
    "js/helpers/plugins.js",
    "js/helpers/utils.js",

    # General files:
    "js/globals.js",
    "js/switches.js",

    # /about:
    "js/about/about.js",

    # /playground:
    "js/playground/indexer.js",
    "js/playground/file-handler.js",
    "js/playground/css.js",
    # /playground/ui - reference built jsx:
    "final/built-jsx/playground/ui/cloud.js",
    "final/built-jsx/playground/ui/stream.js",
    "final/built-jsx/playground/ui/portal.js",
    "final/built-jsx/playground/ui/menu.js",
    "final/built-jsx/playground/ui/form.js",
    "final/built-jsx/playground/ui/memories-area.js",
    "final/built-jsx/playground/ui/download-area.js",
    "final/built-jsx/playground/ui/upload-area.js",
    "final/built-jsx/playground/ui/playground.js",  # Main file.

    filters='jsmin',
    output="final/js/packed.js"
)

assets = Environment()

assets.register("js_all", js)

assets.register("css_all",
    # Use minified version so that icons are included properly.
    "libs/bootstrap/dist/css/bootstrap.css",
    "libs/font-awesome/css/font-awesome.min.css",
    css)

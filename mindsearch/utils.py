# -*- coding: utf-8 -*-
"""Helper utilities and decorators."""
from flask import request, render_template, flash, url_for, redirect
from flask.ext.login import login_user
from mindsearch.public.forms import LoginForm

def flash_errors(form, category='warning'):
    """Flash all errors for a form."""
    for field, errors in form.errors.items():
        for error in errors:
            flash('{0} - {1}'
                    .format(getattr(form, field).label.text, error), category)

def render_navbar_template(tmpl_name, **kwargs):
    login_form = LoginForm(request.form)
    # Handle logging in.
    if request.method == 'POST':
        if login_form.validate_on_submit():
            login_user(login_form.user)
            flash('You are logged in.', 'success')
            redirect_url = request.args.get("next") or url_for("user.members")
            return redirect(redirect_url)
        else:
            flash_errors(login_form)
    return render_template(tmpl_name, login_form=login_form, **kwargs)

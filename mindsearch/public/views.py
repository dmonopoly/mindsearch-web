# -*- coding: utf-8 -*-
'''Public section, including homepage and signup.'''
from flask import (Blueprint, request, render_template, flash, url_for,
                    redirect, session)
from flask import jsonify
from flask.ext.login import login_required
from flask.ext.login import logout_user

from mindsearch.extensions import login_manager
from mindsearch.user.models import User
from mindsearch.public.forms import LoginForm
from mindsearch.user.forms import RegisterForm
from mindsearch.utils import flash_errors
from mindsearch.utils import render_navbar_template
from mindsearch.database import db


blueprint = Blueprint('public', __name__, static_folder="../static")

@login_manager.user_loader
def load_user(id):
    return User.get_by_id(int(id))

@blueprint.route("/", methods=["GET", "POST"])
def home():
    return render_navbar_template("public/home.html")

@blueprint.route("/playground")
def playground():
    return render_navbar_template("public/playground.html")

@blueprint.route('/logout/')
@login_required
def logout():
    logout_user()
    flash('You are logged out.', 'info')
    return redirect(url_for('public.home'))

@blueprint.route("/register/", methods=['GET', 'POST'])
def register():
    register_form = RegisterForm(request.form, csrf_enabled=False)
    if register_form.validate_on_submit():
        new_user = User.create(username=register_form.username.data,
                        email=register_form.email.data,
                        password=register_form.password.data,
                        active=True)
        flash("Thank you for registering. You can now log in.", 'success')
        return redirect(url_for('public.home'))
    else:
        flash_errors(register_form)
    return render_navbar_template('public/register.html', register_form=register_form)

@blueprint.route("/about/")
def about():
    return render_navbar_template("public/about.html")

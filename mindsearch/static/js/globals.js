// Global variables and constants.

// Not used, but could be used for localStorage or sessionStorage feature.
// globalDb
// Stores memories indexed by numbers, from 1..num memories.
// Usage:
//    globalDb.getObject('1')
//    globalDb.getObject(1))
//    globalDb.getObject(1).mykeyname
//    globalDb.setObject(key, oneMem);
// var globalDb = null;
// if (typeof(Storage) !== "undefined") {
//   globalDb = sessionStorage;  // Special JS variable.
// } else {
//   console.error("No Web Storage support.");
// }

global = {
  // The in-memory "database" of memories and metadata.
  Index: {
    // Filled in by memories-indexer.js's indexMemories().
    memoryObjects: [],
    earliestDate: null,
    latestDate: null,
    datesToMemoryCount: {},
  },

  /* High-level data for the main memories area. */

  // Types of actions for interacting with memories.
  Action: {
    EDIT: 'Edit',
    INDEX: 'Index',  // All memory viewing actions.
    NEW: 'New'
  },

  // Types of views for viewing memories, e.g., Stream, Cloud.
  View: {
    STREAM: 'Stream'
  },

  // List representation of View.
  ViewList: ['Stream'],

  /* Form data. */
  FormAction: {
    UPDATE: 'Update',
    CREATE: 'Create'
  },
  FormWritingMode: {
    NORMAL: 'Normal',
    RAMBLING: 'Rambling'
  },
  FormRamblingButtonText: {  // The actual text that appears on the button.
    RAMBLING: 'Thought-spilling mode',
    NORMAL: 'Normal mode'
  },

  // List of date string examples that work.
  DateStringExamples: "3/15/2015, 12/25/2015",

  /* Misc. */

  // Types of Portals, so the Portal component can choose the right design.
  PortalType: {
    RETURN_FROM_FORM: 'Return from form',
    CUSTOM: 'Custom'
  },
}

global.Structure = {
  buildMemory: function(mid, newContent, newDateStr,
                        newDate, newIsExactDate, newNote) {
    return {
      id: mid,
      content: newContent,
      dateStr: newDateStr,
      date: newDate,
      isExactDate: newIsExactDate,
      note: newNote
    }
  },
  buildMemoryNoDate: function(mid, newContent, newDateStr, newIsExactDate, newNote) {
    return {
      id: mid,
      content: newContent,
      dateStr: newDateStr,
      date: null,
      isExactDate: newIsExactDate,
      note: newNote
    }
  },
  getBlankMemory: function() {
    return {
      // This lets a new memory automatically have the right ID already set.
      // IDs begin from 1.
      id: global.Index.memoryObjects.length + 1,
      content: "",
      date: null,
      dateStr: "",
      isExactDate: true,
      note: "",
    }
  },
  getMemoryShapeRequired: function() {
    return React.PropTypes.shape({
      id: React.PropTypes.number,
      content: React.PropTypes.string,
      date: React.PropTypes.object,
      dateStr: React.PropTypes.string,
      isExactDate: React.PropTypes.bool,
      note: React.PropTypes.string
    }).isRequired
  }
}

// A place for quick switches while working, like if you want to skip the
// upload interaction and get right to some raw memories.
var switch_SKIP_TO_EDITING = false;
var switch_SKIP_UPLOAD_INTERACTION = false
var switch_WARN_ON_EXIT = true;

var switch_PRINT_MEMORIES = false;

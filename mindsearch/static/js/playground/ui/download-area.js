// Requirements: file-handler.js for downloadMemories().
var Action = global.Action;

var DownloadArea = React.createClass({
  propTypes: {
  },
  render: function() {
    return (
      <div id="download-area">
        <button onClick={this._download} type="button" className="btn btn-default" aria-label="download">
          <i className="glyphicon glyphicon-download-alt" aria-hidden="true"></i> Download
        </button>
      </div>
    );
  },
  _download: function() {
    downloadMemories();
  }
});

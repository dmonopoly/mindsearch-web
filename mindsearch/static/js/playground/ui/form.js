var FormAction = global.FormAction;
var FormRamblingButtonText = global.FormRamblingButtonText;
var FormWritingMode = global.FormWritingMode;
var Structure = global.Structure;

var MemoryForm = React.createClass({
  propTypes: {
    memory: Structure.getMemoryShapeRequired(),

    // global.FormAction for which action is currently taking place.
    formAction: React.PropTypes.string.isRequired,

    updateMemory: React.PropTypes.func,
    createMemory: React.PropTypes.func
  },
  getInitialState: function() {
    var state = this.props.memory;
    // Additional writing mode - FormRamblingButtonText are possible values.
    state.writingMode = FormWritingMode.NORMAL;
    return state;
  },
  render: function() {
    // TODO: Rambling mode option.
          // <button id="rambling-mode-button"
          //         className="btn btn-default faint-button"
          //         onClick={this._handleRambleButton}
          //         aria-label="toggle rambling mode">
          //   {FormRamblingButtonText.RAMBLING}
          // </button>
    return (
      <form id="memory-form" onSubmit={this.handleSubmit}>
        <div id="error-msg"></div>
        <div id="main-form-area">
          <input type="text" id="memory-date-area" className="faint-border form-space"
                             ref="dateStr" placeholder="When? Default: today"
                             value={this.state.dateStr}
                             onChange={this._handleDateStrChange} />
          <div id="memory-exact-date-area-container">
            <input type="checkbox" id="memory-exact-date-area"
                                   ref="isExactDate" placeholder="SUP"
                                   checked={this.state.isExactDate}
                                   onChange={this._handleIsExactDateChange} />
            <span className="faint-text"> Exact?</span>
          </div>
          <br />
          <br />
          <textarea id="memory-content-area" className="faint-border form-space"
                    ref="content" placeholder="Write for yourself"
                    value={this.state.content}
                    onChange={this._handleContentChange} /><br/>
          <br />
          <button id="memory-submit-button" type="submit"
                  className="btn btn-default faint-button" aria-label="submit">
            <i className="glyphicon glyphicon-check" aria-hidden="true"></i> Done
          </button>
          <input type="text" id="memory-note-area" className="faint-border form-space"
                             ref="note" placeholder="Notes, hashtags"
                             value={this.state.note}
                             onChange={this._handleNoteChange} /><br />
        </div>
      </form>
    )
  },
  componentDidMount: function() {
    // Adjusts width dynamically.
    $("#main-form-area").css('width', $("#memory-content-area").css('width'));
    var margin = parseInt($("#memory-note-area").css('margin-left'));
    $("#memory-note-area").css('width', $("#memory-content-area").width() - $("#memory-date-area").width() - margin + 'px');

    $("#memory-content-area").focus();
  },
  // Briefly highlights a form field that is erroneous.
  // @param {array} errors List of 2-tuples of invalid fields, like this:
  // [ {'html_id' of invalid field, 'msg'}, ... ],
  _flashError: function(errors) {
    console.log('errors:');
    console.log(errors);
    for (var e_idx in errors) {
      var e = errors[e_idx];
      var field = $("#" + e.html_id);
      var oldColor = field.css('background');

      field.animate({backgroundColor: 'gold'});
      field.animate({backgroundColor: oldColor}, 'slow').delay(300);

      if (e.msg) {
        $("#error-msg").append('<div class="alert alert-warning alert-dismissible">' + e.msg + '</div>');
      }
    }
  },
  _handleContentChange: function(event) {
    this.setState({content: event.target.value});
  },
  _handleDateStrChange: function(event) {
    this.setState({dateStr: event.target.value});
  },
  _handleNoteChange: function(event) {
    this.setState({note: event.target.value});
  },
  _handleIsExactDateChange: function(event) {
    this.setState({isExactDate: event.target.checked});
  },
  // _handleRambleButton: function(event) {
  //   event.preventDefault();
  //   $("#memory-content-area").toggleClass('rambling-mode', 800);
  //   console.log(this.state.writingMode);
  //   if (this.state.writingMode == FormWritingMode.NORMAL) {
  //     $("#rambling-mode-button").html(FormRamblingButtonText.NORMAL);
  //     this.setState({writingMode: FormWritingMode.RAMBLING});
  //   } else {
  //     $("#rambling-mode-button").html(FormRamblingButtonText.RAMBLING);
  //     this.setState({writingMode: FormWritingMode.NORMAL});
  //   }
  // },
  handleSubmit: function(e) {
    e.preventDefault();
    var newContent = this.refs.content.getDOMNode().value.trim();
    var newDateStr = this.refs.dateStr.getDOMNode().value.trim();
    var newNote = this.refs.note.getDOMNode().value.trim();
    var newIsExactDate = this.refs.isExactDate.getDOMNode().checked;

    var newMem = Structure.buildMemoryNoDate(this.props.memory.id, newContent,
      newDateStr, newIsExactDate, newNote);
    var errors = this._validateForm(newMem);
    if (errors.length == 0) {
      if (this.props.formAction == FormAction.UPDATE) {
        console.log("Calling updateMemory with id " + this.props.memory.id);
        this.props.updateMemory(newMem);
      } else if (this.props.formAction == FormAction.CREATE) {
        console.log("Calling createMemory with id " + this.props.memory.id);
        this.props.createMemory(newMem);
      } else {
        console.error("No such form action.");
      }
    } else {
      console.log("Error in memory form.");
      this._flashError(errors)
    }
  },
  // Modifies mem for necessary field conversions, like date adjustments.
  // Returns a list of 2-tuples of invalid fields, like this:
  // [ {'html_id' of invalid field, 'msg'}, ... ].
  // @param {object} mem Memory object with standard memory properties.
  _validateForm: function(mem) {
    // First clean up any old error messages.
    $("#error-msg").html('');

    var errors = [];
    _validateContent(mem.content, errors);
    _validateDate(mem, errors);
    return errors;

    function _validateContent(content, errors) {
      var dateLinePattern = /^(\d\d?\/\d\d?\/\d\d\d\d)\b(~*)(.*)\b/g;
      var lines = util.splitIntoLines(content);
      if (content.trim().length == 0) {
        var err_obj = {
          html_id: 'memory-content-area',
          msg: "Memory should have content. That's the whole point of writing!"
        }
        errors.push(err_obj);
        return;
      }
      for (line_idx in lines) {
        var line = lines[line_idx];
        if (line.match(dateLinePattern)) {
          var err_obj = {
            html_id: 'memory-content-area',
            msg: "Content should not contain a date like '1/1/2000' on a new line - otherwise it conflicts with your downloaded text file!"
          }
          errors.push(err_obj);
        }
      }
    }
    function _validateDate(mem, errors) {
      if (!mem.dateStr) {
        // Use today's date.
        mem.date = new Date();
        mem.dateStr = util.convertDateToString(mem.date);
        return;
      }
      var dateLinePattern = /^(\d\d?\/\d\d?\/\d\d\d\d)\b(~*)(.*)\b/g;
      if (!mem.dateStr.match(dateLinePattern)) {
        var err_obj = {
          html_id: 'memory-date-area',
          msg: "Sorry, I can't comprehend the date. You can enter things like this: " + global.DateStringExamples
        }
        errors.push(err_obj);
      } else {
        mem.date = Date.parseString(mem.dateStr);
      }
    }
  }
});

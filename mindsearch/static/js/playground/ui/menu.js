var Action = global.Action;
var View = global.View;

var SearchArea = React.createClass({
  propTypes: {
    searchText: React.PropTypes.string.isRequired,
    handleSearch: React.PropTypes.func.isRequired
  },
  render: function() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input id="search-text-field" type="text" placeholder="Search" ref="searchText"
               defaultValue={this.props.searchText} />
        <button type="submit" className="btn btn-default" aria-label="search">
          <i className="glyphicon glyphicon-search" aria-hidden="true"></i>
        </button>
      </form>
    )
  },
  handleSubmit: function(e) {
    e.preventDefault();
    var searchStr = this.refs.searchText.getDOMNode().value.trim();
    this.props.handleSearch(searchStr);
  }
});

var ViewButton = React.createClass({
  propTypes: {
    active: React.PropTypes.bool.isRequired,
    name: React.PropTypes.string.isRequired,
    setView: React.PropTypes.func.isRequired,
    view: React.PropTypes.string.isRequired
  },
  render: function() {
    var className = this.props.active ? "active" : null;
    return (
      <li role="presentation" className={className}>
        <a href="javascript:void(0);" onClick={this.updateView}>{this.props.name}</a>
      </li>
    )
  },
  updateView: function() {
    this.props.setView(this.props.view);
  }
});

var MenuBar = React.createClass({
  propTypes: {
    handleSearch: React.PropTypes.func.isRequired,
    searchText: React.PropTypes.string.isRequired,
    setView: React.PropTypes.func.isRequired,
    view: React.PropTypes.string.isRequired,
    viewList: React.PropTypes.array.isRequired
  },
  render: function() {
    var currViewName = this.props.view;
    var viewButtons = this.props.viewList.map(function(val, i) {
      return (
        <ViewButton key={val} active={val === currViewName} name={val} index={i}
                    view={this.props.view} setView={this.props.setView} />
      )
    }, this);
    return (
      <div id="menu-bar">
        <SearchArea searchText={this.props.searchText} handleSearch={this.props.handleSearch} />
      </div>
    );

    // View Buttons are currently hidden. When you want more views:
    // return (
    //   <div id="menu-bar">
    //     <SearchArea searchText={this.props.searchText} handleSearch={this.props.handleSearch} />
    //     <ul id="view-buttons" className="nav nav-pills">
    //       {viewButtons}
    //     </ul>
    //   </div>
    // );
  }
});

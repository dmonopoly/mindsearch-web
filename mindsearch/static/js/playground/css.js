// TODO: Reorganize with namespacing, like utils.

function cssGetWidthOfMemoriesArea() {
	return $("#memories-area").width();
}

function cssGetWindowWidth() {
	// return $(window).width();
	return window.innerWidth;
}

function cssGetWindowHeight() {
	// return $(window).height();
	return window.innerHeight;
}

function cssCenterHoverArea() {
	// Since centering fixed divs is impossible in css without knowing the width
	// of the "parent" (which is lost once "fixed" is applied), we use JS to
	// dynamically detect the width.
	$("#hover-area").css('width', cssGetWidthOfMemoriesArea());

	// Dynamically check for changing width.
	new ResizeSensor(jQuery("#memories-area"), function() {
		$("#hover-area").css('width', cssGetWidthOfMemoriesArea());
	});
}

function cssHoverAreaToggle(nodeId, truncatedText, setHoverFn) {
	$(nodeId).hover(
	  function() {
	    setHoverFn(truncatedText);
	    // $("#hover-area").css('z-index', 1);
	    $("#hover-area").stop(true, true).fadeToggle();
	  }, function() {
	    setHoverFn("");
	    // $("#hover-area").css('z-index', 'auto');
	    $("#hover-area").stop(true, true).fadeToggle();
	  }
	);
}

function cssPrettifyDropzoneDragEnterAndLeave() {
	// Make the dropzone's drag enter/leave cause style changes.
	var drop = document.getElementById("dropzone");
	drop.addEventListener("dragenter", change, false);
	drop.addEventListener("dragleave", changeBack, false);
	drop.addEventListener("mouseout", changeBack, false);
	var oldBorder = drop.style.border;

	function change() {
	  drop.style.border = '1px dashed black';
	};

	function changeBack() {
	  drop.style.border = oldBorder;
	};
}

// function cssScatterCloudMemories() {
// 	// Move each .memory in #memories-area
// 	$("#memories-area .memory").each(function(i, elem) {
//     $(elem).css('left', Math.random() * cssGetWidthOfMemoriesArea()); // add extra bit so memories aren't too far left
//     $(elem).css('top', Math.random() * cssGetWindowHeight()); // add extra bit to prevent overlap with header
// 	});
// }

// Common inline styles for react.js. http://facebook.github.io/react/tips/inline-styles.html
// Example:
// var divStyle = {
//   color: 'white',
//   backgroundImage: 'url(' + imgUrl + ')',
//   WebkitTransition: 'all', // note the capital 'W' here
//   msTransition: 'all' // 'ms' is the only lowercase vendor prefix
// };
// React.render(<div style={divStyle}>Hello World!</div>, mountNode);

var displayNone = {
  display: "none"
};

var transparentBackground = {
  background: "transparent"
}


// Avoid acting on absent elements. http://learn.jquery.com/performance/dont-act-on-absent-elements/
// Usage: $("li.cartitems").doOnce(function() ... );
jQuery.fn.doOnce = function( func ) {
  this.length && func.apply( this );
  return this;
}

// Returns a truncated version of a string with ellipsis as appropriate.
// Does not alter the string.
// Usage: myString.truncate(120, true)
var ELLIPSIS = '...'; // '&hellip;'
String.prototype.truncate =
  function(n) {
    var useWordBoundary = true;
    var tooLong = this.length > n;
    var s_ = tooLong ? this.substr(0, n-1) : this;
    s_ = useWordBoundary && tooLong ? s_.substr(0, s_.lastIndexOf(' ')) : s_;
    var finalString = tooLong ? _cleanEllipsis(s_ + ELLIPSIS) : s_;
    return String(finalString);
  };

// Extensions to HTML Storage to handle objects.
Storage.prototype.setObject = function(key, value) {
  this.setItem(key, JSON.stringify(value));
}
Storage.prototype.getObject = function(key) {
  var value = this.getItem(key);
  return value && JSON.parse(value);
}

// Cleans up strange ellipsis endings like ,... and !..., and ?...
function _cleanEllipsis(string) {
  var i = string.lastIndexOf(ELLIPSIS);
  if (string[i-1].match(/[,;:?!\/\.]/)) {
    return string.substr(0, i-1) + ELLIPSIS;
  }
  return string;
}

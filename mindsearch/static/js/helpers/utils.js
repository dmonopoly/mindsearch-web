// Global utility functions.
// Usage: Only use those functions in util object. Use util.(function name) for clarity.
util = {
  convertDateToString: function(dateObj) {
    var mdy = _getMonthDayYearStrArray(dateObj);
    return mdy[0] + '/' + mdy[1] + '/' + mdy[2];
  },
  getFilename: function() {
    var mdy = _getMonthDayYearStrArray(new Date());
    var today = mdy[0] + '-' + mdy[1] + '-' + mdy[2];
    return 'my_memories ' + today + '.txt';
  },
  findById: function(id, list) {
    for (var i = 0; i < list.length; ++i) {
      if (list[i].id == id)
        return list[i];
    }
    return null;
  },
  // http://stackoverflow.com/questions/1244535/alert-when-browser-window-closed-accidentally
  setConfirmUnload: function(on, msgFn) {
    if (msgFn)
      window.onbeforeunload = (on) ? msgFn : null;
    else
      window.onbeforeunload = (on) ? _unloadMessage : null;
  },
  splitIntoLines: function(contents) {
    return contents.split(/\n/);
  }
}

function _unloadMessage() {
  // return "Are you sure you want to leave this page?";
  return "Just checking:";
}

// Returns an array: [month num, day num, year num] like [12, 25, 2015]. Each entry is a string.
function _getMonthDayYearStrArray(dateObj) {
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth() + 1; // January is 0.
  var yyyy = today.getFullYear();
  var TWO_DIGITS = false;
  if (TWO_DIGITS) {
    if (dd < 10)
      dd = '0' + dd;
    if (mm < 10)
      mm = '0' + mm;
  }
  return [mm, dd, yyyy];
}

// TODO: security with escape functions?
/*
 * Prepare text for HTML display safely.
 * http://stackoverflow.com/questions/1787322/htmlspecialchars-equivalent-in-javascript
 */
function escapeHtml(text) {
  var map = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#039;'
  };

  return text.replace(/[&<>"']/g, function(m) { return map[m]; });
}

// function escapeHtml(unsafe) {
//   return unsafe
//    .replace(/&/g, "&amp;")
//    .replace(/</g, "&lt;")
//    .replace(/>/g, "&gt;")
//    .replace(/"/g, "&quot;")
//    .replace(/'/g, "&#039;");
// }

 /*
  * Inverse of escapeHtml.
  */
function unescapeHtml(safe) {
  return safe
   .replace(/&amp;/g, "&")
   .replace(/&lt;/g, "<")
   .replace(/&gt;/g, ">")
   .replace(/&quot;/g, "\"")
   .replace(/&#039;/g, "'");
}

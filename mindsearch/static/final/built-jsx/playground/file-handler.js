// Helper file handling functions for upload-area and download-area.
// Requirements:
// memories-indexer.js for Index and functions.

var Index = global.Index;

/* Download Functions */

function downloadMemories() {
  console.log('Downloading memories');
  var text = getMemoriesText();

  var blob = new Blob([text], {
    type: "text/plain;charset=utf-8;",
  });
  saveAs(blob, util.getFilename());
}

function getMemoriesText() {
  var text = '';
  for (var i = 0; i < Index.memoryObjects.length; ++i) {
    var mem = Index.memoryObjects[i];
    console.log(mem);
    var dateLine = mem.dateStr;
    if (!mem.isExactDate)
      dateLine += '~';
    if (mem.note)
      dateLine += ' ' + mem.note;
    text += dateLine + '\n' + mem.content + '\n';
    if (i != Index.memoryObjects.length - 1) {
      text += '\n';
    }
  }
  return text;
}

/* Upload Functions */

function _collectMemoryFileAndDisplay(theFile, setMemoriesFn) {
	return function(e) {
		var contents = e.target.result;
		processMemoryFile(theFile.name, contents, setMemoriesFn);

		var outputDiv = document.createElement('div');
		outputDiv.innerHTML = ['<span>', escape(theFile.name), '</span>'].join('');
		document.getElementById('list').insertBefore(outputDiv, null);
	}

  function processMemoryFile(fName, fContent, setMemoriesFn) {
    console.log('Got file: ' + fName);

    // Run pure JS to index memories.
    initIndexFromText(fContent);
    var arrayOfMemories = Index.memoryObjects;
    setMemoriesFn(arrayOfMemories);

    showMemoryPlaygroundAndHideUpload();
  }
}

function handleDragOver(evt) {
  evt.stopPropagation();
  evt.preventDefault();
  evt.dataTransfer.dropEffect = 'copy';
}

/*
 * Handles importing multiple files that are dragged and dropped.
 * @param {function} setMemoriesFn Function to update the complete list of all
 * memories in PlaygroundContainer.
 */
function handleDroppedFileSelect(evt, setMemoriesFn) {
  evt.stopPropagation();
  evt.preventDefault();

  var files = evt.dataTransfer.files; // FileList object.

  var output = [];
  for (var i = 0, f; f = files[i]; i++) {
    if (!f.type.match('text.*')) { continue; }
    var r = new FileReader();
    r.onload = _collectMemoryFileAndDisplay(f, setMemoriesFn);
    r.readAsText(f);
  }
}

/*
 * Handles importing a single file triggered by clicking on the dropzone.
 * This expects evt.data.setMemoriesFn to be available.
 */
function readSingleFile(evt) {
  // Retrieve the first (and only) File from the FileList object.
  var f = evt.target.files[0];

  if (!f) {
    alert("Failed to load file.");
  } else if (!f.type.match('text.*')) {
    alert(f.name + " is not a valid text file.");
  } else {
    var r = new FileReader();
    r.onload = _collectMemoryFileAndDisplay(f, evt.data.setMemoriesFn);
    r.readAsText(f);
  }
}

function showMemoryPlaygroundAndHideUpload() {
  $("#playground").show();
  $("#download-area").show();
  $("#upload-area").hide();
}

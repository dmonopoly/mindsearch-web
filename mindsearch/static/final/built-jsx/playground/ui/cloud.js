// Postponed. View not as critical as other features.
var CloudMemoryNode = React.createClass({displayName: "CloudMemoryNode",
  propTypes: {
    id: React.PropTypes.string.isRequired, // Format: '5-memory'
    content: React.PropTypes.string.isRequired,
    dateStr: React.PropTypes.string.isRequired,
    isExactDate: React.PropTypes.bool.isRequired,
    note: React.PropTypes.string.isRequired,
    setHoverText: React.PropTypes.func.isRequired,
    x: React.PropTypes.number,
    y: React.PropTypes.number
  },
  getInitialState: function() {
    return {
      x: Math.random() * cssGetWindowWidth(),
      y: Math.random() * cssGetWindowHeight()
    };
  },
  render: function() {
    return (
      React.createElement("div", {id: this.props.id, className: "memory cloud-memory"}, 
        React.createElement("a", {href: "javascript:void(0);"}, 
          this.props.content, React.createElement("br", null)
        )
      )
    )
  },
  componentDidMount: function() {
    var nodeId = "#" + this.props.id;
    var truncatedText = this.props.content.truncate(140, true);
    var setHoverFn = this.props.setHoverText;
    cssHoverAreaToggle(nodeId, truncatedText, setHoverFn);

    console.log("name: " + this.props.content);
    console.log("x: " + this.state.x);
    console.log("y: " + this.state.y);
    $(nodeId).css('left', this.state.x);
    $(nodeId).css('top', this.state.x);
    $(nodeId).css('left', 0);
    $(nodeId).css('top', 0);
  }
});

// Requirements: file-handler.js.
var UploadArea = React.createClass({displayName: "UploadArea",
  propTypes: {
    // Function to update the complete list of all memories in
    // PlaygroundContainer.
    setMemories: React.PropTypes.func.isRequired,
    launchBlankSlate: React.PropTypes.func.isRequired
  },
  render: function() {
    return (
      React.createElement("div", {id: "upload-area"}, 
        React.createElement("div", {id: "clean-start", className: "big-button well well-lg"}, 
          React.createElement("h1", null, 
            "New ", React.createElement("br", null), React.createElement("br", null), 
            React.createElement("i", {className: "glyphicon glyphicon-pencil"})
          )
        ), 
        React.createElement("input", {type: "file", id: "file-input", style: displayNone}), 
        React.createElement("div", {id: "dropzone", className: "big-button well well-lg"}, 
          React.createElement("h1", null, 
            "Existing ", React.createElement("br", null), React.createElement("small", null, "Drag & Drop, or Click Here"), React.createElement("br", null), React.createElement("br", null), 
            React.createElement("i", {className: "glyphicon glyphicon-cloud-upload"})
          )
        ), 
        React.createElement("output", {id: "list"})
      )
    )
  },
  componentDidMount: function() {
    if (window.File && window.FileReader && window.FileList && window.Blob) {
      if (switch_SKIP_UPLOAD_INTERACTION)
        showMemoryPlaygroundAndHideUpload();
      cssPrettifyDropzoneDragEnterAndLeave();

      /* New file. */
      $("#clean-start").click(this.props.launchBlankSlate);

      /* Choosing a file. */
      // Enable drag-and-drop for choosing files.
      var dropzone = $('#dropzone');
      var domDropzone = dropzone[0];
      domDropzone.addEventListener('dragover', handleDragOver, false);
      domDropzone.addEventListener('drop', (function(evt) {
        handleDroppedFileSelect(evt, this.props.setMemories);
      }).bind(this), false);

      // Enable clicking-on-dropzone for choosing files.
      $('#file-input').bind('change', {
        setMemoriesFn: this.props.setMemories // Make this available in evt.data
      }, readSingleFile);
      dropzone.click(function(evt) {
        $('#file-input').click();
      });
    } else {
      alert('The File APIs are not fully supported by your browser.');
    }
  }
});

// MemoriesArea is the display for all memories, contained in the Playground.

var Action = global.Action;
var FormAction = global.FormAction;
var PortalType = global.PortalType;
var Structure = global.Structure;
var View = global.View;

var MemoriesArea = React.createClass({displayName: "MemoriesArea",
  propTypes: {
    // All memories - provided so we can get unique memIDs for each memory.
    allMemories: React.PropTypes.array.isRequired,

    // global.Action for which action is currently taking place.
    action: React.PropTypes.string.isRequired,

    // The initial memory to fill the memory form with.
    formMemory: Structure.getMemoryShapeRequired(),

    // Functions to activate the memory form view.
    launchNewMemoryForm: React.PropTypes.func.isRequired,
    launchEditMemoryForm: React.PropTypes.func.isRequired,

    // Functions to update/create the memory officially when done editing.
    updateMemory: React.PropTypes.func.isRequired,
    createMemory: React.PropTypes.func.isRequired,

    setAction: React.PropTypes.func.isRequired,

    // Memories currently being shown. Updated through search.
    shownMemories: React.PropTypes.array.isRequired,

    // global.View for which view is currently shown.
    view: React.PropTypes.string.isRequired
  },
  getInitialState: function() {
    return {
      hoverText: ""
    };
  },
  render: function() {
    if (switch_PRINT_MEMORIES) {
      console.log('Shown memories from playground: ');
      console.log(this.props.shownMemories);
      console.log('All memories from playground: ');
      console.log(this.props.allMemories);
    }
    if (this.props.action == Action.INDEX &&
        this.props.shownMemories.length == 0) {
      return (
        React.createElement("div", {id: "memories-area", className: "centered"}, 
          "No memories to show!", 
          React.createElement("br", null), 
          React.createElement("br", null), 
          React.createElement(Portal, {type: PortalType.CUSTOM, icon: "fa fa-plus", msg: "Create one", 
                  detailedAction: this.props.launchNewMemoryForm})
        )
      );
    }

    if (this.props.action == Action.INDEX) {
      var memoryNodes = this._createMemoryNodesOfType(View.STREAM);
      // Join nodes into one list.
      // Array.prototype.push.apply(memoryNodes, cloudNodes);

      var classValues = "my-list-group";
      // Old:
          // <div id="memories-area" className={classValues}>
          //   {memoryNodes}
          // </div>
      return (
        React.createElement("div", {id: "memories-area"}, 
          React.createElement("div", {id: "hover-area", style: displayNone}, 
            React.createElement("div", {id: "hover-area-text"}, 
              this.state.hoverText
            )
          ), 
          React.createElement("div", {id: "memories-area", className: classValues}, 
            memoryNodes
          )
        )
      );
    } else if (this.props.action == Action.EDIT) {
      return (
        React.createElement("div", {id: "memories-area"}, 
          React.createElement(Portal, {type: PortalType.RETURN_FROM_FORM, action: Action.INDEX, 
                  setAction: this.props.setAction}), 
          React.createElement("br", null), 
          React.createElement(MemoryForm, {memory: this.props.formMemory, 
                      formAction: FormAction.UPDATE, 
                      updateMemory: this.props.updateMemory})
        )
      );
    } else if (this.props.action == Action.NEW) {
      return (
        React.createElement("div", {id: "memories-area"}, 
          React.createElement(Portal, {type: PortalType.RETURN_FROM_FORM, action: Action.INDEX, 
                  setAction: this.props.setAction}), 
          React.createElement("br", null), 
          React.createElement(MemoryForm, {memory: this.props.formMemory, 
                      formAction: FormAction.CREATE, 
                      createMemory: this.props.createMemory})
        )
      );
    } else {
      console.error('No such action.');
    }
  },
  componentDidMount: function() {
    cssCenterHoverArea();
  },
  // Param: global.View. Currently not used because only STREAM view exists.
  _createMemoryNodesOfType: function(viewName) {
    var chooseMemoryNode = function(view, generalProps) {
      if (view == View.STREAM) {
        return (
          React.createElement(StreamMemoryNode, React.__spread({},  generalProps, {key: generalProps.id}))
        );
      } else {
        console.log("No such view in chooseMemoryNode.");
      }
    };

    return this.props.shownMemories.map(function(memory) {
      var props = {};

      // ReactJS requires a string id when you render multiple of the same
      // component. We also use this as the required 'key' property for each
      // node: http://facebook.github.io/react/docs/multiple-components.html
      // #dynamic-children
      props.id = memory.id + "-memory";

      var validateDateStr = false;
      // ISSUE
      props.memory = Structure.buildMemoryNoDate(memory.id, memory.content,
        memory.dateStr, memory.isExactDate, memory.note);
      props.setHoverText = this.setHoverText;
      props.launchMemoryForm = this.props.launchEditMemoryForm;
      return chooseMemoryNode(this.props.view, props);
    }, this);
  },
  setHoverText: function(text) {
    this.setState({hoverText: text});
  },
});

// Requirements: file-handler.js for downloadMemories().
var Action = global.Action;

var DownloadArea = React.createClass({displayName: "DownloadArea",
  propTypes: {
  },
  render: function() {
    return (
      React.createElement("div", {id: "download-area"}, 
        React.createElement("button", {onClick: this._download, type: "button", className: "btn btn-default", "aria-label": "download"}, 
          React.createElement("i", {className: "glyphicon glyphicon-download-alt", "aria-hidden": "true"}), " Download"
        )
      )
    );
  },
  _download: function() {
    downloadMemories();
  }
});

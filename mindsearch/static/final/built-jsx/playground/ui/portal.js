// Portal is a generic link used to easily jump to some other action.

var Action = global.Action;
var PortalType = global.PortalType;

var Portal = React.createClass({displayName: "Portal",
  propTypes: {
    // Message to display in the link.
    msg: React.PropTypes.string,

    // Icon to show, like "fa fa-arrow-circle-left". http://fortawesome.github.io/Font-Awesome/icons/
    icon: React.PropTypes.string,

    // PortalType to specify which type of Portal.
    type: React.PropTypes.string.isRequired,

    // Action to execute. 2 options: Just set the action directly, or have more
    // fine-grained control and explicitly do something. Only one of the two
    // should be defined.

    // 1. Set action directly.
    action: React.PropTypes.string,
    setAction: React.PropTypes.func,

    // 2. Action with more fine-grained control.
    detailedAction: React.PropTypes.func
  },
  render: function() {
    this._checkProps();
    var linkContent = null;
    if (this.props.type == PortalType.CUSTOM) {
      linkContent = (
        React.createElement("button", {onClick: this._go, className: "btn btn-default", 
                "aria-label": this.props.msg}, 
          React.createElement("i", {className: this.props.icon}), " ", this.props.msg
        )
      );
    } else if (this.props.type == PortalType.RETURN_FROM_FORM) {
      var icon = "fa fa-arrow-circle-left";
      var msg="Discard changes";
      linkContent = (
        React.createElement("button", {onClick: this._go, className: "btn btn-default faint-button", 
                "aria-label": msg}, 
          React.createElement("i", {className: icon, "aria-hidden": "true"}), " ", msg
        )
      );
    } else {
      console.error("No such PortalType.");
    }
    return (
      React.createElement("div", {className: "portal"}, 
        linkContent
      )
    );
  },
  _checkProps: function() {
    switch (this.props.type) {
      case PortalType.CUSTOM:
        if (!this.props.msg)
          console.error("Portal warning: 'msg' should be provided.");
        if (!this.props.icon)
          console.error("Portal warning: 'icon' should be provided.");
        break;
      case PortalType.RETURN_FROM_FORM:
        if (this.props.msg)
          console.error("Portal warning: unused 'msg' provided.");
        if (this.props.icon)
          console.error("Portal warning: unused 'icon' provided.");
        break;
    }
  },
  _go: function() {
    var usingActionSimple = this.props.action && this.props.setAction &&
      !this.props.detailedAction;
    var usingActionDetailed = !this.props.action &&
      !this.props.setAction && this.props.detailedAction;
    var validActionCombo = usingActionSimple || usingActionDetailed;
    if (!validActionCombo) {
      console.error("Portal warning: Invalid set of actions passed to portal.");
    }

    if (usingActionSimple) {
      this.props.setAction(this.props.action);
    } else {
      this.props.detailedAction();
    }
  }
});
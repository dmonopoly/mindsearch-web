var Action = global.Action;
var Structure = global.Structure;
var View = global.View;

var defaultMemories = [
  // Format should match ConvertPyMemoriesToJsonLikeForm in utils.py.
  {id: 1, content: 'I felt something. Something great.', dateStr: '11/20/2013', date: null, isExactDate: true, note: 'feeling'},
  {id: 2, content: 'I ran to a great place.', dateStr: '10/20/2013', date: null, isExactDate: true, note: 'exercise'},
  {id: 3, content: 'Went through a bunch of my old music from a decade ago! So many notes, so many sheets.', dateStr: '12/20/2013', date: null, isExactDate: true, note: 'piano'},
];
for (var m_idx in defaultMemories) {
  var mem = defaultMemories[m_idx];
  mem.date = Date.parseString(mem.dateStr);
}

var Playground = React.createClass({displayName: "Playground",
  getInitialState: function() {
    var initVars = {
      allMemories: [],
      shownMemories: [],
      searchText: "",

      action: Action.INDEX,
      view: View.STREAM,

      // The initial memory to fill the memory form with.
      formMemory: Structure.getBlankMemory()
    };
    if (switch_SKIP_TO_EDITING) {
      initVars.action = Action.EDIT;
    }
    if (switch_SKIP_UPLOAD_INTERACTION) {
      initIndexDirectly(defaultMemories);
      initVars.allMemories = Index.memoryObjects;
      initVars.shownMemories = this._defaultShownMemories(initVars.allMemories);
    }
    return initVars;
  },
  render: function() {
    var quickControls = null;
    var menu = null;

    if (this.state.action == Action.INDEX) {
      quickControls = (
        React.createElement("div", {id: "quick-controls"}, 
          React.createElement(DownloadArea, null), 
          React.createElement(Portal, {type: PortalType.CUSTOM, icon: "fa fa-plus", msg: "Create", 
                  detailedAction: this.launchNewMemoryForm})
        )
      );
      menu = (
        React.createElement(MenuBar, {searchText: this.state.searchText, 
                 action: this.state.action, 
                 view: this.state.view, 
                 viewList: global.ViewList, 
                 handleSearch: this.handleSearch, 
                 setView: this.setView})
      );
    }
    return (
      React.createElement("div", {id: "main"}, 
        React.createElement("div", {id: "playground"}, 
          quickControls, 
          menu, 
          React.createElement("hr", null), 
          React.createElement(MemoriesArea, {allMemories: this.state.allMemories, 
                        action: this.state.action, 
                        launchNewMemoryForm: this.launchNewMemoryForm, 
                        launchEditMemoryForm: this.launchEditMemoryForm, 
                        formMemory: this.state.formMemory, 
                        createMemory: this.createMemory, 
                        updateMemory: this.updateMemory, 
                        shownMemories: this.state.shownMemories, 
                        setAction: this.setAction, 
                        view: this.state.view})
        ), 
        React.createElement(UploadArea, {setMemories: this.setMemories, launchBlankSlate: this.launchBlankSlate})
      )
    );
  },
  componentDidMount: function() {
    if (switch_SKIP_UPLOAD_INTERACTION) return;
    $("#playground").hide();
  },
  _defaultShownMemories: function(mems) {
    var limit = 30;
    if (mems.length > limit) {
      return mems.slice(0, limit);
    } else {
      return mems;
    }
  },
  _doSearch: function(searchStr) {
    var mems = [];
    for (key in this.state.allMemories) {
      var mem = this.state.allMemories[key];
      if (mem.content.toLowerCase().indexOf(searchStr) > -1 ||
          mem.note && mem.note.toLowerCase().indexOf(searchStr) > -1) {
        mems.push(mem);
      }
    }
    return mems;
  },
  handleSearch: function(searchStr) {
    var newMemoriesToShow = this._doSearch(searchStr.toLowerCase());
    this.setState({searchText: searchStr,
                   shownMemories: newMemoriesToShow});
  },
  launchBlankSlate: function() {
    console.log('launching blank slate');
    showMemoryPlaygroundAndHideUpload();
    this.launchNewMemoryForm();
  },
  // Activates the memory form view for creating memories.
  launchNewMemoryForm: function() {
    console.log('launching new form');
    var mem = Structure.getBlankMemory();
    this.setState({action: Action.NEW, formMemory: mem});
  },
  // Activates the memory form view for editing memories.
  // @param {object} mem The memory to edit.
  launchEditMemoryForm: function(mem) {
    console.log('launching edit form');
    console.log(mem);
    if (!mem) {
      console.error('launchMemoryForm: given memory is null');
    }
    this.setState({action: Action.EDIT, formMemory: mem});
  },
  setAction: function(newAction) {
    this.setState({action: newAction});
  },
  setMemories: function(mems) {
    this.setState({
      allMemories: mems,
      shownMemories: this._defaultShownMemories(mems)
    });
    if (!switch_WARN_ON_EXIT) return;
    var queryUserOnExit = Index.memoryObjects.length > 0 ? true : false;
    util.setConfirmUnload(queryUserOnExit, function() {
      return "Some memories are here.";
    });
  },
  setView: function(newView) {
    this.setState({view: newView});
  },
  // Creates the memory in the Index.
  createMemory: function(newMem) {
    console.log("Calling create memory with new mem: ");
    console.log(newMem);
    var mid = newMem.id;
    var newContent = newMem.content;
    var newDateStr = newMem.dateStr;
    var newDate = newMem.date;
    var newIsExactDate = newMem.isExactDate;
    var newNote = newMem.note;

    var dbMemory = Structure.buildMemory(mid, newContent, newDateStr,
                                         newDate, newIsExactDate, newNote);

    var mems = Index.memoryObjects;
    mems.push(dbMemory);
    this.setMemories(mems);

    this.setState({action: Action.INDEX});
  },
  // Updates the memory in the Index.
  // @param {integer} memID The key of the memory to edit.
  updateMemory: function(newMem) {
    console.log("Calling update memory with new mem: ");
    console.log(newMem);
    var memID = newMem.id;

    var dbMemory = util.findById(memID, Index.memoryObjects);
    dbMemory.content = newMem.content;
    dbMemory.dateStr = newMem.dateStr;
    dbMemory.date = newMem.date;
    dbMemory.note = newMem.note;
    dbMemory.isExactDate = newMem.isExactDate;

    var mems = Index.memoryObjects;
    this.setMemories(mems);

    this.setState({action: Action.INDEX});
  }
});

$(document).ready(function() {
  var containerID = 'playground-container';
  if (document.getElementById(containerID)) {
    React.render(
      React.createElement(Playground, null),
      document.getElementById(containerID)
    );
  }
});

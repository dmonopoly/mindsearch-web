// A single memory node, many of which are displayed in MemoriesArea.

var StreamMemoryNode = React.createClass({displayName: "StreamMemoryNode",
  propTypes: {
    // The id for ReactJS / the DOM, like '1-memory'.
    id: React.PropTypes.string.isRequired,

    // Properties of this memory.
    memory: Structure.getMemoryShapeRequired(),

    // Function to tell the hover area the text to show for this memory.
    setHoverText: React.PropTypes.func.isRequired,

    // Function to activate the memory form view.
    launchMemoryForm: React.PropTypes.func.isRequired
  },
  render: function() {
    return (
      React.createElement("div", {id: this.props.id, className: "memory stream-memory"}, 
        React.createElement("a", {href: "javascript:void(0);", className: "my-list-group-item", 
           onClick: this.launchMemoryForm}, 
          this.props.memory.dateStr, React.createElement("br", null), 
          this._chosenContent(), React.createElement("br", null), 
          React.createElement("span", {className: "note"}, this.props.memory.note)
        )
      )
    )
  },
  componentDidMount: function() {
    var nodeId = "#" + this.props.id;
    var truncatedText = this.props.memory.content.truncate(140);
    var setHoverFn = this.props.setHoverText;
    cssHoverAreaToggle(nodeId, truncatedText, setHoverFn);
  },
  _chosenContent: function() {
    var content = this.props.memory.content;
    if (content.length <= 60)
      return content;
    return content.truncate(180);
  },
  launchMemoryForm: function() {
    this.props.launchMemoryForm(this.props.memory);
  }
});

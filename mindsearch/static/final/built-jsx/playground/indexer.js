/*
 * This prepares the Index of memories, converting the standard memory format to a list of memories of this format:
 * [ {id: 1
 *    content: 'I felt something.',
 *    dateStr: '11/20/2013',
 *    date: Date object
 *    isExactDate: true,
 *    note: 'music'}, ... ]
 */

var Index = global.Index;
var Structure = global.Structure;
var PRINT = false;

function _buildMemory(dateStr) {
  return Structure.buildMemory(null, null, dateStr, Date.parseString(dateStr), null, null);
}

// Fill in the Index.
// @param {contents} contents The complete text of memories in your standard format.
function initIndexFromText(contents) {
  if (PRINT) {
    console.log('--initIndex--');
  }

  // Split memories into 4 parts: date, tilde, note, content, so you have an
  // array like memPieces = ['1/1/2011', '~', 'Note', 'Content', '2/4/2012', '',
  // '', 'Content' ...  (repeat)]. Do this by first splitting into separate
  // lines, and merging only the content lines. Split up the date line into 1-3
  // parts.

  var lines = util.splitIntoLines(contents);

  // Merge content lines.
  var dateLinePattern = /^(\d\d?\/\d\d?\/\d\d\d\d)\b(~*)(.*)\b/g;
  var dateLineAndContents = [];
  var onContent = false;
  for (var i = 0; i < lines.length; ++i) {
    var entry = lines[i].trim();

    if (entry.match(dateLinePattern)) {
      dateLineAndContents.push(entry);
      onContent = false;
    } else {
      if (onContent) {
        var last = dateLineAndContents[dateLineAndContents.length - 1];
        dateLineAndContents[dateLineAndContents.length - 1] = [last, entry].join('\n');
      } else {
        dateLineAndContents.push(entry);
        onContent = true;
      }
    }
  }
  if (dateLineAndContents.length % 2 != 0) {
    console.error("One to one mapping from date to content not satisfied.");
  }

  if (PRINT) {
    console.log('dateLineAndContents:');
    console.log(dateLineAndContents);
    // for (var i = 0; i < dateLineAndContents.length; ++i) {
    //   console.log(dateLineAndContents[i]);
    // }
  }

  var memPieces = [];
  for (var i = 0; i < dateLineAndContents.length; i += 2) {
    var dateLinePattern = /\b(\d\d?\/\d\d?\/\d\d\d\d)(~*)(.*)/g;
    // Date line.
    var match = dateLinePattern.exec(dateLineAndContents[i]);
    if (!match) {
      console.error("No match for " + dateLineAndContents[i]);
    }
    match.shift();
    for (var j = 0; j < match.length; ++j) {
      memPieces.push(match[j]);
    }
    // Content.
    memPieces.push(dateLineAndContents[i + 1])
    dateLinePattern = null;
  }

  if (PRINT) {
    console.log('memPieces:');
    console.log(memPieces);
    for (var i = 0; i < memPieces.length; ++i) {
    	console.log(i + 1 + ': ' + memPieces[i]);
    }
  }

  var MemoryPart = { DATE: 0, DATE_ACCURACY: 1, NOTE: 2, CONTENT: 3 };

  var memoryObjects = [];
  var earliestDate = null;
  var latestDate = null;
  var datesToMemoryCount = {};

  // Take memories as groups of 4.
  var memPart = MemoryPart.DATE;
  var currentMem = null;
  if (memPieces.length % 4 != 0) {
  	console.error("Memories text is not of correct format. Length = " + memPieces.length);
  }

  for (var i = 0; i < memPieces.length; ++i) {
  	var memText = memPieces[i].trim();
  	if (memPart == MemoryPart.DATE) {
  		if (currentMem) {
        memoryObjects.push(currentMem);
      }
  		var dateStr = memText;
  		currentMem = _buildMemory(dateStr);
  		if (!earliestDate || earliestDate.isAfter(currentMem.date))
        earliestDate = currentMem.date;
      if (!latestDate || latestDate.isBefore(currentMem.date))
        latestDate = currentMem.date;
      if (currentMem.date in datesToMemoryCount)
        datesToMemoryCount[currentMem.dateStr] += 1;
      else
        datesToMemoryCount[currentMem.dateStr] = 0;
  	} else if (memPart == MemoryPart.DATE_ACCURACY) {
      if (memText)
        currentMem.isExactDate = false;
      else
        currentMem.isExactDate = true;
  	} else if (memPart == MemoryPart.NOTE) {
  		if (memText)
  			currentMem.note = memText;
  	} else if (memPart == MemoryPart.CONTENT) {
  		currentMem.content = memText;
  	} else {
  		console.error("indexMemories: Invalid memory part.");
  	}
    memPart += 1;
    memPart = memPart % 4;
  }
  memoryObjects.push(currentMem);

  // Add ids to each mem object.
  for (var i = 0; i < memoryObjects.length; ++i) {
    memoryObjects[i].id = i + 1;
  }

	Index.memoryObjects = memoryObjects;
	Index.earliestDate = earliestDate;
	Index.latestDate = latestDate;
	Index.datesToMemoryCount = datesToMemoryCount;
}

// Fill in the Index.
// Args:
//   memArray: The hard-coded array of memories, like
//    var defaultMemories = [
//    {id: 1, content: 'I felt something.', dateStr: '11/20/2013', isExactDate: true, note: 'feeling'},
//     ...
//    ]
function initIndexDirectly(memArray) {
  Index.memoryObjects = memArray;
  Index.earliestDate = null;
  Index.latestDate = null;
  Index.datesToMemoryCount = null;
}

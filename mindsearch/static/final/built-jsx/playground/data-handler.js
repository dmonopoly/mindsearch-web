// NOT USED - but may use if sessionStorage or localStorage becomes a feature.

    // var jsonMems = JSON.stringify(Index.memoryObjects)
    // loadDatabase($.parseJSON(jsonMems));

// Text => memories array
// Given text formatted to represent memories, return an array of memories,
// where each memory has properties id, content, date, dateStr, isExactDate,
// note.
function convertTextToMemories(textContent) {
  // Returns the raw text of this memory with content date string etc. combined
  // exactly as the info would appear in the raw text file.
  // _getFullContent: function() {
  //   var exactDateStr = this.props.isExactDate ? "~" : "";
  //   var notePart = this.props.note.length > 0 ? " " + this.props.note : "";
  //   var dateLine = this.props.dateStr + exactDateStr + notePart + "\n";
  //   return dateLine + this.props.content + "\n";
  // },
}

// globalDb => memories array
function convertDbToMemories() {
  var mems = [];
  for (var id in globalDb) {
    if (!parseInt(id)) continue;
    // Example id, globalDb.getObject(id):
    // 5, {"id":5,"content":"Finally got the Nim 2 song!","dateStr":"12/16/2014","isExactDate":true,"note":"","truncatedContent":"Finally got the Nim 2 song!","timeAgo":null,"date":{"py/object":"datetime.date","__reduce__":[{"py/type":"datetime.date"},["B94MEA=="]]}}
    // 6, {"id":6,"content":"Went through a bunch of my old music from a decade ago! So many notes, so many sheets.","dateStr":"12/16/2014","isExactDate":true,"note":"","truncatedContent":"Went through a bunch of my old music from a decade ago! So many notes, so many sheets.","timeAgo":null,"date":{"py/object":"datetime.date","__reduce__":[{"py/type":"datetime.date"},["B94MEA=="]]}}
    var mem = {
      'id': id,
      'content': globalDb.getObject(id).content,
      'dateStr': globalDb.getObject(id).dateStr,
      'isExactDate': globalDb.getObject(id).isExactDate,
      'note': globalDb.getObject(id).note,
      'truncatedContent': globalDb.getObject(id).content,
      'timeAgo': globalDb.getObject(id).timeAgo,
      'date': globalDb.getObject(id).date,
    }
    mems.push(globalDb.getObject(id));
  }
  return mems;
}

/*
 * Insert one memory into the database.
 * @param {object} oneMem Memory with attributes as shown in loadDatabase.
 */
function _insertIntoDb(oneMem) {
  var key = oneMem.id;
  globalDb.setObject(key, oneMem);
}

/*
 * Load memory data into db when ready.
 * Keys are numbers starting from 1.
 * @param {object} memoryJSON JSON data of the form specified in
 * ConvertPyMemoriesToJsonLikeForm in utils.py.
 *   1: Object
 *     id: ..
 *     content: ..
 *     date: ..
 *     dateStr: ..
 *     isExactDate: ..
 *     note: ..
 *   2: Object
 *     etc.
 */
function loadDatabase(memoryJSON) {
  console.log('loadDatabase:');
  console.log(memoryJSON);
  var elements = [
    'id', // 0
    'content', // 1
    'dateStr', // 2
    'isExactDate', // 3
    'note', // 4
    'truncatedContent', // 5
    'timeAgo', // 6
    'date' // 7
  ];
  for (var key in memoryJSON) {
    var oneRawMem = memoryJSON[key];
    // console.log("key: " + key);
    // console.log("oneRawMem: ");
    // console.log(oneRawMem);
    // var id = parseInt(key) + 1;
    var id = null;
    var content = null;
    var dateStr = null;
    var isExactDate = null;
    var note = null;
    var truncatedContent = null;
    var timeAgo = null;
    var date = null;
    for (var first in oneRawMem) {
      // First is key like 'content', second is value based on that key.
      // The keys are strictly those in ConvertPyMemoriesToJsonLikeForm.
      var second = oneRawMem[first];
      if (first == elements[0]) {
        id = second;
      } else if (first == elements[1]) {
        content = second;
        truncatedContent = second.truncate(120, true);
      } else if (first == elements[2]) {
        dateStr = second;
      } else if (first == elements[3]) {
        isExactDate = second;
      } else if (first == elements[4]) {
        note = second;
      } else if (first == elements[6]) {
        timeAgo = second;  // does not actually set this. Use date.
      } else if (first == elements[7]) {
        date = second;
      }
    }
    var oneMem = {};
    oneMem[elements[0]] = id;
    oneMem[elements[1]] = content;
    oneMem[elements[2]] = dateStr;
    oneMem[elements[3]] = isExactDate;
    oneMem[elements[4]] = note;
    oneMem[elements[5]] = truncatedContent;
    oneMem[elements[6]] = timeAgo;
    oneMem[elements[7]] = date;
    _insertIntoDb(oneMem);
  }
  // console.log('Database:');
  // console.log(globalDb);
  // console.log("1st try: ");
  // console.log(globalDb.getObject('1'));
  // console.log(globalDb.getObject(1));
  // console.log(globalDb.getObject(1).content);
  // console.log(globalDb.getObject(2));
}

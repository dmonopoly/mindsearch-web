(function(global,factory){if(typeof module==="object"&&typeof module.exports==="object"){module.exports=global.document?factory(global,true):function(w){if(!w.document){throw new Error("jQuery requires a window with a document");}
return factory(w);};}else{factory(global);}
}(typeof window!=="undefined"?window:this,function(window,noGlobal){


var arr=[];var slice=arr.slice;var concat=arr.concat;var push=arr.push;var indexOf=arr.indexOf;var class2type={};var toString=class2type.toString;var hasOwn=class2type.hasOwnProperty;var support={};var

document=window.document,version="2.1.4", jQuery=function(selector,context){return new jQuery.fn.init(selector,context);},
 rtrim=/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, rmsPrefix=/^-ms-/,rdashAlpha=/-([\da-z])/gi,fcamelCase=function(all,letter){return letter.toUpperCase();};jQuery.fn=jQuery.prototype={ jquery:version,constructor:jQuery, selector:"", length:0,toArray:function(){return slice.call(this);},
 get:function(num){return num!=null?(num<0?this[num+this.length]:this[num]): slice.call(this);},
pushStack:function(elems){ var ret=jQuery.merge(this.constructor(),elems);ret.prevObject=this;ret.context=this.context; return ret;},
each:function(callback,args){return jQuery.each(this,callback,args);},map:function(callback){return this.pushStack(jQuery.map(this,function(elem,i){return callback.call(elem,i,elem);}));},slice:function(){return this.pushStack(slice.apply(this,arguments));},first:function(){return this.eq(0);},last:function(){return this.eq(-1);},eq:function(i){var len=this.length,j=+i+(i<0?len:0);return this.pushStack(j>=0&&j<len?[this[j]]:[]);},end:function(){return this.prevObject||this.constructor(null);},push:push,sort:arr.sort,splice:arr.splice};jQuery.extend=jQuery.fn.extend=function(){var options,name,src,copy,copyIsArray,clone,target=arguments[0]||{},i=1,length=arguments.length,deep=false; if(typeof target==="boolean"){deep=target; target=arguments[i]||{};i++;}
if(typeof target!=="object"&&!jQuery.isFunction(target)){target={};} 
if(i===length){target=this;i--;}
for(;i<length;i++){ if((options=arguments[i])!=null){ for(name in options){src=target[name];copy=options[name]; if(target===copy){continue;} 
if(deep&&copy&&(jQuery.isPlainObject(copy)||(copyIsArray=jQuery.isArray(copy)))){if(copyIsArray){copyIsArray=false;clone=src&&jQuery.isArray(src)?src:[];}else{clone=src&&jQuery.isPlainObject(src)?src:{};} 
target[name]=jQuery.extend(deep,clone,copy);}else if(copy!==undefined){target[name]=copy;}}}} 
return target;};jQuery.extend({ expando:"jQuery"+(version+Math.random()).replace(/\D/g,""), isReady:true,error:function(msg){throw new Error(msg);},noop:function(){},isFunction:function(obj){return jQuery.type(obj)==="function";},isArray:Array.isArray,isWindow:function(obj){return obj!=null&&obj===obj.window;},isNumeric:function(obj){

return!jQuery.isArray(obj)&&(obj-parseFloat(obj)+1)>=0;},isPlainObject:function(obj){
 if(jQuery.type(obj)!=="object"||obj.nodeType||jQuery.isWindow(obj)){return false;}
if(obj.constructor&&!hasOwn.call(obj.constructor.prototype,"isPrototypeOf")){return false;}
 
return true;},isEmptyObject:function(obj){var name;for(name in obj){return false;}
return true;},type:function(obj){if(obj==null){return obj+"";}
return typeof obj==="object"||typeof obj==="function"?class2type[toString.call(obj)]||"object":typeof obj;}, globalEval:function(code){var script,indirect=eval;code=jQuery.trim(code);if(code){

if(code.indexOf("use strict")===1){script=document.createElement("script");script.text=code;document.head.appendChild(script).parentNode.removeChild(script);}else{
 indirect(code);}}},
camelCase:function(string){return string.replace(rmsPrefix,"ms-").replace(rdashAlpha,fcamelCase);},nodeName:function(elem,name){return elem.nodeName&&elem.nodeName.toLowerCase()===name.toLowerCase();}, each:function(obj,callback,args){var value,i=0,length=obj.length,isArray=isArraylike(obj);if(args){if(isArray){for(;i<length;i++){value=callback.apply(obj[i],args);if(value===false){break;}}}else{for(i in obj){value=callback.apply(obj[i],args);if(value===false){break;}}}
}else{if(isArray){for(;i<length;i++){value=callback.call(obj[i],i,obj[i]);if(value===false){break;}}}else{for(i in obj){value=callback.call(obj[i],i,obj[i]);if(value===false){break;}}}}
return obj;},  trim:function(text){return text==null?"":(text+"").replace(rtrim,"");}, makeArray:function(arr,results){var ret=results||[];if(arr!=null){if(isArraylike(Object(arr))){jQuery.merge(ret,typeof arr==="string"?[arr]:arr);}else{push.call(ret,arr);}}
return ret;},inArray:function(elem,arr,i){return arr==null?-1:indexOf.call(arr,elem,i);},merge:function(first,second){var len=+second.length,j=0,i=first.length;for(;j<len;j++){first[i++]=second[j];}
first.length=i;return first;},grep:function(elems,callback,invert){var callbackInverse,matches=[],i=0,length=elems.length,callbackExpect=!invert;
 for(;i<length;i++){callbackInverse=!callback(elems[i],i);if(callbackInverse!==callbackExpect){matches.push(elems[i]);}}
return matches;}, map:function(elems,callback,arg){var value,i=0,length=elems.length,isArray=isArraylike(elems),ret=[]; if(isArray){for(;i<length;i++){value=callback(elems[i],i,arg);if(value!=null){ret.push(value);}}
}else{for(i in elems){value=callback(elems[i],i,arg);if(value!=null){ret.push(value);}}} 
return concat.apply([],ret);}, guid:1,
proxy:function(fn,context){var tmp,args,proxy;if(typeof context==="string"){tmp=fn[context];context=fn;fn=tmp;}

if(!jQuery.isFunction(fn)){return undefined;} 
args=slice.call(arguments,2);proxy=function(){return fn.apply(context||this,args.concat(slice.call(arguments)));}; proxy.guid=fn.guid=fn.guid||jQuery.guid++;return proxy;},now:Date.now,
support:support});jQuery.each("Boolean Number String Function Array Date RegExp Object Error".split(" "),function(i,name){class2type["[object "+name+"]"]=name.toLowerCase();});function isArraylike(obj){
 
var length="length"in obj&&obj.length,type=jQuery.type(obj);if(type==="function"||jQuery.isWindow(obj)){return false;}
if(obj.nodeType===1&&length){return true;}
return type==="array"||length===0||typeof length==="number"&&length>0&&(length-1)in obj;}
var Sizzle=(function(window){var i,support,Expr,getText,isXML,tokenize,compile,select,outermostContext,sortInput,hasDuplicate, setDocument,document,docElem,documentIsHTML,rbuggyQSA,rbuggyMatches,matches,contains, expando="sizzle"+1*new Date(),preferredDoc=window.document,dirruns=0,done=0,classCache=createCache(),tokenCache=createCache(),compilerCache=createCache(),sortOrder=function(a,b){if(a===b){hasDuplicate=true;}
return 0;},  MAX_NEGATIVE=1<<31,  hasOwn=({}).hasOwnProperty,arr=[],pop=arr.pop,push_native=arr.push,push=arr.push,slice=arr.slice,
 indexOf=function(list,elem){var i=0,len=list.length;for(;i<len;i++){if(list[i]===elem){return i;}}
return-1;},booleans="checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
 whitespace="[\\x20\\t\\r\\n\\f]", characterEncoding="(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",

 identifier=characterEncoding.replace("w","w#"), attributes="\\["+whitespace+"*("+characterEncoding+")(?:"+whitespace+
"*([*^$|!~]?=)"+whitespace+
"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|("+identifier+"))|)"+whitespace+"*\\]",pseudos=":("+characterEncoding+")(?:\\(("+
"('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|"+
"((?:\\\\.|[^\\\\()[\\]]|"+attributes+")*)|"+
".*"+")\\)|)", rwhitespace=new RegExp(whitespace+"+","g"),rtrim=new RegExp("^"+whitespace+"+|((?:^|[^\\\\])(?:\\\\.)*)"+whitespace+"+$","g"),rcomma=new RegExp("^"+whitespace+"*,"+whitespace+"*"),rcombinators=new RegExp("^"+whitespace+"*([>+~]|"+whitespace+")"+whitespace+"*"),rattributeQuotes=new RegExp("="+whitespace+"*([^\\]'\"]*?)"+whitespace+"*\\]","g"),rpseudo=new RegExp(pseudos),ridentifier=new RegExp("^"+identifier+"$"),matchExpr={"ID":new RegExp("^#("+characterEncoding+")"),"CLASS":new RegExp("^\\.("+characterEncoding+")"),"TAG":new RegExp("^("+characterEncoding.replace("w","w*")+")"),"ATTR":new RegExp("^"+attributes),"PSEUDO":new RegExp("^"+pseudos),"CHILD":new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\("+whitespace+"*(even|odd|(([+-]|)(\\d*)n|)"+whitespace+"*(?:([+-]|)"+whitespace+"*(\\d+)|))"+whitespace+"*\\)|)","i"),"bool":new RegExp("^(?:"+booleans+")$","i"),
"needsContext":new RegExp("^"+whitespace+"*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\("+
whitespace+"*((?:-\\d)?\\d*)"+whitespace+"*\\)|)(?=[^-]|$)","i")},rinputs=/^(?:input|select|textarea|button)$/i,rheader=/^h\d$/i,rnative=/^[^{]+\{\s*\[native \w/, rquickExpr=/^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,rsibling=/[+~]/,rescape=/'|\\/g, runescape=new RegExp("\\\\([\\da-f]{1,6}"+whitespace+"?|("+whitespace+")|.)","ig"),funescape=function(_,escaped,escapedWhitespace){var high="0x"+escaped-0x10000;

return high!==high||escapedWhitespace?escaped:high<0? String.fromCharCode(high+0x10000):String.fromCharCode(high>>10|0xD800,high&0x3FF|0xDC00);},
 
unloadHandler=function(){setDocument();};try{push.apply((arr=slice.call(preferredDoc.childNodes)),preferredDoc.childNodes);
 arr[preferredDoc.childNodes.length].nodeType;}catch(e){push={apply:arr.length? function(target,els){push_native.apply(target,slice.call(els));}:
 function(target,els){var j=target.length,i=0; while((target[j++]=els[i++])){}
target.length=j-1;}};}
function Sizzle(selector,context,results,seed){var match,elem,m,nodeType, i,groups,old,nid,newContext,newSelector;if((context?context.ownerDocument||context:preferredDoc)!==document){setDocument(context);}
context=context||document;results=results||[];nodeType=context.nodeType;if(typeof selector!=="string"||!selector||nodeType!==1&&nodeType!==9&&nodeType!==11){return results;}
if(!seed&&documentIsHTML){if(nodeType!==11&&(match=rquickExpr.exec(selector))){if((m=match[1])){if(nodeType===9){elem=context.getElementById(m);
if(elem&&elem.parentNode){
 if(elem.id===m){results.push(elem);return results;}}else{return results;}}else{ if(context.ownerDocument&&(elem=context.ownerDocument.getElementById(m))&&contains(context,elem)&&elem.id===m){results.push(elem);return results;}}
}else if(match[2]){push.apply(results,context.getElementsByTagName(selector));return results;}else if((m=match[3])&&support.getElementsByClassName){push.apply(results,context.getElementsByClassName(m));return results;}} 
if(support.qsa&&(!rbuggyQSA||!rbuggyQSA.test(selector))){nid=old=expando;newContext=context;newSelector=nodeType!==1&&selector;

 
if(nodeType===1&&context.nodeName.toLowerCase()!=="object"){groups=tokenize(selector);if((old=context.getAttribute("id"))){nid=old.replace(rescape,"\\$&");}else{context.setAttribute("id",nid);}
nid="[id='"+nid+"'] ";i=groups.length;while(i--){groups[i]=nid+toSelector(groups[i]);}
newContext=rsibling.test(selector)&&testContext(context.parentNode)||context;newSelector=groups.join(",");}
if(newSelector){try{push.apply(results,newContext.querySelectorAll(newSelector));return results;}catch(qsaError){}finally{if(!old){context.removeAttribute("id");}}}}} 
return select(selector.replace(rtrim,"$1"),context,results,seed);}
function createCache(){var keys=[];function cache(key,value){if(keys.push(key+" ")>Expr.cacheLength){ delete cache[keys.shift()];}
return(cache[key+" "]=value);}
return cache;}
function markFunction(fn){fn[expando]=true;return fn;}
function assert(fn){var div=document.createElement("div");try{return!!fn(div);}catch(e){return false;}finally{ if(div.parentNode){div.parentNode.removeChild(div);} 
div=null;}}
function addHandle(attrs,handler){var arr=attrs.split("|"),i=attrs.length;while(i--){Expr.attrHandle[arr[i]]=handler;}}
function siblingCheck(a,b){var cur=b&&a,diff=cur&&a.nodeType===1&&b.nodeType===1&&(~b.sourceIndex||MAX_NEGATIVE)-
(~a.sourceIndex||MAX_NEGATIVE); if(diff){return diff;} 
if(cur){while((cur=cur.nextSibling)){if(cur===b){return-1;}}}
return a?1:-1;}
function createInputPseudo(type){return function(elem){var name=elem.nodeName.toLowerCase();return name==="input"&&elem.type===type;};}
function createButtonPseudo(type){return function(elem){var name=elem.nodeName.toLowerCase();return(name==="input"||name==="button")&&elem.type===type;};}
function createPositionalPseudo(fn){return markFunction(function(argument){argument=+argument;return markFunction(function(seed,matches){var j,matchIndexes=fn([],seed.length,argument),i=matchIndexes.length; while(i--){if(seed[(j=matchIndexes[i])]){seed[j]=!(matches[j]=seed[j]);}}});});}
function testContext(context){return context&&typeof context.getElementsByTagName!=="undefined"&&context;}
support=Sizzle.support={};isXML=Sizzle.isXML=function(elem){
var documentElement=elem&&(elem.ownerDocument||elem).documentElement;return documentElement?documentElement.nodeName!=="HTML":false;};setDocument=Sizzle.setDocument=function(node){var hasCompare,parent,doc=node?node.ownerDocument||node:preferredDoc; if(doc===document||doc.nodeType!==9||!doc.documentElement){return document;} 
document=doc;docElem=doc.documentElement;parent=doc.defaultView;

 if(parent&&parent!==parent.top){ if(parent.addEventListener){parent.addEventListener("unload",unloadHandler,false);}else if(parent.attachEvent){parent.attachEvent("onunload",unloadHandler);}}
documentIsHTML=!isXML(doc);

support.attributes=assert(function(div){div.className="i";return!div.getAttribute("className");}); support.getElementsByTagName=assert(function(div){div.appendChild(doc.createComment(""));return!div.getElementsByTagName("*").length;}); support.getElementsByClassName=rnative.test(doc.getElementsByClassName);

 support.getById=assert(function(div){docElem.appendChild(div).id=expando;return!doc.getElementsByName||!doc.getElementsByName(expando).length;}); if(support.getById){Expr.find["ID"]=function(id,context){if(typeof context.getElementById!=="undefined"&&documentIsHTML){var m=context.getElementById(id);
 return m&&m.parentNode?[m]:[];}};Expr.filter["ID"]=function(id){var attrId=id.replace(runescape,funescape);return function(elem){return elem.getAttribute("id")===attrId;};};}else{
 delete Expr.find["ID"];Expr.filter["ID"]=function(id){var attrId=id.replace(runescape,funescape);return function(elem){var node=typeof elem.getAttributeNode!=="undefined"&&elem.getAttributeNode("id");return node&&node.value===attrId;};};} 
Expr.find["TAG"]=support.getElementsByTagName?function(tag,context){if(typeof context.getElementsByTagName!=="undefined"){return context.getElementsByTagName(tag);}else if(support.qsa){return context.querySelectorAll(tag);}}:function(tag,context){var elem,tmp=[],i=0, results=context.getElementsByTagName(tag); if(tag==="*"){while((elem=results[i++])){if(elem.nodeType===1){tmp.push(elem);}}
return tmp;}
return results;}; Expr.find["CLASS"]=support.getElementsByClassName&&function(className,context){if(documentIsHTML){return context.getElementsByClassName(className);}};
rbuggyMatches=[];


 
rbuggyQSA=[];if((support.qsa=rnative.test(doc.querySelectorAll))){
 assert(function(div){


 docElem.appendChild(div).innerHTML="<a id='"+expando+"'></a>"+"<select id='"+expando+"-\f]' msallowcapture=''>"+"<option selected=''></option></select>";

 if(div.querySelectorAll("[msallowcapture^='']").length){rbuggyQSA.push("[*^$]="+whitespace+"*(?:''|\"\")");}
 
if(!div.querySelectorAll("[selected]").length){rbuggyQSA.push("\\["+whitespace+"*(?:value|"+booleans+")");}
if(!div.querySelectorAll("[id~="+expando+"-]").length){rbuggyQSA.push("~=");}

 
if(!div.querySelectorAll(":checked").length){rbuggyQSA.push(":checked");}
 
if(!div.querySelectorAll("a#"+expando+"+*").length){rbuggyQSA.push(".#.+[+~]");}});assert(function(div){
 var input=doc.createElement("input");input.setAttribute("type","hidden");div.appendChild(input).setAttribute("name","D");
 if(div.querySelectorAll("[name=d]").length){rbuggyQSA.push("name"+whitespace+"*[*^$|!~]?=");} 
if(!div.querySelectorAll(":enabled").length){rbuggyQSA.push(":enabled",":disabled");} 
div.querySelectorAll("*,:x");rbuggyQSA.push(",.*:");});}
if((support.matchesSelector=rnative.test((matches=docElem.matches||docElem.webkitMatchesSelector||docElem.mozMatchesSelector||docElem.oMatchesSelector||docElem.msMatchesSelector)))){assert(function(div){
support.disconnectedMatch=matches.call(div,"div");
 matches.call(div,"[s!='']:x");rbuggyMatches.push("!=",pseudos);});}
rbuggyQSA=rbuggyQSA.length&&new RegExp(rbuggyQSA.join("|"));rbuggyMatches=rbuggyMatches.length&&new RegExp(rbuggyMatches.join("|"));hasCompare=rnative.test(docElem.compareDocumentPosition);

 contains=hasCompare||rnative.test(docElem.contains)?function(a,b){var adown=a.nodeType===9?a.documentElement:a,bup=b&&b.parentNode;return a===bup||!!(bup&&bup.nodeType===1&&(adown.contains?adown.contains(bup):a.compareDocumentPosition&&a.compareDocumentPosition(bup)&16));}:function(a,b){if(b){while((b=b.parentNode)){if(b===a){return true;}}}
return false;}; sortOrder=hasCompare?function(a,b){ if(a===b){hasDuplicate=true;return 0;} 
var compare=!a.compareDocumentPosition-!b.compareDocumentPosition;if(compare){return compare;} 
compare=(a.ownerDocument||a)===(b.ownerDocument||b)?a.compareDocumentPosition(b): 1; if(compare&1||(!support.sortDetached&&b.compareDocumentPosition(a)===compare)){ if(a===doc||a.ownerDocument===preferredDoc&&contains(preferredDoc,a)){return-1;}
if(b===doc||b.ownerDocument===preferredDoc&&contains(preferredDoc,b)){return 1;} 
return sortInput?(indexOf(sortInput,a)-indexOf(sortInput,b)):0;}
return compare&4?-1:1;}:function(a,b){ if(a===b){hasDuplicate=true;return 0;}
var cur,i=0,aup=a.parentNode,bup=b.parentNode,ap=[a],bp=[b]; if(!aup||!bup){return a===doc?-1:b===doc?1:aup?-1:bup?1:sortInput?(indexOf(sortInput,a)-indexOf(sortInput,b)):0;}else if(aup===bup){return siblingCheck(a,b);} 
cur=a;while((cur=cur.parentNode)){ap.unshift(cur);}
cur=b;while((cur=cur.parentNode)){bp.unshift(cur);} 
while(ap[i]===bp[i]){i++;}
return i?  siblingCheck(ap[i],bp[i]): ap[i]===preferredDoc?-1:bp[i]===preferredDoc?1:0;};return doc;};Sizzle.matches=function(expr,elements){return Sizzle(expr,null,null,elements);};Sizzle.matchesSelector=function(elem,expr){ if((elem.ownerDocument||elem)!==document){setDocument(elem);} 
expr=expr.replace(rattributeQuotes,"='$1']");if(support.matchesSelector&&documentIsHTML&&(!rbuggyMatches||!rbuggyMatches.test(expr))&&(!rbuggyQSA||!rbuggyQSA.test(expr))){try{var ret=matches.call(elem,expr); if(ret||support.disconnectedMatch||
 elem.document&&elem.document.nodeType!==11){return ret;}}catch(e){}}
return Sizzle(expr,document,null,[elem]).length>0;};Sizzle.contains=function(context,elem){ if((context.ownerDocument||context)!==document){setDocument(context);}
return contains(context,elem);};Sizzle.attr=function(elem,name){ if((elem.ownerDocument||elem)!==document){setDocument(elem);}
var fn=Expr.attrHandle[name.toLowerCase()],val=fn&&hasOwn.call(Expr.attrHandle,name.toLowerCase())?fn(elem,name,!documentIsHTML):undefined;return val!==undefined?val:support.attributes||!documentIsHTML?elem.getAttribute(name):(val=elem.getAttributeNode(name))&&val.specified?val.value:null;};Sizzle.error=function(msg){throw new Error("Syntax error, unrecognized expression: "+msg);};Sizzle.uniqueSort=function(results){var elem,duplicates=[],j=0,i=0; hasDuplicate=!support.detectDuplicates;sortInput=!support.sortStable&&results.slice(0);results.sort(sortOrder);if(hasDuplicate){while((elem=results[i++])){if(elem===results[i]){j=duplicates.push(i);}}
while(j--){results.splice(duplicates[j],1);}}
 
sortInput=null;return results;};getText=Sizzle.getText=function(elem){var node,ret="",i=0,nodeType=elem.nodeType;if(!nodeType){ while((node=elem[i++])){ ret+=getText(node);}}else if(nodeType===1||nodeType===9||nodeType===11){
if(typeof elem.textContent==="string"){return elem.textContent;}else{ for(elem=elem.firstChild;elem;elem=elem.nextSibling){ret+=getText(elem);}}}else if(nodeType===3||nodeType===4){return elem.nodeValue;} 
return ret;};Expr=Sizzle.selectors={ cacheLength:50,createPseudo:markFunction,match:matchExpr,attrHandle:{},find:{},relative:{">":{dir:"parentNode",first:true}," ":{dir:"parentNode"},"+":{dir:"previousSibling",first:true},"~":{dir:"previousSibling"}},preFilter:{"ATTR":function(match){match[1]=match[1].replace(runescape,funescape); match[3]=(match[3]||match[4]||match[5]||"").replace(runescape,funescape);if(match[2]==="~="){match[3]=" "+match[3]+" ";}
return match.slice(0,4);},"CHILD":function(match){match[1]=match[1].toLowerCase();if(match[1].slice(0,3)==="nth"){ if(!match[3]){Sizzle.error(match[0]);}
 
match[4]=+(match[4]?match[5]+(match[6]||1):2*(match[3]==="even"||match[3]==="odd"));match[5]=+((match[7]+match[8])||match[3]==="odd");}else if(match[3]){Sizzle.error(match[0]);}
return match;},"PSEUDO":function(match){var excess,unquoted=!match[6]&&match[2];if(matchExpr["CHILD"].test(match[0])){return null;} 
if(match[3]){match[2]=match[4]||match[5]||"";}else if(unquoted&&rpseudo.test(unquoted)&&(excess=tokenize(unquoted,true))&&(excess=unquoted.indexOf(")",unquoted.length-excess)-unquoted.length)){ match[0]=match[0].slice(0,excess);match[2]=unquoted.slice(0,excess);}
return match.slice(0,3);}},filter:{"TAG":function(nodeNameSelector){var nodeName=nodeNameSelector.replace(runescape,funescape).toLowerCase();return nodeNameSelector==="*"?function(){return true;}:function(elem){return elem.nodeName&&elem.nodeName.toLowerCase()===nodeName;};},"CLASS":function(className){var pattern=classCache[className+" "];return pattern||(pattern=new RegExp("(^|"+whitespace+")"+className+"("+whitespace+"|$)"))&&classCache(className,function(elem){return pattern.test(typeof elem.className==="string"&&elem.className||typeof elem.getAttribute!=="undefined"&&elem.getAttribute("class")||"");});},"ATTR":function(name,operator,check){return function(elem){var result=Sizzle.attr(elem,name);if(result==null){return operator==="!=";}
if(!operator){return true;}
result+="";return operator==="="?result===check:operator==="!="?result!==check:operator==="^="?check&&result.indexOf(check)===0:operator==="*="?check&&result.indexOf(check)>-1:operator==="$="?check&&result.slice(-check.length)===check:operator==="~="?(" "+result.replace(rwhitespace," ")+" ").indexOf(check)>-1:operator==="|="?result===check||result.slice(0,check.length+1)===check+"-":false;};},"CHILD":function(type,what,argument,first,last){var simple=type.slice(0,3)!=="nth",forward=type.slice(-4)!=="last",ofType=what==="of-type";return first===1&&last===0?function(elem){return!!elem.parentNode;}:function(elem,context,xml){var cache,outerCache,node,diff,nodeIndex,start,dir=simple!==forward?"nextSibling":"previousSibling",parent=elem.parentNode,name=ofType&&elem.nodeName.toLowerCase(),useCache=!xml&&!ofType;if(parent){if(simple){while(dir){node=elem;while((node=node[dir])){if(ofType?node.nodeName.toLowerCase()===name:node.nodeType===1){return false;}}
start=dir=type==="only"&&!start&&"nextSibling";}
return true;}
start=[forward?parent.firstChild:parent.lastChild];if(forward&&useCache){ outerCache=parent[expando]||(parent[expando]={});cache=outerCache[type]||[];nodeIndex=cache[0]===dirruns&&cache[1];diff=cache[0]===dirruns&&cache[2];node=nodeIndex&&parent.childNodes[nodeIndex];while((node=++nodeIndex&&node&&node[dir]||(diff=nodeIndex=0)||start.pop())){ if(node.nodeType===1&&++diff&&node===elem){outerCache[type]=[dirruns,nodeIndex,diff];break;}}
}else if(useCache&&(cache=(elem[expando]||(elem[expando]={}))[type])&&cache[0]===dirruns){diff=cache[1];}else{ while((node=++nodeIndex&&node&&node[dir]||(diff=nodeIndex=0)||start.pop())){if((ofType?node.nodeName.toLowerCase()===name:node.nodeType===1)&&++diff){ if(useCache){(node[expando]||(node[expando]={}))[type]=[dirruns,diff];}
if(node===elem){break;}}}} 
diff-=last;return diff===first||(diff%first===0&&diff/first>=0);}};},"PSEUDO":function(pseudo,argument){


 var args,fn=Expr.pseudos[pseudo]||Expr.setFilters[pseudo.toLowerCase()]||Sizzle.error("unsupported pseudo: "+pseudo);

 if(fn[expando]){return fn(argument);} 
if(fn.length>1){args=[pseudo,pseudo,"",argument];return Expr.setFilters.hasOwnProperty(pseudo.toLowerCase())?markFunction(function(seed,matches){var idx,matched=fn(seed,argument),i=matched.length;while(i--){idx=indexOf(seed,matched[i]);seed[idx]=!(matches[idx]=matched[i]);}}):function(elem){return fn(elem,0,args);};}
return fn;}},pseudos:{"not":markFunction(function(selector){

 var input=[],results=[],matcher=compile(selector.replace(rtrim,"$1"));return matcher[expando]?markFunction(function(seed,matches,context,xml){var elem,unmatched=matcher(seed,null,xml,[]),i=seed.length;while(i--){if((elem=unmatched[i])){seed[i]=!(matches[i]=elem);}}}):function(elem,context,xml){input[0]=elem;matcher(input,null,xml,results);input[0]=null;return!results.pop();};}),"has":markFunction(function(selector){return function(elem){return Sizzle(selector,elem).length>0;};}),"contains":markFunction(function(text){text=text.replace(runescape,funescape);return function(elem){return(elem.textContent||elem.innerText||getText(elem)).indexOf(text)>-1;};}),

"lang":markFunction(function(lang){ if(!ridentifier.test(lang||"")){Sizzle.error("unsupported lang: "+lang);}
lang=lang.replace(runescape,funescape).toLowerCase();return function(elem){var elemLang;do{if((elemLang=documentIsHTML?elem.lang:elem.getAttribute("xml:lang")||elem.getAttribute("lang"))){elemLang=elemLang.toLowerCase();return elemLang===lang||elemLang.indexOf(lang+"-")===0;}}while((elem=elem.parentNode)&&elem.nodeType===1);return false;};}),"target":function(elem){var hash=window.location&&window.location.hash;return hash&&hash.slice(1)===elem.id;},"root":function(elem){return elem===docElem;},"focus":function(elem){return elem===document.activeElement&&(!document.hasFocus||document.hasFocus())&&!!(elem.type||elem.href||~elem.tabIndex);},"enabled":function(elem){return elem.disabled===false;},"disabled":function(elem){return elem.disabled===true;},"checked":function(elem){
 var nodeName=elem.nodeName.toLowerCase();return(nodeName==="input"&&!!elem.checked)||(nodeName==="option"&&!!elem.selected);},"selected":function(elem){
 if(elem.parentNode){elem.parentNode.selectedIndex;}
return elem.selected===true;},"empty":function(elem){
 
for(elem=elem.firstChild;elem;elem=elem.nextSibling){if(elem.nodeType<6){return false;}}
return true;},"parent":function(elem){return!Expr.pseudos["empty"](elem);},"header":function(elem){return rheader.test(elem.nodeName);},"input":function(elem){return rinputs.test(elem.nodeName);},"button":function(elem){var name=elem.nodeName.toLowerCase();return name==="input"&&elem.type==="button"||name==="button";},"text":function(elem){var attr;return elem.nodeName.toLowerCase()==="input"&&elem.type==="text"&&
((attr=elem.getAttribute("type"))==null||attr.toLowerCase()==="text");},"first":createPositionalPseudo(function(){return[0];}),"last":createPositionalPseudo(function(matchIndexes,length){return[length-1];}),"eq":createPositionalPseudo(function(matchIndexes,length,argument){return[argument<0?argument+length:argument];}),"even":createPositionalPseudo(function(matchIndexes,length){var i=0;for(;i<length;i+=2){matchIndexes.push(i);}
return matchIndexes;}),"odd":createPositionalPseudo(function(matchIndexes,length){var i=1;for(;i<length;i+=2){matchIndexes.push(i);}
return matchIndexes;}),"lt":createPositionalPseudo(function(matchIndexes,length,argument){var i=argument<0?argument+length:argument;for(;--i>=0;){matchIndexes.push(i);}
return matchIndexes;}),"gt":createPositionalPseudo(function(matchIndexes,length,argument){var i=argument<0?argument+length:argument;for(;++i<length;){matchIndexes.push(i);}
return matchIndexes;})}};Expr.pseudos["nth"]=Expr.pseudos["eq"];for(i in{radio:true,checkbox:true,file:true,password:true,image:true}){Expr.pseudos[i]=createInputPseudo(i);}
for(i in{submit:true,reset:true}){Expr.pseudos[i]=createButtonPseudo(i);}
function setFilters(){}
setFilters.prototype=Expr.filters=Expr.pseudos;Expr.setFilters=new setFilters();tokenize=Sizzle.tokenize=function(selector,parseOnly){var matched,match,tokens,type,soFar,groups,preFilters,cached=tokenCache[selector+" "];if(cached){return parseOnly?0:cached.slice(0);}
soFar=selector;groups=[];preFilters=Expr.preFilter;while(soFar){ if(!matched||(match=rcomma.exec(soFar))){if(match){ soFar=soFar.slice(match[0].length)||soFar;}
groups.push((tokens=[]));}
matched=false; if((match=rcombinators.exec(soFar))){matched=match.shift();tokens.push({value:matched, type:match[0].replace(rtrim," ")});soFar=soFar.slice(matched.length);} 
for(type in Expr.filter){if((match=matchExpr[type].exec(soFar))&&(!preFilters[type]||(match=preFilters[type](match)))){matched=match.shift();tokens.push({value:matched,type:type,matches:match});soFar=soFar.slice(matched.length);}}
if(!matched){break;}}

 
return parseOnly?soFar.length:soFar?Sizzle.error(selector): tokenCache(selector,groups).slice(0);};function toSelector(tokens){var i=0,len=tokens.length,selector="";for(;i<len;i++){selector+=tokens[i].value;}
return selector;}
function addCombinator(matcher,combinator,base){var dir=combinator.dir,checkNonElements=base&&dir==="parentNode",doneName=done++;return combinator.first? function(elem,context,xml){while((elem=elem[dir])){if(elem.nodeType===1||checkNonElements){return matcher(elem,context,xml);}}}: function(elem,context,xml){var oldCache,outerCache,newCache=[dirruns,doneName]; if(xml){while((elem=elem[dir])){if(elem.nodeType===1||checkNonElements){if(matcher(elem,context,xml)){return true;}}}}else{while((elem=elem[dir])){if(elem.nodeType===1||checkNonElements){outerCache=elem[expando]||(elem[expando]={});if((oldCache=outerCache[dir])&&oldCache[0]===dirruns&&oldCache[1]===doneName){ return(newCache[2]=oldCache[2]);}else{ outerCache[dir]=newCache; if((newCache[2]=matcher(elem,context,xml))){return true;}}}}}};}
function elementMatcher(matchers){return matchers.length>1?function(elem,context,xml){var i=matchers.length;while(i--){if(!matchers[i](elem,context,xml)){return false;}}
return true;}:matchers[0];}
function multipleContexts(selector,contexts,results){var i=0,len=contexts.length;for(;i<len;i++){Sizzle(selector,contexts[i],results);}
return results;}
function condense(unmatched,map,filter,context,xml){var elem,newUnmatched=[],i=0,len=unmatched.length,mapped=map!=null;for(;i<len;i++){if((elem=unmatched[i])){if(!filter||filter(elem,context,xml)){newUnmatched.push(elem);if(mapped){map.push(i);}}}}
return newUnmatched;}
function setMatcher(preFilter,selector,matcher,postFilter,postFinder,postSelector){if(postFilter&&!postFilter[expando]){postFilter=setMatcher(postFilter);}
if(postFinder&&!postFinder[expando]){postFinder=setMatcher(postFinder,postSelector);}
return markFunction(function(seed,results,context,xml){var temp,i,elem,preMap=[],postMap=[],preexisting=results.length, elems=seed||multipleContexts(selector||"*",context.nodeType?[context]:context,[]), matcherIn=preFilter&&(seed||!selector)?condense(elems,preMap,preFilter,context,xml):elems,matcherOut=matcher?postFinder||(seed?preFilter:preexisting||postFilter)?[]: results:matcherIn; if(matcher){matcher(matcherIn,matcherOut,context,xml);} 
if(postFilter){temp=condense(matcherOut,postMap);postFilter(temp,[],context,xml); i=temp.length;while(i--){if((elem=temp[i])){matcherOut[postMap[i]]=!(matcherIn[postMap[i]]=elem);}}}
if(seed){if(postFinder||preFilter){if(postFinder){ temp=[];i=matcherOut.length;while(i--){if((elem=matcherOut[i])){ temp.push((matcherIn[i]=elem));}}
postFinder(null,(matcherOut=[]),temp,xml);} 
i=matcherOut.length;while(i--){if((elem=matcherOut[i])&&(temp=postFinder?indexOf(seed,elem):preMap[i])>-1){seed[temp]=!(results[temp]=elem);}}}
}else{matcherOut=condense(matcherOut===results?matcherOut.splice(preexisting,matcherOut.length):matcherOut);if(postFinder){postFinder(null,results,matcherOut,xml);}else{push.apply(results,matcherOut);}}});}
function matcherFromTokens(tokens){var checkContext,matcher,j,len=tokens.length,leadingRelative=Expr.relative[tokens[0].type],implicitRelative=leadingRelative||Expr.relative[" "],i=leadingRelative?1:0,matchContext=addCombinator(function(elem){return elem===checkContext;},implicitRelative,true),matchAnyContext=addCombinator(function(elem){return indexOf(checkContext,elem)>-1;},implicitRelative,true),matchers=[function(elem,context,xml){var ret=(!leadingRelative&&(xml||context!==outermostContext))||((checkContext=context).nodeType?matchContext(elem,context,xml):matchAnyContext(elem,context,xml));checkContext=null;return ret;}];for(;i<len;i++){if((matcher=Expr.relative[tokens[i].type])){matchers=[addCombinator(elementMatcher(matchers),matcher)];}else{matcher=Expr.filter[tokens[i].type].apply(null,tokens[i].matches); if(matcher[expando]){ j=++i;for(;j<len;j++){if(Expr.relative[tokens[j].type]){break;}}
return setMatcher(i>1&&elementMatcher(matchers),i>1&&toSelector(tokens.slice(0,i-1).concat({value:tokens[i-2].type===" "?"*":""})).replace(rtrim,"$1"),matcher,i<j&&matcherFromTokens(tokens.slice(i,j)),j<len&&matcherFromTokens((tokens=tokens.slice(j))),j<len&&toSelector(tokens));}
matchers.push(matcher);}}
return elementMatcher(matchers);}
function matcherFromGroupMatchers(elementMatchers,setMatchers){var bySet=setMatchers.length>0,byElement=elementMatchers.length>0,superMatcher=function(seed,context,xml,results,outermost){var elem,j,matcher,matchedCount=0,i="0",unmatched=seed&&[],setMatched=[],contextBackup=outermostContext, elems=seed||byElement&&Expr.find["TAG"]("*",outermost), dirrunsUnique=(dirruns+=contextBackup==null?1:Math.random()||0.1),len=elems.length;if(outermost){outermostContext=context!==document&&context;}


 
for(;i!==len&&(elem=elems[i])!=null;i++){if(byElement&&elem){j=0;while((matcher=elementMatchers[j++])){if(matcher(elem,context,xml)){results.push(elem);break;}}
if(outermost){dirruns=dirrunsUnique;}} 
if(bySet){ if((elem=!matcher&&elem)){matchedCount--;} 
if(seed){unmatched.push(elem);}}} 
matchedCount+=i;if(bySet&&i!==matchedCount){j=0;while((matcher=setMatchers[j++])){matcher(unmatched,setMatched,context,xml);}
if(seed){ if(matchedCount>0){while(i--){if(!(unmatched[i]||setMatched[i])){setMatched[i]=pop.call(results);}}} 
setMatched=condense(setMatched);} 
push.apply(results,setMatched); if(outermost&&!seed&&setMatched.length>0&&(matchedCount+setMatchers.length)>1){Sizzle.uniqueSort(results);}} 
if(outermost){dirruns=dirrunsUnique;outermostContext=contextBackup;}
return unmatched;};return bySet?markFunction(superMatcher):superMatcher;}
compile=Sizzle.compile=function(selector,match ){var i,setMatchers=[],elementMatchers=[],cached=compilerCache[selector+" "];if(!cached){ if(!match){match=tokenize(selector);}
i=match.length;while(i--){cached=matcherFromTokens(match[i]);if(cached[expando]){setMatchers.push(cached);}else{elementMatchers.push(cached);}} 
cached=compilerCache(selector,matcherFromGroupMatchers(elementMatchers,setMatchers)); cached.selector=selector;}
return cached;};select=Sizzle.select=function(selector,context,results,seed){var i,tokens,token,type,find,compiled=typeof selector==="function"&&selector,match=!seed&&tokenize((selector=compiled.selector||selector));results=results||[]; if(match.length===1){ tokens=match[0]=match[0].slice(0);if(tokens.length>2&&(token=tokens[0]).type==="ID"&&support.getById&&context.nodeType===9&&documentIsHTML&&Expr.relative[tokens[1].type]){context=(Expr.find["ID"](token.matches[0].replace(runescape,funescape),context)||[])[0];if(!context){return results;}else if(compiled){context=context.parentNode;}
selector=selector.slice(tokens.shift().value.length);} 
i=matchExpr["needsContext"].test(selector)?0:tokens.length;while(i--){token=tokens[i]; if(Expr.relative[(type=token.type)]){break;}
if((find=Expr.find[type])){ if((seed=find(token.matches[0].replace(runescape,funescape),rsibling.test(tokens[0].type)&&testContext(context.parentNode)||context))){ tokens.splice(i,1);selector=seed.length&&toSelector(tokens);if(!selector){push.apply(results,seed);return results;}
break;}}}}

(compiled||compile(selector,match))(seed,context,!documentIsHTML,results,rsibling.test(selector)&&testContext(context.parentNode)||context);return results;};
support.sortStable=expando.split("").sort(sortOrder).join("")===expando;support.detectDuplicates=!!hasDuplicate;setDocument();
support.sortDetached=assert(function(div1){return div1.compareDocumentPosition(document.createElement("div"))&1;});
if(!assert(function(div){div.innerHTML="<a href='#'></a>";return div.firstChild.getAttribute("href")==="#";})){addHandle("type|href|height|width",function(elem,name,isXML){if(!isXML){return elem.getAttribute(name,name.toLowerCase()==="type"?1:2);}});}

if(!support.attributes||!assert(function(div){div.innerHTML="<input/>";div.firstChild.setAttribute("value","");return div.firstChild.getAttribute("value")==="";})){addHandle("value",function(elem,name,isXML){if(!isXML&&elem.nodeName.toLowerCase()==="input"){return elem.defaultValue;}});}

if(!assert(function(div){return div.getAttribute("disabled")==null;})){addHandle(booleans,function(elem,name,isXML){var val;if(!isXML){return elem[name]===true?name.toLowerCase():(val=elem.getAttributeNode(name))&&val.specified?val.value:null;}});}
return Sizzle;})(window);jQuery.find=Sizzle;jQuery.expr=Sizzle.selectors;jQuery.expr[":"]=jQuery.expr.pseudos;jQuery.unique=Sizzle.uniqueSort;jQuery.text=Sizzle.getText;jQuery.isXMLDoc=Sizzle.isXML;jQuery.contains=Sizzle.contains;var rneedsContext=jQuery.expr.match.needsContext;var rsingleTag=(/^<(\w+)\s*\/?>(?:<\/\1>|)$/);var risSimple=/^.[^:#\[\.,]*$/;function winnow(elements,qualifier,not){if(jQuery.isFunction(qualifier)){return jQuery.grep(elements,function(elem,i){return!!qualifier.call(elem,i,elem)!==not;});}
if(qualifier.nodeType){return jQuery.grep(elements,function(elem){return(elem===qualifier)!==not;});}
if(typeof qualifier==="string"){if(risSimple.test(qualifier)){return jQuery.filter(qualifier,elements,not);}
qualifier=jQuery.filter(qualifier,elements);}
return jQuery.grep(elements,function(elem){return(indexOf.call(qualifier,elem)>=0)!==not;});}
jQuery.filter=function(expr,elems,not){var elem=elems[0];if(not){expr=":not("+expr+")";}
return elems.length===1&&elem.nodeType===1?jQuery.find.matchesSelector(elem,expr)?[elem]:[]:jQuery.find.matches(expr,jQuery.grep(elems,function(elem){return elem.nodeType===1;}));};jQuery.fn.extend({find:function(selector){var i,len=this.length,ret=[],self=this;if(typeof selector!=="string"){return this.pushStack(jQuery(selector).filter(function(){for(i=0;i<len;i++){if(jQuery.contains(self[i],this)){return true;}}}));}
for(i=0;i<len;i++){jQuery.find(selector,self[i],ret);}
ret=this.pushStack(len>1?jQuery.unique(ret):ret);ret.selector=this.selector?this.selector+" "+selector:selector;return ret;},filter:function(selector){return this.pushStack(winnow(this,selector||[],false));},not:function(selector){return this.pushStack(winnow(this,selector||[],true));},is:function(selector){return!!winnow(this,
typeof selector==="string"&&rneedsContext.test(selector)?jQuery(selector):selector||[],false).length;}});
var rootjQuery,

rquickExpr=/^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,init=jQuery.fn.init=function(selector,context){var match,elem;if(!selector){return this;} 
if(typeof selector==="string"){if(selector[0]==="<"&&selector[selector.length-1]===">"&&selector.length>=3){ match=[null,selector,null];}else{match=rquickExpr.exec(selector);} 
if(match&&(match[1]||!context)){if(match[1]){context=context instanceof jQuery?context[0]:context;
 jQuery.merge(this,jQuery.parseHTML(match[1],context&&context.nodeType?context.ownerDocument||context:document,true));if(rsingleTag.test(match[1])&&jQuery.isPlainObject(context)){for(match in context){ if(jQuery.isFunction(this[match])){this[match](context[match]);}else{this.attr(match,context[match]);}}}
return this;}else{elem=document.getElementById(match[2]);
if(elem&&elem.parentNode){ this.length=1;this[0]=elem;}
this.context=document;this.selector=selector;return this;}
}else if(!context||context.jquery){return(context||rootjQuery).find(selector);
}else{return this.constructor(context).find(selector);}
}else if(selector.nodeType){this.context=this[0]=selector;this.length=1;return this;
}else if(jQuery.isFunction(selector)){return typeof rootjQuery.ready!=="undefined"?rootjQuery.ready(selector): selector(jQuery);}
if(selector.selector!==undefined){this.selector=selector.selector;this.context=selector.context;}
return jQuery.makeArray(selector,this);};init.prototype=jQuery.fn;rootjQuery=jQuery(document);var rparentsprev=/^(?:parents|prev(?:Until|All))/, guaranteedUnique={children:true,contents:true,next:true,prev:true};jQuery.extend({dir:function(elem,dir,until){var matched=[],truncate=until!==undefined;while((elem=elem[dir])&&elem.nodeType!==9){if(elem.nodeType===1){if(truncate&&jQuery(elem).is(until)){break;}
matched.push(elem);}}
return matched;},sibling:function(n,elem){var matched=[];for(;n;n=n.nextSibling){if(n.nodeType===1&&n!==elem){matched.push(n);}}
return matched;}});jQuery.fn.extend({has:function(target){var targets=jQuery(target,this),l=targets.length;return this.filter(function(){var i=0;for(;i<l;i++){if(jQuery.contains(this,targets[i])){return true;}}});},closest:function(selectors,context){var cur,i=0,l=this.length,matched=[],pos=rneedsContext.test(selectors)||typeof selectors!=="string"?jQuery(selectors,context||this.context):0;for(;i<l;i++){for(cur=this[i];cur&&cur!==context;cur=cur.parentNode){ if(cur.nodeType<11&&(pos?pos.index(cur)>-1: cur.nodeType===1&&jQuery.find.matchesSelector(cur,selectors))){matched.push(cur);break;}}}
return this.pushStack(matched.length>1?jQuery.unique(matched):matched);}, index:function(elem){ if(!elem){return(this[0]&&this[0].parentNode)?this.first().prevAll().length:-1;} 
if(typeof elem==="string"){return indexOf.call(jQuery(elem),this[0]);} 
return indexOf.call(this, elem.jquery?elem[0]:elem);},add:function(selector,context){return this.pushStack(jQuery.unique(jQuery.merge(this.get(),jQuery(selector,context))));},addBack:function(selector){return this.add(selector==null?this.prevObject:this.prevObject.filter(selector));}});function sibling(cur,dir){while((cur=cur[dir])&&cur.nodeType!==1){}
return cur;}
jQuery.each({parent:function(elem){var parent=elem.parentNode;return parent&&parent.nodeType!==11?parent:null;},parents:function(elem){return jQuery.dir(elem,"parentNode");},parentsUntil:function(elem,i,until){return jQuery.dir(elem,"parentNode",until);},next:function(elem){return sibling(elem,"nextSibling");},prev:function(elem){return sibling(elem,"previousSibling");},nextAll:function(elem){return jQuery.dir(elem,"nextSibling");},prevAll:function(elem){return jQuery.dir(elem,"previousSibling");},nextUntil:function(elem,i,until){return jQuery.dir(elem,"nextSibling",until);},prevUntil:function(elem,i,until){return jQuery.dir(elem,"previousSibling",until);},siblings:function(elem){return jQuery.sibling((elem.parentNode||{}).firstChild,elem);},children:function(elem){return jQuery.sibling(elem.firstChild);},contents:function(elem){return elem.contentDocument||jQuery.merge([],elem.childNodes);}},function(name,fn){jQuery.fn[name]=function(until,selector){var matched=jQuery.map(this,fn,until);if(name.slice(-5)!=="Until"){selector=until;}
if(selector&&typeof selector==="string"){matched=jQuery.filter(selector,matched);}
if(this.length>1){ if(!guaranteedUnique[name]){jQuery.unique(matched);} 
if(rparentsprev.test(name)){matched.reverse();}}
return this.pushStack(matched);};});var rnotwhite=(/\S+/g);var optionsCache={};function createOptions(options){var object=optionsCache[options]={};jQuery.each(options.match(rnotwhite)||[],function(_,flag){object[flag]=true;});return object;}
jQuery.Callbacks=function(options){
options=typeof options==="string"?(optionsCache[options]||createOptions(options)):jQuery.extend({},options);var
memory, fired, firing,firingStart, firingLength,firingIndex, list=[], stack=!options.once&&[], fire=function(data){memory=options.memory&&data;fired=true;firingIndex=firingStart||0;firingStart=0;firingLength=list.length;firing=true;for(;list&&firingIndex<firingLength;firingIndex++){if(list[firingIndex].apply(data[0],data[1])===false&&options.stopOnFalse){memory=false; break;}}
firing=false;if(list){if(stack){if(stack.length){fire(stack.shift());}}else if(memory){list=[];}else{self.disable();}}}, self={ add:function(){if(list){ var start=list.length;(function add(args){jQuery.each(args,function(_,arg){var type=jQuery.type(arg);if(type==="function"){if(!options.unique||!self.has(arg)){list.push(arg);}}else if(arg&&arg.length&&type!=="string"){ add(arg);}});})(arguments);
if(firing){firingLength=list.length;
}else if(memory){firingStart=start;fire(memory);}}
return this;}, remove:function(){if(list){jQuery.each(arguments,function(_,arg){var index;while((index=jQuery.inArray(arg,list,index))>-1){list.splice(index,1); if(firing){if(index<=firingLength){firingLength--;}
if(index<=firingIndex){firingIndex--;}}}});}
return this;},has:function(fn){return fn?jQuery.inArray(fn,list)>-1:!!(list&&list.length);}, empty:function(){list=[];firingLength=0;return this;}, disable:function(){list=stack=memory=undefined;return this;},disabled:function(){return!list;}, lock:function(){stack=undefined;if(!memory){self.disable();}
return this;},locked:function(){return!stack;}, fireWith:function(context,args){if(list&&(!fired||stack)){args=args||[];args=[context,args.slice?args.slice():args];if(firing){stack.push(args);}else{fire(args);}}
return this;}, fire:function(){self.fireWith(this,arguments);return this;}, fired:function(){return!!fired;}};return self;};jQuery.extend({Deferred:function(func){var tuples=[["resolve","done",jQuery.Callbacks("once memory"),"resolved"],["reject","fail",jQuery.Callbacks("once memory"),"rejected"],["notify","progress",jQuery.Callbacks("memory")]],state="pending",promise={state:function(){return state;},always:function(){deferred.done(arguments).fail(arguments);return this;},then:function(){var fns=arguments;return jQuery.Deferred(function(newDefer){jQuery.each(tuples,function(i,tuple){var fn=jQuery.isFunction(fns[i])&&fns[i]; deferred[tuple[1]](function(){var returned=fn&&fn.apply(this,arguments);if(returned&&jQuery.isFunction(returned.promise)){returned.promise().done(newDefer.resolve).fail(newDefer.reject).progress(newDefer.notify);}else{newDefer[tuple[0]+"With"](this===promise?newDefer.promise():this,fn?[returned]:arguments);}});});fns=null;}).promise();},
 promise:function(obj){return obj!=null?jQuery.extend(obj,promise):promise;}},deferred={}; promise.pipe=promise.then; jQuery.each(tuples,function(i,tuple){var list=tuple[2],stateString=tuple[3]; promise[tuple[1]]=list.add; if(stateString){list.add(function(){state=stateString;},tuples[i^1][2].disable,tuples[2][2].lock);}
deferred[tuple[0]]=function(){deferred[tuple[0]+"With"](this===deferred?promise:this,arguments);return this;};deferred[tuple[0]+"With"]=list.fireWith;}); promise.promise(deferred); if(func){func.call(deferred,deferred);}
return deferred;}, when:function(subordinate ){var i=0,resolveValues=slice.call(arguments),length=resolveValues.length, remaining=length!==1||(subordinate&&jQuery.isFunction(subordinate.promise))?length:0,deferred=remaining===1?subordinate:jQuery.Deferred(), updateFunc=function(i,contexts,values){return function(value){contexts[i]=this;values[i]=arguments.length>1?slice.call(arguments):value;if(values===progressValues){deferred.notifyWith(contexts,values);}else if(!(--remaining)){deferred.resolveWith(contexts,values);}};},progressValues,progressContexts,resolveContexts; if(length>1){progressValues=new Array(length);progressContexts=new Array(length);resolveContexts=new Array(length);for(;i<length;i++){if(resolveValues[i]&&jQuery.isFunction(resolveValues[i].promise)){resolveValues[i].promise().done(updateFunc(i,resolveContexts,resolveValues)).fail(deferred.reject).progress(updateFunc(i,progressContexts,progressValues));}else{--remaining;}}} 
if(!remaining){deferred.resolveWith(resolveContexts,resolveValues);}
return deferred.promise();}});var readyList;jQuery.fn.ready=function(fn){ jQuery.ready.promise().done(fn);return this;};jQuery.extend({isReady:false,
 readyWait:1, holdReady:function(hold){if(hold){jQuery.readyWait++;}else{jQuery.ready(true);}}, ready:function(wait){ if(wait===true?--jQuery.readyWait:jQuery.isReady){return;} 
jQuery.isReady=true; if(wait!==true&&--jQuery.readyWait>0){return;} 
readyList.resolveWith(document,[jQuery]); if(jQuery.fn.triggerHandler){jQuery(document).triggerHandler("ready");jQuery(document).off("ready");}}});function completed(){document.removeEventListener("DOMContentLoaded",completed,false);window.removeEventListener("load",completed,false);jQuery.ready();}
jQuery.ready.promise=function(obj){if(!readyList){readyList=jQuery.Deferred();
 if(document.readyState==="complete"){ setTimeout(jQuery.ready);}else{ document.addEventListener("DOMContentLoaded",completed,false); window.addEventListener("load",completed,false);}}
return readyList.promise(obj);};jQuery.ready.promise();
var access=jQuery.access=function(elems,fn,key,value,chainable,emptyGet,raw){var i=0,len=elems.length,bulk=key==null; if(jQuery.type(key)==="object"){chainable=true;for(i in key){jQuery.access(elems,fn,i,key[i],true,emptyGet,raw);}
}else if(value!==undefined){chainable=true;if(!jQuery.isFunction(value)){raw=true;}
if(bulk){ if(raw){fn.call(elems,value);fn=null;}else{bulk=fn;fn=function(elem,key,value){return bulk.call(jQuery(elem),value);};}}
if(fn){for(;i<len;i++){fn(elems[i],key,raw?value:value.call(elems[i],i,fn(elems[i],key)));}}}
return chainable?elems: bulk?fn.call(elems):len?fn(elems[0],key):emptyGet;};jQuery.acceptData=function(owner){



 return owner.nodeType===1||owner.nodeType===9||!(+owner.nodeType);};function Data(){ Object.defineProperty(this.cache={},0,{get:function(){return{};}});this.expando=jQuery.expando+Data.uid++;}
Data.uid=1;Data.accepts=jQuery.acceptData;Data.prototype={key:function(owner){if(!Data.accepts(owner)){return 0;}
var descriptor={}, unlock=owner[this.expando]; if(!unlock){unlock=Data.uid++; try{descriptor[this.expando]={value:unlock};Object.defineProperties(owner,descriptor);
}catch(e){descriptor[this.expando]=unlock;jQuery.extend(owner,descriptor);}} 
if(!this.cache[unlock]){this.cache[unlock]={};}
return unlock;},set:function(owner,data,value){var prop,
 unlock=this.key(owner),cache=this.cache[unlock]; if(typeof data==="string"){cache[data]=value;}else{ if(jQuery.isEmptyObject(cache)){jQuery.extend(this.cache[unlock],data);}else{for(prop in data){cache[prop]=data[prop];}}}
return cache;},get:function(owner,key){
var cache=this.cache[this.key(owner)];return key===undefined?cache:cache[key];},access:function(owner,key,value){var stored;




if(key===undefined||((key&&typeof key==="string")&&value===undefined)){stored=this.get(owner,key);return stored!==undefined?stored:this.get(owner,jQuery.camelCase(key));}



this.set(owner,key,value);
return value!==undefined?value:key;},remove:function(owner,key){var i,name,camel,unlock=this.key(owner),cache=this.cache[unlock];if(key===undefined){this.cache[unlock]={};}else{ if(jQuery.isArray(key)){

name=key.concat(key.map(jQuery.camelCase));}else{camel=jQuery.camelCase(key); if(key in cache){name=[key,camel];}else{ name=camel;name=name in cache?[name]:(name.match(rnotwhite)||[]);}}
i=name.length;while(i--){delete cache[name[i]];}}},hasData:function(owner){return!jQuery.isEmptyObject(this.cache[owner[this.expando]]||{});},discard:function(owner){if(owner[this.expando]){delete this.cache[owner[this.expando]];}}};var data_priv=new Data();var data_user=new Data();



var rbrace=/^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,rmultiDash=/([A-Z])/g;function dataAttr(elem,key,data){var name;
 if(data===undefined&&elem.nodeType===1){name="data-"+key.replace(rmultiDash,"-$1").toLowerCase();data=elem.getAttribute(name);if(typeof data==="string"){try{data=data==="true"?true:data==="false"?false:data==="null"?null:+data+""===data?+data:rbrace.test(data)?jQuery.parseJSON(data):data;}catch(e){} 
data_user.set(elem,key,data);}else{data=undefined;}}
return data;}
jQuery.extend({hasData:function(elem){return data_user.hasData(elem)||data_priv.hasData(elem);},data:function(elem,name,data){return data_user.access(elem,name,data);},removeData:function(elem,name){data_user.remove(elem,name);},
_data:function(elem,name,data){return data_priv.access(elem,name,data);},_removeData:function(elem,name){data_priv.remove(elem,name);}});jQuery.fn.extend({data:function(key,value){var i,name,data,elem=this[0],attrs=elem&&elem.attributes; if(key===undefined){if(this.length){data=data_user.get(elem);if(elem.nodeType===1&&!data_priv.get(elem,"hasDataAttrs")){i=attrs.length;while(i--){if(attrs[i]){name=attrs[i].name;if(name.indexOf("data-")===0){name=jQuery.camelCase(name.slice(5));dataAttr(elem,name,data[name]);}}}
data_priv.set(elem,"hasDataAttrs",true);}}
return data;} 
if(typeof key==="object"){return this.each(function(){data_user.set(this,key);});}
return access(this,function(value){var data,camelKey=jQuery.camelCase(key);



if(elem&&value===undefined){
 data=data_user.get(elem,key);if(data!==undefined){return data;}
 
data=data_user.get(elem,camelKey);if(data!==undefined){return data;}
 
data=dataAttr(elem,camelKey,undefined);if(data!==undefined){return data;}
return;}
this.each(function(){
var data=data_user.get(this,camelKey);
data_user.set(this,camelKey,value);

if(key.indexOf("-")!==-1&&data!==undefined){data_user.set(this,key,value);}});},null,value,arguments.length>1,null,true);},removeData:function(key){return this.each(function(){data_user.remove(this,key);});}});jQuery.extend({queue:function(elem,type,data){var queue;if(elem){type=(type||"fx")+"queue";queue=data_priv.get(elem,type); if(data){if(!queue||jQuery.isArray(data)){queue=data_priv.access(elem,type,jQuery.makeArray(data));}else{queue.push(data);}}
return queue||[];}},dequeue:function(elem,type){type=type||"fx";var queue=jQuery.queue(elem,type),startLength=queue.length,fn=queue.shift(),hooks=jQuery._queueHooks(elem,type),next=function(){jQuery.dequeue(elem,type);}; if(fn==="inprogress"){fn=queue.shift();startLength--;}
if(fn){
 if(type==="fx"){queue.unshift("inprogress");} 
delete hooks.stop;fn.call(elem,next,hooks);}
if(!startLength&&hooks){hooks.empty.fire();}}, _queueHooks:function(elem,type){var key=type+"queueHooks";return data_priv.get(elem,key)||data_priv.access(elem,key,{empty:jQuery.Callbacks("once memory").add(function(){data_priv.remove(elem,[type+"queue",key]);})});}});jQuery.fn.extend({queue:function(type,data){var setter=2;if(typeof type!=="string"){data=type;type="fx";setter--;}
if(arguments.length<setter){return jQuery.queue(this[0],type);}
return data===undefined?this:this.each(function(){var queue=jQuery.queue(this,type,data); jQuery._queueHooks(this,type);if(type==="fx"&&queue[0]!=="inprogress"){jQuery.dequeue(this,type);}});},dequeue:function(type){return this.each(function(){jQuery.dequeue(this,type);});},clearQueue:function(type){return this.queue(type||"fx",[]);},
promise:function(type,obj){var tmp,count=1,defer=jQuery.Deferred(),elements=this,i=this.length,resolve=function(){if(!(--count)){defer.resolveWith(elements,[elements]);}};if(typeof type!=="string"){obj=type;type=undefined;}
type=type||"fx";while(i--){tmp=data_priv.get(elements[i],type+"queueHooks");if(tmp&&tmp.empty){count++;tmp.empty.add(resolve);}}
resolve();return defer.promise(obj);}});var pnum=(/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/).source;var cssExpand=["Top","Right","Bottom","Left"];var isHidden=function(elem,el){ elem=el||elem;return jQuery.css(elem,"display")==="none"||!jQuery.contains(elem.ownerDocument,elem);};var rcheckableType=(/^(?:checkbox|radio)$/i);(function(){var fragment=document.createDocumentFragment(),div=fragment.appendChild(document.createElement("div")),input=document.createElement("input");

input.setAttribute("type","radio");input.setAttribute("checked","checked");input.setAttribute("name","t");div.appendChild(input);
 support.checkClone=div.cloneNode(true).cloneNode(true).lastChild.checked; div.innerHTML="<textarea>x</textarea>";support.noCloneChecked=!!div.cloneNode(true).lastChild.defaultValue;})();var strundefined=typeof undefined;support.focusinBubbles="onfocusin"in window;var
rkeyEvent=/^key/,rmouseEvent=/^(?:mouse|pointer|contextmenu)|click/,rfocusMorph=/^(?:focusinfocus|focusoutblur)$/,rtypenamespace=/^([^.]*)(?:\.(.+)|)$/;function returnTrue(){return true;}
function returnFalse(){return false;}
function safeActiveElement(){try{return document.activeElement;}catch(err){}}
jQuery.event={global:{},add:function(elem,types,handler,data,selector){var handleObjIn,eventHandle,tmp,events,t,handleObj,special,handlers,type,namespaces,origType,elemData=data_priv.get(elem);if(!elemData){return;} 
if(handler.handler){handleObjIn=handler;handler=handleObjIn.handler;selector=handleObjIn.selector;} 
if(!handler.guid){handler.guid=jQuery.guid++;} 
if(!(events=elemData.events)){events=elemData.events={};}
if(!(eventHandle=elemData.handle)){eventHandle=elemData.handle=function(e){
 return typeof jQuery!==strundefined&&jQuery.event.triggered!==e.type?jQuery.event.dispatch.apply(elem,arguments):undefined;};} 
types=(types||"").match(rnotwhite)||[""];t=types.length;while(t--){tmp=rtypenamespace.exec(types[t])||[];type=origType=tmp[1];namespaces=(tmp[2]||"").split(".").sort(); if(!type){continue;} 
special=jQuery.event.special[type]||{}; type=(selector?special.delegateType:special.bindType)||type; special=jQuery.event.special[type]||{}; handleObj=jQuery.extend({type:type,origType:origType,data:data,handler:handler,guid:handler.guid,selector:selector,needsContext:selector&&jQuery.expr.match.needsContext.test(selector),namespace:namespaces.join(".")},handleObjIn); if(!(handlers=events[type])){handlers=events[type]=[];handlers.delegateCount=0; if(!special.setup||special.setup.call(elem,data,namespaces,eventHandle)===false){if(elem.addEventListener){elem.addEventListener(type,eventHandle,false);}}}
if(special.add){special.add.call(elem,handleObj);if(!handleObj.handler.guid){handleObj.handler.guid=handler.guid;}} 
if(selector){handlers.splice(handlers.delegateCount++,0,handleObj);}else{handlers.push(handleObj);} 
jQuery.event.global[type]=true;}}, remove:function(elem,types,handler,selector,mappedTypes){var j,origCount,tmp,events,t,handleObj,special,handlers,type,namespaces,origType,elemData=data_priv.hasData(elem)&&data_priv.get(elem);if(!elemData||!(events=elemData.events)){return;} 
types=(types||"").match(rnotwhite)||[""];t=types.length;while(t--){tmp=rtypenamespace.exec(types[t])||[];type=origType=tmp[1];namespaces=(tmp[2]||"").split(".").sort(); if(!type){for(type in events){jQuery.event.remove(elem,type+types[t],handler,selector,true);}
continue;}
special=jQuery.event.special[type]||{};type=(selector?special.delegateType:special.bindType)||type;handlers=events[type]||[];tmp=tmp[2]&&new RegExp("(^|\\.)"+namespaces.join("\\.(?:.*\\.|)")+"(\\.|$)"); origCount=j=handlers.length;while(j--){handleObj=handlers[j];if((mappedTypes||origType===handleObj.origType)&&(!handler||handler.guid===handleObj.guid)&&(!tmp||tmp.test(handleObj.namespace))&&(!selector||selector===handleObj.selector||selector==="**"&&handleObj.selector)){handlers.splice(j,1);if(handleObj.selector){handlers.delegateCount--;}
if(special.remove){special.remove.call(elem,handleObj);}}}

if(origCount&&!handlers.length){if(!special.teardown||special.teardown.call(elem,namespaces,elemData.handle)===false){jQuery.removeEvent(elem,type,elemData.handle);}
delete events[type];}} 
if(jQuery.isEmptyObject(events)){delete elemData.handle;data_priv.remove(elem,"events");}},trigger:function(event,data,elem,onlyHandlers){var i,cur,tmp,bubbleType,ontype,handle,special,eventPath=[elem||document],type=hasOwn.call(event,"type")?event.type:event,namespaces=hasOwn.call(event,"namespace")?event.namespace.split("."):[];cur=tmp=elem=elem||document; if(elem.nodeType===3||elem.nodeType===8){return;} 
if(rfocusMorph.test(type+jQuery.event.triggered)){return;}
if(type.indexOf(".")>=0){namespaces=type.split(".");type=namespaces.shift();namespaces.sort();}
ontype=type.indexOf(":")<0&&"on"+type; event=event[jQuery.expando]?event:new jQuery.Event(type,typeof event==="object"&&event);event.isTrigger=onlyHandlers?2:3;event.namespace=namespaces.join(".");event.namespace_re=event.namespace?new RegExp("(^|\\.)"+namespaces.join("\\.(?:.*\\.|)")+"(\\.|$)"):null; event.result=undefined;if(!event.target){event.target=elem;} 
data=data==null?[event]:jQuery.makeArray(data,[event]); special=jQuery.event.special[type]||{};if(!onlyHandlers&&special.trigger&&special.trigger.apply(elem,data)===false){return;} 
if(!onlyHandlers&&!special.noBubble&&!jQuery.isWindow(elem)){bubbleType=special.delegateType||type;if(!rfocusMorph.test(bubbleType+type)){cur=cur.parentNode;}
for(;cur;cur=cur.parentNode){eventPath.push(cur);tmp=cur;}
if(tmp===(elem.ownerDocument||document)){eventPath.push(tmp.defaultView||tmp.parentWindow||window);}} 
i=0;while((cur=eventPath[i++])&&!event.isPropagationStopped()){event.type=i>1?bubbleType:special.bindType||type; handle=(data_priv.get(cur,"events")||{})[event.type]&&data_priv.get(cur,"handle");if(handle){handle.apply(cur,data);} 
handle=ontype&&cur[ontype];if(handle&&handle.apply&&jQuery.acceptData(cur)){event.result=handle.apply(cur,data);if(event.result===false){event.preventDefault();}}}
event.type=type; if(!onlyHandlers&&!event.isDefaultPrevented()){if((!special._default||special._default.apply(eventPath.pop(),data)===false)&&jQuery.acceptData(elem)){if(ontype&&jQuery.isFunction(elem[type])&&!jQuery.isWindow(elem)){ tmp=elem[ontype];if(tmp){elem[ontype]=null;} 
jQuery.event.triggered=type;elem[type]();jQuery.event.triggered=undefined;if(tmp){elem[ontype]=tmp;}}}}
return event.result;},dispatch:function(event){ event=jQuery.event.fix(event);var i,j,ret,matched,handleObj,handlerQueue=[],args=slice.call(arguments),handlers=(data_priv.get(this,"events")||{})[event.type]||[],special=jQuery.event.special[event.type]||{}; args[0]=event;event.delegateTarget=this; if(special.preDispatch&&special.preDispatch.call(this,event)===false){return;} 
handlerQueue=jQuery.event.handlers.call(this,event,handlers); i=0;while((matched=handlerQueue[i++])&&!event.isPropagationStopped()){event.currentTarget=matched.elem;j=0;while((handleObj=matched.handlers[j++])&&!event.isImmediatePropagationStopped()){
if(!event.namespace_re||event.namespace_re.test(handleObj.namespace)){event.handleObj=handleObj;event.data=handleObj.data;ret=((jQuery.event.special[handleObj.origType]||{}).handle||handleObj.handler).apply(matched.elem,args);if(ret!==undefined){if((event.result=ret)===false){event.preventDefault();event.stopPropagation();}}}}} 
if(special.postDispatch){special.postDispatch.call(this,event);}
return event.result;},handlers:function(event,handlers){var i,matches,sel,handleObj,handlerQueue=[],delegateCount=handlers.delegateCount,cur=event.target;

if(delegateCount&&cur.nodeType&&(!event.button||event.type!=="click")){for(;cur!==this;cur=cur.parentNode||this){if(cur.disabled!==true||event.type!=="click"){matches=[];for(i=0;i<delegateCount;i++){handleObj=handlers[i];sel=handleObj.selector+" ";if(matches[sel]===undefined){matches[sel]=handleObj.needsContext?jQuery(sel,this).index(cur)>=0:jQuery.find(sel,this,null,[cur]).length;}
if(matches[sel]){matches.push(handleObj);}}
if(matches.length){handlerQueue.push({elem:cur,handlers:matches});}}}} 
if(delegateCount<handlers.length){handlerQueue.push({elem:this,handlers:handlers.slice(delegateCount)});}
return handlerQueue;}, props:"altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),fixHooks:{},keyHooks:{props:"char charCode key keyCode".split(" "),filter:function(event,original){ if(event.which==null){event.which=original.charCode!=null?original.charCode:original.keyCode;}
return event;}},mouseHooks:{props:"button buttons clientX clientY offsetX offsetY pageX pageY screenX screenY toElement".split(" "),filter:function(event,original){var eventDoc,doc,body,button=original.button; if(event.pageX==null&&original.clientX!=null){eventDoc=event.target.ownerDocument||document;doc=eventDoc.documentElement;body=eventDoc.body;event.pageX=original.clientX+(doc&&doc.scrollLeft||body&&body.scrollLeft||0)-(doc&&doc.clientLeft||body&&body.clientLeft||0);event.pageY=original.clientY+(doc&&doc.scrollTop||body&&body.scrollTop||0)-(doc&&doc.clientTop||body&&body.clientTop||0);}
 
if(!event.which&&button!==undefined){event.which=(button&1?1:(button&2?3:(button&4?2:0)));}
return event;}},fix:function(event){if(event[jQuery.expando]){return event;} 
var i,prop,copy,type=event.type,originalEvent=event,fixHook=this.fixHooks[type];if(!fixHook){this.fixHooks[type]=fixHook=rmouseEvent.test(type)?this.mouseHooks:rkeyEvent.test(type)?this.keyHooks:{};}
copy=fixHook.props?this.props.concat(fixHook.props):this.props;event=new jQuery.Event(originalEvent);i=copy.length;while(i--){prop=copy[i];event[prop]=originalEvent[prop];} 
if(!event.target){event.target=document;}

if(event.target.nodeType===3){event.target=event.target.parentNode;}
return fixHook.filter?fixHook.filter(event,originalEvent):event;},special:{load:{ noBubble:true},focus:{ trigger:function(){if(this!==safeActiveElement()&&this.focus){this.focus();return false;}},delegateType:"focusin"},blur:{trigger:function(){if(this===safeActiveElement()&&this.blur){this.blur();return false;}},delegateType:"focusout"},click:{ trigger:function(){if(this.type==="checkbox"&&this.click&&jQuery.nodeName(this,"input")){this.click();return false;}}, _default:function(event){return jQuery.nodeName(event.target,"a");}},beforeunload:{postDispatch:function(event){if(event.result!==undefined&&event.originalEvent){event.originalEvent.returnValue=event.result;}}}},simulate:function(type,elem,event,bubble){
var e=jQuery.extend(new jQuery.Event(),event,{type:type,isSimulated:true,originalEvent:{}});if(bubble){jQuery.event.trigger(e,null,elem);}else{jQuery.event.dispatch.call(elem,e);}
if(e.isDefaultPrevented()){event.preventDefault();}}};jQuery.removeEvent=function(elem,type,handle){if(elem.removeEventListener){elem.removeEventListener(type,handle,false);}};jQuery.Event=function(src,props){ if(!(this instanceof jQuery.Event)){return new jQuery.Event(src,props);} 
if(src&&src.type){this.originalEvent=src;this.type=src.type;
this.isDefaultPrevented=src.defaultPrevented||src.defaultPrevented===undefined&& src.returnValue===false?returnTrue:returnFalse;}else{this.type=src;} 
if(props){jQuery.extend(this,props);} 
this.timeStamp=src&&src.timeStamp||jQuery.now(); this[jQuery.expando]=true;};
jQuery.Event.prototype={isDefaultPrevented:returnFalse,isPropagationStopped:returnFalse,isImmediatePropagationStopped:returnFalse,preventDefault:function(){var e=this.originalEvent;this.isDefaultPrevented=returnTrue;if(e&&e.preventDefault){e.preventDefault();}},stopPropagation:function(){var e=this.originalEvent;this.isPropagationStopped=returnTrue;if(e&&e.stopPropagation){e.stopPropagation();}},stopImmediatePropagation:function(){var e=this.originalEvent;this.isImmediatePropagationStopped=returnTrue;if(e&&e.stopImmediatePropagation){e.stopImmediatePropagation();}
this.stopPropagation();}};
jQuery.each({mouseenter:"mouseover",mouseleave:"mouseout",pointerenter:"pointerover",pointerleave:"pointerout"},function(orig,fix){jQuery.event.special[orig]={delegateType:fix,bindType:fix,handle:function(event){var ret,target=this,related=event.relatedTarget,handleObj=event.handleObj; if(!related||(related!==target&&!jQuery.contains(target,related))){event.type=handleObj.origType;ret=handleObj.handler.apply(this,arguments);event.type=fix;}
return ret;}};});
if(!support.focusinBubbles){jQuery.each({focus:"focusin",blur:"focusout"},function(orig,fix){ var handler=function(event){jQuery.event.simulate(fix,event.target,jQuery.event.fix(event),true);};jQuery.event.special[fix]={setup:function(){var doc=this.ownerDocument||this,attaches=data_priv.access(doc,fix);if(!attaches){doc.addEventListener(orig,handler,true);}
data_priv.access(doc,fix,(attaches||0)+1);},teardown:function(){var doc=this.ownerDocument||this,attaches=data_priv.access(doc,fix)-1;if(!attaches){doc.removeEventListener(orig,handler,true);data_priv.remove(doc,fix);}else{data_priv.access(doc,fix,attaches);}}};});}
jQuery.fn.extend({on:function(types,selector,data,fn,one){var origFn,type; if(typeof types==="object"){if(typeof selector!=="string"){data=data||selector;selector=undefined;}
for(type in types){this.on(type,selector,data,types[type],one);}
return this;}
if(data==null&&fn==null){fn=selector;data=selector=undefined;}else if(fn==null){if(typeof selector==="string"){fn=data;data=undefined;}else{fn=data;data=selector;selector=undefined;}}
if(fn===false){fn=returnFalse;}else if(!fn){return this;}
if(one===1){origFn=fn;fn=function(event){ jQuery().off(event);return origFn.apply(this,arguments);}; fn.guid=origFn.guid||(origFn.guid=jQuery.guid++);}
return this.each(function(){jQuery.event.add(this,types,fn,data,selector);});},one:function(types,selector,data,fn){return this.on(types,selector,data,fn,1);},off:function(types,selector,fn){var handleObj,type;if(types&&types.preventDefault&&types.handleObj){ handleObj=types.handleObj;jQuery(types.delegateTarget).off(handleObj.namespace?handleObj.origType+"."+handleObj.namespace:handleObj.origType,handleObj.selector,handleObj.handler);return this;}
if(typeof types==="object"){for(type in types){this.off(type,selector,types[type]);}
return this;}
if(selector===false||typeof selector==="function"){fn=selector;selector=undefined;}
if(fn===false){fn=returnFalse;}
return this.each(function(){jQuery.event.remove(this,types,fn,selector);});},trigger:function(type,data){return this.each(function(){jQuery.event.trigger(type,data,this);});},triggerHandler:function(type,data){var elem=this[0];if(elem){return jQuery.event.trigger(type,data,elem,true);}}});var
rxhtmlTag=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,rtagName=/<([\w:]+)/,rhtml=/<|&#?\w+;/,rnoInnerhtml=/<(?:script|style|link)/i, rchecked=/checked\s*(?:[^=]|=\s*.checked.)/i,rscriptType=/^$|\/(?:java|ecma)script/i,rscriptTypeMasked=/^true\/(.*)/,rcleanScript=/^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,wrapMap={ option:[1,"<select multiple='multiple'>","</select>"],thead:[1,"<table>","</table>"],col:[2,"<table><colgroup>","</colgroup></table>"],tr:[2,"<table><tbody>","</tbody></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],_default:[0,"",""]};wrapMap.optgroup=wrapMap.option;wrapMap.tbody=wrapMap.tfoot=wrapMap.colgroup=wrapMap.caption=wrapMap.thead;wrapMap.th=wrapMap.td;
function manipulationTarget(elem,content){return jQuery.nodeName(elem,"table")&&jQuery.nodeName(content.nodeType!==11?content:content.firstChild,"tr")?elem.getElementsByTagName("tbody")[0]||elem.appendChild(elem.ownerDocument.createElement("tbody")):elem;}
function disableScript(elem){elem.type=(elem.getAttribute("type")!==null)+"/"+elem.type;return elem;}
function restoreScript(elem){var match=rscriptTypeMasked.exec(elem.type);if(match){elem.type=match[1];}else{elem.removeAttribute("type");}
return elem;}
function setGlobalEval(elems,refElements){var i=0,l=elems.length;for(;i<l;i++){data_priv.set(elems[i],"globalEval",!refElements||data_priv.get(refElements[i],"globalEval"));}}
function cloneCopyEvent(src,dest){var i,l,type,pdataOld,pdataCur,udataOld,udataCur,events;if(dest.nodeType!==1){return;}
if(data_priv.hasData(src)){pdataOld=data_priv.access(src);pdataCur=data_priv.set(dest,pdataOld);events=pdataOld.events;if(events){delete pdataCur.handle;pdataCur.events={};for(type in events){for(i=0,l=events[type].length;i<l;i++){jQuery.event.add(dest,type,events[type][i]);}}}} 
if(data_user.hasData(src)){udataOld=data_user.access(src);udataCur=jQuery.extend({},udataOld);data_user.set(dest,udataCur);}}
function getAll(context,tag){var ret=context.getElementsByTagName?context.getElementsByTagName(tag||"*"):context.querySelectorAll?context.querySelectorAll(tag||"*"):[];return tag===undefined||tag&&jQuery.nodeName(context,tag)?jQuery.merge([context],ret):ret;}
function fixInput(src,dest){var nodeName=dest.nodeName.toLowerCase();if(nodeName==="input"&&rcheckableType.test(src.type)){dest.checked=src.checked;}else if(nodeName==="input"||nodeName==="textarea"){dest.defaultValue=src.defaultValue;}}
jQuery.extend({clone:function(elem,dataAndEvents,deepDataAndEvents){var i,l,srcElements,destElements,clone=elem.cloneNode(true),inPage=jQuery.contains(elem.ownerDocument,elem); if(!support.noCloneChecked&&(elem.nodeType===1||elem.nodeType===11)&&!jQuery.isXMLDoc(elem)){ destElements=getAll(clone);srcElements=getAll(elem);for(i=0,l=srcElements.length;i<l;i++){fixInput(srcElements[i],destElements[i]);}} 
if(dataAndEvents){if(deepDataAndEvents){srcElements=srcElements||getAll(elem);destElements=destElements||getAll(clone);for(i=0,l=srcElements.length;i<l;i++){cloneCopyEvent(srcElements[i],destElements[i]);}}else{cloneCopyEvent(elem,clone);}} 
destElements=getAll(clone,"script");if(destElements.length>0){setGlobalEval(destElements,!inPage&&getAll(elem,"script"));} 
return clone;},buildFragment:function(elems,context,scripts,selection){var elem,tmp,tag,wrap,contains,j,fragment=context.createDocumentFragment(),nodes=[],i=0,l=elems.length;for(;i<l;i++){elem=elems[i];if(elem||elem===0){ if(jQuery.type(elem)==="object"){
 jQuery.merge(nodes,elem.nodeType?[elem]:elem);}else if(!rhtml.test(elem)){nodes.push(context.createTextNode(elem));}else{tmp=tmp||fragment.appendChild(context.createElement("div")); tag=(rtagName.exec(elem)||["",""])[1].toLowerCase();wrap=wrapMap[tag]||wrapMap._default;tmp.innerHTML=wrap[1]+elem.replace(rxhtmlTag,"<$1></$2>")+wrap[2]; j=wrap[0];while(j--){tmp=tmp.lastChild;}
 
jQuery.merge(nodes,tmp.childNodes); tmp=fragment.firstChild;tmp.textContent="";}}} 
fragment.textContent="";i=0;while((elem=nodes[i++])){
 if(selection&&jQuery.inArray(elem,selection)!==-1){continue;}
contains=jQuery.contains(elem.ownerDocument,elem); tmp=getAll(fragment.appendChild(elem),"script"); if(contains){setGlobalEval(tmp);} 
if(scripts){j=0;while((elem=tmp[j++])){if(rscriptType.test(elem.type||"")){scripts.push(elem);}}}}
return fragment;},cleanData:function(elems){var data,elem,type,key,special=jQuery.event.special,i=0;for(;(elem=elems[i])!==undefined;i++){if(jQuery.acceptData(elem)){key=elem[data_priv.expando];if(key&&(data=data_priv.cache[key])){if(data.events){for(type in data.events){if(special[type]){jQuery.event.remove(elem,type);}else{jQuery.removeEvent(elem,type,data.handle);}}}
if(data_priv.cache[key]){ delete data_priv.cache[key];}}} 
delete data_user.cache[elem[data_user.expando]];}}});jQuery.fn.extend({text:function(value){return access(this,function(value){return value===undefined?jQuery.text(this):this.empty().each(function(){if(this.nodeType===1||this.nodeType===11||this.nodeType===9){this.textContent=value;}});},null,value,arguments.length);},append:function(){return this.domManip(arguments,function(elem){if(this.nodeType===1||this.nodeType===11||this.nodeType===9){var target=manipulationTarget(this,elem);target.appendChild(elem);}});},prepend:function(){return this.domManip(arguments,function(elem){if(this.nodeType===1||this.nodeType===11||this.nodeType===9){var target=manipulationTarget(this,elem);target.insertBefore(elem,target.firstChild);}});},before:function(){return this.domManip(arguments,function(elem){if(this.parentNode){this.parentNode.insertBefore(elem,this);}});},after:function(){return this.domManip(arguments,function(elem){if(this.parentNode){this.parentNode.insertBefore(elem,this.nextSibling);}});},remove:function(selector,keepData ){var elem,elems=selector?jQuery.filter(selector,this):this,i=0;for(;(elem=elems[i])!=null;i++){if(!keepData&&elem.nodeType===1){jQuery.cleanData(getAll(elem));}
if(elem.parentNode){if(keepData&&jQuery.contains(elem.ownerDocument,elem)){setGlobalEval(getAll(elem,"script"));}
elem.parentNode.removeChild(elem);}}
return this;},empty:function(){var elem,i=0;for(;(elem=this[i])!=null;i++){if(elem.nodeType===1){ jQuery.cleanData(getAll(elem,false)); elem.textContent="";}}
return this;},clone:function(dataAndEvents,deepDataAndEvents){dataAndEvents=dataAndEvents==null?false:dataAndEvents;deepDataAndEvents=deepDataAndEvents==null?dataAndEvents:deepDataAndEvents;return this.map(function(){return jQuery.clone(this,dataAndEvents,deepDataAndEvents);});},html:function(value){return access(this,function(value){var elem=this[0]||{},i=0,l=this.length;if(value===undefined&&elem.nodeType===1){return elem.innerHTML;} 
if(typeof value==="string"&&!rnoInnerhtml.test(value)&&!wrapMap[(rtagName.exec(value)||["",""])[1].toLowerCase()]){value=value.replace(rxhtmlTag,"<$1></$2>");try{for(;i<l;i++){elem=this[i]||{}; if(elem.nodeType===1){jQuery.cleanData(getAll(elem,false));elem.innerHTML=value;}}
elem=0;}catch(e){}}
if(elem){this.empty().append(value);}},null,value,arguments.length);},replaceWith:function(){var arg=arguments[0]; this.domManip(arguments,function(elem){arg=this.parentNode;jQuery.cleanData(getAll(this));if(arg){arg.replaceChild(elem,this);}});return arg&&(arg.length||arg.nodeType)?this:this.remove();},detach:function(selector){return this.remove(selector,true);},domManip:function(args,callback){ args=concat.apply([],args);var fragment,first,scripts,hasScripts,node,doc,i=0,l=this.length,set=this,iNoClone=l-1,value=args[0],isFunction=jQuery.isFunction(value); if(isFunction||(l>1&&typeof value==="string"&&!support.checkClone&&rchecked.test(value))){return this.each(function(index){var self=set.eq(index);if(isFunction){args[0]=value.call(this,index,self.html());}
self.domManip(args,callback);});}
if(l){fragment=jQuery.buildFragment(args,this[0].ownerDocument,false,this);first=fragment.firstChild;if(fragment.childNodes.length===1){fragment=first;}
if(first){scripts=jQuery.map(getAll(fragment,"script"),disableScript);hasScripts=scripts.length;
for(;i<l;i++){node=fragment;if(i!==iNoClone){node=jQuery.clone(node,true,true); if(hasScripts){
 jQuery.merge(scripts,getAll(node,"script"));}}
callback.call(this[i],node,i);}
if(hasScripts){doc=scripts[scripts.length-1].ownerDocument; jQuery.map(scripts,restoreScript); for(i=0;i<hasScripts;i++){node=scripts[i];if(rscriptType.test(node.type||"")&&!data_priv.access(node,"globalEval")&&jQuery.contains(doc,node)){if(node.src){ if(jQuery._evalUrl){jQuery._evalUrl(node.src);}}else{jQuery.globalEval(node.textContent.replace(rcleanScript,""));}}}}}}
return this;}});jQuery.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(name,original){jQuery.fn[name]=function(selector){var elems,ret=[],insert=jQuery(selector),last=insert.length-1,i=0;for(;i<=last;i++){elems=i===last?this:this.clone(true);jQuery(insert[i])[original](elems);
 push.apply(ret,elems.get());}
return this.pushStack(ret);};});var iframe,elemdisplay={};function actualDisplay(name,doc){var style,elem=jQuery(doc.createElement(name)).appendTo(doc.body), display=window.getDefaultComputedStyle&&(style=window.getDefaultComputedStyle(elem[0]))? style.display:jQuery.css(elem[0],"display"); elem.detach();return display;}
function defaultDisplay(nodeName){var doc=document,display=elemdisplay[nodeName];if(!display){display=actualDisplay(nodeName,doc); if(display==="none"||!display){ iframe=(iframe||jQuery("<iframe frameborder='0' width='0' height='0'/>")).appendTo(doc.documentElement); doc=iframe[0].contentDocument; doc.write();doc.close();display=actualDisplay(nodeName,doc);iframe.detach();} 
elemdisplay[nodeName]=display;}
return display;}
var rmargin=(/^margin/);var rnumnonpx=new RegExp("^("+pnum+")(?!px)[a-z%]+$","i");var getStyles=function(elem){

if(elem.ownerDocument.defaultView.opener){return elem.ownerDocument.defaultView.getComputedStyle(elem,null);}
return window.getComputedStyle(elem,null);};function curCSS(elem,name,computed){var width,minWidth,maxWidth,ret,style=elem.style;computed=computed||getStyles(elem);
if(computed){ret=computed.getPropertyValue(name)||computed[name];}
if(computed){if(ret===""&&!jQuery.contains(elem.ownerDocument,elem)){ret=jQuery.style(elem,name);}

 
if(rnumnonpx.test(ret)&&rmargin.test(name)){ width=style.width;minWidth=style.minWidth;maxWidth=style.maxWidth; style.minWidth=style.maxWidth=style.width=ret;ret=computed.width; style.width=width;style.minWidth=minWidth;style.maxWidth=maxWidth;}}
return ret!==undefined?
ret+"":ret;}
function addGetHookIf(conditionFn,hookFn){return{get:function(){if(conditionFn()){
delete this.get;return;}
return(this.get=hookFn).apply(this,arguments);}};}
(function(){var pixelPositionVal,boxSizingReliableVal,docElem=document.documentElement,container=document.createElement("div"),div=document.createElement("div");if(!div.style){return;} 
div.style.backgroundClip="content-box";div.cloneNode(true).style.backgroundClip="";support.clearCloneStyle=div.style.backgroundClip==="content-box";container.style.cssText="border:0;width:0;height:0;top:0;left:-9999px;margin-top:1px;"+"position:absolute";container.appendChild(div);
function computePixelPositionAndBoxSizingReliable(){div.style.cssText=
"-webkit-box-sizing:border-box;-moz-box-sizing:border-box;"+"box-sizing:border-box;display:block;margin-top:1%;top:1%;"+"border:1px;padding:1px;width:4px;position:absolute";div.innerHTML="";docElem.appendChild(container);var divStyle=window.getComputedStyle(div,null);pixelPositionVal=divStyle.top!=="1%";boxSizingReliableVal=divStyle.width==="4px";docElem.removeChild(container);}
 
if(window.getComputedStyle){jQuery.extend(support,{pixelPosition:function(){
computePixelPositionAndBoxSizingReliable();return pixelPositionVal;},boxSizingReliable:function(){if(boxSizingReliableVal==null){computePixelPositionAndBoxSizingReliable();}
return boxSizingReliableVal;},reliableMarginRight:function(){



var ret,marginDiv=div.appendChild(document.createElement("div")); marginDiv.style.cssText=div.style.cssText=
"-webkit-box-sizing:content-box;-moz-box-sizing:content-box;"+"box-sizing:content-box;display:block;margin:0;border:0;padding:0";marginDiv.style.marginRight=marginDiv.style.width="0";div.style.width="1px";docElem.appendChild(container);ret=!parseFloat(window.getComputedStyle(marginDiv,null).marginRight);docElem.removeChild(container);div.removeChild(marginDiv);return ret;}});}})();jQuery.swap=function(elem,options,callback,args){var ret,name,old={}; for(name in options){old[name]=elem.style[name];elem.style[name]=options[name];}
ret=callback.apply(elem,args||[]); for(name in options){elem.style[name]=old[name];}
return ret;};var
 
rdisplayswap=/^(none|table(?!-c[ea]).+)/,rnumsplit=new RegExp("^("+pnum+")(.*)$","i"),rrelNum=new RegExp("^([+-])=("+pnum+")","i"),cssShow={position:"absolute",visibility:"hidden",display:"block"},cssNormalTransform={letterSpacing:"0",fontWeight:"400"},cssPrefixes=["Webkit","O","Moz","ms"];function vendorPropName(style,name){ if(name in style){return name;} 
var capName=name[0].toUpperCase()+name.slice(1),origName=name,i=cssPrefixes.length;while(i--){name=cssPrefixes[i]+capName;if(name in style){return name;}}
return origName;}
function setPositiveNumber(elem,value,subtract){var matches=rnumsplit.exec(value);return matches? Math.max(0,matches[1]-(subtract||0))+(matches[2]||"px"):value;}
function augmentWidthOrHeight(elem,name,extra,isBorderBox,styles){var i=extra===(isBorderBox?"border":"content")? 4: name==="width"?1:0,val=0;for(;i<4;i+=2){ if(extra==="margin"){val+=jQuery.css(elem,extra+cssExpand[i],true,styles);}
if(isBorderBox){ if(extra==="content"){val-=jQuery.css(elem,"padding"+cssExpand[i],true,styles);} 
if(extra!=="margin"){val-=jQuery.css(elem,"border"+cssExpand[i]+"Width",true,styles);}}else{ val+=jQuery.css(elem,"padding"+cssExpand[i],true,styles); if(extra!=="padding"){val+=jQuery.css(elem,"border"+cssExpand[i]+"Width",true,styles);}}}
return val;}
function getWidthOrHeight(elem,name,extra){ var valueIsBorderBox=true,val=name==="width"?elem.offsetWidth:elem.offsetHeight,styles=getStyles(elem),isBorderBox=jQuery.css(elem,"boxSizing",false,styles)==="border-box";

 if(val<=0||val==null){ val=curCSS(elem,name,styles);if(val<0||val==null){val=elem.style[name];}
if(rnumnonpx.test(val)){return val;} 
 
valueIsBorderBox=isBorderBox&&(support.boxSizingReliable()||val===elem.style[name]); val=parseFloat(val)||0;} 
return(val+
augmentWidthOrHeight(elem,name,extra||(isBorderBox?"border":"content"),valueIsBorderBox,styles))+"px";}
function showHide(elements,show){var display,elem,hidden,values=[],index=0,length=elements.length;for(;index<length;index++){elem=elements[index];if(!elem.style){continue;}
values[index]=data_priv.get(elem,"olddisplay");display=elem.style.display;if(show){
 if(!values[index]&&display==="none"){elem.style.display="";}

 
if(elem.style.display===""&&isHidden(elem)){values[index]=data_priv.access(elem,"olddisplay",defaultDisplay(elem.nodeName));}}else{hidden=isHidden(elem);if(display!=="none"||!hidden){data_priv.set(elem,"olddisplay",hidden?display:jQuery.css(elem,"display"));}}}
 
for(index=0;index<length;index++){elem=elements[index];if(!elem.style){continue;}
if(!show||elem.style.display==="none"||elem.style.display===""){elem.style.display=show?values[index]||"":"none";}}
return elements;}
jQuery.extend({
 cssHooks:{opacity:{get:function(elem,computed){if(computed){ var ret=curCSS(elem,"opacity");return ret===""?"1":ret;}}}}, cssNumber:{"columnCount":true,"fillOpacity":true,"flexGrow":true,"flexShrink":true,"fontWeight":true,"lineHeight":true,"opacity":true,"order":true,"orphans":true,"widows":true,"zIndex":true,"zoom":true},
 cssProps:{"float":"cssFloat"}, style:function(elem,name,value,extra){ if(!elem||elem.nodeType===3||elem.nodeType===8||!elem.style){return;} 
var ret,type,hooks,origName=jQuery.camelCase(name),style=elem.style;name=jQuery.cssProps[origName]||(jQuery.cssProps[origName]=vendorPropName(style,origName)); hooks=jQuery.cssHooks[name]||jQuery.cssHooks[origName]; if(value!==undefined){type=typeof value;if(type==="string"&&(ret=rrelNum.exec(value))){value=(ret[1]+1)*ret[2]+parseFloat(jQuery.css(elem,name)); type="number";}
if(value==null||value!==value){return;}
if(type==="number"&&!jQuery.cssNumber[origName]){value+="px";} 
if(!support.clearCloneStyle&&value===""&&name.indexOf("background")===0){style[name]="inherit";} 
if(!hooks||!("set"in hooks)||(value=hooks.set(elem,value,extra))!==undefined){style[name]=value;}}else{ if(hooks&&"get"in hooks&&(ret=hooks.get(elem,false,extra))!==undefined){return ret;} 
return style[name];}},css:function(elem,name,extra,styles){var val,num,hooks,origName=jQuery.camelCase(name); name=jQuery.cssProps[origName]||(jQuery.cssProps[origName]=vendorPropName(elem.style,origName)); hooks=jQuery.cssHooks[name]||jQuery.cssHooks[origName]; if(hooks&&"get"in hooks){val=hooks.get(elem,true,extra);} 
if(val===undefined){val=curCSS(elem,name,styles);} 
if(val==="normal"&&name in cssNormalTransform){val=cssNormalTransform[name];} 
if(extra===""||extra){num=parseFloat(val);return extra===true||jQuery.isNumeric(num)?num||0:val;}
return val;}});jQuery.each(["height","width"],function(i,name){jQuery.cssHooks[name]={get:function(elem,computed,extra){if(computed){
 return rdisplayswap.test(jQuery.css(elem,"display"))&&elem.offsetWidth===0?jQuery.swap(elem,cssShow,function(){return getWidthOrHeight(elem,name,extra);}):getWidthOrHeight(elem,name,extra);}},set:function(elem,value,extra){var styles=extra&&getStyles(elem);return setPositiveNumber(elem,value,extra?augmentWidthOrHeight(elem,name,extra,jQuery.css(elem,"boxSizing",false,styles)==="border-box",styles):0);}};});jQuery.cssHooks.marginRight=addGetHookIf(support.reliableMarginRight,function(elem,computed){if(computed){return jQuery.swap(elem,{"display":"inline-block"},curCSS,[elem,"marginRight"]);}});jQuery.each({margin:"",padding:"",border:"Width"},function(prefix,suffix){jQuery.cssHooks[prefix+suffix]={expand:function(value){var i=0,expanded={}, parts=typeof value==="string"?value.split(" "):[value];for(;i<4;i++){expanded[prefix+cssExpand[i]+suffix]=parts[i]||parts[i-2]||parts[0];}
return expanded;}};if(!rmargin.test(prefix)){jQuery.cssHooks[prefix+suffix].set=setPositiveNumber;}});jQuery.fn.extend({css:function(name,value){return access(this,function(elem,name,value){var styles,len,map={},i=0;if(jQuery.isArray(name)){styles=getStyles(elem);len=name.length;for(;i<len;i++){map[name[i]]=jQuery.css(elem,name[i],false,styles);}
return map;}
return value!==undefined?jQuery.style(elem,name,value):jQuery.css(elem,name);},name,value,arguments.length>1);},show:function(){return showHide(this,true);},hide:function(){return showHide(this);},toggle:function(state){if(typeof state==="boolean"){return state?this.show():this.hide();}
return this.each(function(){if(isHidden(this)){jQuery(this).show();}else{jQuery(this).hide();}});}});function Tween(elem,options,prop,end,easing){return new Tween.prototype.init(elem,options,prop,end,easing);}
jQuery.Tween=Tween;Tween.prototype={constructor:Tween,init:function(elem,options,prop,end,easing,unit){this.elem=elem;this.prop=prop;this.easing=easing||"swing";this.options=options;this.start=this.now=this.cur();this.end=end;this.unit=unit||(jQuery.cssNumber[prop]?"":"px");},cur:function(){var hooks=Tween.propHooks[this.prop];return hooks&&hooks.get?hooks.get(this):Tween.propHooks._default.get(this);},run:function(percent){var eased,hooks=Tween.propHooks[this.prop];if(this.options.duration){this.pos=eased=jQuery.easing[this.easing](percent,this.options.duration*percent,0,1,this.options.duration);}else{this.pos=eased=percent;}
this.now=(this.end-this.start)*eased+this.start;if(this.options.step){this.options.step.call(this.elem,this.now,this);}
if(hooks&&hooks.set){hooks.set(this);}else{Tween.propHooks._default.set(this);}
return this;}};Tween.prototype.init.prototype=Tween.prototype;Tween.propHooks={_default:{get:function(tween){var result;if(tween.elem[tween.prop]!=null&&(!tween.elem.style||tween.elem.style[tween.prop]==null)){return tween.elem[tween.prop];}

result=jQuery.css(tween.elem,tween.prop,"");return!result||result==="auto"?0:result;},set:function(tween){if(jQuery.fx.step[tween.prop]){jQuery.fx.step[tween.prop](tween);}else if(tween.elem.style&&(tween.elem.style[jQuery.cssProps[tween.prop]]!=null||jQuery.cssHooks[tween.prop])){jQuery.style(tween.elem,tween.prop,tween.now+tween.unit);}else{tween.elem[tween.prop]=tween.now;}}}};
Tween.propHooks.scrollTop=Tween.propHooks.scrollLeft={set:function(tween){if(tween.elem.nodeType&&tween.elem.parentNode){tween.elem[tween.prop]=tween.now;}}};jQuery.easing={linear:function(p){return p;},swing:function(p){return 0.5-Math.cos(p*Math.PI)/2;}};jQuery.fx=Tween.prototype.init;jQuery.fx.step={};var
fxNow,timerId,rfxtypes=/^(?:toggle|show|hide)$/,rfxnum=new RegExp("^(?:([+-])=|)("+pnum+")([a-z%]*)$","i"),rrun=/queueHooks$/,animationPrefilters=[defaultPrefilter],tweeners={"*":[function(prop,value){var tween=this.createTween(prop,value),target=tween.cur(),parts=rfxnum.exec(value),unit=parts&&parts[3]||(jQuery.cssNumber[prop]?"":"px"), start=(jQuery.cssNumber[prop]||unit!=="px"&&+target)&&rfxnum.exec(jQuery.css(tween.elem,prop)),scale=1,maxIterations=20;if(start&&start[3]!==unit){ unit=unit||start[3]; parts=parts||[]; start=+target||1;do{ scale=scale||".5"; start=start/scale;jQuery.style(tween.elem,prop,start+unit);}while(scale!==(scale=tween.cur()/target)&&scale!==1&&--maxIterations);} 
if(parts){start=tween.start=+start||+target||0;tween.unit=unit; tween.end=parts[1]?start+(parts[1]+1)*parts[2]:+parts[2];}
return tween;}]};function createFxNow(){setTimeout(function(){fxNow=undefined;});return(fxNow=jQuery.now());}
function genFx(type,includeWidth){var which,i=0,attrs={height:type}; includeWidth=includeWidth?1:0;for(;i<4;i+=2-includeWidth){which=cssExpand[i];attrs["margin"+which]=attrs["padding"+which]=type;}
if(includeWidth){attrs.opacity=attrs.width=type;}
return attrs;}
function createTween(value,prop,animation){var tween,collection=(tweeners[prop]||[]).concat(tweeners["*"]),index=0,length=collection.length;for(;index<length;index++){if((tween=collection[index].call(animation,prop,value))){ return tween;}}}
function defaultPrefilter(elem,props,opts){var prop,value,toggle,tween,hooks,oldfire,display,checkDisplay,anim=this,orig={},style=elem.style,hidden=elem.nodeType&&isHidden(elem),dataShow=data_priv.get(elem,"fxshow"); if(!opts.queue){hooks=jQuery._queueHooks(elem,"fx");if(hooks.unqueued==null){hooks.unqueued=0;oldfire=hooks.empty.fire;hooks.empty.fire=function(){if(!hooks.unqueued){oldfire();}};}
hooks.unqueued++;anim.always(function(){ anim.always(function(){hooks.unqueued--;if(!jQuery.queue(elem,"fx").length){hooks.empty.fire();}});});} 
if(elem.nodeType===1&&("height"in props||"width"in props)){


 opts.overflow=[style.overflow,style.overflowX,style.overflowY];
 display=jQuery.css(elem,"display");checkDisplay=display==="none"?data_priv.get(elem,"olddisplay")||defaultDisplay(elem.nodeName):display;if(checkDisplay==="inline"&&jQuery.css(elem,"float")==="none"){style.display="inline-block";}}
if(opts.overflow){style.overflow="hidden";anim.always(function(){style.overflow=opts.overflow[0];style.overflowX=opts.overflow[1];style.overflowY=opts.overflow[2];});} 
for(prop in props){value=props[prop];if(rfxtypes.exec(value)){delete props[prop];toggle=toggle||value==="toggle";if(value===(hidden?"hide":"show")){ if(value==="show"&&dataShow&&dataShow[prop]!==undefined){hidden=true;}else{continue;}}
orig[prop]=dataShow&&dataShow[prop]||jQuery.style(elem,prop);}else{display=undefined;}}
if(!jQuery.isEmptyObject(orig)){if(dataShow){if("hidden"in dataShow){hidden=dataShow.hidden;}}else{dataShow=data_priv.access(elem,"fxshow",{});}
if(toggle){dataShow.hidden=!hidden;}
if(hidden){jQuery(elem).show();}else{anim.done(function(){jQuery(elem).hide();});}
anim.done(function(){var prop;data_priv.remove(elem,"fxshow");for(prop in orig){jQuery.style(elem,prop,orig[prop]);}});for(prop in orig){tween=createTween(hidden?dataShow[prop]:0,prop,anim);if(!(prop in dataShow)){dataShow[prop]=tween.start;if(hidden){tween.end=tween.start;tween.start=prop==="width"||prop==="height"?1:0;}}}
}else if((display==="none"?defaultDisplay(elem.nodeName):display)==="inline"){style.display=display;}}
function propFilter(props,specialEasing){var index,name,easing,value,hooks; for(index in props){name=jQuery.camelCase(index);easing=specialEasing[name];value=props[index];if(jQuery.isArray(value)){easing=value[1];value=props[index]=value[0];}
if(index!==name){props[name]=value;delete props[index];}
hooks=jQuery.cssHooks[name];if(hooks&&"expand"in hooks){value=hooks.expand(value);delete props[name];for(index in value){if(!(index in props)){props[index]=value[index];specialEasing[index]=easing;}}}else{specialEasing[name]=easing;}}}
function Animation(elem,properties,options){var result,stopped,index=0,length=animationPrefilters.length,deferred=jQuery.Deferred().always(function(){ delete tick.elem;}),tick=function(){if(stopped){return false;}
var currentTime=fxNow||createFxNow(),remaining=Math.max(0,animation.startTime+animation.duration-currentTime),
temp=remaining/animation.duration||0,percent=1-temp,index=0,length=animation.tweens.length;for(;index<length;index++){animation.tweens[index].run(percent);}
deferred.notifyWith(elem,[animation,percent,remaining]);if(percent<1&&length){return remaining;}else{deferred.resolveWith(elem,[animation]);return false;}},animation=deferred.promise({elem:elem,props:jQuery.extend({},properties),opts:jQuery.extend(true,{specialEasing:{}},options),originalProperties:properties,originalOptions:options,startTime:fxNow||createFxNow(),duration:options.duration,tweens:[],createTween:function(prop,end){var tween=jQuery.Tween(elem,animation.opts,prop,end,animation.opts.specialEasing[prop]||animation.opts.easing);animation.tweens.push(tween);return tween;},stop:function(gotoEnd){var index=0,
 length=gotoEnd?animation.tweens.length:0;if(stopped){return this;}
stopped=true;for(;index<length;index++){animation.tweens[index].run(1);} 
if(gotoEnd){deferred.resolveWith(elem,[animation,gotoEnd]);}else{deferred.rejectWith(elem,[animation,gotoEnd]);}
return this;}}),props=animation.props;propFilter(props,animation.opts.specialEasing);for(;index<length;index++){result=animationPrefilters[index].call(animation,elem,props,animation.opts);if(result){return result;}}
jQuery.map(props,createTween,animation);if(jQuery.isFunction(animation.opts.start)){animation.opts.start.call(elem,animation);}
jQuery.fx.timer(jQuery.extend(tick,{elem:elem,anim:animation,queue:animation.opts.queue})); return animation.progress(animation.opts.progress).done(animation.opts.done,animation.opts.complete).fail(animation.opts.fail).always(animation.opts.always);}
jQuery.Animation=jQuery.extend(Animation,{tweener:function(props,callback){if(jQuery.isFunction(props)){callback=props;props=["*"];}else{props=props.split(" ");}
var prop,index=0,length=props.length;for(;index<length;index++){prop=props[index];tweeners[prop]=tweeners[prop]||[];tweeners[prop].unshift(callback);}},prefilter:function(callback,prepend){if(prepend){animationPrefilters.unshift(callback);}else{animationPrefilters.push(callback);}}});jQuery.speed=function(speed,easing,fn){var opt=speed&&typeof speed==="object"?jQuery.extend({},speed):{complete:fn||!fn&&easing||jQuery.isFunction(speed)&&speed,duration:speed,easing:fn&&easing||easing&&!jQuery.isFunction(easing)&&easing};opt.duration=jQuery.fx.off?0:typeof opt.duration==="number"?opt.duration:opt.duration in jQuery.fx.speeds?jQuery.fx.speeds[opt.duration]:jQuery.fx.speeds._default;if(opt.queue==null||opt.queue===true){opt.queue="fx";} 
opt.old=opt.complete;opt.complete=function(){if(jQuery.isFunction(opt.old)){opt.old.call(this);}
if(opt.queue){jQuery.dequeue(this,opt.queue);}};return opt;};jQuery.fn.extend({fadeTo:function(speed,to,easing,callback){ return this.filter(isHidden).css("opacity",0).show()
.end().animate({opacity:to},speed,easing,callback);},animate:function(prop,speed,easing,callback){var empty=jQuery.isEmptyObject(prop),optall=jQuery.speed(speed,easing,callback),doAnimation=function(){ var anim=Animation(this,jQuery.extend({},prop),optall); if(empty||data_priv.get(this,"finish")){anim.stop(true);}};doAnimation.finish=doAnimation;return empty||optall.queue===false?this.each(doAnimation):this.queue(optall.queue,doAnimation);},stop:function(type,clearQueue,gotoEnd){var stopQueue=function(hooks){var stop=hooks.stop;delete hooks.stop;stop(gotoEnd);};if(typeof type!=="string"){gotoEnd=clearQueue;clearQueue=type;type=undefined;}
if(clearQueue&&type!==false){this.queue(type||"fx",[]);}
return this.each(function(){var dequeue=true,index=type!=null&&type+"queueHooks",timers=jQuery.timers,data=data_priv.get(this);if(index){if(data[index]&&data[index].stop){stopQueue(data[index]);}}else{for(index in data){if(data[index]&&data[index].stop&&rrun.test(index)){stopQueue(data[index]);}}}
for(index=timers.length;index--;){if(timers[index].elem===this&&(type==null||timers[index].queue===type)){timers[index].anim.stop(gotoEnd);dequeue=false;timers.splice(index,1);}}

if(dequeue||!gotoEnd){jQuery.dequeue(this,type);}});},finish:function(type){if(type!==false){type=type||"fx";}
return this.each(function(){var index,data=data_priv.get(this),queue=data[type+"queue"],hooks=data[type+"queueHooks"],timers=jQuery.timers,length=queue?queue.length:0; data.finish=true; jQuery.queue(this,type,[]);if(hooks&&hooks.stop){hooks.stop.call(this,true);} 
for(index=timers.length;index--;){if(timers[index].elem===this&&timers[index].queue===type){timers[index].anim.stop(true);timers.splice(index,1);}} 
for(index=0;index<length;index++){if(queue[index]&&queue[index].finish){queue[index].finish.call(this);}} 
delete data.finish;});}});jQuery.each(["toggle","show","hide"],function(i,name){var cssFn=jQuery.fn[name];jQuery.fn[name]=function(speed,easing,callback){return speed==null||typeof speed==="boolean"?cssFn.apply(this,arguments):this.animate(genFx(name,true),speed,easing,callback);};});jQuery.each({slideDown:genFx("show"),slideUp:genFx("hide"),slideToggle:genFx("toggle"),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(name,props){jQuery.fn[name]=function(speed,easing,callback){return this.animate(props,speed,easing,callback);};});jQuery.timers=[];jQuery.fx.tick=function(){var timer,i=0,timers=jQuery.timers;fxNow=jQuery.now();for(;i<timers.length;i++){timer=timers[i]; if(!timer()&&timers[i]===timer){timers.splice(i--,1);}}
if(!timers.length){jQuery.fx.stop();}
fxNow=undefined;};jQuery.fx.timer=function(timer){jQuery.timers.push(timer);if(timer()){jQuery.fx.start();}else{jQuery.timers.pop();}};jQuery.fx.interval=13;jQuery.fx.start=function(){if(!timerId){timerId=setInterval(jQuery.fx.tick,jQuery.fx.interval);}};jQuery.fx.stop=function(){clearInterval(timerId);timerId=null;};jQuery.fx.speeds={slow:600,fast:200, _default:400};jQuery.fn.delay=function(time,type){time=jQuery.fx?jQuery.fx.speeds[time]||time:time;type=type||"fx";return this.queue(type,function(next,hooks){var timeout=setTimeout(next,time);hooks.stop=function(){clearTimeout(timeout);};});};(function(){var input=document.createElement("input"),select=document.createElement("select"),opt=select.appendChild(document.createElement("option"));input.type="checkbox";support.checkOn=input.value!==""; support.optSelected=opt.selected;
 select.disabled=true;support.optDisabled=!opt.disabled; input=document.createElement("input");input.value="t";input.type="radio";support.radioValue=input.value==="t";})();var nodeHook,boolHook,attrHandle=jQuery.expr.attrHandle;jQuery.fn.extend({attr:function(name,value){return access(this,jQuery.attr,name,value,arguments.length>1);},removeAttr:function(name){return this.each(function(){jQuery.removeAttr(this,name);});}});jQuery.extend({attr:function(elem,name,value){var hooks,ret,nType=elem.nodeType; if(!elem||nType===3||nType===8||nType===2){return;} 
if(typeof elem.getAttribute===strundefined){return jQuery.prop(elem,name,value);}
 
if(nType!==1||!jQuery.isXMLDoc(elem)){name=name.toLowerCase();hooks=jQuery.attrHooks[name]||(jQuery.expr.match.bool.test(name)?boolHook:nodeHook);}
if(value!==undefined){if(value===null){jQuery.removeAttr(elem,name);}else if(hooks&&"set"in hooks&&(ret=hooks.set(elem,value,name))!==undefined){return ret;}else{elem.setAttribute(name,value+"");return value;}}else if(hooks&&"get"in hooks&&(ret=hooks.get(elem,name))!==null){return ret;}else{ret=jQuery.find.attr(elem,name); return ret==null?undefined:ret;}},removeAttr:function(elem,value){var name,propName,i=0,attrNames=value&&value.match(rnotwhite);if(attrNames&&elem.nodeType===1){while((name=attrNames[i++])){propName=jQuery.propFix[name]||name;if(jQuery.expr.match.bool.test(name)){ elem[propName]=false;}
elem.removeAttribute(name);}}},attrHooks:{type:{set:function(elem,value){if(!support.radioValue&&value==="radio"&&jQuery.nodeName(elem,"input")){var val=elem.value;elem.setAttribute("type",value);if(val){elem.value=val;}
return value;}}}}});boolHook={set:function(elem,value,name){if(value===false){ jQuery.removeAttr(elem,name);}else{elem.setAttribute(name,name);}
return name;}};jQuery.each(jQuery.expr.match.bool.source.match(/\w+/g),function(i,name){var getter=attrHandle[name]||jQuery.find.attr;attrHandle[name]=function(elem,name,isXML){var ret,handle;if(!isXML){ handle=attrHandle[name];attrHandle[name]=ret;ret=getter(elem,name,isXML)!=null?name.toLowerCase():null;attrHandle[name]=handle;}
return ret;};});var rfocusable=/^(?:input|select|textarea|button)$/i;jQuery.fn.extend({prop:function(name,value){return access(this,jQuery.prop,name,value,arguments.length>1);},removeProp:function(name){return this.each(function(){delete this[jQuery.propFix[name]||name];});}});jQuery.extend({propFix:{"for":"htmlFor","class":"className"},prop:function(elem,name,value){var ret,hooks,notxml,nType=elem.nodeType; if(!elem||nType===3||nType===8||nType===2){return;}
notxml=nType!==1||!jQuery.isXMLDoc(elem);if(notxml){ name=jQuery.propFix[name]||name;hooks=jQuery.propHooks[name];}
if(value!==undefined){return hooks&&"set"in hooks&&(ret=hooks.set(elem,value,name))!==undefined?ret:(elem[name]=value);}else{return hooks&&"get"in hooks&&(ret=hooks.get(elem,name))!==null?ret:elem[name];}},propHooks:{tabIndex:{get:function(elem){return elem.hasAttribute("tabindex")||rfocusable.test(elem.nodeName)||elem.href?elem.tabIndex:-1;}}}});if(!support.optSelected){jQuery.propHooks.selected={get:function(elem){var parent=elem.parentNode;if(parent&&parent.parentNode){parent.parentNode.selectedIndex;}
return null;}};}
jQuery.each(["tabIndex","readOnly","maxLength","cellSpacing","cellPadding","rowSpan","colSpan","useMap","frameBorder","contentEditable"],function(){jQuery.propFix[this.toLowerCase()]=this;});var rclass=/[\t\r\n\f]/g;jQuery.fn.extend({addClass:function(value){var classes,elem,cur,clazz,j,finalValue,proceed=typeof value==="string"&&value,i=0,len=this.length;if(jQuery.isFunction(value)){return this.each(function(j){jQuery(this).addClass(value.call(this,j,this.className));});}
if(proceed){classes=(value||"").match(rnotwhite)||[];for(;i<len;i++){elem=this[i];cur=elem.nodeType===1&&(elem.className?(" "+elem.className+" ").replace(rclass," "):" ");if(cur){j=0;while((clazz=classes[j++])){if(cur.indexOf(" "+clazz+" ")<0){cur+=clazz+" ";}}
finalValue=jQuery.trim(cur);if(elem.className!==finalValue){elem.className=finalValue;}}}}
return this;},removeClass:function(value){var classes,elem,cur,clazz,j,finalValue,proceed=arguments.length===0||typeof value==="string"&&value,i=0,len=this.length;if(jQuery.isFunction(value)){return this.each(function(j){jQuery(this).removeClass(value.call(this,j,this.className));});}
if(proceed){classes=(value||"").match(rnotwhite)||[];for(;i<len;i++){elem=this[i];cur=elem.nodeType===1&&(elem.className?(" "+elem.className+" ").replace(rclass," "):"");if(cur){j=0;while((clazz=classes[j++])){ while(cur.indexOf(" "+clazz+" ")>=0){cur=cur.replace(" "+clazz+" "," ");}}
finalValue=value?jQuery.trim(cur):"";if(elem.className!==finalValue){elem.className=finalValue;}}}}
return this;},toggleClass:function(value,stateVal){var type=typeof value;if(typeof stateVal==="boolean"&&type==="string"){return stateVal?this.addClass(value):this.removeClass(value);}
if(jQuery.isFunction(value)){return this.each(function(i){jQuery(this).toggleClass(value.call(this,i,this.className,stateVal),stateVal);});}
return this.each(function(){if(type==="string"){ var className,i=0,self=jQuery(this),classNames=value.match(rnotwhite)||[];while((className=classNames[i++])){ if(self.hasClass(className)){self.removeClass(className);}else{self.addClass(className);}}
}else if(type===strundefined||type==="boolean"){if(this.className){ data_priv.set(this,"__className__",this.className);}
this.className=this.className||value===false?"":data_priv.get(this,"__className__")||"";}});},hasClass:function(selector){var className=" "+selector+" ",i=0,l=this.length;for(;i<l;i++){if(this[i].nodeType===1&&(" "+this[i].className+" ").replace(rclass," ").indexOf(className)>=0){return true;}}
return false;}});var rreturn=/\r/g;jQuery.fn.extend({val:function(value){var hooks,ret,isFunction,elem=this[0];if(!arguments.length){if(elem){hooks=jQuery.valHooks[elem.type]||jQuery.valHooks[elem.nodeName.toLowerCase()];if(hooks&&"get"in hooks&&(ret=hooks.get(elem,"value"))!==undefined){return ret;}
ret=elem.value;return typeof ret==="string"? ret.replace(rreturn,""): ret==null?"":ret;}
return;}
isFunction=jQuery.isFunction(value);return this.each(function(i){var val;if(this.nodeType!==1){return;}
if(isFunction){val=value.call(this,i,jQuery(this).val());}else{val=value;} 
if(val==null){val="";}else if(typeof val==="number"){val+="";}else if(jQuery.isArray(val)){val=jQuery.map(val,function(value){return value==null?"":value+"";});}
hooks=jQuery.valHooks[this.type]||jQuery.valHooks[this.nodeName.toLowerCase()]; if(!hooks||!("set"in hooks)||hooks.set(this,val,"value")===undefined){this.value=val;}});}});jQuery.extend({valHooks:{option:{get:function(elem){var val=jQuery.find.attr(elem,"value");return val!=null?val:jQuery.trim(jQuery.text(elem));}},select:{get:function(elem){var value,option,options=elem.options,index=elem.selectedIndex,one=elem.type==="select-one"||index<0,values=one?null:[],max=one?index+1:options.length,i=index<0?max:one?index:0; for(;i<max;i++){option=options[i];if((option.selected||i===index)&&(support.optDisabled?!option.disabled:option.getAttribute("disabled")===null)&&(!option.parentNode.disabled||!jQuery.nodeName(option.parentNode,"optgroup"))){ value=jQuery(option).val(); if(one){return value;} 
values.push(value);}}
return values;},set:function(elem,value){var optionSet,option,options=elem.options,values=jQuery.makeArray(value),i=options.length;while(i--){option=options[i];if((option.selected=jQuery.inArray(option.value,values)>=0)){optionSet=true;}} 
if(!optionSet){elem.selectedIndex=-1;}
return values;}}}});jQuery.each(["radio","checkbox"],function(){jQuery.valHooks[this]={set:function(elem,value){if(jQuery.isArray(value)){return(elem.checked=jQuery.inArray(jQuery(elem).val(),value)>=0);}}};if(!support.checkOn){jQuery.valHooks[this].get=function(elem){return elem.getAttribute("value")===null?"on":elem.value;};}});jQuery.each(("blur focus focusin focusout load resize scroll unload click dblclick "+"mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave "+"change select submit keydown keypress keyup error contextmenu").split(" "),function(i,name){ jQuery.fn[name]=function(data,fn){return arguments.length>0?this.on(name,null,data,fn):this.trigger(name);};});jQuery.fn.extend({hover:function(fnOver,fnOut){return this.mouseenter(fnOver).mouseleave(fnOut||fnOver);},bind:function(types,data,fn){return this.on(types,null,data,fn);},unbind:function(types,fn){return this.off(types,null,fn);},delegate:function(selector,types,data,fn){return this.on(types,selector,data,fn);},undelegate:function(selector,types,fn){return arguments.length===1?this.off(selector,"**"):this.off(types,selector||"**",fn);}});var nonce=jQuery.now();var rquery=(/\?/);
jQuery.parseJSON=function(data){return JSON.parse(data+"");};jQuery.parseXML=function(data){var xml,tmp;if(!data||typeof data!=="string"){return null;} 
try{tmp=new DOMParser();xml=tmp.parseFromString(data,"text/xml");}catch(e){xml=undefined;}
if(!xml||xml.getElementsByTagName("parsererror").length){jQuery.error("Invalid XML: "+data);}
return xml;};var
rhash=/#.*$/,rts=/([?&])_=[^&]*/,rheaders=/^(.*?):[ \t]*([^\r\n]*)$/mg, rlocalProtocol=/^(?:about|app|app-storage|.+-extension|file|res|widget):$/,rnoContent=/^(?:GET|HEAD)$/,rprotocol=/^\/\//,rurl=/^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,prefilters={},transports={}, allTypes="*/".concat("*"), ajaxLocation=window.location.href, ajaxLocParts=rurl.exec(ajaxLocation.toLowerCase())||[];function addToPrefiltersOrTransports(structure){return function(dataTypeExpression,func){if(typeof dataTypeExpression!=="string"){func=dataTypeExpression;dataTypeExpression="*";}
var dataType,i=0,dataTypes=dataTypeExpression.toLowerCase().match(rnotwhite)||[];if(jQuery.isFunction(func)){ while((dataType=dataTypes[i++])){ if(dataType[0]==="+"){dataType=dataType.slice(1)||"*";(structure[dataType]=structure[dataType]||[]).unshift(func);}else{(structure[dataType]=structure[dataType]||[]).push(func);}}}};}
function inspectPrefiltersOrTransports(structure,options,originalOptions,jqXHR){var inspected={},seekingTransport=(structure===transports);function inspect(dataType){var selected;inspected[dataType]=true;jQuery.each(structure[dataType]||[],function(_,prefilterOrFactory){var dataTypeOrTransport=prefilterOrFactory(options,originalOptions,jqXHR);if(typeof dataTypeOrTransport==="string"&&!seekingTransport&&!inspected[dataTypeOrTransport]){options.dataTypes.unshift(dataTypeOrTransport);inspect(dataTypeOrTransport);return false;}else if(seekingTransport){return!(selected=dataTypeOrTransport);}});return selected;}
return inspect(options.dataTypes[0])||!inspected["*"]&&inspect("*");}

function ajaxExtend(target,src){var key,deep,flatOptions=jQuery.ajaxSettings.flatOptions||{};for(key in src){if(src[key]!==undefined){(flatOptions[key]?target:(deep||(deep={})))[key]=src[key];}}
if(deep){jQuery.extend(true,target,deep);}
return target;}
function ajaxHandleResponses(s,jqXHR,responses){var ct,type,finalDataType,firstDataType,contents=s.contents,dataTypes=s.dataTypes; while(dataTypes[0]==="*"){dataTypes.shift();if(ct===undefined){ct=s.mimeType||jqXHR.getResponseHeader("Content-Type");}} 
if(ct){for(type in contents){if(contents[type]&&contents[type].test(ct)){dataTypes.unshift(type);break;}}} 
if(dataTypes[0]in responses){finalDataType=dataTypes[0];}else{ for(type in responses){if(!dataTypes[0]||s.converters[type+" "+dataTypes[0]]){finalDataType=type;break;}
if(!firstDataType){firstDataType=type;}} 
finalDataType=finalDataType||firstDataType;}

 
if(finalDataType){if(finalDataType!==dataTypes[0]){dataTypes.unshift(finalDataType);}
return responses[finalDataType];}}
function ajaxConvert(s,response,jqXHR,isSuccess){var conv2,current,conv,tmp,prev,converters={}, dataTypes=s.dataTypes.slice(); if(dataTypes[1]){for(conv in s.converters){converters[conv.toLowerCase()]=s.converters[conv];}}
current=dataTypes.shift(); while(current){if(s.responseFields[current]){jqXHR[s.responseFields[current]]=response;} 
if(!prev&&isSuccess&&s.dataFilter){response=s.dataFilter(response,s.dataType);}
prev=current;current=dataTypes.shift();if(current){ if(current==="*"){current=prev;}else if(prev!=="*"&&prev!==current){ conv=converters[prev+" "+current]||converters["* "+current]; if(!conv){for(conv2 in converters){ tmp=conv2.split(" ");if(tmp[1]===current){ conv=converters[prev+" "+tmp[0]]||converters["* "+tmp[0]];if(conv){ if(conv===true){conv=converters[conv2];}else if(converters[conv2]!==true){current=tmp[0];dataTypes.unshift(tmp[1]);}
break;}}}}
if(conv!==true){ if(conv&&s["throws"]){response=conv(response);}else{try{response=conv(response);}catch(e){return{state:"parsererror",error:conv?e:"No conversion from "+prev+" to "+current};}}}}}}
return{state:"success",data:response};}
jQuery.extend({ active:0, lastModified:{},etag:{},ajaxSettings:{url:ajaxLocation,type:"GET",isLocal:rlocalProtocol.test(ajaxLocParts[1]),global:true,processData:true,async:true,contentType:"application/x-www-form-urlencoded; charset=UTF-8",accepts:{"*":allTypes,text:"text/plain",html:"text/html",xml:"application/xml, text/xml",json:"application/json, text/javascript"},contents:{xml:/xml/,html:/html/,json:/json/},responseFields:{xml:"responseXML",text:"responseText",json:"responseJSON"},
 converters:{"* text":String,"text html":true,"text json":jQuery.parseJSON,"text xml":jQuery.parseXML},

flatOptions:{url:true,context:true}},
ajaxSetup:function(target,settings){return settings? ajaxExtend(ajaxExtend(target,jQuery.ajaxSettings),settings): ajaxExtend(jQuery.ajaxSettings,target);},ajaxPrefilter:addToPrefiltersOrTransports(prefilters),ajaxTransport:addToPrefiltersOrTransports(transports), ajax:function(url,options){ if(typeof url==="object"){options=url;url=undefined;} 
options=options||{};var transport, cacheURL, responseHeadersString,responseHeaders, timeoutTimer, parts, fireGlobals, i, s=jQuery.ajaxSetup({},options), callbackContext=s.context||s, globalEventContext=s.context&&(callbackContext.nodeType||callbackContext.jquery)?jQuery(callbackContext):jQuery.event, deferred=jQuery.Deferred(),completeDeferred=jQuery.Callbacks("once memory"), statusCode=s.statusCode||{},requestHeaders={},requestHeadersNames={}, state=0, strAbort="canceled", jqXHR={readyState:0, getResponseHeader:function(key){var match;if(state===2){if(!responseHeaders){responseHeaders={};while((match=rheaders.exec(responseHeadersString))){responseHeaders[match[1].toLowerCase()]=match[2];}}
match=responseHeaders[key.toLowerCase()];}
return match==null?null:match;}, getAllResponseHeaders:function(){return state===2?responseHeadersString:null;}, setRequestHeader:function(name,value){var lname=name.toLowerCase();if(!state){name=requestHeadersNames[lname]=requestHeadersNames[lname]||name;requestHeaders[name]=value;}
return this;}, overrideMimeType:function(type){if(!state){s.mimeType=type;}
return this;}, statusCode:function(map){var code;if(map){if(state<2){for(code in map){ statusCode[code]=[statusCode[code],map[code]];}}else{ jqXHR.always(map[jqXHR.status]);}}
return this;}, abort:function(statusText){var finalText=statusText||strAbort;if(transport){transport.abort(finalText);}
done(0,finalText);return this;}}; deferred.promise(jqXHR).complete=completeDeferred.add;jqXHR.success=jqXHR.done;jqXHR.error=jqXHR.fail; 
s.url=((url||s.url||ajaxLocation)+"").replace(rhash,"").replace(rprotocol,ajaxLocParts[1]+"//"); s.type=options.method||options.type||s.method||s.type; s.dataTypes=jQuery.trim(s.dataType||"*").toLowerCase().match(rnotwhite)||[""]; if(s.crossDomain==null){parts=rurl.exec(s.url.toLowerCase());s.crossDomain=!!(parts&&(parts[1]!==ajaxLocParts[1]||parts[2]!==ajaxLocParts[2]||(parts[3]||(parts[1]==="http:"?"80":"443"))!==(ajaxLocParts[3]||(ajaxLocParts[1]==="http:"?"80":"443"))));} 
if(s.data&&s.processData&&typeof s.data!=="string"){s.data=jQuery.param(s.data,s.traditional);} 
inspectPrefiltersOrTransports(prefilters,s,options,jqXHR); if(state===2){return jqXHR;} 

fireGlobals=jQuery.event&&s.global; if(fireGlobals&&jQuery.active++===0){jQuery.event.trigger("ajaxStart");} 
s.type=s.type.toUpperCase(); s.hasContent=!rnoContent.test(s.type);
 cacheURL=s.url; if(!s.hasContent){ if(s.data){cacheURL=(s.url+=(rquery.test(cacheURL)?"&":"?")+s.data); delete s.data;} 
if(s.cache===false){s.url=rts.test(cacheURL)? cacheURL.replace(rts,"$1_="+nonce++): cacheURL+(rquery.test(cacheURL)?"&":"?")+"_="+nonce++;}}
if(s.ifModified){if(jQuery.lastModified[cacheURL]){jqXHR.setRequestHeader("If-Modified-Since",jQuery.lastModified[cacheURL]);}
if(jQuery.etag[cacheURL]){jqXHR.setRequestHeader("If-None-Match",jQuery.etag[cacheURL]);}} 
if(s.data&&s.hasContent&&s.contentType!==false||options.contentType){jqXHR.setRequestHeader("Content-Type",s.contentType);} 
jqXHR.setRequestHeader("Accept",s.dataTypes[0]&&s.accepts[s.dataTypes[0]]?s.accepts[s.dataTypes[0]]+(s.dataTypes[0]!=="*"?", "+allTypes+"; q=0.01":""):s.accepts["*"]); for(i in s.headers){jqXHR.setRequestHeader(i,s.headers[i]);} 
if(s.beforeSend&&(s.beforeSend.call(callbackContext,jqXHR,s)===false||state===2)){ return jqXHR.abort();} 
strAbort="abort"; for(i in{success:1,error:1,complete:1}){jqXHR[i](s[i]);} 
transport=inspectPrefiltersOrTransports(transports,s,options,jqXHR); if(!transport){done(-1,"No Transport");}else{jqXHR.readyState=1; if(fireGlobals){globalEventContext.trigger("ajaxSend",[jqXHR,s]);} 
if(s.async&&s.timeout>0){timeoutTimer=setTimeout(function(){jqXHR.abort("timeout");},s.timeout);}
try{state=1;transport.send(requestHeaders,done);}catch(e){ if(state<2){done(-1,e);}else{throw e;}}} 
function done(status,nativeStatusText,responses,headers){var isSuccess,success,error,response,modified,statusText=nativeStatusText; if(state===2){return;} 
state=2; if(timeoutTimer){clearTimeout(timeoutTimer);}

transport=undefined; responseHeadersString=headers||""; jqXHR.readyState=status>0?4:0; isSuccess=status>=200&&status<300||status===304; if(responses){response=ajaxHandleResponses(s,jqXHR,responses);}
response=ajaxConvert(s,response,jqXHR,isSuccess); if(isSuccess){if(s.ifModified){modified=jqXHR.getResponseHeader("Last-Modified");if(modified){jQuery.lastModified[cacheURL]=modified;}
modified=jqXHR.getResponseHeader("etag");if(modified){jQuery.etag[cacheURL]=modified;}} 
if(status===204||s.type==="HEAD"){statusText="nocontent";}else if(status===304){statusText="notmodified";}else{statusText=response.state;success=response.data;error=response.error;isSuccess=!error;}}else{ error=statusText;if(status||!statusText){statusText="error";if(status<0){status=0;}}} 
jqXHR.status=status;jqXHR.statusText=(nativeStatusText||statusText)+""; if(isSuccess){deferred.resolveWith(callbackContext,[success,statusText,jqXHR]);}else{deferred.rejectWith(callbackContext,[jqXHR,statusText,error]);} 
jqXHR.statusCode(statusCode);statusCode=undefined;if(fireGlobals){globalEventContext.trigger(isSuccess?"ajaxSuccess":"ajaxError",[jqXHR,s,isSuccess?success:error]);} 
completeDeferred.fireWith(callbackContext,[jqXHR,statusText]);if(fireGlobals){globalEventContext.trigger("ajaxComplete",[jqXHR,s]); if(!(--jQuery.active)){jQuery.event.trigger("ajaxStop");}}}
return jqXHR;},getJSON:function(url,data,callback){return jQuery.get(url,data,callback,"json");},getScript:function(url,callback){return jQuery.get(url,undefined,callback,"script");}});jQuery.each(["get","post"],function(i,method){jQuery[method]=function(url,data,callback,type){ if(jQuery.isFunction(data)){type=type||callback;callback=data;data=undefined;}
return jQuery.ajax({url:url,type:method,dataType:type,data:data,success:callback});};});jQuery._evalUrl=function(url){return jQuery.ajax({url:url,type:"GET",dataType:"script",async:false,global:false,"throws":true});};jQuery.fn.extend({wrapAll:function(html){var wrap;if(jQuery.isFunction(html)){return this.each(function(i){jQuery(this).wrapAll(html.call(this,i));});}
if(this[0]){ wrap=jQuery(html,this[0].ownerDocument).eq(0).clone(true);if(this[0].parentNode){wrap.insertBefore(this[0]);}
wrap.map(function(){var elem=this;while(elem.firstElementChild){elem=elem.firstElementChild;}
return elem;}).append(this);}
return this;},wrapInner:function(html){if(jQuery.isFunction(html)){return this.each(function(i){jQuery(this).wrapInner(html.call(this,i));});}
return this.each(function(){var self=jQuery(this),contents=self.contents();if(contents.length){contents.wrapAll(html);}else{self.append(html);}});},wrap:function(html){var isFunction=jQuery.isFunction(html);return this.each(function(i){jQuery(this).wrapAll(isFunction?html.call(this,i):html);});},unwrap:function(){return this.parent().each(function(){if(!jQuery.nodeName(this,"body")){jQuery(this).replaceWith(this.childNodes);}}).end();}});jQuery.expr.filters.hidden=function(elem){
 return elem.offsetWidth<=0&&elem.offsetHeight<=0;};jQuery.expr.filters.visible=function(elem){return!jQuery.expr.filters.hidden(elem);};var r20=/%20/g,rbracket=/\[\]$/,rCRLF=/\r?\n/g,rsubmitterTypes=/^(?:submit|button|image|reset|file)$/i,rsubmittable=/^(?:input|select|textarea|keygen)/i;function buildParams(prefix,obj,traditional,add){var name;if(jQuery.isArray(obj)){jQuery.each(obj,function(i,v){if(traditional||rbracket.test(prefix)){add(prefix,v);}else{buildParams(prefix+"["+(typeof v==="object"?i:"")+"]",v,traditional,add);}});}else if(!traditional&&jQuery.type(obj)==="object"){for(name in obj){buildParams(prefix+"["+name+"]",obj[name],traditional,add);}}else{add(prefix,obj);}}

jQuery.param=function(a,traditional){var prefix,s=[],add=function(key,value){ value=jQuery.isFunction(value)?value():(value==null?"":value);s[s.length]=encodeURIComponent(key)+"="+encodeURIComponent(value);};if(traditional===undefined){traditional=jQuery.ajaxSettings&&jQuery.ajaxSettings.traditional;}
if(jQuery.isArray(a)||(a.jquery&&!jQuery.isPlainObject(a))){ jQuery.each(a,function(){add(this.name,this.value);});}else{
for(prefix in a){buildParams(prefix,a[prefix],traditional,add);}} 
return s.join("&").replace(r20,"+");};jQuery.fn.extend({serialize:function(){return jQuery.param(this.serializeArray());},serializeArray:function(){return this.map(function(){ var elements=jQuery.prop(this,"elements");return elements?jQuery.makeArray(elements):this;}).filter(function(){var type=this.type; return this.name&&!jQuery(this).is(":disabled")&&rsubmittable.test(this.nodeName)&&!rsubmitterTypes.test(type)&&(this.checked||!rcheckableType.test(type));}).map(function(i,elem){var val=jQuery(this).val();return val==null?null:jQuery.isArray(val)?jQuery.map(val,function(val){return{name:elem.name,value:val.replace(rCRLF,"\r\n")};}):{name:elem.name,value:val.replace(rCRLF,"\r\n")};}).get();}});jQuery.ajaxSettings.xhr=function(){try{return new XMLHttpRequest();}catch(e){}};var xhrId=0,xhrCallbacks={},xhrSuccessStatus={ 0:200,
 1223:204},xhrSupported=jQuery.ajaxSettings.xhr();

if(window.attachEvent){window.attachEvent("onunload",function(){for(var key in xhrCallbacks){xhrCallbacks[key]();}});}
support.cors=!!xhrSupported&&("withCredentials"in xhrSupported);support.ajax=xhrSupported=!!xhrSupported;jQuery.ajaxTransport(function(options){var callback; if(support.cors||xhrSupported&&!options.crossDomain){return{send:function(headers,complete){var i,xhr=options.xhr(),id=++xhrId;xhr.open(options.type,options.url,options.async,options.username,options.password); if(options.xhrFields){for(i in options.xhrFields){xhr[i]=options.xhrFields[i];}} 
if(options.mimeType&&xhr.overrideMimeType){xhr.overrideMimeType(options.mimeType);}


if(!options.crossDomain&&!headers["X-Requested-With"]){headers["X-Requested-With"]="XMLHttpRequest";} 
for(i in headers){xhr.setRequestHeader(i,headers[i]);} 
callback=function(type){return function(){if(callback){delete xhrCallbacks[id];callback=xhr.onload=xhr.onerror=null;if(type==="abort"){xhr.abort();}else if(type==="error"){complete( xhr.status,xhr.statusText);}else{complete(xhrSuccessStatus[xhr.status]||xhr.status,xhr.statusText,

typeof xhr.responseText==="string"?{text:xhr.responseText}:undefined,xhr.getAllResponseHeaders());}}};}; xhr.onload=callback();xhr.onerror=callback("error"); callback=xhrCallbacks[id]=callback("abort");try{xhr.send(options.hasContent&&options.data||null);}catch(e){ if(callback){throw e;}}},abort:function(){if(callback){callback();}}};}});jQuery.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},contents:{script:/(?:java|ecma)script/},converters:{"text script":function(text){jQuery.globalEval(text);return text;}}});jQuery.ajaxPrefilter("script",function(s){if(s.cache===undefined){s.cache=false;}
if(s.crossDomain){s.type="GET";}});jQuery.ajaxTransport("script",function(s){ if(s.crossDomain){var script,callback;return{send:function(_,complete){script=jQuery("<script>").prop({async:true,charset:s.scriptCharset,src:s.url}).on("load error",callback=function(evt){script.remove();callback=null;if(evt){complete(evt.type==="error"?404:200,evt.type);}});document.head.appendChild(script[0]);},abort:function(){if(callback){callback();}}};}});var oldCallbacks=[],rjsonp=/(=)\?(?=&|$)|\?\?/;jQuery.ajaxSetup({jsonp:"callback",jsonpCallback:function(){var callback=oldCallbacks.pop()||(jQuery.expando+"_"+(nonce++));this[callback]=true;return callback;}});jQuery.ajaxPrefilter("json jsonp",function(s,originalSettings,jqXHR){var callbackName,overwritten,responseContainer,jsonProp=s.jsonp!==false&&(rjsonp.test(s.url)?"url":typeof s.data==="string"&&!(s.contentType||"").indexOf("application/x-www-form-urlencoded")&&rjsonp.test(s.data)&&"data"); if(jsonProp||s.dataTypes[0]==="jsonp"){ callbackName=s.jsonpCallback=jQuery.isFunction(s.jsonpCallback)?s.jsonpCallback():s.jsonpCallback; if(jsonProp){s[jsonProp]=s[jsonProp].replace(rjsonp,"$1"+callbackName);}else if(s.jsonp!==false){s.url+=(rquery.test(s.url)?"&":"?")+s.jsonp+"="+callbackName;} 
s.converters["script json"]=function(){if(!responseContainer){jQuery.error(callbackName+" was not called");}
return responseContainer[0];}; s.dataTypes[0]="json"; overwritten=window[callbackName];window[callbackName]=function(){responseContainer=arguments;};jqXHR.always(function(){ window[callbackName]=overwritten; if(s[callbackName]){ s.jsonpCallback=originalSettings.jsonpCallback; oldCallbacks.push(callbackName);} 
if(responseContainer&&jQuery.isFunction(overwritten)){overwritten(responseContainer[0]);}
responseContainer=overwritten=undefined;}); return"script";}});

jQuery.parseHTML=function(data,context,keepScripts){if(!data||typeof data!=="string"){return null;}
if(typeof context==="boolean"){keepScripts=context;context=false;}
context=context||document;var parsed=rsingleTag.exec(data),scripts=!keepScripts&&[]; if(parsed){return[context.createElement(parsed[1])];}
parsed=jQuery.buildFragment([data],context,scripts);if(scripts&&scripts.length){jQuery(scripts).remove();}
return jQuery.merge([],parsed.childNodes);};var _load=jQuery.fn.load;jQuery.fn.load=function(url,params,callback){if(typeof url!=="string"&&_load){return _load.apply(this,arguments);}
var selector,type,response,self=this,off=url.indexOf(" ");if(off>=0){selector=jQuery.trim(url.slice(off));url=url.slice(0,off);} 
if(jQuery.isFunction(params)){ callback=params;params=undefined;}else if(params&&typeof params==="object"){type="POST";} 
if(self.length>0){jQuery.ajax({url:url, type:type,dataType:"html",data:params}).done(function(responseText){ response=arguments;self.html(selector?
 jQuery("<div>").append(jQuery.parseHTML(responseText)).find(selector): responseText);}).complete(callback&&function(jqXHR,status){self.each(callback,response||[jqXHR.responseText,status,jqXHR]);});}
return this;};jQuery.each(["ajaxStart","ajaxStop","ajaxComplete","ajaxError","ajaxSuccess","ajaxSend"],function(i,type){jQuery.fn[type]=function(fn){return this.on(type,fn);};});jQuery.expr.filters.animated=function(elem){return jQuery.grep(jQuery.timers,function(fn){return elem===fn.elem;}).length;};var docElem=window.document.documentElement;function getWindow(elem){return jQuery.isWindow(elem)?elem:elem.nodeType===9&&elem.defaultView;}
jQuery.offset={setOffset:function(elem,options,i){var curPosition,curLeft,curCSSTop,curTop,curOffset,curCSSLeft,calculatePosition,position=jQuery.css(elem,"position"),curElem=jQuery(elem),props={}; if(position==="static"){elem.style.position="relative";}
curOffset=curElem.offset();curCSSTop=jQuery.css(elem,"top");curCSSLeft=jQuery.css(elem,"left");calculatePosition=(position==="absolute"||position==="fixed")&&(curCSSTop+curCSSLeft).indexOf("auto")>-1;
 if(calculatePosition){curPosition=curElem.position();curTop=curPosition.top;curLeft=curPosition.left;}else{curTop=parseFloat(curCSSTop)||0;curLeft=parseFloat(curCSSLeft)||0;}
if(jQuery.isFunction(options)){options=options.call(elem,i,curOffset);}
if(options.top!=null){props.top=(options.top-curOffset.top)+curTop;}
if(options.left!=null){props.left=(options.left-curOffset.left)+curLeft;}
if("using"in options){options.using.call(elem,props);}else{curElem.css(props);}}};jQuery.fn.extend({offset:function(options){if(arguments.length){return options===undefined?this:this.each(function(i){jQuery.offset.setOffset(this,options,i);});}
var docElem,win,elem=this[0],box={top:0,left:0},doc=elem&&elem.ownerDocument;if(!doc){return;}
docElem=doc.documentElement; if(!jQuery.contains(docElem,elem)){return box;}  
if(typeof elem.getBoundingClientRect!==strundefined){box=elem.getBoundingClientRect();}
win=getWindow(doc);return{top:box.top+win.pageYOffset-docElem.clientTop,left:box.left+win.pageXOffset-docElem.clientLeft};},position:function(){if(!this[0]){return;}
var offsetParent,offset,elem=this[0],parentOffset={top:0,left:0}; if(jQuery.css(elem,"position")==="fixed"){ offset=elem.getBoundingClientRect();}else{ offsetParent=this.offsetParent(); offset=this.offset();if(!jQuery.nodeName(offsetParent[0],"html")){parentOffset=offsetParent.offset();} 
parentOffset.top+=jQuery.css(offsetParent[0],"borderTopWidth",true);parentOffset.left+=jQuery.css(offsetParent[0],"borderLeftWidth",true);} 
return{top:offset.top-parentOffset.top-jQuery.css(elem,"marginTop",true),left:offset.left-parentOffset.left-jQuery.css(elem,"marginLeft",true)};},offsetParent:function(){return this.map(function(){var offsetParent=this.offsetParent||docElem;while(offsetParent&&(!jQuery.nodeName(offsetParent,"html")&&jQuery.css(offsetParent,"position")==="static")){offsetParent=offsetParent.offsetParent;}
return offsetParent||docElem;});}});jQuery.each({scrollLeft:"pageXOffset",scrollTop:"pageYOffset"},function(method,prop){var top="pageYOffset"===prop;jQuery.fn[method]=function(val){return access(this,function(elem,method,val){var win=getWindow(elem);if(val===undefined){return win?win[prop]:elem[method];}
if(win){win.scrollTo(!top?val:window.pageXOffset,top?val:window.pageYOffset);}else{elem[method]=val;}},method,val,arguments.length,null);};});


jQuery.each(["top","left"],function(i,prop){jQuery.cssHooks[prop]=addGetHookIf(support.pixelPosition,function(elem,computed){if(computed){computed=curCSS(elem,prop); return rnumnonpx.test(computed)?jQuery(elem).position()[prop]+"px":computed;}});});jQuery.each({Height:"height",Width:"width"},function(name,type){jQuery.each({padding:"inner"+name,content:type,"":"outer"+name},function(defaultExtra,funcName){ jQuery.fn[funcName]=function(margin,value){var chainable=arguments.length&&(defaultExtra||typeof margin!=="boolean"),extra=defaultExtra||(margin===true||value===true?"margin":"border");return access(this,function(elem,type,value){var doc;if(jQuery.isWindow(elem)){
 return elem.document.documentElement["client"+name];} 
if(elem.nodeType===9){doc=elem.documentElement; return Math.max(elem.body["scroll"+name],doc["scroll"+name],elem.body["offset"+name],doc["offset"+name],doc["client"+name]);}
return value===undefined? jQuery.css(elem,type,extra): jQuery.style(elem,type,value,extra);},type,chainable?margin:undefined,chainable,null);};});});jQuery.fn.size=function(){return this.length;};jQuery.fn.andSelf=jQuery.fn.addBack;








if(typeof define==="function"&&define.amd){define("jquery",[],function(){return jQuery;});}
var
 
_jQuery=window.jQuery, _$=window.$;jQuery.noConflict=function(deep){if(window.$===jQuery){window.$=_$;}
if(deep&&window.jQuery===jQuery){window.jQuery=_jQuery;}
return jQuery;};

if(typeof noGlobal===strundefined){window.jQuery=window.$=jQuery;}
return jQuery;}));if("undefined"==typeof jQuery)throw new Error("Bootstrap's JavaScript requires jQuery");+function(a){"use strict";var b=a.fn.jquery.split(" ")[0].split(".");if(b[0]<2&&b[1]<9||1==b[0]&&9==b[1]&&b[2]<1)throw new Error("Bootstrap's JavaScript requires jQuery version 1.9.1 or higher")}(jQuery),+function(a){"use strict";function b(){var a=document.createElement("bootstrap"),b={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd otransitionend",transition:"transitionend"};for(var c in b)if(void 0!==a.style[c])return{end:b[c]};return!1}a.fn.emulateTransitionEnd=function(b){var c=!1,d=this;a(this).one("bsTransitionEnd",function(){c=!0});var e=function(){c||a(d).trigger(a.support.transition.end)};return setTimeout(e,b),this},a(function(){a.support.transition=b(),a.support.transition&&(a.event.special.bsTransitionEnd={bindType:a.support.transition.end,delegateType:a.support.transition.end,handle:function(b){return a(b.target).is(this)?b.handleObj.handler.apply(this,arguments):void 0}})})}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var c=a(this),e=c.data("bs.alert");e||c.data("bs.alert",e=new d(this)),"string"==typeof b&&e[b].call(c)})}var c='[data-dismiss="alert"]',d=function(b){a(b).on("click",c,this.close)};d.VERSION="3.3.4",d.TRANSITION_DURATION=150,d.prototype.close=function(b){function c(){g.detach().trigger("closed.bs.alert").remove()}var e=a(this),f=e.attr("data-target");f||(f=e.attr("href"),f=f&&f.replace(/.*(?=#[^\s]*$)/,""));var g=a(f);b&&b.preventDefault(),g.length||(g=e.closest(".alert")),g.trigger(b=a.Event("close.bs.alert")),b.isDefaultPrevented()||(g.removeClass("in"),a.support.transition&&g.hasClass("fade")?g.one("bsTransitionEnd",c).emulateTransitionEnd(d.TRANSITION_DURATION):c())};var e=a.fn.alert;a.fn.alert=b,a.fn.alert.Constructor=d,a.fn.alert.noConflict=function(){return a.fn.alert=e,this},a(document).on("click.bs.alert.data-api",c,d.prototype.close)}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var d=a(this),e=d.data("bs.button"),f="object"==typeof b&&b;e||d.data("bs.button",e=new c(this,f)),"toggle"==b?e.toggle():b&&e.setState(b)})}var c=function(b,d){this.$element=a(b),this.options=a.extend({},c.DEFAULTS,d),this.isLoading=!1};c.VERSION="3.3.4",c.DEFAULTS={loadingText:"loading..."},c.prototype.setState=function(b){var c="disabled",d=this.$element,e=d.is("input")?"val":"html",f=d.data();b+="Text",null==f.resetText&&d.data("resetText",d[e]()),setTimeout(a.proxy(function(){d[e](null==f[b]?this.options[b]:f[b]),"loadingText"==b?(this.isLoading=!0,d.addClass(c).attr(c,c)):this.isLoading&&(this.isLoading=!1,d.removeClass(c).removeAttr(c))},this),0)},c.prototype.toggle=function(){var a=!0,b=this.$element.closest('[data-toggle="buttons"]');if(b.length){var c=this.$element.find("input");"radio"==c.prop("type")&&(c.prop("checked")&&this.$element.hasClass("active")?a=!1:b.find(".active").removeClass("active")),a&&c.prop("checked",!this.$element.hasClass("active")).trigger("change")}else this.$element.attr("aria-pressed",!this.$element.hasClass("active"));a&&this.$element.toggleClass("active")};var d=a.fn.button;a.fn.button=b,a.fn.button.Constructor=c,a.fn.button.noConflict=function(){return a.fn.button=d,this},a(document).on("click.bs.button.data-api",'[data-toggle^="button"]',function(c){var d=a(c.target);d.hasClass("btn")||(d=d.closest(".btn")),b.call(d,"toggle"),c.preventDefault()}).on("focus.bs.button.data-api blur.bs.button.data-api",'[data-toggle^="button"]',function(b){a(b.target).closest(".btn").toggleClass("focus",/^focus(in)?$/.test(b.type))})}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var d=a(this),e=d.data("bs.carousel"),f=a.extend({},c.DEFAULTS,d.data(),"object"==typeof b&&b),g="string"==typeof b?b:f.slide;e||d.data("bs.carousel",e=new c(this,f)),"number"==typeof b?e.to(b):g?e[g]():f.interval&&e.pause().cycle()})}var c=function(b,c){this.$element=a(b),this.$indicators=this.$element.find(".carousel-indicators"),this.options=c,this.paused=null,this.sliding=null,this.interval=null,this.$active=null,this.$items=null,this.options.keyboard&&this.$element.on("keydown.bs.carousel",a.proxy(this.keydown,this)),"hover"==this.options.pause&&!("ontouchstart"in document.documentElement)&&this.$element.on("mouseenter.bs.carousel",a.proxy(this.pause,this)).on("mouseleave.bs.carousel",a.proxy(this.cycle,this))};c.VERSION="3.3.4",c.TRANSITION_DURATION=600,c.DEFAULTS={interval:5e3,pause:"hover",wrap:!0,keyboard:!0},c.prototype.keydown=function(a){if(!/input|textarea/i.test(a.target.tagName)){switch(a.which){case 37:this.prev();break;case 39:this.next();break;default:return}a.preventDefault()}},c.prototype.cycle=function(b){return b||(this.paused=!1),this.interval&&clearInterval(this.interval),this.options.interval&&!this.paused&&(this.interval=setInterval(a.proxy(this.next,this),this.options.interval)),this},c.prototype.getItemIndex=function(a){return this.$items=a.parent().children(".item"),this.$items.index(a||this.$active)},c.prototype.getItemForDirection=function(a,b){var c=this.getItemIndex(b),d="prev"==a&&0===c||"next"==a&&c==this.$items.length-1;if(d&&!this.options.wrap)return b;var e="prev"==a?-1:1,f=(c+e)%this.$items.length;return this.$items.eq(f)},c.prototype.to=function(a){var b=this,c=this.getItemIndex(this.$active=this.$element.find(".item.active"));return a>this.$items.length-1||0>a?void 0:this.sliding?this.$element.one("slid.bs.carousel",function(){b.to(a)}):c==a?this.pause().cycle():this.slide(a>c?"next":"prev",this.$items.eq(a))},c.prototype.pause=function(b){return b||(this.paused=!0),this.$element.find(".next, .prev").length&&a.support.transition&&(this.$element.trigger(a.support.transition.end),this.cycle(!0)),this.interval=clearInterval(this.interval),this},c.prototype.next=function(){return this.sliding?void 0:this.slide("next")},c.prototype.prev=function(){return this.sliding?void 0:this.slide("prev")},c.prototype.slide=function(b,d){var e=this.$element.find(".item.active"),f=d||this.getItemForDirection(b,e),g=this.interval,h="next"==b?"left":"right",i=this;if(f.hasClass("active"))return this.sliding=!1;var j=f[0],k=a.Event("slide.bs.carousel",{relatedTarget:j,direction:h});if(this.$element.trigger(k),!k.isDefaultPrevented()){if(this.sliding=!0,g&&this.pause(),this.$indicators.length){this.$indicators.find(".active").removeClass("active");var l=a(this.$indicators.children()[this.getItemIndex(f)]);l&&l.addClass("active")}var m=a.Event("slid.bs.carousel",{relatedTarget:j,direction:h});return a.support.transition&&this.$element.hasClass("slide")?(f.addClass(b),f[0].offsetWidth,e.addClass(h),f.addClass(h),e.one("bsTransitionEnd",function(){f.removeClass([b,h].join(" ")).addClass("active"),e.removeClass(["active",h].join(" ")),i.sliding=!1,setTimeout(function(){i.$element.trigger(m)},0)}).emulateTransitionEnd(c.TRANSITION_DURATION)):(e.removeClass("active"),f.addClass("active"),this.sliding=!1,this.$element.trigger(m)),g&&this.cycle(),this}};var d=a.fn.carousel;a.fn.carousel=b,a.fn.carousel.Constructor=c,a.fn.carousel.noConflict=function(){return a.fn.carousel=d,this};var e=function(c){var d,e=a(this),f=a(e.attr("data-target")||(d=e.attr("href"))&&d.replace(/.*(?=#[^\s]+$)/,""));if(f.hasClass("carousel")){var g=a.extend({},f.data(),e.data()),h=e.attr("data-slide-to");h&&(g.interval=!1),b.call(f,g),h&&f.data("bs.carousel").to(h),c.preventDefault()}};a(document).on("click.bs.carousel.data-api","[data-slide]",e).on("click.bs.carousel.data-api","[data-slide-to]",e),a(window).on("load",function(){a('[data-ride="carousel"]').each(function(){var c=a(this);b.call(c,c.data())})})}(jQuery),+function(a){"use strict";function b(b){var c,d=b.attr("data-target")||(c=b.attr("href"))&&c.replace(/.*(?=#[^\s]+$)/,"");return a(d)}function c(b){return this.each(function(){var c=a(this),e=c.data("bs.collapse"),f=a.extend({},d.DEFAULTS,c.data(),"object"==typeof b&&b);!e&&f.toggle&&/show|hide/.test(b)&&(f.toggle=!1),e||c.data("bs.collapse",e=new d(this,f)),"string"==typeof b&&e[b]()})}var d=function(b,c){this.$element=a(b),this.options=a.extend({},d.DEFAULTS,c),this.$trigger=a('[data-toggle="collapse"][href="#'+b.id+'"],[data-toggle="collapse"][data-target="#'+b.id+'"]'),this.transitioning=null,this.options.parent?this.$parent=this.getParent():this.addAriaAndCollapsedClass(this.$element,this.$trigger),this.options.toggle&&this.toggle()};d.VERSION="3.3.4",d.TRANSITION_DURATION=350,d.DEFAULTS={toggle:!0},d.prototype.dimension=function(){var a=this.$element.hasClass("width");return a?"width":"height"},d.prototype.show=function(){if(!this.transitioning&&!this.$element.hasClass("in")){var b,e=this.$parent&&this.$parent.children(".panel").children(".in, .collapsing");if(!(e&&e.length&&(b=e.data("bs.collapse"),b&&b.transitioning))){var f=a.Event("show.bs.collapse");if(this.$element.trigger(f),!f.isDefaultPrevented()){e&&e.length&&(c.call(e,"hide"),b||e.data("bs.collapse",null));var g=this.dimension();this.$element.removeClass("collapse").addClass("collapsing")[g](0).attr("aria-expanded",!0),this.$trigger.removeClass("collapsed").attr("aria-expanded",!0),this.transitioning=1;var h=function(){this.$element.removeClass("collapsing").addClass("collapse in")[g](""),this.transitioning=0,this.$element.trigger("shown.bs.collapse")};if(!a.support.transition)return h.call(this);var i=a.camelCase(["scroll",g].join("-"));this.$element.one("bsTransitionEnd",a.proxy(h,this)).emulateTransitionEnd(d.TRANSITION_DURATION)[g](this.$element[0][i])}}}},d.prototype.hide=function(){if(!this.transitioning&&this.$element.hasClass("in")){var b=a.Event("hide.bs.collapse");if(this.$element.trigger(b),!b.isDefaultPrevented()){var c=this.dimension();this.$element[c](this.$element[c]())[0].offsetHeight,this.$element.addClass("collapsing").removeClass("collapse in").attr("aria-expanded",!1),this.$trigger.addClass("collapsed").attr("aria-expanded",!1),this.transitioning=1;var e=function(){this.transitioning=0,this.$element.removeClass("collapsing").addClass("collapse").trigger("hidden.bs.collapse")};return a.support.transition?void this.$element[c](0).one("bsTransitionEnd",a.proxy(e,this)).emulateTransitionEnd(d.TRANSITION_DURATION):e.call(this)}}},d.prototype.toggle=function(){this[this.$element.hasClass("in")?"hide":"show"]()},d.prototype.getParent=function(){return a(this.options.parent).find('[data-toggle="collapse"][data-parent="'+this.options.parent+'"]').each(a.proxy(function(c,d){var e=a(d);this.addAriaAndCollapsedClass(b(e),e)},this)).end()},d.prototype.addAriaAndCollapsedClass=function(a,b){var c=a.hasClass("in");a.attr("aria-expanded",c),b.toggleClass("collapsed",!c).attr("aria-expanded",c)};var e=a.fn.collapse;a.fn.collapse=c,a.fn.collapse.Constructor=d,a.fn.collapse.noConflict=function(){return a.fn.collapse=e,this},a(document).on("click.bs.collapse.data-api",'[data-toggle="collapse"]',function(d){var e=a(this);e.attr("data-target")||d.preventDefault();var f=b(e),g=f.data("bs.collapse"),h=g?"toggle":e.data();c.call(f,h)})}(jQuery),+function(a){"use strict";function b(b){b&&3===b.which||(a(e).remove(),a(f).each(function(){var d=a(this),e=c(d),f={relatedTarget:this};e.hasClass("open")&&(e.trigger(b=a.Event("hide.bs.dropdown",f)),b.isDefaultPrevented()||(d.attr("aria-expanded","false"),e.removeClass("open").trigger("hidden.bs.dropdown",f)))}))}function c(b){var c=b.attr("data-target");c||(c=b.attr("href"),c=c&&/#[A-Za-z]/.test(c)&&c.replace(/.*(?=#[^\s]*$)/,""));var d=c&&a(c);return d&&d.length?d:b.parent()}function d(b){return this.each(function(){var c=a(this),d=c.data("bs.dropdown");d||c.data("bs.dropdown",d=new g(this)),"string"==typeof b&&d[b].call(c)})}var e=".dropdown-backdrop",f='[data-toggle="dropdown"]',g=function(b){a(b).on("click.bs.dropdown",this.toggle)};g.VERSION="3.3.4",g.prototype.toggle=function(d){var e=a(this);if(!e.is(".disabled, :disabled")){var f=c(e),g=f.hasClass("open");if(b(),!g){"ontouchstart"in document.documentElement&&!f.closest(".navbar-nav").length&&a('<div class="dropdown-backdrop"/>').insertAfter(a(this)).on("click",b);var h={relatedTarget:this};if(f.trigger(d=a.Event("show.bs.dropdown",h)),d.isDefaultPrevented())return;e.trigger("focus").attr("aria-expanded","true"),f.toggleClass("open").trigger("shown.bs.dropdown",h)}return!1}},g.prototype.keydown=function(b){if(/(38|40|27|32)/.test(b.which)&&!/input|textarea/i.test(b.target.tagName)){var d=a(this);if(b.preventDefault(),b.stopPropagation(),!d.is(".disabled, :disabled")){var e=c(d),g=e.hasClass("open");if(!g&&27!=b.which||g&&27==b.which)return 27==b.which&&e.find(f).trigger("focus"),d.trigger("click");var h=" li:not(.disabled):visible a",i=e.find('[role="menu"]'+h+', [role="listbox"]'+h);if(i.length){var j=i.index(b.target);38==b.which&&j>0&&j--,40==b.which&&j<i.length-1&&j++,~j||(j=0),i.eq(j).trigger("focus")}}}};var h=a.fn.dropdown;a.fn.dropdown=d,a.fn.dropdown.Constructor=g,a.fn.dropdown.noConflict=function(){return a.fn.dropdown=h,this},a(document).on("click.bs.dropdown.data-api",b).on("click.bs.dropdown.data-api",".dropdown form",function(a){a.stopPropagation()}).on("click.bs.dropdown.data-api",f,g.prototype.toggle).on("keydown.bs.dropdown.data-api",f,g.prototype.keydown).on("keydown.bs.dropdown.data-api",'[role="menu"]',g.prototype.keydown).on("keydown.bs.dropdown.data-api",'[role="listbox"]',g.prototype.keydown)}(jQuery),+function(a){"use strict";function b(b,d){return this.each(function(){var e=a(this),f=e.data("bs.modal"),g=a.extend({},c.DEFAULTS,e.data(),"object"==typeof b&&b);f||e.data("bs.modal",f=new c(this,g)),"string"==typeof b?f[b](d):g.show&&f.show(d)})}var c=function(b,c){this.options=c,this.$body=a(document.body),this.$element=a(b),this.$dialog=this.$element.find(".modal-dialog"),this.$backdrop=null,this.isShown=null,this.originalBodyPad=null,this.scrollbarWidth=0,this.ignoreBackdropClick=!1,this.options.remote&&this.$element.find(".modal-content").load(this.options.remote,a.proxy(function(){this.$element.trigger("loaded.bs.modal")},this))};c.VERSION="3.3.4",c.TRANSITION_DURATION=300,c.BACKDROP_TRANSITION_DURATION=150,c.DEFAULTS={backdrop:!0,keyboard:!0,show:!0},c.prototype.toggle=function(a){return this.isShown?this.hide():this.show(a)},c.prototype.show=function(b){var d=this,e=a.Event("show.bs.modal",{relatedTarget:b});this.$element.trigger(e),this.isShown||e.isDefaultPrevented()||(this.isShown=!0,this.checkScrollbar(),this.setScrollbar(),this.$body.addClass("modal-open"),this.escape(),this.resize(),this.$element.on("click.dismiss.bs.modal",'[data-dismiss="modal"]',a.proxy(this.hide,this)),this.$dialog.on("mousedown.dismiss.bs.modal",function(){d.$element.one("mouseup.dismiss.bs.modal",function(b){a(b.target).is(d.$element)&&(d.ignoreBackdropClick=!0)})}),this.backdrop(function(){var e=a.support.transition&&d.$element.hasClass("fade");d.$element.parent().length||d.$element.appendTo(d.$body),d.$element.show().scrollTop(0),d.adjustDialog(),e&&d.$element[0].offsetWidth,d.$element.addClass("in").attr("aria-hidden",!1),d.enforceFocus();var f=a.Event("shown.bs.modal",{relatedTarget:b});e?d.$dialog.one("bsTransitionEnd",function(){d.$element.trigger("focus").trigger(f)}).emulateTransitionEnd(c.TRANSITION_DURATION):d.$element.trigger("focus").trigger(f)}))},c.prototype.hide=function(b){b&&b.preventDefault(),b=a.Event("hide.bs.modal"),this.$element.trigger(b),this.isShown&&!b.isDefaultPrevented()&&(this.isShown=!1,this.escape(),this.resize(),a(document).off("focusin.bs.modal"),this.$element.removeClass("in").attr("aria-hidden",!0).off("click.dismiss.bs.modal").off("mouseup.dismiss.bs.modal"),this.$dialog.off("mousedown.dismiss.bs.modal"),a.support.transition&&this.$element.hasClass("fade")?this.$element.one("bsTransitionEnd",a.proxy(this.hideModal,this)).emulateTransitionEnd(c.TRANSITION_DURATION):this.hideModal())},c.prototype.enforceFocus=function(){a(document).off("focusin.bs.modal").on("focusin.bs.modal",a.proxy(function(a){this.$element[0]===a.target||this.$element.has(a.target).length||this.$element.trigger("focus")},this))},c.prototype.escape=function(){this.isShown&&this.options.keyboard?this.$element.on("keydown.dismiss.bs.modal",a.proxy(function(a){27==a.which&&this.hide()},this)):this.isShown||this.$element.off("keydown.dismiss.bs.modal")},c.prototype.resize=function(){this.isShown?a(window).on("resize.bs.modal",a.proxy(this.handleUpdate,this)):a(window).off("resize.bs.modal")},c.prototype.hideModal=function(){var a=this;this.$element.hide(),this.backdrop(function(){a.$body.removeClass("modal-open"),a.resetAdjustments(),a.resetScrollbar(),a.$element.trigger("hidden.bs.modal")})},c.prototype.removeBackdrop=function(){this.$backdrop&&this.$backdrop.remove(),this.$backdrop=null},c.prototype.backdrop=function(b){var d=this,e=this.$element.hasClass("fade")?"fade":"";if(this.isShown&&this.options.backdrop){var f=a.support.transition&&e;if(this.$backdrop=a('<div class="modal-backdrop '+e+'" />').appendTo(this.$body),this.$element.on("click.dismiss.bs.modal",a.proxy(function(a){return this.ignoreBackdropClick?void(this.ignoreBackdropClick=!1):void(a.target===a.currentTarget&&("static"==this.options.backdrop?this.$element[0].focus():this.hide()))},this)),f&&this.$backdrop[0].offsetWidth,this.$backdrop.addClass("in"),!b)return;f?this.$backdrop.one("bsTransitionEnd",b).emulateTransitionEnd(c.BACKDROP_TRANSITION_DURATION):b()}else if(!this.isShown&&this.$backdrop){this.$backdrop.removeClass("in");var g=function(){d.removeBackdrop(),b&&b()};a.support.transition&&this.$element.hasClass("fade")?this.$backdrop.one("bsTransitionEnd",g).emulateTransitionEnd(c.BACKDROP_TRANSITION_DURATION):g()}else b&&b()},c.prototype.handleUpdate=function(){this.adjustDialog()},c.prototype.adjustDialog=function(){var a=this.$element[0].scrollHeight>document.documentElement.clientHeight;this.$element.css({paddingLeft:!this.bodyIsOverflowing&&a?this.scrollbarWidth:"",paddingRight:this.bodyIsOverflowing&&!a?this.scrollbarWidth:""})},c.prototype.resetAdjustments=function(){this.$element.css({paddingLeft:"",paddingRight:""})},c.prototype.checkScrollbar=function(){var a=window.innerWidth;if(!a){var b=document.documentElement.getBoundingClientRect();a=b.right-Math.abs(b.left)}this.bodyIsOverflowing=document.body.clientWidth<a,this.scrollbarWidth=this.measureScrollbar()},c.prototype.setScrollbar=function(){var a=parseInt(this.$body.css("padding-right")||0,10);this.originalBodyPad=document.body.style.paddingRight||"",this.bodyIsOverflowing&&this.$body.css("padding-right",a+this.scrollbarWidth)},c.prototype.resetScrollbar=function(){this.$body.css("padding-right",this.originalBodyPad)},c.prototype.measureScrollbar=function(){var a=document.createElement("div");a.className="modal-scrollbar-measure",this.$body.append(a);var b=a.offsetWidth-a.clientWidth;return this.$body[0].removeChild(a),b};var d=a.fn.modal;a.fn.modal=b,a.fn.modal.Constructor=c,a.fn.modal.noConflict=function(){return a.fn.modal=d,this},a(document).on("click.bs.modal.data-api",'[data-toggle="modal"]',function(c){var d=a(this),e=d.attr("href"),f=a(d.attr("data-target")||e&&e.replace(/.*(?=#[^\s]+$)/,"")),g=f.data("bs.modal")?"toggle":a.extend({remote:!/#/.test(e)&&e},f.data(),d.data());d.is("a")&&c.preventDefault(),f.one("show.bs.modal",function(a){a.isDefaultPrevented()||f.one("hidden.bs.modal",function(){d.is(":visible")&&d.trigger("focus")})}),b.call(f,g,this)})}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var d=a(this),e=d.data("bs.tooltip"),f="object"==typeof b&&b;(e||!/destroy|hide/.test(b))&&(e||d.data("bs.tooltip",e=new c(this,f)),"string"==typeof b&&e[b]())})}var c=function(a,b){this.type=null,this.options=null,this.enabled=null,this.timeout=null,this.hoverState=null,this.$element=null,this.init("tooltip",a,b)};c.VERSION="3.3.4",c.TRANSITION_DURATION=150,c.DEFAULTS={animation:!0,placement:"top",selector:!1,template:'<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',trigger:"hover focus",title:"",delay:0,html:!1,container:!1,viewport:{selector:"body",padding:0}},c.prototype.init=function(b,c,d){if(this.enabled=!0,this.type=b,this.$element=a(c),this.options=this.getOptions(d),this.$viewport=this.options.viewport&&a(this.options.viewport.selector||this.options.viewport),this.$element[0]instanceof document.constructor&&!this.options.selector)throw new Error("`selector` option must be specified when initializing "+this.type+" on the window.document object!");for(var e=this.options.trigger.split(" "),f=e.length;f--;){var g=e[f];if("click"==g)this.$element.on("click."+this.type,this.options.selector,a.proxy(this.toggle,this));else if("manual"!=g){var h="hover"==g?"mouseenter":"focusin",i="hover"==g?"mouseleave":"focusout";this.$element.on(h+"."+this.type,this.options.selector,a.proxy(this.enter,this)),this.$element.on(i+"."+this.type,this.options.selector,a.proxy(this.leave,this))}}this.options.selector?this._options=a.extend({},this.options,{trigger:"manual",selector:""}):this.fixTitle()},c.prototype.getDefaults=function(){return c.DEFAULTS},c.prototype.getOptions=function(b){return b=a.extend({},this.getDefaults(),this.$element.data(),b),b.delay&&"number"==typeof b.delay&&(b.delay={show:b.delay,hide:b.delay}),b},c.prototype.getDelegateOptions=function(){var b={},c=this.getDefaults();return this._options&&a.each(this._options,function(a,d){c[a]!=d&&(b[a]=d)}),b},c.prototype.enter=function(b){var c=b instanceof this.constructor?b:a(b.currentTarget).data("bs."+this.type);return c&&c.$tip&&c.$tip.is(":visible")?void(c.hoverState="in"):(c||(c=new this.constructor(b.currentTarget,this.getDelegateOptions()),a(b.currentTarget).data("bs."+this.type,c)),clearTimeout(c.timeout),c.hoverState="in",c.options.delay&&c.options.delay.show?void(c.timeout=setTimeout(function(){"in"==c.hoverState&&c.show()},c.options.delay.show)):c.show())},c.prototype.leave=function(b){var c=b instanceof this.constructor?b:a(b.currentTarget).data("bs."+this.type);return c||(c=new this.constructor(b.currentTarget,this.getDelegateOptions()),a(b.currentTarget).data("bs."+this.type,c)),clearTimeout(c.timeout),c.hoverState="out",c.options.delay&&c.options.delay.hide?void(c.timeout=setTimeout(function(){"out"==c.hoverState&&c.hide()},c.options.delay.hide)):c.hide()},c.prototype.show=function(){var b=a.Event("show.bs."+this.type);if(this.hasContent()&&this.enabled){this.$element.trigger(b);var d=a.contains(this.$element[0].ownerDocument.documentElement,this.$element[0]);if(b.isDefaultPrevented()||!d)return;var e=this,f=this.tip(),g=this.getUID(this.type);this.setContent(),f.attr("id",g),this.$element.attr("aria-describedby",g),this.options.animation&&f.addClass("fade");var h="function"==typeof this.options.placement?this.options.placement.call(this,f[0],this.$element[0]):this.options.placement,i=/\s?auto?\s?/i,j=i.test(h);j&&(h=h.replace(i,"")||"top"),f.detach().css({top:0,left:0,display:"block"}).addClass(h).data("bs."+this.type,this),this.options.container?f.appendTo(this.options.container):f.insertAfter(this.$element);var k=this.getPosition(),l=f[0].offsetWidth,m=f[0].offsetHeight;if(j){var n=h,o=this.options.container?a(this.options.container):this.$element.parent(),p=this.getPosition(o);h="bottom"==h&&k.bottom+m>p.bottom?"top":"top"==h&&k.top-m<p.top?"bottom":"right"==h&&k.right+l>p.width?"left":"left"==h&&k.left-l<p.left?"right":h,f.removeClass(n).addClass(h)}var q=this.getCalculatedOffset(h,k,l,m);this.applyPlacement(q,h);var r=function(){var a=e.hoverState;e.$element.trigger("shown.bs."+e.type),e.hoverState=null,"out"==a&&e.leave(e)};a.support.transition&&this.$tip.hasClass("fade")?f.one("bsTransitionEnd",r).emulateTransitionEnd(c.TRANSITION_DURATION):r()}},c.prototype.applyPlacement=function(b,c){var d=this.tip(),e=d[0].offsetWidth,f=d[0].offsetHeight,g=parseInt(d.css("margin-top"),10),h=parseInt(d.css("margin-left"),10);isNaN(g)&&(g=0),isNaN(h)&&(h=0),b.top=b.top+g,b.left=b.left+h,a.offset.setOffset(d[0],a.extend({using:function(a){d.css({top:Math.round(a.top),left:Math.round(a.left)})}},b),0),d.addClass("in");var i=d[0].offsetWidth,j=d[0].offsetHeight;"top"==c&&j!=f&&(b.top=b.top+f-j);var k=this.getViewportAdjustedDelta(c,b,i,j);k.left?b.left+=k.left:b.top+=k.top;var l=/top|bottom/.test(c),m=l?2*k.left-e+i:2*k.top-f+j,n=l?"offsetWidth":"offsetHeight";d.offset(b),this.replaceArrow(m,d[0][n],l)},c.prototype.replaceArrow=function(a,b,c){this.arrow().css(c?"left":"top",50*(1-a/b)+"%").css(c?"top":"left","")},c.prototype.setContent=function(){var a=this.tip(),b=this.getTitle();a.find(".tooltip-inner")[this.options.html?"html":"text"](b),a.removeClass("fade in top bottom left right")},c.prototype.hide=function(b){function d(){"in"!=e.hoverState&&f.detach(),e.$element.removeAttr("aria-describedby").trigger("hidden.bs."+e.type),b&&b()}var e=this,f=a(this.$tip),g=a.Event("hide.bs."+this.type);return this.$element.trigger(g),g.isDefaultPrevented()?void 0:(f.removeClass("in"),a.support.transition&&f.hasClass("fade")?f.one("bsTransitionEnd",d).emulateTransitionEnd(c.TRANSITION_DURATION):d(),this.hoverState=null,this)},c.prototype.fixTitle=function(){var a=this.$element;(a.attr("title")||"string"!=typeof a.attr("data-original-title"))&&a.attr("data-original-title",a.attr("title")||"").attr("title","")},c.prototype.hasContent=function(){return this.getTitle()},c.prototype.getPosition=function(b){b=b||this.$element;var c=b[0],d="BODY"==c.tagName,e=c.getBoundingClientRect();null==e.width&&(e=a.extend({},e,{width:e.right-e.left,height:e.bottom-e.top}));var f=d?{top:0,left:0}:b.offset(),g={scroll:d?document.documentElement.scrollTop||document.body.scrollTop:b.scrollTop()},h=d?{width:a(window).width(),height:a(window).height()}:null;return a.extend({},e,g,h,f)},c.prototype.getCalculatedOffset=function(a,b,c,d){return"bottom"==a?{top:b.top+b.height,left:b.left+b.width/2-c/2}:"top"==a?{top:b.top-d,left:b.left+b.width/2-c/2}:"left"==a?{top:b.top+b.height/2-d/2,left:b.left-c}:{top:b.top+b.height/2-d/2,left:b.left+b.width}},c.prototype.getViewportAdjustedDelta=function(a,b,c,d){var e={top:0,left:0};if(!this.$viewport)return e;var f=this.options.viewport&&this.options.viewport.padding||0,g=this.getPosition(this.$viewport);if(/right|left/.test(a)){var h=b.top-f-g.scroll,i=b.top+f-g.scroll+d;h<g.top?e.top=g.top-h:i>g.top+g.height&&(e.top=g.top+g.height-i)}else{var j=b.left-f,k=b.left+f+c;j<g.left?e.left=g.left-j:k>g.width&&(e.left=g.left+g.width-k)}return e},c.prototype.getTitle=function(){var a,b=this.$element,c=this.options;return a=b.attr("data-original-title")||("function"==typeof c.title?c.title.call(b[0]):c.title)},c.prototype.getUID=function(a){do a+=~~(1e6*Math.random());while(document.getElementById(a));return a},c.prototype.tip=function(){return this.$tip=this.$tip||a(this.options.template)},c.prototype.arrow=function(){return this.$arrow=this.$arrow||this.tip().find(".tooltip-arrow")},c.prototype.enable=function(){this.enabled=!0},c.prototype.disable=function(){this.enabled=!1},c.prototype.toggleEnabled=function(){this.enabled=!this.enabled},c.prototype.toggle=function(b){var c=this;b&&(c=a(b.currentTarget).data("bs."+this.type),c||(c=new this.constructor(b.currentTarget,this.getDelegateOptions()),a(b.currentTarget).data("bs."+this.type,c))),c.tip().hasClass("in")?c.leave(c):c.enter(c)},c.prototype.destroy=function(){var a=this;clearTimeout(this.timeout),this.hide(function(){a.$element.off("."+a.type).removeData("bs."+a.type)})};var d=a.fn.tooltip;a.fn.tooltip=b,a.fn.tooltip.Constructor=c,a.fn.tooltip.noConflict=function(){return a.fn.tooltip=d,this}}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var d=a(this),e=d.data("bs.popover"),f="object"==typeof b&&b;(e||!/destroy|hide/.test(b))&&(e||d.data("bs.popover",e=new c(this,f)),"string"==typeof b&&e[b]())})}var c=function(a,b){this.init("popover",a,b)};if(!a.fn.tooltip)throw new Error("Popover requires tooltip.js");c.VERSION="3.3.4",c.DEFAULTS=a.extend({},a.fn.tooltip.Constructor.DEFAULTS,{placement:"right",trigger:"click",content:"",template:'<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'}),c.prototype=a.extend({},a.fn.tooltip.Constructor.prototype),c.prototype.constructor=c,c.prototype.getDefaults=function(){return c.DEFAULTS},c.prototype.setContent=function(){var a=this.tip(),b=this.getTitle(),c=this.getContent();a.find(".popover-title")[this.options.html?"html":"text"](b),a.find(".popover-content").children().detach().end()[this.options.html?"string"==typeof c?"html":"append":"text"](c),a.removeClass("fade top bottom left right in"),a.find(".popover-title").html()||a.find(".popover-title").hide()},c.prototype.hasContent=function(){return this.getTitle()||this.getContent()},c.prototype.getContent=function(){var a=this.$element,b=this.options;return a.attr("data-content")||("function"==typeof b.content?b.content.call(a[0]):b.content)},c.prototype.arrow=function(){return this.$arrow=this.$arrow||this.tip().find(".arrow")};var d=a.fn.popover;a.fn.popover=b,a.fn.popover.Constructor=c,a.fn.popover.noConflict=function(){return a.fn.popover=d,this}}(jQuery),+function(a){"use strict";function b(c,d){this.$body=a(document.body),this.$scrollElement=a(a(c).is(document.body)?window:c),this.options=a.extend({},b.DEFAULTS,d),this.selector=(this.options.target||"")+" .nav li > a",this.offsets=[],this.targets=[],this.activeTarget=null,this.scrollHeight=0,this.$scrollElement.on("scroll.bs.scrollspy",a.proxy(this.process,this)),this.refresh(),this.process()}function c(c){return this.each(function(){var d=a(this),e=d.data("bs.scrollspy"),f="object"==typeof c&&c;e||d.data("bs.scrollspy",e=new b(this,f)),"string"==typeof c&&e[c]()})}b.VERSION="3.3.4",b.DEFAULTS={offset:10},b.prototype.getScrollHeight=function(){return this.$scrollElement[0].scrollHeight||Math.max(this.$body[0].scrollHeight,document.documentElement.scrollHeight)},b.prototype.refresh=function(){var b=this,c="offset",d=0;this.offsets=[],this.targets=[],this.scrollHeight=this.getScrollHeight(),a.isWindow(this.$scrollElement[0])||(c="position",d=this.$scrollElement.scrollTop()),this.$body.find(this.selector).map(function(){var b=a(this),e=b.data("target")||b.attr("href"),f=/^#./.test(e)&&a(e);return f&&f.length&&f.is(":visible")&&[[f[c]().top+d,e]]||null}).sort(function(a,b){return a[0]-b[0]}).each(function(){b.offsets.push(this[0]),b.targets.push(this[1])})},b.prototype.process=function(){var a,b=this.$scrollElement.scrollTop()+this.options.offset,c=this.getScrollHeight(),d=this.options.offset+c-this.$scrollElement.height(),e=this.offsets,f=this.targets,g=this.activeTarget;if(this.scrollHeight!=c&&this.refresh(),b>=d)return g!=(a=f[f.length-1])&&this.activate(a);if(g&&b<e[0])return this.activeTarget=null,this.clear();for(a=e.length;a--;)g!=f[a]&&b>=e[a]&&(void 0===e[a+1]||b<e[a+1])&&this.activate(f[a])},b.prototype.activate=function(b){this.activeTarget=b,this.clear();var c=this.selector+'[data-target="'+b+'"],'+this.selector+'[href="'+b+'"]',d=a(c).parents("li").addClass("active");d.parent(".dropdown-menu").length&&(d=d.closest("li.dropdown").addClass("active")),d.trigger("activate.bs.scrollspy")},b.prototype.clear=function(){a(this.selector).parentsUntil(this.options.target,".active").removeClass("active")};var d=a.fn.scrollspy;a.fn.scrollspy=c,a.fn.scrollspy.Constructor=b,a.fn.scrollspy.noConflict=function(){return a.fn.scrollspy=d,this},a(window).on("load.bs.scrollspy.data-api",function(){a('[data-spy="scroll"]').each(function(){var b=a(this);c.call(b,b.data())})})}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var d=a(this),e=d.data("bs.tab");e||d.data("bs.tab",e=new c(this)),"string"==typeof b&&e[b]()})}var c=function(b){this.element=a(b)};c.VERSION="3.3.4",c.TRANSITION_DURATION=150,c.prototype.show=function(){var b=this.element,c=b.closest("ul:not(.dropdown-menu)"),d=b.data("target");if(d||(d=b.attr("href"),d=d&&d.replace(/.*(?=#[^\s]*$)/,"")),!b.parent("li").hasClass("active")){var e=c.find(".active:last a"),f=a.Event("hide.bs.tab",{relatedTarget:b[0]}),g=a.Event("show.bs.tab",{relatedTarget:e[0]});if(e.trigger(f),b.trigger(g),!g.isDefaultPrevented()&&!f.isDefaultPrevented()){var h=a(d);this.activate(b.closest("li"),c),this.activate(h,h.parent(),function(){e.trigger({type:"hidden.bs.tab",relatedTarget:b[0]}),b.trigger({type:"shown.bs.tab",relatedTarget:e[0]})})}}},c.prototype.activate=function(b,d,e){function f(){g.removeClass("active").find("> .dropdown-menu > .active").removeClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded",!1),b.addClass("active").find('[data-toggle="tab"]').attr("aria-expanded",!0),h?(b[0].offsetWidth,b.addClass("in")):b.removeClass("fade"),b.parent(".dropdown-menu").length&&b.closest("li.dropdown").addClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded",!0),e&&e()}var g=d.find("> .active"),h=e&&a.support.transition&&(g.length&&g.hasClass("fade")||!!d.find("> .fade").length);g.length&&h?g.one("bsTransitionEnd",f).emulateTransitionEnd(c.TRANSITION_DURATION):f(),g.removeClass("in")};var d=a.fn.tab;a.fn.tab=b,a.fn.tab.Constructor=c,a.fn.tab.noConflict=function(){return a.fn.tab=d,this};var e=function(c){c.preventDefault(),b.call(a(this),"show")};a(document).on("click.bs.tab.data-api",'[data-toggle="tab"]',e).on("click.bs.tab.data-api",'[data-toggle="pill"]',e)}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var d=a(this),e=d.data("bs.affix"),f="object"==typeof b&&b;e||d.data("bs.affix",e=new c(this,f)),"string"==typeof b&&e[b]()})}var c=function(b,d){this.options=a.extend({},c.DEFAULTS,d),this.$target=a(this.options.target).on("scroll.bs.affix.data-api",a.proxy(this.checkPosition,this)).on("click.bs.affix.data-api",a.proxy(this.checkPositionWithEventLoop,this)),this.$element=a(b),this.affixed=null,this.unpin=null,this.pinnedOffset=null,this.checkPosition()};c.VERSION="3.3.4",c.RESET="affix affix-top affix-bottom",c.DEFAULTS={offset:0,target:window},c.prototype.getState=function(a,b,c,d){var e=this.$target.scrollTop(),f=this.$element.offset(),g=this.$target.height();if(null!=c&&"top"==this.affixed)return c>e?"top":!1;if("bottom"==this.affixed)return null!=c?e+this.unpin<=f.top?!1:"bottom":a-d>=e+g?!1:"bottom";var h=null==this.affixed,i=h?e:f.top,j=h?g:b;return null!=c&&c>=e?"top":null!=d&&i+j>=a-d?"bottom":!1},c.prototype.getPinnedOffset=function(){if(this.pinnedOffset)return this.pinnedOffset;this.$element.removeClass(c.RESET).addClass("affix");var a=this.$target.scrollTop(),b=this.$element.offset();return this.pinnedOffset=b.top-a},c.prototype.checkPositionWithEventLoop=function(){setTimeout(a.proxy(this.checkPosition,this),1)},c.prototype.checkPosition=function(){if(this.$element.is(":visible")){var b=this.$element.height(),d=this.options.offset,e=d.top,f=d.bottom,g=a(document.body).height();"object"!=typeof d&&(f=e=d),"function"==typeof e&&(e=d.top(this.$element)),"function"==typeof f&&(f=d.bottom(this.$element));var h=this.getState(g,b,e,f);if(this.affixed!=h){null!=this.unpin&&this.$element.css("top","");var i="affix"+(h?"-"+h:""),j=a.Event(i+".bs.affix");if(this.$element.trigger(j),j.isDefaultPrevented())return;this.affixed=h,this.unpin="bottom"==h?this.getPinnedOffset():null,this.$element.removeClass(c.RESET).addClass(i).trigger(i.replace("affix","affixed")+".bs.affix")}"bottom"==h&&this.$element.offset({top:g-b-f})}};var d=a.fn.affix;a.fn.affix=b,a.fn.affix.Constructor=c,a.fn.affix.noConflict=function(){return a.fn.affix=d,this},a(window).on("load",function(){a('[data-spy="affix"]').each(function(){var c=a(this),d=c.data();d.offset=d.offset||{},null!=d.offsetBottom&&(d.offset.bottom=d.offsetBottom),null!=d.offsetTop&&(d.offset.top=d.offsetTop),b.call(c,d)})})}(jQuery);(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.React=f()}})(function(){var define,module,exports;return(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(_dereq_,module,exports){'use strict';var EventPluginUtils=_dereq_(19);var ReactChildren=_dereq_(32);var ReactComponent=_dereq_(34);var ReactClass=_dereq_(33);var ReactContext=_dereq_(38);var ReactCurrentOwner=_dereq_(39);var ReactElement=_dereq_(57);var ReactElementValidator=_dereq_(58);var ReactDOM=_dereq_(40);var ReactDOMTextComponent=_dereq_(51);var ReactDefaultInjection=_dereq_(54);var ReactInstanceHandles=_dereq_(66);var ReactMount=_dereq_(70);var ReactPerf=_dereq_(75);var ReactPropTypes=_dereq_(78);var ReactReconciler=_dereq_(81);var ReactServerRendering=_dereq_(84);var assign=_dereq_(27);var findDOMNode=_dereq_(117);var onlyChild=_dereq_(144);ReactDefaultInjection.inject();var createElement=ReactElement.createElement;var createFactory=ReactElement.createFactory;var cloneElement=ReactElement.cloneElement;if("production"!=="development"){createElement=ReactElementValidator.createElement;createFactory=ReactElementValidator.createFactory;cloneElement=ReactElementValidator.cloneElement;}
var render=ReactPerf.measure('React','render',ReactMount.render);var React={Children:{map:ReactChildren.map,forEach:ReactChildren.forEach,count:ReactChildren.count,only:onlyChild},Component:ReactComponent,DOM:ReactDOM,PropTypes:ReactPropTypes,initializeTouchEvents:function(shouldUseTouch){EventPluginUtils.useTouchEvents=shouldUseTouch;},createClass:ReactClass.createClass,createElement:createElement,cloneElement:cloneElement,createFactory:createFactory,createMixin:function(mixin){return mixin;},constructAndRenderComponent:ReactMount.constructAndRenderComponent,constructAndRenderComponentByID:ReactMount.constructAndRenderComponentByID,findDOMNode:findDOMNode,render:render,renderToString:ReactServerRendering.renderToString,renderToStaticMarkup:ReactServerRendering.renderToStaticMarkup,unmountComponentAtNode:ReactMount.unmountComponentAtNode,isValidElement:ReactElement.isValidElement,withContext:ReactContext.withContext,__spread:assign};if(typeof __REACT_DEVTOOLS_GLOBAL_HOOK__!=='undefined'&&typeof __REACT_DEVTOOLS_GLOBAL_HOOK__.inject==='function'){__REACT_DEVTOOLS_GLOBAL_HOOK__.inject({CurrentOwner:ReactCurrentOwner,InstanceHandles:ReactInstanceHandles,Mount:ReactMount,Reconciler:ReactReconciler,TextComponent:ReactDOMTextComponent});}
if("production"!=="development"){var ExecutionEnvironment=_dereq_(21);if(ExecutionEnvironment.canUseDOM&&window.top===window.self){
if(navigator.userAgent.indexOf('Chrome')>-1){if(typeof __REACT_DEVTOOLS_GLOBAL_HOOK__==='undefined'){console.debug('Download the React DevTools for a better development experience: '+'http://fb.me/react-devtools');}}
var expectedFeatures=[ Array.isArray,Array.prototype.every,Array.prototype.forEach,Array.prototype.indexOf,Array.prototype.map,Date.now,Function.prototype.bind,Object.keys,String.prototype.split,String.prototype.trim, Object.create,Object.freeze];for(var i=0;i<expectedFeatures.length;i++){if(!expectedFeatures[i]){console.error('One or more ES5 shim/shams expected by React are not available: '+'http://fb.me/react-warning-polyfills');break;}}}}
React.version='0.13.2';module.exports=React;},{"117":117,"144":144,"19":19,"21":21,"27":27,"32":32,"33":33,"34":34,"38":38,"39":39,"40":40,"51":51,"54":54,"57":57,"58":58,"66":66,"70":70,"75":75,"78":78,"81":81,"84":84}],2:[function(_dereq_,module,exports){'use strict';var focusNode=_dereq_(119);var AutoFocusMixin={componentDidMount:function(){if(this.props.autoFocus){focusNode(this.getDOMNode());}}};module.exports=AutoFocusMixin;},{"119":119}],3:[function(_dereq_,module,exports){'use strict';var EventConstants=_dereq_(15);var EventPropagators=_dereq_(20);var ExecutionEnvironment=_dereq_(21);var FallbackCompositionState=_dereq_(22);var SyntheticCompositionEvent=_dereq_(93);var SyntheticInputEvent=_dereq_(97);var keyOf=_dereq_(141);var END_KEYCODES=[9,13,27,32];var START_KEYCODE=229;var canUseCompositionEvent=(ExecutionEnvironment.canUseDOM&&'CompositionEvent'in window);var documentMode=null;if(ExecutionEnvironment.canUseDOM&&'documentMode'in document){documentMode=document.documentMode;}


var canUseTextInputEvent=(ExecutionEnvironment.canUseDOM&&'TextEvent'in window&&!documentMode&&!isPresto());

var useFallbackCompositionData=(ExecutionEnvironment.canUseDOM&&((!canUseCompositionEvent||documentMode&&documentMode>8&&documentMode<=11)));function isPresto(){var opera=window.opera;return(typeof opera==='object'&&typeof opera.version==='function'&&parseInt(opera.version(),10)<=12);}
var SPACEBAR_CODE=32;var SPACEBAR_CHAR=String.fromCharCode(SPACEBAR_CODE);var topLevelTypes=EventConstants.topLevelTypes;var eventTypes={beforeInput:{phasedRegistrationNames:{bubbled:keyOf({onBeforeInput:null}),captured:keyOf({onBeforeInputCapture:null})},dependencies:[topLevelTypes.topCompositionEnd,topLevelTypes.topKeyPress,topLevelTypes.topTextInput,topLevelTypes.topPaste]},compositionEnd:{phasedRegistrationNames:{bubbled:keyOf({onCompositionEnd:null}),captured:keyOf({onCompositionEndCapture:null})},dependencies:[topLevelTypes.topBlur,topLevelTypes.topCompositionEnd,topLevelTypes.topKeyDown,topLevelTypes.topKeyPress,topLevelTypes.topKeyUp,topLevelTypes.topMouseDown]},compositionStart:{phasedRegistrationNames:{bubbled:keyOf({onCompositionStart:null}),captured:keyOf({onCompositionStartCapture:null})},dependencies:[topLevelTypes.topBlur,topLevelTypes.topCompositionStart,topLevelTypes.topKeyDown,topLevelTypes.topKeyPress,topLevelTypes.topKeyUp,topLevelTypes.topMouseDown]},compositionUpdate:{phasedRegistrationNames:{bubbled:keyOf({onCompositionUpdate:null}),captured:keyOf({onCompositionUpdateCapture:null})},dependencies:[topLevelTypes.topBlur,topLevelTypes.topCompositionUpdate,topLevelTypes.topKeyDown,topLevelTypes.topKeyPress,topLevelTypes.topKeyUp,topLevelTypes.topMouseDown]}};var hasSpaceKeypress=false;function isKeypressCommand(nativeEvent){return((nativeEvent.ctrlKey||nativeEvent.altKey||nativeEvent.metaKey)&&!(nativeEvent.ctrlKey&&nativeEvent.altKey));}
function getCompositionEventType(topLevelType){switch(topLevelType){case topLevelTypes.topCompositionStart:return eventTypes.compositionStart;case topLevelTypes.topCompositionEnd:return eventTypes.compositionEnd;case topLevelTypes.topCompositionUpdate:return eventTypes.compositionUpdate;}}
function isFallbackCompositionStart(topLevelType,nativeEvent){return(topLevelType===topLevelTypes.topKeyDown&&nativeEvent.keyCode===START_KEYCODE);}
function isFallbackCompositionEnd(topLevelType,nativeEvent){switch(topLevelType){case topLevelTypes.topKeyUp:return(END_KEYCODES.indexOf(nativeEvent.keyCode)!==-1);case topLevelTypes.topKeyDown:
return(nativeEvent.keyCode!==START_KEYCODE);case topLevelTypes.topKeyPress:case topLevelTypes.topMouseDown:case topLevelTypes.topBlur:return true;default:return false;}}
function getDataFromCustomEvent(nativeEvent){var detail=nativeEvent.detail;if(typeof detail==='object'&&'data'in detail){return detail.data;}
return null;}
var currentComposition=null;function extractCompositionEvent(topLevelType,topLevelTarget,topLevelTargetID,nativeEvent){var eventType;var fallbackData;if(canUseCompositionEvent){eventType=getCompositionEventType(topLevelType);}else if(!currentComposition){if(isFallbackCompositionStart(topLevelType,nativeEvent)){eventType=eventTypes.compositionStart;}}else if(isFallbackCompositionEnd(topLevelType,nativeEvent)){eventType=eventTypes.compositionEnd;}
if(!eventType){return null;}
if(useFallbackCompositionData){
if(!currentComposition&&eventType===eventTypes.compositionStart){currentComposition=FallbackCompositionState.getPooled(topLevelTarget);}else if(eventType===eventTypes.compositionEnd){if(currentComposition){fallbackData=currentComposition.getData();}}}
var event=SyntheticCompositionEvent.getPooled(eventType,topLevelTargetID,nativeEvent);if(fallbackData){event.data=fallbackData;}else{var customData=getDataFromCustomEvent(nativeEvent);if(customData!==null){event.data=customData;}}
EventPropagators.accumulateTwoPhaseDispatches(event);return event;}
function getNativeBeforeInputChars(topLevelType,nativeEvent){switch(topLevelType){case topLevelTypes.topCompositionEnd:return getDataFromCustomEvent(nativeEvent);case topLevelTypes.topKeyPress:var which=nativeEvent.which;if(which!==SPACEBAR_CODE){return null;}
hasSpaceKeypress=true;return SPACEBAR_CHAR;case topLevelTypes.topTextInput:var chars=nativeEvent.data;

if(chars===SPACEBAR_CHAR&&hasSpaceKeypress){return null;}
return chars;default:return null;}}
function getFallbackBeforeInputChars(topLevelType,nativeEvent){if(currentComposition){if(topLevelType===topLevelTypes.topCompositionEnd||isFallbackCompositionEnd(topLevelType,nativeEvent)){var chars=currentComposition.getData();FallbackCompositionState.release(currentComposition);currentComposition=null;return chars;}
return null;}
switch(topLevelType){case topLevelTypes.topPaste:
return null;case topLevelTypes.topKeyPress:if(nativeEvent.which&&!isKeypressCommand(nativeEvent)){return String.fromCharCode(nativeEvent.which);}
return null;case topLevelTypes.topCompositionEnd:return useFallbackCompositionData?null:nativeEvent.data;default:return null;}}
function extractBeforeInputEvent(topLevelType,topLevelTarget,topLevelTargetID,nativeEvent){var chars;if(canUseTextInputEvent){chars=getNativeBeforeInputChars(topLevelType,nativeEvent);}else{chars=getFallbackBeforeInputChars(topLevelType,nativeEvent);}

if(!chars){return null;}
var event=SyntheticInputEvent.getPooled(eventTypes.beforeInput,topLevelTargetID,nativeEvent);event.data=chars;EventPropagators.accumulateTwoPhaseDispatches(event);return event;}
var BeforeInputEventPlugin={eventTypes:eventTypes,extractEvents:function(topLevelType,topLevelTarget,topLevelTargetID,nativeEvent){return[extractCompositionEvent(topLevelType,topLevelTarget,topLevelTargetID,nativeEvent),extractBeforeInputEvent(topLevelType,topLevelTarget,topLevelTargetID,nativeEvent)];}};module.exports=BeforeInputEventPlugin;},{"141":141,"15":15,"20":20,"21":21,"22":22,"93":93,"97":97}],4:[function(_dereq_,module,exports){'use strict';var isUnitlessNumber={boxFlex:true,boxFlexGroup:true,columnCount:true,flex:true,flexGrow:true,flexPositive:true,flexShrink:true,flexNegative:true,fontWeight:true,lineClamp:true,lineHeight:true,opacity:true,order:true,orphans:true,widows:true,zIndex:true,zoom:true, fillOpacity:true,strokeDashoffset:true,strokeOpacity:true,strokeWidth:true};function prefixKey(prefix,key){return prefix+key.charAt(0).toUpperCase()+key.substring(1);}
var prefixes=['Webkit','ms','Moz','O'];
Object.keys(isUnitlessNumber).forEach(function(prop){prefixes.forEach(function(prefix){isUnitlessNumber[prefixKey(prefix,prop)]=isUnitlessNumber[prop];});});var shorthandPropertyExpansions={background:{backgroundImage:true,backgroundPosition:true,backgroundRepeat:true,backgroundColor:true},border:{borderWidth:true,borderStyle:true,borderColor:true},borderBottom:{borderBottomWidth:true,borderBottomStyle:true,borderBottomColor:true},borderLeft:{borderLeftWidth:true,borderLeftStyle:true,borderLeftColor:true},borderRight:{borderRightWidth:true,borderRightStyle:true,borderRightColor:true},borderTop:{borderTopWidth:true,borderTopStyle:true,borderTopColor:true},font:{fontStyle:true,fontVariant:true,fontWeight:true,fontSize:true,lineHeight:true,fontFamily:true}};var CSSProperty={isUnitlessNumber:isUnitlessNumber,shorthandPropertyExpansions:shorthandPropertyExpansions};module.exports=CSSProperty;},{}],5:[function(_dereq_,module,exports){'use strict';var CSSProperty=_dereq_(4);var ExecutionEnvironment=_dereq_(21);var camelizeStyleName=_dereq_(108);var dangerousStyleValue=_dereq_(113);var hyphenateStyleName=_dereq_(133);var memoizeStringOnly=_dereq_(143);var warning=_dereq_(154);var processStyleName=memoizeStringOnly(function(styleName){return hyphenateStyleName(styleName);});var styleFloatAccessor='cssFloat';if(ExecutionEnvironment.canUseDOM){ if(document.documentElement.style.cssFloat===undefined){styleFloatAccessor='styleFloat';}}
if("production"!=="development"){ var badVendoredStyleNamePattern=/^(?:webkit|moz|o)[A-Z]/; var badStyleValueWithSemicolonPattern=/;\s*$/;var warnedStyleNames={};var warnedStyleValues={};var warnHyphenatedStyleName=function(name){if(warnedStyleNames.hasOwnProperty(name)&&warnedStyleNames[name]){return;}
warnedStyleNames[name]=true;("production"!=="development"?warning(false,'Unsupported style property %s. Did you mean %s?',name,camelizeStyleName(name)):null);};var warnBadVendoredStyleName=function(name){if(warnedStyleNames.hasOwnProperty(name)&&warnedStyleNames[name]){return;}
warnedStyleNames[name]=true;("production"!=="development"?warning(false,'Unsupported vendor-prefixed style property %s. Did you mean %s?',name,name.charAt(0).toUpperCase()+name.slice(1)):null);};var warnStyleValueWithSemicolon=function(name,value){if(warnedStyleValues.hasOwnProperty(value)&&warnedStyleValues[value]){return;}
warnedStyleValues[value]=true;("production"!=="development"?warning(false,'Style property values shouldn\'t contain a semicolon. '+'Try "%s: %s" instead.',name,value.replace(badStyleValueWithSemicolonPattern,'')):null);};var warnValidStyle=function(name,value){if(name.indexOf('-')>-1){warnHyphenatedStyleName(name);}else if(badVendoredStyleNamePattern.test(name)){warnBadVendoredStyleName(name);}else if(badStyleValueWithSemicolonPattern.test(value)){warnStyleValueWithSemicolon(name,value);}};}
var CSSPropertyOperations={createMarkupForStyles:function(styles){var serialized='';for(var styleName in styles){if(!styles.hasOwnProperty(styleName)){continue;}
var styleValue=styles[styleName];if("production"!=="development"){warnValidStyle(styleName,styleValue);}
if(styleValue!=null){serialized+=processStyleName(styleName)+':';serialized+=dangerousStyleValue(styleName,styleValue)+';';}}
return serialized||null;},setValueForStyles:function(node,styles){var style=node.style;for(var styleName in styles){if(!styles.hasOwnProperty(styleName)){continue;}
if("production"!=="development"){warnValidStyle(styleName,styles[styleName]);}
var styleValue=dangerousStyleValue(styleName,styles[styleName]);if(styleName==='float'){styleName=styleFloatAccessor;}
if(styleValue){style[styleName]=styleValue;}else{var expansion=CSSProperty.shorthandPropertyExpansions[styleName];if(expansion){
 for(var individualStyleName in expansion){style[individualStyleName]='';}}else{style[styleName]='';}}}}};module.exports=CSSPropertyOperations;},{"108":108,"113":113,"133":133,"143":143,"154":154,"21":21,"4":4}],6:[function(_dereq_,module,exports){'use strict';var PooledClass=_dereq_(28);var assign=_dereq_(27);var invariant=_dereq_(135);function CallbackQueue(){this._callbacks=null;this._contexts=null;}
assign(CallbackQueue.prototype,{enqueue:function(callback,context){this._callbacks=this._callbacks||[];this._contexts=this._contexts||[];this._callbacks.push(callback);this._contexts.push(context);},notifyAll:function(){var callbacks=this._callbacks;var contexts=this._contexts;if(callbacks){("production"!=="development"?invariant(callbacks.length===contexts.length,'Mismatched list of contexts in callback queue'):invariant(callbacks.length===contexts.length));this._callbacks=null;this._contexts=null;for(var i=0,l=callbacks.length;i<l;i++){callbacks[i].call(contexts[i]);}
callbacks.length=0;contexts.length=0;}},reset:function(){this._callbacks=null;this._contexts=null;},destructor:function(){this.reset();}});PooledClass.addPoolingTo(CallbackQueue);module.exports=CallbackQueue;},{"135":135,"27":27,"28":28}],7:[function(_dereq_,module,exports){'use strict';var EventConstants=_dereq_(15);var EventPluginHub=_dereq_(17);var EventPropagators=_dereq_(20);var ExecutionEnvironment=_dereq_(21);var ReactUpdates=_dereq_(87);var SyntheticEvent=_dereq_(95);var isEventSupported=_dereq_(136);var isTextInputElement=_dereq_(138);var keyOf=_dereq_(141);var topLevelTypes=EventConstants.topLevelTypes;var eventTypes={change:{phasedRegistrationNames:{bubbled:keyOf({onChange:null}),captured:keyOf({onChangeCapture:null})},dependencies:[topLevelTypes.topBlur,topLevelTypes.topChange,topLevelTypes.topClick,topLevelTypes.topFocus,topLevelTypes.topInput,topLevelTypes.topKeyDown,topLevelTypes.topKeyUp,topLevelTypes.topSelectionChange]}};var activeElement=null;var activeElementID=null;var activeElementValue=null;var activeElementValueProp=null;function shouldUseChangeEvent(elem){return(elem.nodeName==='SELECT'||(elem.nodeName==='INPUT'&&elem.type==='file'));}
var doesChangeEventBubble=false;if(ExecutionEnvironment.canUseDOM){ doesChangeEventBubble=isEventSupported('change')&&((!('documentMode'in document)||document.documentMode>8));}
function manualDispatchChangeEvent(nativeEvent){var event=SyntheticEvent.getPooled(eventTypes.change,activeElementID,nativeEvent);EventPropagators.accumulateTwoPhaseDispatches(event);







ReactUpdates.batchedUpdates(runEventInBatch,event);}
function runEventInBatch(event){EventPluginHub.enqueueEvents(event);EventPluginHub.processEventQueue();}
function startWatchingForChangeEventIE8(target,targetID){activeElement=target;activeElementID=targetID;activeElement.attachEvent('onchange',manualDispatchChangeEvent);}
function stopWatchingForChangeEventIE8(){if(!activeElement){return;}
activeElement.detachEvent('onchange',manualDispatchChangeEvent);activeElement=null;activeElementID=null;}
function getTargetIDForChangeEvent(topLevelType,topLevelTarget,topLevelTargetID){if(topLevelType===topLevelTypes.topChange){return topLevelTargetID;}}
function handleEventsForChangeEventIE8(topLevelType,topLevelTarget,topLevelTargetID){if(topLevelType===topLevelTypes.topFocus){
stopWatchingForChangeEventIE8();startWatchingForChangeEventIE8(topLevelTarget,topLevelTargetID);}else if(topLevelType===topLevelTypes.topBlur){stopWatchingForChangeEventIE8();}}
var isInputEventSupported=false;if(ExecutionEnvironment.canUseDOM){
 isInputEventSupported=isEventSupported('input')&&((!('documentMode'in document)||document.documentMode>9));}
var newValueProp={get:function(){return activeElementValueProp.get.call(this);},set:function(val){activeElementValue=''+val;activeElementValueProp.set.call(this,val);}};function startWatchingForValueChange(target,targetID){activeElement=target;activeElementID=targetID;activeElementValue=target.value;activeElementValueProp=Object.getOwnPropertyDescriptor(target.constructor.prototype,'value');Object.defineProperty(activeElement,'value',newValueProp);activeElement.attachEvent('onpropertychange',handlePropertyChange);}
function stopWatchingForValueChange(){if(!activeElement){return;} 
delete activeElement.value;activeElement.detachEvent('onpropertychange',handlePropertyChange);activeElement=null;activeElementID=null;activeElementValue=null;activeElementValueProp=null;}
function handlePropertyChange(nativeEvent){if(nativeEvent.propertyName!=='value'){return;}
var value=nativeEvent.srcElement.value;if(value===activeElementValue){return;}
activeElementValue=value;manualDispatchChangeEvent(nativeEvent);}
function getTargetIDForInputEvent(topLevelType,topLevelTarget,topLevelTargetID){if(topLevelType===topLevelTypes.topInput){
 return topLevelTargetID;}}
function handleEventsForInputEventIE(topLevelType,topLevelTarget,topLevelTargetID){if(topLevelType===topLevelTypes.topFocus){









stopWatchingForValueChange();startWatchingForValueChange(topLevelTarget,topLevelTargetID);}else if(topLevelType===topLevelTypes.topBlur){stopWatchingForValueChange();}}
function getTargetIDForInputEventIE(topLevelType,topLevelTarget,topLevelTargetID){if(topLevelType===topLevelTypes.topSelectionChange||topLevelType===topLevelTypes.topKeyUp||topLevelType===topLevelTypes.topKeyDown){






if(activeElement&&activeElement.value!==activeElementValue){activeElementValue=activeElement.value;return activeElementID;}}}
function shouldUseClickEvent(elem){
return(elem.nodeName==='INPUT'&&(elem.type==='checkbox'||elem.type==='radio'));}
function getTargetIDForClickEvent(topLevelType,topLevelTarget,topLevelTargetID){if(topLevelType===topLevelTypes.topClick){return topLevelTargetID;}}
var ChangeEventPlugin={eventTypes:eventTypes,extractEvents:function(topLevelType,topLevelTarget,topLevelTargetID,nativeEvent){var getTargetIDFunc,handleEventFunc;if(shouldUseChangeEvent(topLevelTarget)){if(doesChangeEventBubble){getTargetIDFunc=getTargetIDForChangeEvent;}else{handleEventFunc=handleEventsForChangeEventIE8;}}else if(isTextInputElement(topLevelTarget)){if(isInputEventSupported){getTargetIDFunc=getTargetIDForInputEvent;}else{getTargetIDFunc=getTargetIDForInputEventIE;handleEventFunc=handleEventsForInputEventIE;}}else if(shouldUseClickEvent(topLevelTarget)){getTargetIDFunc=getTargetIDForClickEvent;}
if(getTargetIDFunc){var targetID=getTargetIDFunc(topLevelType,topLevelTarget,topLevelTargetID);if(targetID){var event=SyntheticEvent.getPooled(eventTypes.change,targetID,nativeEvent);EventPropagators.accumulateTwoPhaseDispatches(event);return event;}}
if(handleEventFunc){handleEventFunc(topLevelType,topLevelTarget,topLevelTargetID);}}};module.exports=ChangeEventPlugin;},{"136":136,"138":138,"141":141,"15":15,"17":17,"20":20,"21":21,"87":87,"95":95}],8:[function(_dereq_,module,exports){'use strict';var nextReactRootIndex=0;var ClientReactRootIndex={createReactRootIndex:function(){return nextReactRootIndex++;}};module.exports=ClientReactRootIndex;},{}],9:[function(_dereq_,module,exports){'use strict';var Danger=_dereq_(12);var ReactMultiChildUpdateTypes=_dereq_(72);var setTextContent=_dereq_(149);var invariant=_dereq_(135);function insertChildAt(parentNode,childNode,index){


parentNode.insertBefore(childNode,parentNode.childNodes[index]||null);}
var DOMChildrenOperations={dangerouslyReplaceNodeWithMarkup:Danger.dangerouslyReplaceNodeWithMarkup,updateTextContent:setTextContent,processUpdates:function(updates,markupList){var update;var initialChildren=null;var updatedChildren=null;for(var i=0;i<updates.length;i++){update=updates[i];if(update.type===ReactMultiChildUpdateTypes.MOVE_EXISTING||update.type===ReactMultiChildUpdateTypes.REMOVE_NODE){var updatedIndex=update.fromIndex;var updatedChild=update.parentNode.childNodes[updatedIndex];var parentID=update.parentID;("production"!=="development"?invariant(updatedChild,'processUpdates(): Unable to find child %s of element. This '+'probably means the DOM was unexpectedly mutated (e.g., by the '+'browser), usually due to forgetting a <tbody> when using tables, '+'nesting tags like <form>, <p>, or <a>, or using non-SVG elements '+'in an <svg> parent. Try inspecting the child nodes of the element '+'with React ID `%s`.',updatedIndex,parentID):invariant(updatedChild));initialChildren=initialChildren||{};initialChildren[parentID]=initialChildren[parentID]||[];initialChildren[parentID][updatedIndex]=updatedChild;updatedChildren=updatedChildren||[];updatedChildren.push(updatedChild);}}
var renderedMarkup=Danger.dangerouslyRenderMarkup(markupList);if(updatedChildren){for(var j=0;j<updatedChildren.length;j++){updatedChildren[j].parentNode.removeChild(updatedChildren[j]);}}
for(var k=0;k<updates.length;k++){update=updates[k];switch(update.type){case ReactMultiChildUpdateTypes.INSERT_MARKUP:insertChildAt(update.parentNode,renderedMarkup[update.markupIndex],update.toIndex);break;case ReactMultiChildUpdateTypes.MOVE_EXISTING:insertChildAt(update.parentNode,initialChildren[update.parentID][update.fromIndex],update.toIndex);break;case ReactMultiChildUpdateTypes.TEXT_CONTENT:setTextContent(update.parentNode,update.textContent);break;case ReactMultiChildUpdateTypes.REMOVE_NODE:break;}}}};module.exports=DOMChildrenOperations;},{"12":12,"135":135,"149":149,"72":72}],10:[function(_dereq_,module,exports){'use strict';var invariant=_dereq_(135);function checkMask(value,bitmask){return(value&bitmask)===bitmask;}
var DOMPropertyInjection={MUST_USE_ATTRIBUTE:0x1,MUST_USE_PROPERTY:0x2,HAS_SIDE_EFFECTS:0x4,HAS_BOOLEAN_VALUE:0x8,HAS_NUMERIC_VALUE:0x10,HAS_POSITIVE_NUMERIC_VALUE:0x20|0x10,HAS_OVERLOADED_BOOLEAN_VALUE:0x40,injectDOMPropertyConfig:function(domPropertyConfig){var Properties=domPropertyConfig.Properties||{};var DOMAttributeNames=domPropertyConfig.DOMAttributeNames||{};var DOMPropertyNames=domPropertyConfig.DOMPropertyNames||{};var DOMMutationMethods=domPropertyConfig.DOMMutationMethods||{};if(domPropertyConfig.isCustomAttribute){DOMProperty._isCustomAttributeFunctions.push(domPropertyConfig.isCustomAttribute);}
for(var propName in Properties){("production"!=="development"?invariant(!DOMProperty.isStandardName.hasOwnProperty(propName),'injectDOMPropertyConfig(...): You\'re trying to inject DOM property '+'\'%s\' which has already been injected. You may be accidentally '+'injecting the same DOM property config twice, or you may be '+'injecting two configs that have conflicting property names.',propName):invariant(!DOMProperty.isStandardName.hasOwnProperty(propName)));DOMProperty.isStandardName[propName]=true;var lowerCased=propName.toLowerCase();DOMProperty.getPossibleStandardName[lowerCased]=propName;if(DOMAttributeNames.hasOwnProperty(propName)){var attributeName=DOMAttributeNames[propName];DOMProperty.getPossibleStandardName[attributeName]=propName;DOMProperty.getAttributeName[propName]=attributeName;}else{DOMProperty.getAttributeName[propName]=lowerCased;}
DOMProperty.getPropertyName[propName]=DOMPropertyNames.hasOwnProperty(propName)?DOMPropertyNames[propName]:propName;if(DOMMutationMethods.hasOwnProperty(propName)){DOMProperty.getMutationMethod[propName]=DOMMutationMethods[propName];}else{DOMProperty.getMutationMethod[propName]=null;}
var propConfig=Properties[propName];DOMProperty.mustUseAttribute[propName]=checkMask(propConfig,DOMPropertyInjection.MUST_USE_ATTRIBUTE);DOMProperty.mustUseProperty[propName]=checkMask(propConfig,DOMPropertyInjection.MUST_USE_PROPERTY);DOMProperty.hasSideEffects[propName]=checkMask(propConfig,DOMPropertyInjection.HAS_SIDE_EFFECTS);DOMProperty.hasBooleanValue[propName]=checkMask(propConfig,DOMPropertyInjection.HAS_BOOLEAN_VALUE);DOMProperty.hasNumericValue[propName]=checkMask(propConfig,DOMPropertyInjection.HAS_NUMERIC_VALUE);DOMProperty.hasPositiveNumericValue[propName]=checkMask(propConfig,DOMPropertyInjection.HAS_POSITIVE_NUMERIC_VALUE);DOMProperty.hasOverloadedBooleanValue[propName]=checkMask(propConfig,DOMPropertyInjection.HAS_OVERLOADED_BOOLEAN_VALUE);("production"!=="development"?invariant(!DOMProperty.mustUseAttribute[propName]||!DOMProperty.mustUseProperty[propName],'DOMProperty: Cannot require using both attribute and property: %s',propName):invariant(!DOMProperty.mustUseAttribute[propName]||!DOMProperty.mustUseProperty[propName]));("production"!=="development"?invariant(DOMProperty.mustUseProperty[propName]||!DOMProperty.hasSideEffects[propName],'DOMProperty: Properties that have side effects must use property: %s',propName):invariant(DOMProperty.mustUseProperty[propName]||!DOMProperty.hasSideEffects[propName]));("production"!=="development"?invariant(!!DOMProperty.hasBooleanValue[propName]+!!DOMProperty.hasNumericValue[propName]+!!DOMProperty.hasOverloadedBooleanValue[propName]<=1,'DOMProperty: Value can be one of boolean, overloaded boolean, or '+'numeric value, but not a combination: %s',propName):invariant(!!DOMProperty.hasBooleanValue[propName]+!!DOMProperty.hasNumericValue[propName]+!!DOMProperty.hasOverloadedBooleanValue[propName]<=1));}}};var defaultValueCache={};var DOMProperty={ID_ATTRIBUTE_NAME:'data-reactid',isStandardName:{},getPossibleStandardName:{},getAttributeName:{},getPropertyName:{},getMutationMethod:{},mustUseAttribute:{},mustUseProperty:{},hasSideEffects:{},hasBooleanValue:{},hasNumericValue:{},hasPositiveNumericValue:{},hasOverloadedBooleanValue:{},_isCustomAttributeFunctions:[],isCustomAttribute:function(attributeName){for(var i=0;i<DOMProperty._isCustomAttributeFunctions.length;i++){var isCustomAttributeFn=DOMProperty._isCustomAttributeFunctions[i];if(isCustomAttributeFn(attributeName)){return true;}}
return false;},getDefaultValueForProperty:function(nodeName,prop){var nodeDefaults=defaultValueCache[nodeName];var testElement;if(!nodeDefaults){defaultValueCache[nodeName]=nodeDefaults={};}
if(!(prop in nodeDefaults)){testElement=document.createElement(nodeName);nodeDefaults[prop]=testElement[prop];}
return nodeDefaults[prop];},injection:DOMPropertyInjection};module.exports=DOMProperty;},{"135":135}],11:[function(_dereq_,module,exports){'use strict';var DOMProperty=_dereq_(10);var quoteAttributeValueForBrowser=_dereq_(147);var warning=_dereq_(154);function shouldIgnoreValue(name,value){return value==null||(DOMProperty.hasBooleanValue[name]&&!value)||(DOMProperty.hasNumericValue[name]&&isNaN(value))||(DOMProperty.hasPositiveNumericValue[name]&&(value<1))||(DOMProperty.hasOverloadedBooleanValue[name]&&value===false);}
if("production"!=="development"){var reactProps={children:true,dangerouslySetInnerHTML:true,key:true,ref:true};var warnedProperties={};var warnUnknownProperty=function(name){if(reactProps.hasOwnProperty(name)&&reactProps[name]||warnedProperties.hasOwnProperty(name)&&warnedProperties[name]){return;}
warnedProperties[name]=true;var lowerCasedName=name.toLowerCase(); var standardName=(DOMProperty.isCustomAttribute(lowerCasedName)?lowerCasedName:DOMProperty.getPossibleStandardName.hasOwnProperty(lowerCasedName)?DOMProperty.getPossibleStandardName[lowerCasedName]:null);
("production"!=="development"?warning(standardName==null,'Unknown DOM property %s. Did you mean %s?',name,standardName):null);};}
var DOMPropertyOperations={createMarkupForID:function(id){return DOMProperty.ID_ATTRIBUTE_NAME+'='+
quoteAttributeValueForBrowser(id);},createMarkupForProperty:function(name,value){if(DOMProperty.isStandardName.hasOwnProperty(name)&&DOMProperty.isStandardName[name]){if(shouldIgnoreValue(name,value)){return'';}
var attributeName=DOMProperty.getAttributeName[name];if(DOMProperty.hasBooleanValue[name]||(DOMProperty.hasOverloadedBooleanValue[name]&&value===true)){return attributeName;}
return attributeName+'='+quoteAttributeValueForBrowser(value);}else if(DOMProperty.isCustomAttribute(name)){if(value==null){return'';}
return name+'='+quoteAttributeValueForBrowser(value);}else if("production"!=="development"){warnUnknownProperty(name);}
return null;},setValueForProperty:function(node,name,value){if(DOMProperty.isStandardName.hasOwnProperty(name)&&DOMProperty.isStandardName[name]){var mutationMethod=DOMProperty.getMutationMethod[name];if(mutationMethod){mutationMethod(node,value);}else if(shouldIgnoreValue(name,value)){this.deleteValueForProperty(node,name);}else if(DOMProperty.mustUseAttribute[name]){node.setAttribute(DOMProperty.getAttributeName[name],''+value);}else{var propName=DOMProperty.getPropertyName[name];
if(!DOMProperty.hasSideEffects[name]||(''+node[propName])!==(''+value)){
node[propName]=value;}}}else if(DOMProperty.isCustomAttribute(name)){if(value==null){node.removeAttribute(name);}else{node.setAttribute(name,''+value);}}else if("production"!=="development"){warnUnknownProperty(name);}},deleteValueForProperty:function(node,name){if(DOMProperty.isStandardName.hasOwnProperty(name)&&DOMProperty.isStandardName[name]){var mutationMethod=DOMProperty.getMutationMethod[name];if(mutationMethod){mutationMethod(node,undefined);}else if(DOMProperty.mustUseAttribute[name]){node.removeAttribute(DOMProperty.getAttributeName[name]);}else{var propName=DOMProperty.getPropertyName[name];var defaultValue=DOMProperty.getDefaultValueForProperty(node.nodeName,propName);if(!DOMProperty.hasSideEffects[name]||(''+node[propName])!==defaultValue){node[propName]=defaultValue;}}}else if(DOMProperty.isCustomAttribute(name)){node.removeAttribute(name);}else if("production"!=="development"){warnUnknownProperty(name);}}};module.exports=DOMPropertyOperations;},{"10":10,"147":147,"154":154}],12:[function(_dereq_,module,exports){'use strict';var ExecutionEnvironment=_dereq_(21);var createNodesFromMarkup=_dereq_(112);var emptyFunction=_dereq_(114);var getMarkupWrap=_dereq_(127);var invariant=_dereq_(135);var OPEN_TAG_NAME_EXP=/^(<[^ \/>]+)/;var RESULT_INDEX_ATTR='data-danger-index';function getNodeName(markup){return markup.substring(1,markup.indexOf(' '));}
var Danger={dangerouslyRenderMarkup:function(markupList){("production"!=="development"?invariant(ExecutionEnvironment.canUseDOM,'dangerouslyRenderMarkup(...): Cannot render markup in a worker '+'thread. Make sure `window` and `document` are available globally '+'before requiring React when unit testing or use '+'React.renderToString for server rendering.'):invariant(ExecutionEnvironment.canUseDOM));var nodeName;var markupByNodeName={};for(var i=0;i<markupList.length;i++){("production"!=="development"?invariant(markupList[i],'dangerouslyRenderMarkup(...): Missing markup.'):invariant(markupList[i]));nodeName=getNodeName(markupList[i]);nodeName=getMarkupWrap(nodeName)?nodeName:'*';markupByNodeName[nodeName]=markupByNodeName[nodeName]||[];markupByNodeName[nodeName][i]=markupList[i];}
var resultList=[];var resultListAssignmentCount=0;for(nodeName in markupByNodeName){if(!markupByNodeName.hasOwnProperty(nodeName)){continue;}
var markupListByNodeName=markupByNodeName[nodeName];

var resultIndex;for(resultIndex in markupListByNodeName){if(markupListByNodeName.hasOwnProperty(resultIndex)){var markup=markupListByNodeName[resultIndex];

markupListByNodeName[resultIndex]=markup.replace(OPEN_TAG_NAME_EXP,'$1 '+RESULT_INDEX_ATTR+'="'+resultIndex+'" ');}}
var renderNodes=createNodesFromMarkup(markupListByNodeName.join(''),emptyFunction
);for(var j=0;j<renderNodes.length;++j){var renderNode=renderNodes[j];if(renderNode.hasAttribute&&renderNode.hasAttribute(RESULT_INDEX_ATTR)){resultIndex=+renderNode.getAttribute(RESULT_INDEX_ATTR);renderNode.removeAttribute(RESULT_INDEX_ATTR);("production"!=="development"?invariant(!resultList.hasOwnProperty(resultIndex),'Danger: Assigning to an already-occupied result index.'):invariant(!resultList.hasOwnProperty(resultIndex)));resultList[resultIndex]=renderNode;
resultListAssignmentCount+=1;}else if("production"!=="development"){console.error('Danger: Discarding unexpected node:',renderNode);}}}

("production"!=="development"?invariant(resultListAssignmentCount===resultList.length,'Danger: Did not assign to every index of resultList.'):invariant(resultListAssignmentCount===resultList.length));("production"!=="development"?invariant(resultList.length===markupList.length,'Danger: Expected markup to render %s nodes, but rendered %s.',markupList.length,resultList.length):invariant(resultList.length===markupList.length));return resultList;},dangerouslyReplaceNodeWithMarkup:function(oldChild,markup){("production"!=="development"?invariant(ExecutionEnvironment.canUseDOM,'dangerouslyReplaceNodeWithMarkup(...): Cannot render markup in a '+'worker thread. Make sure `window` and `document` are available '+'globally before requiring React when unit testing or use '+'React.renderToString for server rendering.'):invariant(ExecutionEnvironment.canUseDOM));("production"!=="development"?invariant(markup,'dangerouslyReplaceNodeWithMarkup(...): Missing markup.'):invariant(markup));("production"!=="development"?invariant(oldChild.tagName.toLowerCase()!=='html','dangerouslyReplaceNodeWithMarkup(...): Cannot replace markup of the '+'<html> node. This is because browser quirks make this unreliable '+'and/or slow. If you want to render to the root you must use '+'server rendering. See React.renderToString().'):invariant(oldChild.tagName.toLowerCase()!=='html'));var newChild=createNodesFromMarkup(markup,emptyFunction)[0];oldChild.parentNode.replaceChild(newChild,oldChild);}};module.exports=Danger;},{"112":112,"114":114,"127":127,"135":135,"21":21}],13:[function(_dereq_,module,exports){'use strict';var keyOf=_dereq_(141);var DefaultEventPluginOrder=[keyOf({ResponderEventPlugin:null}),keyOf({SimpleEventPlugin:null}),keyOf({TapEventPlugin:null}),keyOf({EnterLeaveEventPlugin:null}),keyOf({ChangeEventPlugin:null}),keyOf({SelectEventPlugin:null}),keyOf({BeforeInputEventPlugin:null}),keyOf({AnalyticsEventPlugin:null}),keyOf({MobileSafariClickEventPlugin:null})];module.exports=DefaultEventPluginOrder;},{"141":141}],14:[function(_dereq_,module,exports){'use strict';var EventConstants=_dereq_(15);var EventPropagators=_dereq_(20);var SyntheticMouseEvent=_dereq_(99);var ReactMount=_dereq_(70);var keyOf=_dereq_(141);var topLevelTypes=EventConstants.topLevelTypes;var getFirstReactDOM=ReactMount.getFirstReactDOM;var eventTypes={mouseEnter:{registrationName:keyOf({onMouseEnter:null}),dependencies:[topLevelTypes.topMouseOut,topLevelTypes.topMouseOver]},mouseLeave:{registrationName:keyOf({onMouseLeave:null}),dependencies:[topLevelTypes.topMouseOut,topLevelTypes.topMouseOver]}};var extractedEvents=[null,null];var EnterLeaveEventPlugin={eventTypes:eventTypes,extractEvents:function(topLevelType,topLevelTarget,topLevelTargetID,nativeEvent){if(topLevelType===topLevelTypes.topMouseOver&&(nativeEvent.relatedTarget||nativeEvent.fromElement)){return null;}
if(topLevelType!==topLevelTypes.topMouseOut&&topLevelType!==topLevelTypes.topMouseOver){return null;}
var win;if(topLevelTarget.window===topLevelTarget){win=topLevelTarget;}else{var doc=topLevelTarget.ownerDocument;if(doc){win=doc.defaultView||doc.parentWindow;}else{win=window;}}
var from,to;if(topLevelType===topLevelTypes.topMouseOut){from=topLevelTarget;to=getFirstReactDOM(nativeEvent.relatedTarget||nativeEvent.toElement)||win;}else{from=win;to=topLevelTarget;}
if(from===to){return null;}
var fromID=from?ReactMount.getID(from):'';var toID=to?ReactMount.getID(to):'';var leave=SyntheticMouseEvent.getPooled(eventTypes.mouseLeave,fromID,nativeEvent);leave.type='mouseleave';leave.target=from;leave.relatedTarget=to;var enter=SyntheticMouseEvent.getPooled(eventTypes.mouseEnter,toID,nativeEvent);enter.type='mouseenter';enter.target=to;enter.relatedTarget=from;EventPropagators.accumulateEnterLeaveDispatches(leave,enter,fromID,toID);extractedEvents[0]=leave;extractedEvents[1]=enter;return extractedEvents;}};module.exports=EnterLeaveEventPlugin;},{"141":141,"15":15,"20":20,"70":70,"99":99}],15:[function(_dereq_,module,exports){'use strict';var keyMirror=_dereq_(140);var PropagationPhases=keyMirror({bubbled:null,captured:null});var topLevelTypes=keyMirror({topBlur:null,topChange:null,topClick:null,topCompositionEnd:null,topCompositionStart:null,topCompositionUpdate:null,topContextMenu:null,topCopy:null,topCut:null,topDoubleClick:null,topDrag:null,topDragEnd:null,topDragEnter:null,topDragExit:null,topDragLeave:null,topDragOver:null,topDragStart:null,topDrop:null,topError:null,topFocus:null,topInput:null,topKeyDown:null,topKeyPress:null,topKeyUp:null,topLoad:null,topMouseDown:null,topMouseMove:null,topMouseOut:null,topMouseOver:null,topMouseUp:null,topPaste:null,topReset:null,topScroll:null,topSelectionChange:null,topSubmit:null,topTextInput:null,topTouchCancel:null,topTouchEnd:null,topTouchMove:null,topTouchStart:null,topWheel:null});var EventConstants={topLevelTypes:topLevelTypes,PropagationPhases:PropagationPhases};module.exports=EventConstants;},{"140":140}],16:[function(_dereq_,module,exports){var emptyFunction=_dereq_(114);var EventListener={listen:function(target,eventType,callback){if(target.addEventListener){target.addEventListener(eventType,callback,false);return{remove:function(){target.removeEventListener(eventType,callback,false);}};}else if(target.attachEvent){target.attachEvent('on'+eventType,callback);return{remove:function(){target.detachEvent('on'+eventType,callback);}};}},capture:function(target,eventType,callback){if(!target.addEventListener){if("production"!=="development"){console.error('Attempted to listen to events during the capture phase on a '+'browser that does not support the capture phase. Your application '+'will not receive some events.');}
return{remove:emptyFunction};}else{target.addEventListener(eventType,callback,true);return{remove:function(){target.removeEventListener(eventType,callback,true);}};}},registerDefault:function(){}};module.exports=EventListener;},{"114":114}],17:[function(_dereq_,module,exports){'use strict';var EventPluginRegistry=_dereq_(18);var EventPluginUtils=_dereq_(19);var accumulateInto=_dereq_(105);var forEachAccumulated=_dereq_(120);var invariant=_dereq_(135);var listenerBank={};var eventQueue=null;var executeDispatchesAndRelease=function(event){if(event){var executeDispatch=EventPluginUtils.executeDispatch;var PluginModule=EventPluginRegistry.getPluginModuleForEvent(event);if(PluginModule&&PluginModule.executeDispatch){executeDispatch=PluginModule.executeDispatch;}
EventPluginUtils.executeDispatchesInOrder(event,executeDispatch);if(!event.isPersistent()){event.constructor.release(event);}}};var InstanceHandle=null;function validateInstanceHandle(){var valid=InstanceHandle&&InstanceHandle.traverseTwoPhase&&InstanceHandle.traverseEnterLeave;("production"!=="development"?invariant(valid,'InstanceHandle not injected before use!'):invariant(valid));}
var EventPluginHub={injection:{injectMount:EventPluginUtils.injection.injectMount,injectInstanceHandle:function(InjectedInstanceHandle){InstanceHandle=InjectedInstanceHandle;if("production"!=="development"){validateInstanceHandle();}},getInstanceHandle:function(){if("production"!=="development"){validateInstanceHandle();}
return InstanceHandle;},injectEventPluginOrder:EventPluginRegistry.injectEventPluginOrder,injectEventPluginsByName:EventPluginRegistry.injectEventPluginsByName},eventNameDispatchConfigs:EventPluginRegistry.eventNameDispatchConfigs,registrationNameModules:EventPluginRegistry.registrationNameModules,putListener:function(id,registrationName,listener){("production"!=="development"?invariant(!listener||typeof listener==='function','Expected %s listener to be a function, instead got type %s',registrationName,typeof listener):invariant(!listener||typeof listener==='function'));var bankForRegistrationName=listenerBank[registrationName]||(listenerBank[registrationName]={});bankForRegistrationName[id]=listener;},getListener:function(id,registrationName){var bankForRegistrationName=listenerBank[registrationName];return bankForRegistrationName&&bankForRegistrationName[id];},deleteListener:function(id,registrationName){var bankForRegistrationName=listenerBank[registrationName];if(bankForRegistrationName){delete bankForRegistrationName[id];}},deleteAllListeners:function(id){for(var registrationName in listenerBank){delete listenerBank[registrationName][id];}},extractEvents:function(topLevelType,topLevelTarget,topLevelTargetID,nativeEvent){var events;var plugins=EventPluginRegistry.plugins;for(var i=0,l=plugins.length;i<l;i++){var possiblePlugin=plugins[i];if(possiblePlugin){var extractedEvents=possiblePlugin.extractEvents(topLevelType,topLevelTarget,topLevelTargetID,nativeEvent);if(extractedEvents){events=accumulateInto(events,extractedEvents);}}}
return events;},enqueueEvents:function(events){if(events){eventQueue=accumulateInto(eventQueue,events);}},processEventQueue:function(){
var processingEventQueue=eventQueue;eventQueue=null;forEachAccumulated(processingEventQueue,executeDispatchesAndRelease);("production"!=="development"?invariant(!eventQueue,'processEventQueue(): Additional events were enqueued while processing '+'an event queue. Support for this has not yet been implemented.'):invariant(!eventQueue));},__purge:function(){listenerBank={};},__getListenerBank:function(){return listenerBank;}};module.exports=EventPluginHub;},{"105":105,"120":120,"135":135,"18":18,"19":19}],18:[function(_dereq_,module,exports){'use strict';var invariant=_dereq_(135);var EventPluginOrder=null;var namesToPlugins={};function recomputePluginOrdering(){if(!EventPluginOrder){return;}
for(var pluginName in namesToPlugins){var PluginModule=namesToPlugins[pluginName];var pluginIndex=EventPluginOrder.indexOf(pluginName);("production"!=="development"?invariant(pluginIndex>-1,'EventPluginRegistry: Cannot inject event plugins that do not exist in '+'the plugin ordering, `%s`.',pluginName):invariant(pluginIndex>-1));if(EventPluginRegistry.plugins[pluginIndex]){continue;}
("production"!=="development"?invariant(PluginModule.extractEvents,'EventPluginRegistry: Event plugins must implement an `extractEvents` '+'method, but `%s` does not.',pluginName):invariant(PluginModule.extractEvents));EventPluginRegistry.plugins[pluginIndex]=PluginModule;var publishedEvents=PluginModule.eventTypes;for(var eventName in publishedEvents){("production"!=="development"?invariant(publishEventForPlugin(publishedEvents[eventName],PluginModule,eventName),'EventPluginRegistry: Failed to publish event `%s` for plugin `%s`.',eventName,pluginName):invariant(publishEventForPlugin(publishedEvents[eventName],PluginModule,eventName)));}}}
function publishEventForPlugin(dispatchConfig,PluginModule,eventName){("production"!=="development"?invariant(!EventPluginRegistry.eventNameDispatchConfigs.hasOwnProperty(eventName),'EventPluginHub: More than one plugin attempted to publish the same '+'event name, `%s`.',eventName):invariant(!EventPluginRegistry.eventNameDispatchConfigs.hasOwnProperty(eventName)));EventPluginRegistry.eventNameDispatchConfigs[eventName]=dispatchConfig;var phasedRegistrationNames=dispatchConfig.phasedRegistrationNames;if(phasedRegistrationNames){for(var phaseName in phasedRegistrationNames){if(phasedRegistrationNames.hasOwnProperty(phaseName)){var phasedRegistrationName=phasedRegistrationNames[phaseName];publishRegistrationName(phasedRegistrationName,PluginModule,eventName);}}
return true;}else if(dispatchConfig.registrationName){publishRegistrationName(dispatchConfig.registrationName,PluginModule,eventName);return true;}
return false;}
function publishRegistrationName(registrationName,PluginModule,eventName){("production"!=="development"?invariant(!EventPluginRegistry.registrationNameModules[registrationName],'EventPluginHub: More than one plugin attempted to publish the same '+'registration name, `%s`.',registrationName):invariant(!EventPluginRegistry.registrationNameModules[registrationName]));EventPluginRegistry.registrationNameModules[registrationName]=PluginModule;EventPluginRegistry.registrationNameDependencies[registrationName]=PluginModule.eventTypes[eventName].dependencies;}
var EventPluginRegistry={plugins:[],eventNameDispatchConfigs:{},registrationNameModules:{},registrationNameDependencies:{},injectEventPluginOrder:function(InjectedEventPluginOrder){("production"!=="development"?invariant(!EventPluginOrder,'EventPluginRegistry: Cannot inject event plugin ordering more than '+'once. You are likely trying to load more than one copy of React.'):invariant(!EventPluginOrder));EventPluginOrder=Array.prototype.slice.call(InjectedEventPluginOrder);recomputePluginOrdering();},injectEventPluginsByName:function(injectedNamesToPlugins){var isOrderingDirty=false;for(var pluginName in injectedNamesToPlugins){if(!injectedNamesToPlugins.hasOwnProperty(pluginName)){continue;}
var PluginModule=injectedNamesToPlugins[pluginName];if(!namesToPlugins.hasOwnProperty(pluginName)||namesToPlugins[pluginName]!==PluginModule){("production"!=="development"?invariant(!namesToPlugins[pluginName],'EventPluginRegistry: Cannot inject two different event plugins '+'using the same name, `%s`.',pluginName):invariant(!namesToPlugins[pluginName]));namesToPlugins[pluginName]=PluginModule;isOrderingDirty=true;}}
if(isOrderingDirty){recomputePluginOrdering();}},getPluginModuleForEvent:function(event){var dispatchConfig=event.dispatchConfig;if(dispatchConfig.registrationName){return EventPluginRegistry.registrationNameModules[dispatchConfig.registrationName]||null;}
for(var phase in dispatchConfig.phasedRegistrationNames){if(!dispatchConfig.phasedRegistrationNames.hasOwnProperty(phase)){continue;}
var PluginModule=EventPluginRegistry.registrationNameModules[dispatchConfig.phasedRegistrationNames[phase]];if(PluginModule){return PluginModule;}}
return null;},_resetEventPlugins:function(){EventPluginOrder=null;for(var pluginName in namesToPlugins){if(namesToPlugins.hasOwnProperty(pluginName)){delete namesToPlugins[pluginName];}}
EventPluginRegistry.plugins.length=0;var eventNameDispatchConfigs=EventPluginRegistry.eventNameDispatchConfigs;for(var eventName in eventNameDispatchConfigs){if(eventNameDispatchConfigs.hasOwnProperty(eventName)){delete eventNameDispatchConfigs[eventName];}}
var registrationNameModules=EventPluginRegistry.registrationNameModules;for(var registrationName in registrationNameModules){if(registrationNameModules.hasOwnProperty(registrationName)){delete registrationNameModules[registrationName];}}}};module.exports=EventPluginRegistry;},{"135":135}],19:[function(_dereq_,module,exports){'use strict';var EventConstants=_dereq_(15);var invariant=_dereq_(135);var injection={Mount:null,injectMount:function(InjectedMount){injection.Mount=InjectedMount;if("production"!=="development"){("production"!=="development"?invariant(InjectedMount&&InjectedMount.getNode,'EventPluginUtils.injection.injectMount(...): Injected Mount module '+'is missing getNode.'):invariant(InjectedMount&&InjectedMount.getNode));}}};var topLevelTypes=EventConstants.topLevelTypes;function isEndish(topLevelType){return topLevelType===topLevelTypes.topMouseUp||topLevelType===topLevelTypes.topTouchEnd||topLevelType===topLevelTypes.topTouchCancel;}
function isMoveish(topLevelType){return topLevelType===topLevelTypes.topMouseMove||topLevelType===topLevelTypes.topTouchMove;}
function isStartish(topLevelType){return topLevelType===topLevelTypes.topMouseDown||topLevelType===topLevelTypes.topTouchStart;}
var validateEventDispatches;if("production"!=="development"){validateEventDispatches=function(event){var dispatchListeners=event._dispatchListeners;var dispatchIDs=event._dispatchIDs;var listenersIsArr=Array.isArray(dispatchListeners);var idsIsArr=Array.isArray(dispatchIDs);var IDsLen=idsIsArr?dispatchIDs.length:dispatchIDs?1:0;var listenersLen=listenersIsArr?dispatchListeners.length:dispatchListeners?1:0;("production"!=="development"?invariant(idsIsArr===listenersIsArr&&IDsLen===listenersLen,'EventPluginUtils: Invalid `event`.'):invariant(idsIsArr===listenersIsArr&&IDsLen===listenersLen));};}
function forEachEventDispatch(event,cb){var dispatchListeners=event._dispatchListeners;var dispatchIDs=event._dispatchIDs;if("production"!=="development"){validateEventDispatches(event);}
if(Array.isArray(dispatchListeners)){for(var i=0;i<dispatchListeners.length;i++){if(event.isPropagationStopped()){break;}
cb(event,dispatchListeners[i],dispatchIDs[i]);}}else if(dispatchListeners){cb(event,dispatchListeners,dispatchIDs);}}
function executeDispatch(event,listener,domID){event.currentTarget=injection.Mount.getNode(domID);var returnValue=listener(event,domID);event.currentTarget=null;return returnValue;}
function executeDispatchesInOrder(event,cb){forEachEventDispatch(event,cb);event._dispatchListeners=null;event._dispatchIDs=null;}
function executeDispatchesInOrderStopAtTrueImpl(event){var dispatchListeners=event._dispatchListeners;var dispatchIDs=event._dispatchIDs;if("production"!=="development"){validateEventDispatches(event);}
if(Array.isArray(dispatchListeners)){for(var i=0;i<dispatchListeners.length;i++){if(event.isPropagationStopped()){break;}
if(dispatchListeners[i](event,dispatchIDs[i])){return dispatchIDs[i];}}}else if(dispatchListeners){if(dispatchListeners(event,dispatchIDs)){return dispatchIDs;}}
return null;}
function executeDispatchesInOrderStopAtTrue(event){var ret=executeDispatchesInOrderStopAtTrueImpl(event);event._dispatchIDs=null;event._dispatchListeners=null;return ret;}
function executeDirectDispatch(event){if("production"!=="development"){validateEventDispatches(event);}
var dispatchListener=event._dispatchListeners;var dispatchID=event._dispatchIDs;("production"!=="development"?invariant(!Array.isArray(dispatchListener),'executeDirectDispatch(...): Invalid `event`.'):invariant(!Array.isArray(dispatchListener)));var res=dispatchListener?dispatchListener(event,dispatchID):null;event._dispatchListeners=null;event._dispatchIDs=null;return res;}
function hasDispatches(event){return!!event._dispatchListeners;}
var EventPluginUtils={isEndish:isEndish,isMoveish:isMoveish,isStartish:isStartish,executeDirectDispatch:executeDirectDispatch,executeDispatch:executeDispatch,executeDispatchesInOrder:executeDispatchesInOrder,executeDispatchesInOrderStopAtTrue:executeDispatchesInOrderStopAtTrue,hasDispatches:hasDispatches,injection:injection,useTouchEvents:false};module.exports=EventPluginUtils;},{"135":135,"15":15}],20:[function(_dereq_,module,exports){'use strict';var EventConstants=_dereq_(15);var EventPluginHub=_dereq_(17);var accumulateInto=_dereq_(105);var forEachAccumulated=_dereq_(120);var PropagationPhases=EventConstants.PropagationPhases;var getListener=EventPluginHub.getListener;function listenerAtPhase(id,event,propagationPhase){var registrationName=event.dispatchConfig.phasedRegistrationNames[propagationPhase];return getListener(id,registrationName);}
function accumulateDirectionalDispatches(domID,upwards,event){if("production"!=="development"){if(!domID){throw new Error('Dispatching id must not be null');}}
var phase=upwards?PropagationPhases.bubbled:PropagationPhases.captured;var listener=listenerAtPhase(domID,event,phase);if(listener){event._dispatchListeners=accumulateInto(event._dispatchListeners,listener);event._dispatchIDs=accumulateInto(event._dispatchIDs,domID);}}
function accumulateTwoPhaseDispatchesSingle(event){if(event&&event.dispatchConfig.phasedRegistrationNames){EventPluginHub.injection.getInstanceHandle().traverseTwoPhase(event.dispatchMarker,accumulateDirectionalDispatches,event);}}
function accumulateDispatches(id,ignoredDirection,event){if(event&&event.dispatchConfig.registrationName){var registrationName=event.dispatchConfig.registrationName;var listener=getListener(id,registrationName);if(listener){event._dispatchListeners=accumulateInto(event._dispatchListeners,listener);event._dispatchIDs=accumulateInto(event._dispatchIDs,id);}}}
function accumulateDirectDispatchesSingle(event){if(event&&event.dispatchConfig.registrationName){accumulateDispatches(event.dispatchMarker,null,event);}}
function accumulateTwoPhaseDispatches(events){forEachAccumulated(events,accumulateTwoPhaseDispatchesSingle);}
function accumulateEnterLeaveDispatches(leave,enter,fromID,toID){EventPluginHub.injection.getInstanceHandle().traverseEnterLeave(fromID,toID,accumulateDispatches,leave,enter);}
function accumulateDirectDispatches(events){forEachAccumulated(events,accumulateDirectDispatchesSingle);}
var EventPropagators={accumulateTwoPhaseDispatches:accumulateTwoPhaseDispatches,accumulateDirectDispatches:accumulateDirectDispatches,accumulateEnterLeaveDispatches:accumulateEnterLeaveDispatches};module.exports=EventPropagators;},{"105":105,"120":120,"15":15,"17":17}],21:[function(_dereq_,module,exports){"use strict";var canUseDOM=!!((typeof window!=='undefined'&&window.document&&window.document.createElement));var ExecutionEnvironment={canUseDOM:canUseDOM,canUseWorkers:typeof Worker!=='undefined',canUseEventListeners:canUseDOM&&!!(window.addEventListener||window.attachEvent),canUseViewport:canUseDOM&&!!window.screen,isInWorker:!canUseDOM
};module.exports=ExecutionEnvironment;},{}],22:[function(_dereq_,module,exports){'use strict';var PooledClass=_dereq_(28);var assign=_dereq_(27);var getTextContentAccessor=_dereq_(130);function FallbackCompositionState(root){this._root=root;this._startText=this.getText();this._fallbackText=null;}
assign(FallbackCompositionState.prototype,{getText:function(){if('value'in this._root){return this._root.value;}
return this._root[getTextContentAccessor()];},getData:function(){if(this._fallbackText){return this._fallbackText;}
var start;var startValue=this._startText;var startLength=startValue.length;var end;var endValue=this.getText();var endLength=endValue.length;for(start=0;start<startLength;start++){if(startValue[start]!==endValue[start]){break;}}
var minEnd=startLength-start;for(end=1;end<=minEnd;end++){if(startValue[startLength-end]!==endValue[endLength-end]){break;}}
var sliceTail=end>1?1-end:undefined;this._fallbackText=endValue.slice(start,sliceTail);return this._fallbackText;}});PooledClass.addPoolingTo(FallbackCompositionState);module.exports=FallbackCompositionState;},{"130":130,"27":27,"28":28}],23:[function(_dereq_,module,exports){'use strict';var DOMProperty=_dereq_(10);var ExecutionEnvironment=_dereq_(21);var MUST_USE_ATTRIBUTE=DOMProperty.injection.MUST_USE_ATTRIBUTE;var MUST_USE_PROPERTY=DOMProperty.injection.MUST_USE_PROPERTY;var HAS_BOOLEAN_VALUE=DOMProperty.injection.HAS_BOOLEAN_VALUE;var HAS_SIDE_EFFECTS=DOMProperty.injection.HAS_SIDE_EFFECTS;var HAS_NUMERIC_VALUE=DOMProperty.injection.HAS_NUMERIC_VALUE;var HAS_POSITIVE_NUMERIC_VALUE=DOMProperty.injection.HAS_POSITIVE_NUMERIC_VALUE;var HAS_OVERLOADED_BOOLEAN_VALUE=DOMProperty.injection.HAS_OVERLOADED_BOOLEAN_VALUE;var hasSVG;if(ExecutionEnvironment.canUseDOM){var implementation=document.implementation;hasSVG=(implementation&&implementation.hasFeature&&implementation.hasFeature('http://www.w3.org/TR/SVG11/feature#BasicStructure','1.1'));}
var HTMLDOMPropertyConfig={isCustomAttribute:RegExp.prototype.test.bind(/^(data|aria)-[a-z_][a-z\d_.\-]*$/),Properties:{accept:null,acceptCharset:null,accessKey:null,action:null,allowFullScreen:MUST_USE_ATTRIBUTE|HAS_BOOLEAN_VALUE,allowTransparency:MUST_USE_ATTRIBUTE,alt:null,async:HAS_BOOLEAN_VALUE,autoComplete:null,
autoPlay:HAS_BOOLEAN_VALUE,cellPadding:null,cellSpacing:null,charSet:MUST_USE_ATTRIBUTE,checked:MUST_USE_PROPERTY|HAS_BOOLEAN_VALUE,classID:MUST_USE_ATTRIBUTE,
className:hasSVG?MUST_USE_ATTRIBUTE:MUST_USE_PROPERTY,cols:MUST_USE_ATTRIBUTE|HAS_POSITIVE_NUMERIC_VALUE,colSpan:null,content:null,contentEditable:null,contextMenu:MUST_USE_ATTRIBUTE,controls:MUST_USE_PROPERTY|HAS_BOOLEAN_VALUE,coords:null,crossOrigin:null,data:null,dateTime:MUST_USE_ATTRIBUTE,defer:HAS_BOOLEAN_VALUE,dir:null,disabled:MUST_USE_ATTRIBUTE|HAS_BOOLEAN_VALUE,download:HAS_OVERLOADED_BOOLEAN_VALUE,draggable:null,encType:null,form:MUST_USE_ATTRIBUTE,formAction:MUST_USE_ATTRIBUTE,formEncType:MUST_USE_ATTRIBUTE,formMethod:MUST_USE_ATTRIBUTE,formNoValidate:HAS_BOOLEAN_VALUE,formTarget:MUST_USE_ATTRIBUTE,frameBorder:MUST_USE_ATTRIBUTE,headers:null,height:MUST_USE_ATTRIBUTE,hidden:MUST_USE_ATTRIBUTE|HAS_BOOLEAN_VALUE,high:null,href:null,hrefLang:null,htmlFor:null,httpEquiv:null,icon:null,id:MUST_USE_PROPERTY,label:null,lang:null,list:MUST_USE_ATTRIBUTE,loop:MUST_USE_PROPERTY|HAS_BOOLEAN_VALUE,low:null,manifest:MUST_USE_ATTRIBUTE,marginHeight:null,marginWidth:null,max:null,maxLength:MUST_USE_ATTRIBUTE,media:MUST_USE_ATTRIBUTE,mediaGroup:null,method:null,min:null,multiple:MUST_USE_PROPERTY|HAS_BOOLEAN_VALUE,muted:MUST_USE_PROPERTY|HAS_BOOLEAN_VALUE,name:null,noValidate:HAS_BOOLEAN_VALUE,open:HAS_BOOLEAN_VALUE,optimum:null,pattern:null,placeholder:null,poster:null,preload:null,radioGroup:null,readOnly:MUST_USE_PROPERTY|HAS_BOOLEAN_VALUE,rel:null,required:HAS_BOOLEAN_VALUE,role:MUST_USE_ATTRIBUTE,rows:MUST_USE_ATTRIBUTE|HAS_POSITIVE_NUMERIC_VALUE,rowSpan:null,sandbox:null,scope:null,scoped:HAS_BOOLEAN_VALUE,scrolling:null,seamless:MUST_USE_ATTRIBUTE|HAS_BOOLEAN_VALUE,selected:MUST_USE_PROPERTY|HAS_BOOLEAN_VALUE,shape:null,size:MUST_USE_ATTRIBUTE|HAS_POSITIVE_NUMERIC_VALUE,sizes:MUST_USE_ATTRIBUTE,span:HAS_POSITIVE_NUMERIC_VALUE,spellCheck:null,src:null,srcDoc:MUST_USE_PROPERTY,srcSet:MUST_USE_ATTRIBUTE,start:HAS_NUMERIC_VALUE,step:null,style:null,tabIndex:null,target:null,title:null,type:null,useMap:null,value:MUST_USE_PROPERTY|HAS_SIDE_EFFECTS,width:MUST_USE_ATTRIBUTE,wmode:MUST_USE_ATTRIBUTE,
autoCapitalize:null,autoCorrect:null,
 itemProp:MUST_USE_ATTRIBUTE,itemScope:MUST_USE_ATTRIBUTE|HAS_BOOLEAN_VALUE,itemType:MUST_USE_ATTRIBUTE,

 itemID:MUST_USE_ATTRIBUTE,itemRef:MUST_USE_ATTRIBUTE,property:null, unselectable:MUST_USE_ATTRIBUTE},DOMAttributeNames:{acceptCharset:'accept-charset',className:'class',htmlFor:'for',httpEquiv:'http-equiv'},DOMPropertyNames:{autoCapitalize:'autocapitalize',autoComplete:'autocomplete',autoCorrect:'autocorrect',autoFocus:'autofocus',autoPlay:'autoplay', encType:'encoding',hrefLang:'hreflang',radioGroup:'radiogroup',spellCheck:'spellcheck',srcDoc:'srcdoc',srcSet:'srcset'}};module.exports=HTMLDOMPropertyConfig;},{"10":10,"21":21}],24:[function(_dereq_,module,exports){'use strict';var ReactPropTypes=_dereq_(78);var invariant=_dereq_(135);var hasReadOnlyValue={'button':true,'checkbox':true,'image':true,'hidden':true,'radio':true,'reset':true,'submit':true};function _assertSingleLink(input){("production"!=="development"?invariant(input.props.checkedLink==null||input.props.valueLink==null,'Cannot provide a checkedLink and a valueLink. If you want to use '+'checkedLink, you probably don\'t want to use valueLink and vice versa.'):invariant(input.props.checkedLink==null||input.props.valueLink==null));}
function _assertValueLink(input){_assertSingleLink(input);("production"!=="development"?invariant(input.props.value==null&&input.props.onChange==null,'Cannot provide a valueLink and a value or onChange event. If you want '+'to use value or onChange, you probably don\'t want to use valueLink.'):invariant(input.props.value==null&&input.props.onChange==null));}
function _assertCheckedLink(input){_assertSingleLink(input);("production"!=="development"?invariant(input.props.checked==null&&input.props.onChange==null,'Cannot provide a checkedLink and a checked property or onChange event. '+'If you want to use checked or onChange, you probably don\'t want to '+'use checkedLink'):invariant(input.props.checked==null&&input.props.onChange==null));}
function _handleLinkedValueChange(e){this.props.valueLink.requestChange(e.target.value);}
function _handleLinkedCheckChange(e){this.props.checkedLink.requestChange(e.target.checked);}
var LinkedValueUtils={Mixin:{propTypes:{value:function(props,propName,componentName){if(!props[propName]||hasReadOnlyValue[props.type]||props.onChange||props.readOnly||props.disabled){return null;}
return new Error('You provided a `value` prop to a form field without an '+'`onChange` handler. This will render a read-only field. If '+'the field should be mutable use `defaultValue`. Otherwise, '+'set either `onChange` or `readOnly`.');},checked:function(props,propName,componentName){if(!props[propName]||props.onChange||props.readOnly||props.disabled){return null;}
return new Error('You provided a `checked` prop to a form field without an '+'`onChange` handler. This will render a read-only field. If '+'the field should be mutable use `defaultChecked`. Otherwise, '+'set either `onChange` or `readOnly`.');},onChange:ReactPropTypes.func}},getValue:function(input){if(input.props.valueLink){_assertValueLink(input);return input.props.valueLink.value;}
return input.props.value;},getChecked:function(input){if(input.props.checkedLink){_assertCheckedLink(input);return input.props.checkedLink.value;}
return input.props.checked;},getOnChange:function(input){if(input.props.valueLink){_assertValueLink(input);return _handleLinkedValueChange;}else if(input.props.checkedLink){_assertCheckedLink(input);return _handleLinkedCheckChange;}
return input.props.onChange;}};module.exports=LinkedValueUtils;},{"135":135,"78":78}],25:[function(_dereq_,module,exports){'use strict';var ReactBrowserEventEmitter=_dereq_(30);var accumulateInto=_dereq_(105);var forEachAccumulated=_dereq_(120);var invariant=_dereq_(135);function remove(event){event.remove();}
var LocalEventTrapMixin={trapBubbledEvent:function(topLevelType,handlerBaseName){("production"!=="development"?invariant(this.isMounted(),'Must be mounted to trap events'):invariant(this.isMounted()));
var node=this.getDOMNode();("production"!=="development"?invariant(node,'LocalEventTrapMixin.trapBubbledEvent(...): Requires node to be rendered.'):invariant(node));var listener=ReactBrowserEventEmitter.trapBubbledEvent(topLevelType,handlerBaseName,node);this._localEventListeners=accumulateInto(this._localEventListeners,listener);},
componentWillUnmount:function(){if(this._localEventListeners){forEachAccumulated(this._localEventListeners,remove);}}};module.exports=LocalEventTrapMixin;},{"105":105,"120":120,"135":135,"30":30}],26:[function(_dereq_,module,exports){'use strict';var EventConstants=_dereq_(15);var emptyFunction=_dereq_(114);var topLevelTypes=EventConstants.topLevelTypes;var MobileSafariClickEventPlugin={eventTypes:null,extractEvents:function(topLevelType,topLevelTarget,topLevelTargetID,nativeEvent){if(topLevelType===topLevelTypes.topTouchStart){var target=nativeEvent.target;if(target&&!target.onclick){target.onclick=emptyFunction;}}}};module.exports=MobileSafariClickEventPlugin;},{"114":114,"15":15}],27:[function(_dereq_,module,exports){'use strict';function assign(target,sources){if(target==null){throw new TypeError('Object.assign target cannot be null or undefined');}
var to=Object(target);var hasOwnProperty=Object.prototype.hasOwnProperty;for(var nextIndex=1;nextIndex<arguments.length;nextIndex++){var nextSource=arguments[nextIndex];if(nextSource==null){continue;}
var from=Object(nextSource);


for(var key in from){if(hasOwnProperty.call(from,key)){to[key]=from[key];}}}
return to;}
module.exports=assign;},{}],28:[function(_dereq_,module,exports){'use strict';var invariant=_dereq_(135);var oneArgumentPooler=function(copyFieldsFrom){var Klass=this;if(Klass.instancePool.length){var instance=Klass.instancePool.pop();Klass.call(instance,copyFieldsFrom);return instance;}else{return new Klass(copyFieldsFrom);}};var twoArgumentPooler=function(a1,a2){var Klass=this;if(Klass.instancePool.length){var instance=Klass.instancePool.pop();Klass.call(instance,a1,a2);return instance;}else{return new Klass(a1,a2);}};var threeArgumentPooler=function(a1,a2,a3){var Klass=this;if(Klass.instancePool.length){var instance=Klass.instancePool.pop();Klass.call(instance,a1,a2,a3);return instance;}else{return new Klass(a1,a2,a3);}};var fiveArgumentPooler=function(a1,a2,a3,a4,a5){var Klass=this;if(Klass.instancePool.length){var instance=Klass.instancePool.pop();Klass.call(instance,a1,a2,a3,a4,a5);return instance;}else{return new Klass(a1,a2,a3,a4,a5);}};var standardReleaser=function(instance){var Klass=this;("production"!=="development"?invariant(instance instanceof Klass,'Trying to release an instance into a pool of a different type.'):invariant(instance instanceof Klass));if(instance.destructor){instance.destructor();}
if(Klass.instancePool.length<Klass.poolSize){Klass.instancePool.push(instance);}};var DEFAULT_POOL_SIZE=10;var DEFAULT_POOLER=oneArgumentPooler;var addPoolingTo=function(CopyConstructor,pooler){var NewKlass=CopyConstructor;NewKlass.instancePool=[];NewKlass.getPooled=pooler||DEFAULT_POOLER;if(!NewKlass.poolSize){NewKlass.poolSize=DEFAULT_POOL_SIZE;}
NewKlass.release=standardReleaser;return NewKlass;};var PooledClass={addPoolingTo:addPoolingTo,oneArgumentPooler:oneArgumentPooler,twoArgumentPooler:twoArgumentPooler,threeArgumentPooler:threeArgumentPooler,fiveArgumentPooler:fiveArgumentPooler};module.exports=PooledClass;},{"135":135}],29:[function(_dereq_,module,exports){'use strict';var findDOMNode=_dereq_(117);var ReactBrowserComponentMixin={getDOMNode:function(){return findDOMNode(this);}};module.exports=ReactBrowserComponentMixin;},{"117":117}],30:[function(_dereq_,module,exports){'use strict';var EventConstants=_dereq_(15);var EventPluginHub=_dereq_(17);var EventPluginRegistry=_dereq_(18);var ReactEventEmitterMixin=_dereq_(61);var ViewportMetrics=_dereq_(104);var assign=_dereq_(27);var isEventSupported=_dereq_(136);var alreadyListeningTo={};var isMonitoringScrollValue=false;var reactTopListenersCounter=0;

var topEventMapping={topBlur:'blur',topChange:'change',topClick:'click',topCompositionEnd:'compositionend',topCompositionStart:'compositionstart',topCompositionUpdate:'compositionupdate',topContextMenu:'contextmenu',topCopy:'copy',topCut:'cut',topDoubleClick:'dblclick',topDrag:'drag',topDragEnd:'dragend',topDragEnter:'dragenter',topDragExit:'dragexit',topDragLeave:'dragleave',topDragOver:'dragover',topDragStart:'dragstart',topDrop:'drop',topFocus:'focus',topInput:'input',topKeyDown:'keydown',topKeyPress:'keypress',topKeyUp:'keyup',topMouseDown:'mousedown',topMouseMove:'mousemove',topMouseOut:'mouseout',topMouseOver:'mouseover',topMouseUp:'mouseup',topPaste:'paste',topScroll:'scroll',topSelectionChange:'selectionchange',topTextInput:'textInput',topTouchCancel:'touchcancel',topTouchEnd:'touchend',topTouchMove:'touchmove',topTouchStart:'touchstart',topWheel:'wheel'};var topListenersIDKey='_reactListenersID'+String(Math.random()).slice(2);function getListeningForDocument(mountAt){if(!Object.prototype.hasOwnProperty.call(mountAt,topListenersIDKey)){mountAt[topListenersIDKey]=reactTopListenersCounter++;alreadyListeningTo[mountAt[topListenersIDKey]]={};}
return alreadyListeningTo[mountAt[topListenersIDKey]];}
var ReactBrowserEventEmitter=assign({},ReactEventEmitterMixin,{ReactEventListener:null,injection:{injectReactEventListener:function(ReactEventListener){ReactEventListener.setHandleTopLevel(ReactBrowserEventEmitter.handleTopLevel);ReactBrowserEventEmitter.ReactEventListener=ReactEventListener;}},setEnabled:function(enabled){if(ReactBrowserEventEmitter.ReactEventListener){ReactBrowserEventEmitter.ReactEventListener.setEnabled(enabled);}},isEnabled:function(){return!!((ReactBrowserEventEmitter.ReactEventListener&&ReactBrowserEventEmitter.ReactEventListener.isEnabled()));},listenTo:function(registrationName,contentDocumentHandle){var mountAt=contentDocumentHandle;var isListening=getListeningForDocument(mountAt);var dependencies=EventPluginRegistry.registrationNameDependencies[registrationName];var topLevelTypes=EventConstants.topLevelTypes;for(var i=0,l=dependencies.length;i<l;i++){var dependency=dependencies[i];if(!((isListening.hasOwnProperty(dependency)&&isListening[dependency]))){if(dependency===topLevelTypes.topWheel){if(isEventSupported('wheel')){ReactBrowserEventEmitter.ReactEventListener.trapBubbledEvent(topLevelTypes.topWheel,'wheel',mountAt);}else if(isEventSupported('mousewheel')){ReactBrowserEventEmitter.ReactEventListener.trapBubbledEvent(topLevelTypes.topWheel,'mousewheel',mountAt);}else{ ReactBrowserEventEmitter.ReactEventListener.trapBubbledEvent(topLevelTypes.topWheel,'DOMMouseScroll',mountAt);}}else if(dependency===topLevelTypes.topScroll){if(isEventSupported('scroll',true)){ReactBrowserEventEmitter.ReactEventListener.trapCapturedEvent(topLevelTypes.topScroll,'scroll',mountAt);}else{ReactBrowserEventEmitter.ReactEventListener.trapBubbledEvent(topLevelTypes.topScroll,'scroll',ReactBrowserEventEmitter.ReactEventListener.WINDOW_HANDLE);}}else if(dependency===topLevelTypes.topFocus||dependency===topLevelTypes.topBlur){if(isEventSupported('focus',true)){ReactBrowserEventEmitter.ReactEventListener.trapCapturedEvent(topLevelTypes.topFocus,'focus',mountAt);ReactBrowserEventEmitter.ReactEventListener.trapCapturedEvent(topLevelTypes.topBlur,'blur',mountAt);}else if(isEventSupported('focusin')){ ReactBrowserEventEmitter.ReactEventListener.trapBubbledEvent(topLevelTypes.topFocus,'focusin',mountAt);ReactBrowserEventEmitter.ReactEventListener.trapBubbledEvent(topLevelTypes.topBlur,'focusout',mountAt);} 
isListening[topLevelTypes.topBlur]=true;isListening[topLevelTypes.topFocus]=true;}else if(topEventMapping.hasOwnProperty(dependency)){ReactBrowserEventEmitter.ReactEventListener.trapBubbledEvent(dependency,topEventMapping[dependency],mountAt);}
isListening[dependency]=true;}}},trapBubbledEvent:function(topLevelType,handlerBaseName,handle){return ReactBrowserEventEmitter.ReactEventListener.trapBubbledEvent(topLevelType,handlerBaseName,handle);},trapCapturedEvent:function(topLevelType,handlerBaseName,handle){return ReactBrowserEventEmitter.ReactEventListener.trapCapturedEvent(topLevelType,handlerBaseName,handle);},ensureScrollValueMonitoring:function(){if(!isMonitoringScrollValue){var refresh=ViewportMetrics.refreshScrollValues;ReactBrowserEventEmitter.ReactEventListener.monitorScrollValue(refresh);isMonitoringScrollValue=true;}},eventNameDispatchConfigs:EventPluginHub.eventNameDispatchConfigs,registrationNameModules:EventPluginHub.registrationNameModules,putListener:EventPluginHub.putListener,getListener:EventPluginHub.getListener,deleteListener:EventPluginHub.deleteListener,deleteAllListeners:EventPluginHub.deleteAllListeners});module.exports=ReactBrowserEventEmitter;},{"104":104,"136":136,"15":15,"17":17,"18":18,"27":27,"61":61}],31:[function(_dereq_,module,exports){'use strict';var ReactReconciler=_dereq_(81);var flattenChildren=_dereq_(118);var instantiateReactComponent=_dereq_(134);var shouldUpdateReactComponent=_dereq_(151);var ReactChildReconciler={instantiateChildren:function(nestedChildNodes,transaction,context){var children=flattenChildren(nestedChildNodes);for(var name in children){if(children.hasOwnProperty(name)){var child=children[name];
var childInstance=instantiateReactComponent(child,null);children[name]=childInstance;}}
return children;},updateChildren:function(prevChildren,nextNestedChildNodes,transaction,context){


var nextChildren=flattenChildren(nextNestedChildNodes);if(!nextChildren&&!prevChildren){return null;}
var name;for(name in nextChildren){if(!nextChildren.hasOwnProperty(name)){continue;}
var prevChild=prevChildren&&prevChildren[name];var prevElement=prevChild&&prevChild._currentElement;var nextElement=nextChildren[name];if(shouldUpdateReactComponent(prevElement,nextElement)){ReactReconciler.receiveComponent(prevChild,nextElement,transaction,context);nextChildren[name]=prevChild;}else{if(prevChild){ReactReconciler.unmountComponent(prevChild,name);}
var nextChildInstance=instantiateReactComponent(nextElement,null);nextChildren[name]=nextChildInstance;}}
for(name in prevChildren){if(prevChildren.hasOwnProperty(name)&&!(nextChildren&&nextChildren.hasOwnProperty(name))){ReactReconciler.unmountComponent(prevChildren[name]);}}
return nextChildren;},unmountChildren:function(renderedChildren){for(var name in renderedChildren){var renderedChild=renderedChildren[name];ReactReconciler.unmountComponent(renderedChild);}}};module.exports=ReactChildReconciler;},{"118":118,"134":134,"151":151,"81":81}],32:[function(_dereq_,module,exports){'use strict';var PooledClass=_dereq_(28);var ReactFragment=_dereq_(63);var traverseAllChildren=_dereq_(153);var warning=_dereq_(154);var twoArgumentPooler=PooledClass.twoArgumentPooler;var threeArgumentPooler=PooledClass.threeArgumentPooler;function ForEachBookKeeping(forEachFunction,forEachContext){this.forEachFunction=forEachFunction;this.forEachContext=forEachContext;}
PooledClass.addPoolingTo(ForEachBookKeeping,twoArgumentPooler);function forEachSingleChild(traverseContext,child,name,i){var forEachBookKeeping=traverseContext;forEachBookKeeping.forEachFunction.call(forEachBookKeeping.forEachContext,child,i);}
function forEachChildren(children,forEachFunc,forEachContext){if(children==null){return children;}
var traverseContext=ForEachBookKeeping.getPooled(forEachFunc,forEachContext);traverseAllChildren(children,forEachSingleChild,traverseContext);ForEachBookKeeping.release(traverseContext);}
function MapBookKeeping(mapResult,mapFunction,mapContext){this.mapResult=mapResult;this.mapFunction=mapFunction;this.mapContext=mapContext;}
PooledClass.addPoolingTo(MapBookKeeping,threeArgumentPooler);function mapSingleChildIntoContext(traverseContext,child,name,i){var mapBookKeeping=traverseContext;var mapResult=mapBookKeeping.mapResult;var keyUnique=!mapResult.hasOwnProperty(name);if("production"!=="development"){("production"!=="development"?warning(keyUnique,'ReactChildren.map(...): Encountered two children with the same key, '+'`%s`. Child keys must be unique; when two children share a key, only '+'the first child will be used.',name):null);}
if(keyUnique){var mappedChild=mapBookKeeping.mapFunction.call(mapBookKeeping.mapContext,child,i);mapResult[name]=mappedChild;}}
function mapChildren(children,func,context){if(children==null){return children;}
var mapResult={};var traverseContext=MapBookKeeping.getPooled(mapResult,func,context);traverseAllChildren(children,mapSingleChildIntoContext,traverseContext);MapBookKeeping.release(traverseContext);return ReactFragment.create(mapResult);}
function forEachSingleChildDummy(traverseContext,child,name,i){return null;}
function countChildren(children,context){return traverseAllChildren(children,forEachSingleChildDummy,null);}
var ReactChildren={forEach:forEachChildren,map:mapChildren,count:countChildren};module.exports=ReactChildren;},{"153":153,"154":154,"28":28,"63":63}],33:[function(_dereq_,module,exports){'use strict';var ReactComponent=_dereq_(34);var ReactCurrentOwner=_dereq_(39);var ReactElement=_dereq_(57);var ReactErrorUtils=_dereq_(60);var ReactInstanceMap=_dereq_(67);var ReactLifeCycle=_dereq_(68);var ReactPropTypeLocations=_dereq_(77);var ReactPropTypeLocationNames=_dereq_(76);var ReactUpdateQueue=_dereq_(86);var assign=_dereq_(27);var invariant=_dereq_(135);var keyMirror=_dereq_(140);var keyOf=_dereq_(141);var warning=_dereq_(154);var MIXINS_KEY=keyOf({mixins:null});var SpecPolicy=keyMirror({DEFINE_ONCE:null,DEFINE_MANY:null,OVERRIDE_BASE:null,DEFINE_MANY_MERGED:null});var injectedMixins=[];var ReactClassInterface={mixins:SpecPolicy.DEFINE_MANY,statics:SpecPolicy.DEFINE_MANY,propTypes:SpecPolicy.DEFINE_MANY,contextTypes:SpecPolicy.DEFINE_MANY,childContextTypes:SpecPolicy.DEFINE_MANY,getDefaultProps:SpecPolicy.DEFINE_MANY_MERGED,getInitialState:SpecPolicy.DEFINE_MANY_MERGED,getChildContext:SpecPolicy.DEFINE_MANY_MERGED,render:SpecPolicy.DEFINE_ONCE,componentWillMount:SpecPolicy.DEFINE_MANY,componentDidMount:SpecPolicy.DEFINE_MANY,componentWillReceiveProps:SpecPolicy.DEFINE_MANY,shouldComponentUpdate:SpecPolicy.DEFINE_ONCE,componentWillUpdate:SpecPolicy.DEFINE_MANY,componentDidUpdate:SpecPolicy.DEFINE_MANY,componentWillUnmount:SpecPolicy.DEFINE_MANY,updateComponent:SpecPolicy.OVERRIDE_BASE};var RESERVED_SPEC_KEYS={displayName:function(Constructor,displayName){Constructor.displayName=displayName;},mixins:function(Constructor,mixins){if(mixins){for(var i=0;i<mixins.length;i++){mixSpecIntoComponent(Constructor,mixins[i]);}}},childContextTypes:function(Constructor,childContextTypes){if("production"!=="development"){validateTypeDef(Constructor,childContextTypes,ReactPropTypeLocations.childContext);}
Constructor.childContextTypes=assign({},Constructor.childContextTypes,childContextTypes);},contextTypes:function(Constructor,contextTypes){if("production"!=="development"){validateTypeDef(Constructor,contextTypes,ReactPropTypeLocations.context);}
Constructor.contextTypes=assign({},Constructor.contextTypes,contextTypes);},getDefaultProps:function(Constructor,getDefaultProps){if(Constructor.getDefaultProps){Constructor.getDefaultProps=createMergedResultFunction(Constructor.getDefaultProps,getDefaultProps);}else{Constructor.getDefaultProps=getDefaultProps;}},propTypes:function(Constructor,propTypes){if("production"!=="development"){validateTypeDef(Constructor,propTypes,ReactPropTypeLocations.prop);}
Constructor.propTypes=assign({},Constructor.propTypes,propTypes);},statics:function(Constructor,statics){mixStaticSpecIntoComponent(Constructor,statics);}};function validateTypeDef(Constructor,typeDef,location){for(var propName in typeDef){if(typeDef.hasOwnProperty(propName)){
("production"!=="development"?warning(typeof typeDef[propName]==='function','%s: %s type `%s` is invalid; it must be a function, usually from '+'React.PropTypes.',Constructor.displayName||'ReactClass',ReactPropTypeLocationNames[location],propName):null);}}}
function validateMethodOverride(proto,name){var specPolicy=ReactClassInterface.hasOwnProperty(name)?ReactClassInterface[name]:null;if(ReactClassMixin.hasOwnProperty(name)){("production"!=="development"?invariant(specPolicy===SpecPolicy.OVERRIDE_BASE,'ReactClassInterface: You are attempting to override '+'`%s` from your class specification. Ensure that your method names '+'do not overlap with React methods.',name):invariant(specPolicy===SpecPolicy.OVERRIDE_BASE));}
if(proto.hasOwnProperty(name)){("production"!=="development"?invariant(specPolicy===SpecPolicy.DEFINE_MANY||specPolicy===SpecPolicy.DEFINE_MANY_MERGED,'ReactClassInterface: You are attempting to define '+'`%s` on your component more than once. This conflict may be due '+'to a mixin.',name):invariant(specPolicy===SpecPolicy.DEFINE_MANY||specPolicy===SpecPolicy.DEFINE_MANY_MERGED));}}
function mixSpecIntoComponent(Constructor,spec){if(!spec){return;}
("production"!=="development"?invariant(typeof spec!=='function','ReactClass: You\'re attempting to '+'use a component class as a mixin. Instead, just use a regular object.'):invariant(typeof spec!=='function'));("production"!=="development"?invariant(!ReactElement.isValidElement(spec),'ReactClass: You\'re attempting to '+'use a component as a mixin. Instead, just use a regular object.'):invariant(!ReactElement.isValidElement(spec)));var proto=Constructor.prototype;

if(spec.hasOwnProperty(MIXINS_KEY)){RESERVED_SPEC_KEYS.mixins(Constructor,spec.mixins);}
for(var name in spec){if(!spec.hasOwnProperty(name)){continue;}
if(name===MIXINS_KEY){ continue;}
var property=spec[name];validateMethodOverride(proto,name);if(RESERVED_SPEC_KEYS.hasOwnProperty(name)){RESERVED_SPEC_KEYS[name](Constructor,property);}else{var isReactClassMethod=ReactClassInterface.hasOwnProperty(name);var isAlreadyDefined=proto.hasOwnProperty(name);var markedDontBind=property&&property.__reactDontBind;var isFunction=typeof property==='function';var shouldAutoBind=isFunction&&!isReactClassMethod&&!isAlreadyDefined&&!markedDontBind;if(shouldAutoBind){if(!proto.__reactAutoBindMap){proto.__reactAutoBindMap={};}
proto.__reactAutoBindMap[name]=property;proto[name]=property;}else{if(isAlreadyDefined){var specPolicy=ReactClassInterface[name];("production"!=="development"?invariant(isReactClassMethod&&((specPolicy===SpecPolicy.DEFINE_MANY_MERGED||specPolicy===SpecPolicy.DEFINE_MANY)),'ReactClass: Unexpected spec policy %s for key %s '+'when mixing in component specs.',specPolicy,name):invariant(isReactClassMethod&&((specPolicy===SpecPolicy.DEFINE_MANY_MERGED||specPolicy===SpecPolicy.DEFINE_MANY))));
if(specPolicy===SpecPolicy.DEFINE_MANY_MERGED){proto[name]=createMergedResultFunction(proto[name],property);}else if(specPolicy===SpecPolicy.DEFINE_MANY){proto[name]=createChainedFunction(proto[name],property);}}else{proto[name]=property;if("production"!=="development"){
if(typeof property==='function'&&spec.displayName){proto[name].displayName=spec.displayName+'_'+name;}}}}}}}
function mixStaticSpecIntoComponent(Constructor,statics){if(!statics){return;}
for(var name in statics){var property=statics[name];if(!statics.hasOwnProperty(name)){continue;}
var isReserved=name in RESERVED_SPEC_KEYS;("production"!=="development"?invariant(!isReserved,'ReactClass: You are attempting to define a reserved '+'property, `%s`, that shouldn\'t be on the "statics" key. Define it '+'as an instance property instead; it will still be accessible on the '+'constructor.',name):invariant(!isReserved));var isInherited=name in Constructor;("production"!=="development"?invariant(!isInherited,'ReactClass: You are attempting to define '+'`%s` on your component more than once. This conflict may be '+'due to a mixin.',name):invariant(!isInherited));Constructor[name]=property;}}
function mergeIntoWithNoDuplicateKeys(one,two){("production"!=="development"?invariant(one&&two&&typeof one==='object'&&typeof two==='object','mergeIntoWithNoDuplicateKeys(): Cannot merge non-objects.'):invariant(one&&two&&typeof one==='object'&&typeof two==='object'));for(var key in two){if(two.hasOwnProperty(key)){("production"!=="development"?invariant(one[key]===undefined,'mergeIntoWithNoDuplicateKeys(): '+'Tried to merge two objects with the same key: `%s`. This conflict '+'may be due to a mixin; in particular, this may be caused by two '+'getInitialState() or getDefaultProps() methods returning objects '+'with clashing keys.',key):invariant(one[key]===undefined));one[key]=two[key];}}
return one;}
function createMergedResultFunction(one,two){return function mergedResult(){var a=one.apply(this,arguments);var b=two.apply(this,arguments);if(a==null){return b;}else if(b==null){return a;}
var c={};mergeIntoWithNoDuplicateKeys(c,a);mergeIntoWithNoDuplicateKeys(c,b);return c;};}
function createChainedFunction(one,two){return function chainedFunction(){one.apply(this,arguments);two.apply(this,arguments);};}
function bindAutoBindMethod(component,method){var boundMethod=method.bind(component);if("production"!=="development"){boundMethod.__reactBoundContext=component;boundMethod.__reactBoundMethod=method;boundMethod.__reactBoundArguments=null;var componentName=component.constructor.displayName;var _bind=boundMethod.bind;boundMethod.bind=function(newThis){for(var args=[],$__0=1,$__1=arguments.length;$__0<$__1;$__0++)args.push(arguments[$__0]);

if(newThis!==component&&newThis!==null){("production"!=="development"?warning(false,'bind(): React component methods may only be bound to the '+'component instance. See %s',componentName):null);}else if(!args.length){("production"!=="development"?warning(false,'bind(): You are binding a component method to the component. '+'React does this for you automatically in a high-performance '+'way, so you can safely remove this call. See %s',componentName):null);return boundMethod;}
var reboundMethod=_bind.apply(boundMethod,arguments);reboundMethod.__reactBoundContext=component;reboundMethod.__reactBoundMethod=method;reboundMethod.__reactBoundArguments=args;return reboundMethod;};}
return boundMethod;}
function bindAutoBindMethods(component){for(var autoBindKey in component.__reactAutoBindMap){if(component.__reactAutoBindMap.hasOwnProperty(autoBindKey)){var method=component.__reactAutoBindMap[autoBindKey];component[autoBindKey]=bindAutoBindMethod(component,ReactErrorUtils.guard(method,component.constructor.displayName+'.'+autoBindKey));}}}
var typeDeprecationDescriptor={enumerable:false,get:function(){var displayName=this.displayName||this.name||'Component';("production"!=="development"?warning(false,'%s.type is deprecated. Use %s directly to access the class.',displayName,displayName):null);Object.defineProperty(this,'type',{value:this});return this;}};var ReactClassMixin={replaceState:function(newState,callback){ReactUpdateQueue.enqueueReplaceState(this,newState);if(callback){ReactUpdateQueue.enqueueCallback(this,callback);}},isMounted:function(){if("production"!=="development"){var owner=ReactCurrentOwner.current;if(owner!==null){("production"!=="development"?warning(owner._warnedAboutRefsInRender,'%s is accessing isMounted inside its render() function. '+'render() should be a pure function of props and state. It should '+'never access something that requires stale data from the previous '+'render, such as refs. Move this logic to componentDidMount and '+'componentDidUpdate instead.',owner.getName()||'A component'):null);owner._warnedAboutRefsInRender=true;}}
var internalInstance=ReactInstanceMap.get(this);return(internalInstance&&internalInstance!==ReactLifeCycle.currentlyMountingInstance);},setProps:function(partialProps,callback){ReactUpdateQueue.enqueueSetProps(this,partialProps);if(callback){ReactUpdateQueue.enqueueCallback(this,callback);}},replaceProps:function(newProps,callback){ReactUpdateQueue.enqueueReplaceProps(this,newProps);if(callback){ReactUpdateQueue.enqueueCallback(this,callback);}}};var ReactClassComponent=function(){};assign(ReactClassComponent.prototype,ReactComponent.prototype,ReactClassMixin);var ReactClass={createClass:function(spec){var Constructor=function(props,context){
if("production"!=="development"){("production"!=="development"?warning(this instanceof Constructor,'Something is calling a React component directly. Use a factory or '+'JSX instead. See: http://fb.me/react-legacyfactory'):null);} 
if(this.__reactAutoBindMap){bindAutoBindMethods(this);}
this.props=props;this.context=context;this.state=null;
var initialState=this.getInitialState?this.getInitialState():null;if("production"!=="development"){if(typeof initialState==='undefined'&&this.getInitialState._isMockFunction){
initialState=null;}}
("production"!=="development"?invariant(typeof initialState==='object'&&!Array.isArray(initialState),'%s.getInitialState(): must return an object or null',Constructor.displayName||'ReactCompositeComponent'):invariant(typeof initialState==='object'&&!Array.isArray(initialState)));this.state=initialState;};Constructor.prototype=new ReactClassComponent();Constructor.prototype.constructor=Constructor;injectedMixins.forEach(mixSpecIntoComponent.bind(null,Constructor));mixSpecIntoComponent(Constructor,spec); if(Constructor.getDefaultProps){Constructor.defaultProps=Constructor.getDefaultProps();}
if("production"!=="development"){

if(Constructor.getDefaultProps){Constructor.getDefaultProps.isReactClassApproved={};}
if(Constructor.prototype.getInitialState){Constructor.prototype.getInitialState.isReactClassApproved={};}}
("production"!=="development"?invariant(Constructor.prototype.render,'createClass(...): Class specification must implement a `render` method.'):invariant(Constructor.prototype.render));if("production"!=="development"){("production"!=="development"?warning(!Constructor.prototype.componentShouldUpdate,'%s has a method called '+'componentShouldUpdate(). Did you mean shouldComponentUpdate()? '+'The name is phrased as a question because the function is '+'expected to return a value.',spec.displayName||'A component'):null);}
for(var methodName in ReactClassInterface){if(!Constructor.prototype[methodName]){Constructor.prototype[methodName]=null;}} 
Constructor.type=Constructor;if("production"!=="development"){try{Object.defineProperty(Constructor,'type',typeDeprecationDescriptor);}catch(x){}}
return Constructor;},injection:{injectMixin:function(mixin){injectedMixins.push(mixin);}}};module.exports=ReactClass;},{"135":135,"140":140,"141":141,"154":154,"27":27,"34":34,"39":39,"57":57,"60":60,"67":67,"68":68,"76":76,"77":77,"86":86}],34:[function(_dereq_,module,exports){'use strict';var ReactUpdateQueue=_dereq_(86);var invariant=_dereq_(135);var warning=_dereq_(154);function ReactComponent(props,context){this.props=props;this.context=context;}
ReactComponent.prototype.setState=function(partialState,callback){("production"!=="development"?invariant(typeof partialState==='object'||typeof partialState==='function'||partialState==null,'setState(...): takes an object of state variables to update or a '+'function which returns an object of state variables.'):invariant(typeof partialState==='object'||typeof partialState==='function'||partialState==null));if("production"!=="development"){("production"!=="development"?warning(partialState!=null,'setState(...): You passed an undefined or null state object; '+'instead, use forceUpdate().'):null);}
ReactUpdateQueue.enqueueSetState(this,partialState);if(callback){ReactUpdateQueue.enqueueCallback(this,callback);}};ReactComponent.prototype.forceUpdate=function(callback){ReactUpdateQueue.enqueueForceUpdate(this);if(callback){ReactUpdateQueue.enqueueCallback(this,callback);}};if("production"!=="development"){var deprecatedAPIs={getDOMNode:'getDOMNode',isMounted:'isMounted',replaceProps:'replaceProps',replaceState:'replaceState',setProps:'setProps'};var defineDeprecationWarning=function(methodName,displayName){try{Object.defineProperty(ReactComponent.prototype,methodName,{get:function(){("production"!=="development"?warning(false,'%s(...) is deprecated in plain JavaScript React classes.',displayName):null);return undefined;}});}catch(x){}};for(var fnName in deprecatedAPIs){if(deprecatedAPIs.hasOwnProperty(fnName)){defineDeprecationWarning(fnName,deprecatedAPIs[fnName]);}}}
module.exports=ReactComponent;},{"135":135,"154":154,"86":86}],35:[function(_dereq_,module,exports){'use strict';var ReactDOMIDOperations=_dereq_(44);var ReactMount=_dereq_(70);var ReactComponentBrowserEnvironment={processChildrenUpdates:ReactDOMIDOperations.dangerouslyProcessChildrenUpdates,replaceNodeWithMarkupByID:ReactDOMIDOperations.dangerouslyReplaceNodeWithMarkupByID,unmountIDFromEnvironment:function(rootNodeID){ReactMount.purgeID(rootNodeID);}};module.exports=ReactComponentBrowserEnvironment;},{"44":44,"70":70}],36:[function(_dereq_,module,exports){'use strict';var invariant=_dereq_(135);var injected=false;var ReactComponentEnvironment={unmountIDFromEnvironment:null,replaceNodeWithMarkupByID:null,processChildrenUpdates:null,injection:{injectEnvironment:function(environment){("production"!=="development"?invariant(!injected,'ReactCompositeComponent: injectEnvironment() can only be called once.'):invariant(!injected));ReactComponentEnvironment.unmountIDFromEnvironment=environment.unmountIDFromEnvironment;ReactComponentEnvironment.replaceNodeWithMarkupByID=environment.replaceNodeWithMarkupByID;ReactComponentEnvironment.processChildrenUpdates=environment.processChildrenUpdates;injected=true;}}};module.exports=ReactComponentEnvironment;},{"135":135}],37:[function(_dereq_,module,exports){'use strict';var ReactComponentEnvironment=_dereq_(36);var ReactContext=_dereq_(38);var ReactCurrentOwner=_dereq_(39);var ReactElement=_dereq_(57);var ReactElementValidator=_dereq_(58);var ReactInstanceMap=_dereq_(67);var ReactLifeCycle=_dereq_(68);var ReactNativeComponent=_dereq_(73);var ReactPerf=_dereq_(75);var ReactPropTypeLocations=_dereq_(77);var ReactPropTypeLocationNames=_dereq_(76);var ReactReconciler=_dereq_(81);var ReactUpdates=_dereq_(87);var assign=_dereq_(27);var emptyObject=_dereq_(115);var invariant=_dereq_(135);var shouldUpdateReactComponent=_dereq_(151);var warning=_dereq_(154);function getDeclarationErrorAddendum(component){var owner=component._currentElement._owner||null;if(owner){var name=owner.getName();if(name){return' Check the render method of `'+name+'`.';}}
return'';}
var nextMountID=1;var ReactCompositeComponentMixin={construct:function(element){this._currentElement=element;this._rootNodeID=null;this._instance=null; this._pendingElement=null;this._pendingStateQueue=null;this._pendingReplaceState=false;this._pendingForceUpdate=false;this._renderedComponent=null;this._context=null;this._mountOrder=0;this._isTopLevel=false;this._pendingCallbacks=null;},mountComponent:function(rootID,transaction,context){this._context=context;this._mountOrder=nextMountID++;this._rootNodeID=rootID;var publicProps=this._processProps(this._currentElement.props);var publicContext=this._processContext(this._currentElement._context);var Component=ReactNativeComponent.getComponentClassForElement(this._currentElement); var inst=new Component(publicProps,publicContext);if("production"!=="development"){
("production"!=="development"?warning(inst.render!=null,'%s(...): No `render` method found on the returned component '+'instance: you may have forgotten to define `render` in your '+'component or you may have accidentally tried to render an element '+'whose type is a function that isn\'t a React component.',Component.displayName||Component.name||'Component'):null);}

inst.props=publicProps;inst.context=publicContext;inst.refs=emptyObject;this._instance=inst; ReactInstanceMap.set(inst,this);if("production"!=="development"){this._warnIfContextsDiffer(this._currentElement._context,context);}
if("production"!=="development"){

("production"!=="development"?warning(!inst.getInitialState||inst.getInitialState.isReactClassApproved,'getInitialState was defined on %s, a plain JavaScript class. '+'This is only supported for classes created using React.createClass. '+'Did you mean to define a state property instead?',this.getName()||'a component'):null);("production"!=="development"?warning(!inst.getDefaultProps||inst.getDefaultProps.isReactClassApproved,'getDefaultProps was defined on %s, a plain JavaScript class. '+'This is only supported for classes created using React.createClass. '+'Use a static property to define defaultProps instead.',this.getName()||'a component'):null);("production"!=="development"?warning(!inst.propTypes,'propTypes was defined as an instance property on %s. Use a static '+'property to define propTypes instead.',this.getName()||'a component'):null);("production"!=="development"?warning(!inst.contextTypes,'contextTypes was defined as an instance property on %s. Use a '+'static property to define contextTypes instead.',this.getName()||'a component'):null);("production"!=="development"?warning(typeof inst.componentShouldUpdate!=='function','%s has a method called '+'componentShouldUpdate(). Did you mean shouldComponentUpdate()? '+'The name is phrased as a question because the function is '+'expected to return a value.',(this.getName()||'A component')):null);}
var initialState=inst.state;if(initialState===undefined){inst.state=initialState=null;}
("production"!=="development"?invariant(typeof initialState==='object'&&!Array.isArray(initialState),'%s.state: must be set to an object or null',this.getName()||'ReactCompositeComponent'):invariant(typeof initialState==='object'&&!Array.isArray(initialState)));this._pendingStateQueue=null;this._pendingReplaceState=false;this._pendingForceUpdate=false;var renderedElement;var previouslyMounting=ReactLifeCycle.currentlyMountingInstance;ReactLifeCycle.currentlyMountingInstance=this;try{if(inst.componentWillMount){inst.componentWillMount();
if(this._pendingStateQueue){inst.state=this._processPendingState(inst.props,inst.context);}}
renderedElement=this._renderValidatedComponent();}finally{ReactLifeCycle.currentlyMountingInstance=previouslyMounting;}
this._renderedComponent=this._instantiateReactComponent(renderedElement,this._currentElement.type
);var markup=ReactReconciler.mountComponent(this._renderedComponent,rootID,transaction,this._processChildContext(context));if(inst.componentDidMount){transaction.getReactMountReady().enqueue(inst.componentDidMount,inst);}
return markup;},unmountComponent:function(){var inst=this._instance;if(inst.componentWillUnmount){var previouslyUnmounting=ReactLifeCycle.currentlyUnmountingInstance;ReactLifeCycle.currentlyUnmountingInstance=this;try{inst.componentWillUnmount();}finally{ReactLifeCycle.currentlyUnmountingInstance=previouslyUnmounting;}}
ReactReconciler.unmountComponent(this._renderedComponent);this._renderedComponent=null; this._pendingStateQueue=null;this._pendingReplaceState=false;this._pendingForceUpdate=false;this._pendingCallbacks=null;this._pendingElement=null;
this._context=null;this._rootNodeID=null;

ReactInstanceMap.remove(inst);
},_setPropsInternal:function(partialProps,callback){var element=this._pendingElement||this._currentElement;this._pendingElement=ReactElement.cloneAndReplaceProps(element,assign({},element.props,partialProps));ReactUpdates.enqueueUpdate(this,callback);},_maskContext:function(context){var maskedContext=null;if(typeof this._currentElement.type==='string'){return emptyObject;}
var contextTypes=this._currentElement.type.contextTypes;if(!contextTypes){return emptyObject;}
maskedContext={};for(var contextName in contextTypes){maskedContext[contextName]=context[contextName];}
return maskedContext;},_processContext:function(context){var maskedContext=this._maskContext(context);if("production"!=="development"){var Component=ReactNativeComponent.getComponentClassForElement(this._currentElement);if(Component.contextTypes){this._checkPropTypes(Component.contextTypes,maskedContext,ReactPropTypeLocations.context);}}
return maskedContext;},_processChildContext:function(currentContext){var inst=this._instance;var childContext=inst.getChildContext&&inst.getChildContext();if(childContext){("production"!=="development"?invariant(typeof inst.constructor.childContextTypes==='object','%s.getChildContext(): childContextTypes must be defined in order to '+'use getChildContext().',this.getName()||'ReactCompositeComponent'):invariant(typeof inst.constructor.childContextTypes==='object'));if("production"!=="development"){this._checkPropTypes(inst.constructor.childContextTypes,childContext,ReactPropTypeLocations.childContext);}
for(var name in childContext){("production"!=="development"?invariant(name in inst.constructor.childContextTypes,'%s.getChildContext(): key "%s" is not defined in childContextTypes.',this.getName()||'ReactCompositeComponent',name):invariant(name in inst.constructor.childContextTypes));}
return assign({},currentContext,childContext);}
return currentContext;},_processProps:function(newProps){if("production"!=="development"){var Component=ReactNativeComponent.getComponentClassForElement(this._currentElement);if(Component.propTypes){this._checkPropTypes(Component.propTypes,newProps,ReactPropTypeLocations.prop);}}
return newProps;},_checkPropTypes:function(propTypes,props,location){
var componentName=this.getName();for(var propName in propTypes){if(propTypes.hasOwnProperty(propName)){var error;try{
("production"!=="development"?invariant(typeof propTypes[propName]==='function','%s: %s type `%s` is invalid; it must be a function, usually '+'from React.PropTypes.',componentName||'React class',ReactPropTypeLocationNames[location],propName):invariant(typeof propTypes[propName]==='function'));error=propTypes[propName](props,propName,componentName,location);}catch(ex){error=ex;}
if(error instanceof Error){

 var addendum=getDeclarationErrorAddendum(this);if(location===ReactPropTypeLocations.prop){("production"!=="development"?warning(false,'Failed Composite propType: %s%s',error.message,addendum):null);}else{("production"!=="development"?warning(false,'Failed Context Types: %s%s',error.message,addendum):null);}}}}},receiveComponent:function(nextElement,transaction,nextContext){var prevElement=this._currentElement;var prevContext=this._context;this._pendingElement=null;this.updateComponent(transaction,prevElement,nextElement,prevContext,nextContext);},performUpdateIfNecessary:function(transaction){if(this._pendingElement!=null){ReactReconciler.receiveComponent(this,this._pendingElement||this._currentElement,transaction,this._context);}
if(this._pendingStateQueue!==null||this._pendingForceUpdate){if("production"!=="development"){ReactElementValidator.checkAndWarnForMutatedProps(this._currentElement);}
this.updateComponent(transaction,this._currentElement,this._currentElement,this._context,this._context);}},_warnIfContextsDiffer:function(ownerBasedContext,parentBasedContext){ownerBasedContext=this._maskContext(ownerBasedContext);parentBasedContext=this._maskContext(parentBasedContext);var parentKeys=Object.keys(parentBasedContext).sort();var displayName=this.getName()||'ReactCompositeComponent';for(var i=0;i<parentKeys.length;i++){var key=parentKeys[i];("production"!=="development"?warning(ownerBasedContext[key]===parentBasedContext[key],'owner-based and parent-based contexts differ '+'(values: `%s` vs `%s`) for key (%s) while mounting %s '+'(see: http://fb.me/react-context-by-parent)',ownerBasedContext[key],parentBasedContext[key],key,displayName):null);}},updateComponent:function(transaction,prevParentElement,nextParentElement,prevUnmaskedContext,nextUnmaskedContext){var inst=this._instance;var nextContext=inst.context;var nextProps=inst.props; if(prevParentElement!==nextParentElement){nextContext=this._processContext(nextParentElement._context);nextProps=this._processProps(nextParentElement.props);if("production"!=="development"){if(nextUnmaskedContext!=null){this._warnIfContextsDiffer(nextParentElement._context,nextUnmaskedContext);}}


if(inst.componentWillReceiveProps){inst.componentWillReceiveProps(nextProps,nextContext);}}
var nextState=this._processPendingState(nextProps,nextContext);var shouldUpdate=this._pendingForceUpdate||!inst.shouldComponentUpdate||inst.shouldComponentUpdate(nextProps,nextState,nextContext);if("production"!=="development"){("production"!=="development"?warning(typeof shouldUpdate!=='undefined','%s.shouldComponentUpdate(): Returned undefined instead of a '+'boolean value. Make sure to return true or false.',this.getName()||'ReactCompositeComponent'):null);}
if(shouldUpdate){this._pendingForceUpdate=false;this._performComponentUpdate(nextParentElement,nextProps,nextState,nextContext,transaction,nextUnmaskedContext);}else{
this._currentElement=nextParentElement;this._context=nextUnmaskedContext;inst.props=nextProps;inst.state=nextState;inst.context=nextContext;}},_processPendingState:function(props,context){var inst=this._instance;var queue=this._pendingStateQueue;var replace=this._pendingReplaceState;this._pendingReplaceState=false;this._pendingStateQueue=null;if(!queue){return inst.state;}
var nextState=assign({},replace?queue[0]:inst.state);for(var i=replace?1:0;i<queue.length;i++){var partial=queue[i];assign(nextState,typeof partial==='function'?partial.call(inst,nextState,props,context):partial);}
return nextState;},_performComponentUpdate:function(nextElement,nextProps,nextState,nextContext,transaction,unmaskedContext){var inst=this._instance;var prevProps=inst.props;var prevState=inst.state;var prevContext=inst.context;if(inst.componentWillUpdate){inst.componentWillUpdate(nextProps,nextState,nextContext);}
this._currentElement=nextElement;this._context=unmaskedContext;inst.props=nextProps;inst.state=nextState;inst.context=nextContext;this._updateRenderedComponent(transaction,unmaskedContext);if(inst.componentDidUpdate){transaction.getReactMountReady().enqueue(inst.componentDidUpdate.bind(inst,prevProps,prevState,prevContext),inst);}},_updateRenderedComponent:function(transaction,context){var prevComponentInstance=this._renderedComponent;var prevRenderedElement=prevComponentInstance._currentElement;var nextRenderedElement=this._renderValidatedComponent();if(shouldUpdateReactComponent(prevRenderedElement,nextRenderedElement)){ReactReconciler.receiveComponent(prevComponentInstance,nextRenderedElement,transaction,this._processChildContext(context));}else{var thisID=this._rootNodeID;var prevComponentID=prevComponentInstance._rootNodeID;ReactReconciler.unmountComponent(prevComponentInstance);this._renderedComponent=this._instantiateReactComponent(nextRenderedElement,this._currentElement.type);var nextMarkup=ReactReconciler.mountComponent(this._renderedComponent,thisID,transaction,this._processChildContext(context));this._replaceNodeWithMarkupByID(prevComponentID,nextMarkup);}},_replaceNodeWithMarkupByID:function(prevComponentID,nextMarkup){ReactComponentEnvironment.replaceNodeWithMarkupByID(prevComponentID,nextMarkup);},_renderValidatedComponentWithoutOwnerOrContext:function(){var inst=this._instance;var renderedComponent=inst.render();if("production"!=="development"){if(typeof renderedComponent==='undefined'&&inst.render._isMockFunction){
renderedComponent=null;}}
return renderedComponent;},_renderValidatedComponent:function(){var renderedComponent;var previousContext=ReactContext.current;ReactContext.current=this._processChildContext(this._currentElement._context);ReactCurrentOwner.current=this;try{renderedComponent=this._renderValidatedComponentWithoutOwnerOrContext();}finally{ReactContext.current=previousContext;ReactCurrentOwner.current=null;}
("production"!=="development"?invariant( renderedComponent===null||renderedComponent===false||ReactElement.isValidElement(renderedComponent),'%s.render(): A valid ReactComponent must be returned. You may have '+'returned undefined, an array or some other invalid object.',this.getName()||'ReactCompositeComponent'):invariant( renderedComponent===null||renderedComponent===false||ReactElement.isValidElement(renderedComponent)));return renderedComponent;},attachRef:function(ref,component){var inst=this.getPublicInstance();var refs=inst.refs===emptyObject?(inst.refs={}):inst.refs;refs[ref]=component.getPublicInstance();},detachRef:function(ref){var refs=this.getPublicInstance().refs;delete refs[ref];},getName:function(){var type=this._currentElement.type;var constructor=this._instance&&this._instance.constructor;return(type.displayName||(constructor&&constructor.displayName)||type.name||(constructor&&constructor.name)||null);},getPublicInstance:function(){return this._instance;}, _instantiateReactComponent:null};ReactPerf.measureMethods(ReactCompositeComponentMixin,'ReactCompositeComponent',{mountComponent:'mountComponent',updateComponent:'updateComponent',_renderValidatedComponent:'_renderValidatedComponent'});var ReactCompositeComponent={Mixin:ReactCompositeComponentMixin};module.exports=ReactCompositeComponent;},{"115":115,"135":135,"151":151,"154":154,"27":27,"36":36,"38":38,"39":39,"57":57,"58":58,"67":67,"68":68,"73":73,"75":75,"76":76,"77":77,"81":81,"87":87}],38:[function(_dereq_,module,exports){'use strict';var assign=_dereq_(27);var emptyObject=_dereq_(115);var warning=_dereq_(154);var didWarn=false;var ReactContext={current:emptyObject,withContext:function(newContext,scopedCallback){if("production"!=="development"){("production"!=="development"?warning(didWarn,'withContext is deprecated and will be removed in a future version. '+'Use a wrapper component with getChildContext instead.'):null);didWarn=true;}
var result;var previousContext=ReactContext.current;ReactContext.current=assign({},previousContext,newContext);try{result=scopedCallback();}finally{ReactContext.current=previousContext;}
return result;}};module.exports=ReactContext;},{"115":115,"154":154,"27":27}],39:[function(_dereq_,module,exports){'use strict';var ReactCurrentOwner={current:null};module.exports=ReactCurrentOwner;},{}],40:[function(_dereq_,module,exports){'use strict';var ReactElement=_dereq_(57);var ReactElementValidator=_dereq_(58);var mapObject=_dereq_(142);function createDOMFactory(tag){if("production"!=="development"){return ReactElementValidator.createFactory(tag);}
return ReactElement.createFactory(tag);}
var ReactDOM=mapObject({a:'a',abbr:'abbr',address:'address',area:'area',article:'article',aside:'aside',audio:'audio',b:'b',base:'base',bdi:'bdi',bdo:'bdo',big:'big',blockquote:'blockquote',body:'body',br:'br',button:'button',canvas:'canvas',caption:'caption',cite:'cite',code:'code',col:'col',colgroup:'colgroup',data:'data',datalist:'datalist',dd:'dd',del:'del',details:'details',dfn:'dfn',dialog:'dialog',div:'div',dl:'dl',dt:'dt',em:'em',embed:'embed',fieldset:'fieldset',figcaption:'figcaption',figure:'figure',footer:'footer',form:'form',h1:'h1',h2:'h2',h3:'h3',h4:'h4',h5:'h5',h6:'h6',head:'head',header:'header',hr:'hr',html:'html',i:'i',iframe:'iframe',img:'img',input:'input',ins:'ins',kbd:'kbd',keygen:'keygen',label:'label',legend:'legend',li:'li',link:'link',main:'main',map:'map',mark:'mark',menu:'menu',menuitem:'menuitem',meta:'meta',meter:'meter',nav:'nav',noscript:'noscript',object:'object',ol:'ol',optgroup:'optgroup',option:'option',output:'output',p:'p',param:'param',picture:'picture',pre:'pre',progress:'progress',q:'q',rp:'rp',rt:'rt',ruby:'ruby',s:'s',samp:'samp',script:'script',section:'section',select:'select',small:'small',source:'source',span:'span',strong:'strong',style:'style',sub:'sub',summary:'summary',sup:'sup',table:'table',tbody:'tbody',td:'td',textarea:'textarea',tfoot:'tfoot',th:'th',thead:'thead',time:'time',title:'title',tr:'tr',track:'track',u:'u',ul:'ul','var':'var',video:'video',wbr:'wbr', circle:'circle',defs:'defs',ellipse:'ellipse',g:'g',line:'line',linearGradient:'linearGradient',mask:'mask',path:'path',pattern:'pattern',polygon:'polygon',polyline:'polyline',radialGradient:'radialGradient',rect:'rect',stop:'stop',svg:'svg',text:'text',tspan:'tspan'},createDOMFactory);module.exports=ReactDOM;},{"142":142,"57":57,"58":58}],41:[function(_dereq_,module,exports){'use strict';var AutoFocusMixin=_dereq_(2);var ReactBrowserComponentMixin=_dereq_(29);var ReactClass=_dereq_(33);var ReactElement=_dereq_(57);var keyMirror=_dereq_(140);var button=ReactElement.createFactory('button');var mouseListenerNames=keyMirror({onClick:true,onDoubleClick:true,onMouseDown:true,onMouseMove:true,onMouseUp:true,onClickCapture:true,onDoubleClickCapture:true,onMouseDownCapture:true,onMouseMoveCapture:true,onMouseUpCapture:true});var ReactDOMButton=ReactClass.createClass({displayName:'ReactDOMButton',tagName:'BUTTON',mixins:[AutoFocusMixin,ReactBrowserComponentMixin],render:function(){var props={}; for(var key in this.props){if(this.props.hasOwnProperty(key)&&(!this.props.disabled||!mouseListenerNames[key])){props[key]=this.props[key];}}
return button(props,this.props.children);}});module.exports=ReactDOMButton;},{"140":140,"2":2,"29":29,"33":33,"57":57}],42:[function(_dereq_,module,exports){'use strict';var CSSPropertyOperations=_dereq_(5);var DOMProperty=_dereq_(10);var DOMPropertyOperations=_dereq_(11);var ReactBrowserEventEmitter=_dereq_(30);var ReactComponentBrowserEnvironment=_dereq_(35);var ReactMount=_dereq_(70);var ReactMultiChild=_dereq_(71);var ReactPerf=_dereq_(75);var assign=_dereq_(27);var escapeTextContentForBrowser=_dereq_(116);var invariant=_dereq_(135);var isEventSupported=_dereq_(136);var keyOf=_dereq_(141);var warning=_dereq_(154);var deleteListener=ReactBrowserEventEmitter.deleteListener;var listenTo=ReactBrowserEventEmitter.listenTo;var registrationNameModules=ReactBrowserEventEmitter.registrationNameModules;var CONTENT_TYPES={'string':true,'number':true};var STYLE=keyOf({style:null});var ELEMENT_NODE_TYPE=1;var BackendIDOperations=null;function assertValidProps(props){if(!props){return;}
if(props.dangerouslySetInnerHTML!=null){("production"!=="development"?invariant(props.children==null,'Can only set one of `children` or `props.dangerouslySetInnerHTML`.'):invariant(props.children==null));("production"!=="development"?invariant(props.dangerouslySetInnerHTML.__html!=null,'`props.dangerouslySetInnerHTML` must be in the form `{__html: ...}`. '+'Please visit http://fb.me/react-invariant-dangerously-set-inner-html '+'for more information.'):invariant(props.dangerouslySetInnerHTML.__html!=null));}
if("production"!=="development"){("production"!=="development"?warning(props.innerHTML==null,'Directly setting property `innerHTML` is not permitted. '+'For more information, lookup documentation on `dangerouslySetInnerHTML`.'):null);("production"!=="development"?warning(!props.contentEditable||props.children==null,'A component is `contentEditable` and contains `children` managed by '+'React. It is now your responsibility to guarantee that none of '+'those nodes are unexpectedly modified or duplicated. This is '+'probably not intentional.'):null);}
("production"!=="development"?invariant(props.style==null||typeof props.style==='object','The `style` prop expects a mapping from style properties to values, '+'not a string. For example, style={{marginRight: spacing + \'em\'}} when '+'using JSX.'):invariant(props.style==null||typeof props.style==='object'));}
function putListener(id,registrationName,listener,transaction){if("production"!=="development"){
("production"!=="development"?warning(registrationName!=='onScroll'||isEventSupported('scroll',true),'This browser doesn\'t support the `onScroll` event'):null);}
var container=ReactMount.findReactContainerForID(id);if(container){var doc=container.nodeType===ELEMENT_NODE_TYPE?container.ownerDocument:container;listenTo(registrationName,doc);}
transaction.getPutListenerQueue().enqueuePutListener(id,registrationName,listener);}

var omittedCloseTags={'area':true,'base':true,'br':true,'col':true,'embed':true,'hr':true,'img':true,'input':true,'keygen':true,'link':true,'meta':true,'param':true,'source':true,'track':true,'wbr':true

};
var VALID_TAG_REGEX=/^[a-zA-Z][a-zA-Z:_\.\-\d]*$/;var validatedTagCache={};var hasOwnProperty={}.hasOwnProperty;function validateDangerousTag(tag){if(!hasOwnProperty.call(validatedTagCache,tag)){("production"!=="development"?invariant(VALID_TAG_REGEX.test(tag),'Invalid tag: %s',tag):invariant(VALID_TAG_REGEX.test(tag)));validatedTagCache[tag]=true;}}
function ReactDOMComponent(tag){validateDangerousTag(tag);this._tag=tag;this._renderedChildren=null;this._previousStyleCopy=null;this._rootNodeID=null;}
ReactDOMComponent.displayName='ReactDOMComponent';ReactDOMComponent.Mixin={construct:function(element){this._currentElement=element;},mountComponent:function(rootID,transaction,context){this._rootNodeID=rootID;assertValidProps(this._currentElement.props);var closeTag=omittedCloseTags[this._tag]?'':'</'+this._tag+'>';return(this._createOpenTagMarkupAndPutListeners(transaction)+
this._createContentMarkup(transaction,context)+
closeTag);},_createOpenTagMarkupAndPutListeners:function(transaction){var props=this._currentElement.props;var ret='<'+this._tag;for(var propKey in props){if(!props.hasOwnProperty(propKey)){continue;}
var propValue=props[propKey];if(propValue==null){continue;}
if(registrationNameModules.hasOwnProperty(propKey)){putListener(this._rootNodeID,propKey,propValue,transaction);}else{if(propKey===STYLE){if(propValue){propValue=this._previousStyleCopy=assign({},props.style);}
propValue=CSSPropertyOperations.createMarkupForStyles(propValue);}
var markup=DOMPropertyOperations.createMarkupForProperty(propKey,propValue);if(markup){ret+=' '+markup;}}}

if(transaction.renderToStaticMarkup){return ret+'>';}
var markupForID=DOMPropertyOperations.createMarkupForID(this._rootNodeID);return ret+' '+markupForID+'>';},_createContentMarkup:function(transaction,context){var prefix='';if(this._tag==='listing'||this._tag==='pre'||this._tag==='textarea'){

prefix='\n';}
var props=this._currentElement.props;var innerHTML=props.dangerouslySetInnerHTML;if(innerHTML!=null){if(innerHTML.__html!=null){return prefix+innerHTML.__html;}}else{var contentToUse=CONTENT_TYPES[typeof props.children]?props.children:null;var childrenToUse=contentToUse!=null?null:props.children;if(contentToUse!=null){return prefix+escapeTextContentForBrowser(contentToUse);}else if(childrenToUse!=null){var mountImages=this.mountChildren(childrenToUse,transaction,context);return prefix+mountImages.join('');}}
return prefix;},receiveComponent:function(nextElement,transaction,context){var prevElement=this._currentElement;this._currentElement=nextElement;this.updateComponent(transaction,prevElement,nextElement,context);},updateComponent:function(transaction,prevElement,nextElement,context){assertValidProps(this._currentElement.props);this._updateDOMProperties(prevElement.props,transaction);this._updateDOMChildren(prevElement.props,transaction,context);},_updateDOMProperties:function(lastProps,transaction){var nextProps=this._currentElement.props;var propKey;var styleName;var styleUpdates;for(propKey in lastProps){if(nextProps.hasOwnProperty(propKey)||!lastProps.hasOwnProperty(propKey)){continue;}
if(propKey===STYLE){var lastStyle=this._previousStyleCopy;for(styleName in lastStyle){if(lastStyle.hasOwnProperty(styleName)){styleUpdates=styleUpdates||{};styleUpdates[styleName]='';}}
this._previousStyleCopy=null;}else if(registrationNameModules.hasOwnProperty(propKey)){deleteListener(this._rootNodeID,propKey);}else if(DOMProperty.isStandardName[propKey]||DOMProperty.isCustomAttribute(propKey)){BackendIDOperations.deletePropertyByID(this._rootNodeID,propKey);}}
for(propKey in nextProps){var nextProp=nextProps[propKey];var lastProp=propKey===STYLE?this._previousStyleCopy:lastProps[propKey];if(!nextProps.hasOwnProperty(propKey)||nextProp===lastProp){continue;}
if(propKey===STYLE){if(nextProp){nextProp=this._previousStyleCopy=assign({},nextProp);}else{this._previousStyleCopy=null;}
if(lastProp){for(styleName in lastProp){if(lastProp.hasOwnProperty(styleName)&&(!nextProp||!nextProp.hasOwnProperty(styleName))){styleUpdates=styleUpdates||{};styleUpdates[styleName]='';}}
for(styleName in nextProp){if(nextProp.hasOwnProperty(styleName)&&lastProp[styleName]!==nextProp[styleName]){styleUpdates=styleUpdates||{};styleUpdates[styleName]=nextProp[styleName];}}}else{styleUpdates=nextProp;}}else if(registrationNameModules.hasOwnProperty(propKey)){putListener(this._rootNodeID,propKey,nextProp,transaction);}else if(DOMProperty.isStandardName[propKey]||DOMProperty.isCustomAttribute(propKey)){BackendIDOperations.updatePropertyByID(this._rootNodeID,propKey,nextProp);}}
if(styleUpdates){BackendIDOperations.updateStylesByID(this._rootNodeID,styleUpdates);}},_updateDOMChildren:function(lastProps,transaction,context){var nextProps=this._currentElement.props;var lastContent=CONTENT_TYPES[typeof lastProps.children]?lastProps.children:null;var nextContent=CONTENT_TYPES[typeof nextProps.children]?nextProps.children:null;var lastHtml=lastProps.dangerouslySetInnerHTML&&lastProps.dangerouslySetInnerHTML.__html;var nextHtml=nextProps.dangerouslySetInnerHTML&&nextProps.dangerouslySetInnerHTML.__html;var lastChildren=lastContent!=null?null:lastProps.children;var nextChildren=nextContent!=null?null:nextProps.children;
 var lastHasContentOrHtml=lastContent!=null||lastHtml!=null;var nextHasContentOrHtml=nextContent!=null||nextHtml!=null;if(lastChildren!=null&&nextChildren==null){this.updateChildren(null,transaction,context);}else if(lastHasContentOrHtml&&!nextHasContentOrHtml){this.updateTextContent('');}
if(nextContent!=null){if(lastContent!==nextContent){this.updateTextContent(''+nextContent);}}else if(nextHtml!=null){if(lastHtml!==nextHtml){BackendIDOperations.updateInnerHTMLByID(this._rootNodeID,nextHtml);}}else if(nextChildren!=null){this.updateChildren(nextChildren,transaction,context);}},unmountComponent:function(){this.unmountChildren();ReactBrowserEventEmitter.deleteAllListeners(this._rootNodeID);ReactComponentBrowserEnvironment.unmountIDFromEnvironment(this._rootNodeID);this._rootNodeID=null;}};ReactPerf.measureMethods(ReactDOMComponent,'ReactDOMComponent',{mountComponent:'mountComponent',updateComponent:'updateComponent'});assign(ReactDOMComponent.prototype,ReactDOMComponent.Mixin,ReactMultiChild.Mixin);ReactDOMComponent.injection={injectIDOperations:function(IDOperations){ReactDOMComponent.BackendIDOperations=BackendIDOperations=IDOperations;}};module.exports=ReactDOMComponent;},{"10":10,"11":11,"116":116,"135":135,"136":136,"141":141,"154":154,"27":27,"30":30,"35":35,"5":5,"70":70,"71":71,"75":75}],43:[function(_dereq_,module,exports){'use strict';var EventConstants=_dereq_(15);var LocalEventTrapMixin=_dereq_(25);var ReactBrowserComponentMixin=_dereq_(29);var ReactClass=_dereq_(33);var ReactElement=_dereq_(57);var form=ReactElement.createFactory('form');var ReactDOMForm=ReactClass.createClass({displayName:'ReactDOMForm',tagName:'FORM',mixins:[ReactBrowserComponentMixin,LocalEventTrapMixin],render:function(){
return form(this.props);},componentDidMount:function(){this.trapBubbledEvent(EventConstants.topLevelTypes.topReset,'reset');this.trapBubbledEvent(EventConstants.topLevelTypes.topSubmit,'submit');}});module.exports=ReactDOMForm;},{"15":15,"25":25,"29":29,"33":33,"57":57}],44:[function(_dereq_,module,exports){'use strict';var CSSPropertyOperations=_dereq_(5);var DOMChildrenOperations=_dereq_(9);var DOMPropertyOperations=_dereq_(11);var ReactMount=_dereq_(70);var ReactPerf=_dereq_(75);var invariant=_dereq_(135);var setInnerHTML=_dereq_(148);var INVALID_PROPERTY_ERRORS={dangerouslySetInnerHTML:'`dangerouslySetInnerHTML` must be set using `updateInnerHTMLByID()`.',style:'`style` must be set using `updateStylesByID()`.'};var ReactDOMIDOperations={updatePropertyByID:function(id,name,value){var node=ReactMount.getNode(id);("production"!=="development"?invariant(!INVALID_PROPERTY_ERRORS.hasOwnProperty(name),'updatePropertyByID(...): %s',INVALID_PROPERTY_ERRORS[name]):invariant(!INVALID_PROPERTY_ERRORS.hasOwnProperty(name)));

if(value!=null){DOMPropertyOperations.setValueForProperty(node,name,value);}else{DOMPropertyOperations.deleteValueForProperty(node,name);}},deletePropertyByID:function(id,name,value){var node=ReactMount.getNode(id);("production"!=="development"?invariant(!INVALID_PROPERTY_ERRORS.hasOwnProperty(name),'updatePropertyByID(...): %s',INVALID_PROPERTY_ERRORS[name]):invariant(!INVALID_PROPERTY_ERRORS.hasOwnProperty(name)));DOMPropertyOperations.deleteValueForProperty(node,name,value);},updateStylesByID:function(id,styles){var node=ReactMount.getNode(id);CSSPropertyOperations.setValueForStyles(node,styles);},updateInnerHTMLByID:function(id,html){var node=ReactMount.getNode(id);setInnerHTML(node,html);},updateTextContentByID:function(id,content){var node=ReactMount.getNode(id);DOMChildrenOperations.updateTextContent(node,content);},dangerouslyReplaceNodeWithMarkupByID:function(id,markup){var node=ReactMount.getNode(id);DOMChildrenOperations.dangerouslyReplaceNodeWithMarkup(node,markup);},dangerouslyProcessChildrenUpdates:function(updates,markup){for(var i=0;i<updates.length;i++){updates[i].parentNode=ReactMount.getNode(updates[i].parentID);}
DOMChildrenOperations.processUpdates(updates,markup);}};ReactPerf.measureMethods(ReactDOMIDOperations,'ReactDOMIDOperations',{updatePropertyByID:'updatePropertyByID',deletePropertyByID:'deletePropertyByID',updateStylesByID:'updateStylesByID',updateInnerHTMLByID:'updateInnerHTMLByID',updateTextContentByID:'updateTextContentByID',dangerouslyReplaceNodeWithMarkupByID:'dangerouslyReplaceNodeWithMarkupByID',dangerouslyProcessChildrenUpdates:'dangerouslyProcessChildrenUpdates'});module.exports=ReactDOMIDOperations;},{"11":11,"135":135,"148":148,"5":5,"70":70,"75":75,"9":9}],45:[function(_dereq_,module,exports){'use strict';var EventConstants=_dereq_(15);var LocalEventTrapMixin=_dereq_(25);var ReactBrowserComponentMixin=_dereq_(29);var ReactClass=_dereq_(33);var ReactElement=_dereq_(57);var iframe=ReactElement.createFactory('iframe');var ReactDOMIframe=ReactClass.createClass({displayName:'ReactDOMIframe',tagName:'IFRAME',mixins:[ReactBrowserComponentMixin,LocalEventTrapMixin],render:function(){return iframe(this.props);},componentDidMount:function(){this.trapBubbledEvent(EventConstants.topLevelTypes.topLoad,'load');}});module.exports=ReactDOMIframe;},{"15":15,"25":25,"29":29,"33":33,"57":57}],46:[function(_dereq_,module,exports){'use strict';var EventConstants=_dereq_(15);var LocalEventTrapMixin=_dereq_(25);var ReactBrowserComponentMixin=_dereq_(29);var ReactClass=_dereq_(33);var ReactElement=_dereq_(57);var img=ReactElement.createFactory('img');var ReactDOMImg=ReactClass.createClass({displayName:'ReactDOMImg',tagName:'IMG',mixins:[ReactBrowserComponentMixin,LocalEventTrapMixin],render:function(){return img(this.props);},componentDidMount:function(){this.trapBubbledEvent(EventConstants.topLevelTypes.topLoad,'load');this.trapBubbledEvent(EventConstants.topLevelTypes.topError,'error');}});module.exports=ReactDOMImg;},{"15":15,"25":25,"29":29,"33":33,"57":57}],47:[function(_dereq_,module,exports){'use strict';var AutoFocusMixin=_dereq_(2);var DOMPropertyOperations=_dereq_(11);var LinkedValueUtils=_dereq_(24);var ReactBrowserComponentMixin=_dereq_(29);var ReactClass=_dereq_(33);var ReactElement=_dereq_(57);var ReactMount=_dereq_(70);var ReactUpdates=_dereq_(87);var assign=_dereq_(27);var invariant=_dereq_(135);var input=ReactElement.createFactory('input');var instancesByReactID={};function forceUpdateIfMounted(){if(this.isMounted()){this.forceUpdate();}}
var ReactDOMInput=ReactClass.createClass({displayName:'ReactDOMInput',tagName:'INPUT',mixins:[AutoFocusMixin,LinkedValueUtils.Mixin,ReactBrowserComponentMixin],getInitialState:function(){var defaultValue=this.props.defaultValue;return{initialChecked:this.props.defaultChecked||false,initialValue:defaultValue!=null?defaultValue:null};},render:function(){var props=assign({},this.props);props.defaultChecked=null;props.defaultValue=null;var value=LinkedValueUtils.getValue(this);props.value=value!=null?value:this.state.initialValue;var checked=LinkedValueUtils.getChecked(this);props.checked=checked!=null?checked:this.state.initialChecked;props.onChange=this._handleChange;return input(props,this.props.children);},componentDidMount:function(){var id=ReactMount.getID(this.getDOMNode());instancesByReactID[id]=this;},componentWillUnmount:function(){var rootNode=this.getDOMNode();var id=ReactMount.getID(rootNode);delete instancesByReactID[id];},componentDidUpdate:function(prevProps,prevState,prevContext){var rootNode=this.getDOMNode();if(this.props.checked!=null){DOMPropertyOperations.setValueForProperty(rootNode,'checked',this.props.checked||false);}
var value=LinkedValueUtils.getValue(this);if(value!=null){
DOMPropertyOperations.setValueForProperty(rootNode,'value',''+value);}},_handleChange:function(event){var returnValue;var onChange=LinkedValueUtils.getOnChange(this);if(onChange){returnValue=onChange.call(this,event);}
 
ReactUpdates.asap(forceUpdateIfMounted,this);var name=this.props.name;if(this.props.type==='radio'&&name!=null){var rootNode=this.getDOMNode();var queryRoot=rootNode;while(queryRoot.parentNode){queryRoot=queryRoot.parentNode;}




var group=queryRoot.querySelectorAll('input[name='+JSON.stringify(''+name)+'][type="radio"]');for(var i=0,groupLen=group.length;i<groupLen;i++){var otherNode=group[i];if(otherNode===rootNode||otherNode.form!==rootNode.form){continue;}
var otherID=ReactMount.getID(otherNode);("production"!=="development"?invariant(otherID,'ReactDOMInput: Mixing React and non-React radio inputs with the '+'same `name` is not supported.'):invariant(otherID));var otherInstance=instancesByReactID[otherID];("production"!=="development"?invariant(otherInstance,'ReactDOMInput: Unknown radio button ID %s.',otherID):invariant(otherInstance));

ReactUpdates.asap(forceUpdateIfMounted,otherInstance);}}
return returnValue;}});module.exports=ReactDOMInput;},{"11":11,"135":135,"2":2,"24":24,"27":27,"29":29,"33":33,"57":57,"70":70,"87":87}],48:[function(_dereq_,module,exports){'use strict';var ReactBrowserComponentMixin=_dereq_(29);var ReactClass=_dereq_(33);var ReactElement=_dereq_(57);var warning=_dereq_(154);var option=ReactElement.createFactory('option');var ReactDOMOption=ReactClass.createClass({displayName:'ReactDOMOption',tagName:'OPTION',mixins:[ReactBrowserComponentMixin],componentWillMount:function(){if("production"!=="development"){("production"!=="development"?warning(this.props.selected==null,'Use the `defaultValue` or `value` props on <select> instead of '+'setting `selected` on <option>.'):null);}},render:function(){return option(this.props,this.props.children);}});module.exports=ReactDOMOption;},{"154":154,"29":29,"33":33,"57":57}],49:[function(_dereq_,module,exports){'use strict';var AutoFocusMixin=_dereq_(2);var LinkedValueUtils=_dereq_(24);var ReactBrowserComponentMixin=_dereq_(29);var ReactClass=_dereq_(33);var ReactElement=_dereq_(57);var ReactUpdates=_dereq_(87);var assign=_dereq_(27);var select=ReactElement.createFactory('select');function updateOptionsIfPendingUpdateAndMounted(){if(this._pendingUpdate){this._pendingUpdate=false;var value=LinkedValueUtils.getValue(this);if(value!=null&&this.isMounted()){updateOptions(this,value);}}}
function selectValueType(props,propName,componentName){if(props[propName]==null){return null;}
if(props.multiple){if(!Array.isArray(props[propName])){return new Error(("The `"+propName+"` prop supplied to <select> must be an array if ")+
("`multiple` is true."));}}else{if(Array.isArray(props[propName])){return new Error(("The `"+propName+"` prop supplied to <select> must be a scalar ")+
("value if `multiple` is false."));}}}
function updateOptions(component,propValue){var selectedValue,i,l;var options=component.getDOMNode().options;if(component.props.multiple){selectedValue={};for(i=0,l=propValue.length;i<l;i++){selectedValue[''+propValue[i]]=true;}
for(i=0,l=options.length;i<l;i++){var selected=selectedValue.hasOwnProperty(options[i].value);if(options[i].selected!==selected){options[i].selected=selected;}}}else{
selectedValue=''+propValue;for(i=0,l=options.length;i<l;i++){if(options[i].value===selectedValue){options[i].selected=true;return;}}
if(options.length){options[0].selected=true;}}}
var ReactDOMSelect=ReactClass.createClass({displayName:'ReactDOMSelect',tagName:'SELECT',mixins:[AutoFocusMixin,LinkedValueUtils.Mixin,ReactBrowserComponentMixin],propTypes:{defaultValue:selectValueType,value:selectValueType},render:function(){var props=assign({},this.props);props.onChange=this._handleChange;props.value=null;return select(props,this.props.children);},componentWillMount:function(){this._pendingUpdate=false;},componentDidMount:function(){var value=LinkedValueUtils.getValue(this);if(value!=null){updateOptions(this,value);}else if(this.props.defaultValue!=null){updateOptions(this,this.props.defaultValue);}},componentDidUpdate:function(prevProps){var value=LinkedValueUtils.getValue(this);if(value!=null){this._pendingUpdate=false;updateOptions(this,value);}else if(!prevProps.multiple!==!this.props.multiple){if(this.props.defaultValue!=null){updateOptions(this,this.props.defaultValue);}else{updateOptions(this,this.props.multiple?[]:'');}}},_handleChange:function(event){var returnValue;var onChange=LinkedValueUtils.getOnChange(this);if(onChange){returnValue=onChange.call(this,event);}
this._pendingUpdate=true;ReactUpdates.asap(updateOptionsIfPendingUpdateAndMounted,this);return returnValue;}});module.exports=ReactDOMSelect;},{"2":2,"24":24,"27":27,"29":29,"33":33,"57":57,"87":87}],50:[function(_dereq_,module,exports){'use strict';var ExecutionEnvironment=_dereq_(21);var getNodeForCharacterOffset=_dereq_(128);var getTextContentAccessor=_dereq_(130);function isCollapsed(anchorNode,anchorOffset,focusNode,focusOffset){return anchorNode===focusNode&&anchorOffset===focusOffset;}
function getIEOffsets(node){var selection=document.selection;var selectedRange=selection.createRange();var selectedLength=selectedRange.text.length;var fromStart=selectedRange.duplicate();fromStart.moveToElementText(node);fromStart.setEndPoint('EndToStart',selectedRange);var startOffset=fromStart.text.length;var endOffset=startOffset+selectedLength;return{start:startOffset,end:endOffset};}
function getModernOffsets(node){var selection=window.getSelection&&window.getSelection();if(!selection||selection.rangeCount===0){return null;}
var anchorNode=selection.anchorNode;var anchorOffset=selection.anchorOffset;var focusNode=selection.focusNode;var focusOffset=selection.focusOffset;var currentRange=selection.getRangeAt(0);
var isSelectionCollapsed=isCollapsed(selection.anchorNode,selection.anchorOffset,selection.focusNode,selection.focusOffset);var rangeLength=isSelectionCollapsed?0:currentRange.toString().length;var tempRange=currentRange.cloneRange();tempRange.selectNodeContents(node);tempRange.setEnd(currentRange.startContainer,currentRange.startOffset);var isTempRangeCollapsed=isCollapsed(tempRange.startContainer,tempRange.startOffset,tempRange.endContainer,tempRange.endOffset);var start=isTempRangeCollapsed?0:tempRange.toString().length;var end=start+rangeLength;var detectionRange=document.createRange();detectionRange.setStart(anchorNode,anchorOffset);detectionRange.setEnd(focusNode,focusOffset);var isBackward=detectionRange.collapsed;return{start:isBackward?end:start,end:isBackward?start:end};}
function setIEOffsets(node,offsets){var range=document.selection.createRange().duplicate();var start,end;if(typeof offsets.end==='undefined'){start=offsets.start;end=start;}else if(offsets.start>offsets.end){start=offsets.end;end=offsets.start;}else{start=offsets.start;end=offsets.end;}
range.moveToElementText(node);range.moveStart('character',start);range.setEndPoint('EndToStart',range);range.moveEnd('character',end-start);range.select();}
function setModernOffsets(node,offsets){if(!window.getSelection){return;}
var selection=window.getSelection();var length=node[getTextContentAccessor()].length;var start=Math.min(offsets.start,length);var end=typeof offsets.end==='undefined'?start:Math.min(offsets.end,length);if(!selection.extend&&start>end){var temp=end;end=start;start=temp;}
var startMarker=getNodeForCharacterOffset(node,start);var endMarker=getNodeForCharacterOffset(node,end);if(startMarker&&endMarker){var range=document.createRange();range.setStart(startMarker.node,startMarker.offset);selection.removeAllRanges();if(start>end){selection.addRange(range);selection.extend(endMarker.node,endMarker.offset);}else{range.setEnd(endMarker.node,endMarker.offset);selection.addRange(range);}}}
var useIEOffsets=(ExecutionEnvironment.canUseDOM&&'selection'in document&&!('getSelection'in window));var ReactDOMSelection={getOffsets:useIEOffsets?getIEOffsets:getModernOffsets,setOffsets:useIEOffsets?setIEOffsets:setModernOffsets};module.exports=ReactDOMSelection;},{"128":128,"130":130,"21":21}],51:[function(_dereq_,module,exports){'use strict';var DOMPropertyOperations=_dereq_(11);var ReactComponentBrowserEnvironment=_dereq_(35);var ReactDOMComponent=_dereq_(42);var assign=_dereq_(27);var escapeTextContentForBrowser=_dereq_(116);var ReactDOMTextComponent=function(props){};assign(ReactDOMTextComponent.prototype,{construct:function(text){ this._currentElement=text;this._stringText=''+text; this._rootNodeID=null;this._mountIndex=0;},mountComponent:function(rootID,transaction,context){this._rootNodeID=rootID;var escapedText=escapeTextContentForBrowser(this._stringText);if(transaction.renderToStaticMarkup){
return escapedText;}
return('<span '+DOMPropertyOperations.createMarkupForID(rootID)+'>'+
escapedText+'</span>');},receiveComponent:function(nextText,transaction){if(nextText!==this._currentElement){this._currentElement=nextText;var nextStringText=''+nextText;if(nextStringText!==this._stringText){

this._stringText=nextStringText;ReactDOMComponent.BackendIDOperations.updateTextContentByID(this._rootNodeID,nextStringText);}}},unmountComponent:function(){ReactComponentBrowserEnvironment.unmountIDFromEnvironment(this._rootNodeID);}});module.exports=ReactDOMTextComponent;},{"11":11,"116":116,"27":27,"35":35,"42":42}],52:[function(_dereq_,module,exports){'use strict';var AutoFocusMixin=_dereq_(2);var DOMPropertyOperations=_dereq_(11);var LinkedValueUtils=_dereq_(24);var ReactBrowserComponentMixin=_dereq_(29);var ReactClass=_dereq_(33);var ReactElement=_dereq_(57);var ReactUpdates=_dereq_(87);var assign=_dereq_(27);var invariant=_dereq_(135);var warning=_dereq_(154);var textarea=ReactElement.createFactory('textarea');function forceUpdateIfMounted(){if(this.isMounted()){this.forceUpdate();}}
var ReactDOMTextarea=ReactClass.createClass({displayName:'ReactDOMTextarea',tagName:'TEXTAREA',mixins:[AutoFocusMixin,LinkedValueUtils.Mixin,ReactBrowserComponentMixin],getInitialState:function(){var defaultValue=this.props.defaultValue;var children=this.props.children;if(children!=null){if("production"!=="development"){("production"!=="development"?warning(false,'Use the `defaultValue` or `value` props instead of setting '+'children on <textarea>.'):null);}
("production"!=="development"?invariant(defaultValue==null,'If you supply `defaultValue` on a <textarea>, do not pass children.'):invariant(defaultValue==null));if(Array.isArray(children)){("production"!=="development"?invariant(children.length<=1,'<textarea> can only have at most one child.'):invariant(children.length<=1));children=children[0];}
defaultValue=''+children;}
if(defaultValue==null){defaultValue='';}
var value=LinkedValueUtils.getValue(this);return{  
  
initialValue:''+(value!=null?value:defaultValue)};},render:function(){var props=assign({},this.props);("production"!=="development"?invariant(props.dangerouslySetInnerHTML==null,'`dangerouslySetInnerHTML` does not make sense on <textarea>.'):invariant(props.dangerouslySetInnerHTML==null));props.defaultValue=null;props.value=null;props.onChange=this._handleChange;
return textarea(props,this.state.initialValue);},componentDidUpdate:function(prevProps,prevState,prevContext){var value=LinkedValueUtils.getValue(this);if(value!=null){var rootNode=this.getDOMNode();
DOMPropertyOperations.setValueForProperty(rootNode,'value',''+value);}},_handleChange:function(event){var returnValue;var onChange=LinkedValueUtils.getOnChange(this);if(onChange){returnValue=onChange.call(this,event);}
ReactUpdates.asap(forceUpdateIfMounted,this);return returnValue;}});module.exports=ReactDOMTextarea;},{"11":11,"135":135,"154":154,"2":2,"24":24,"27":27,"29":29,"33":33,"57":57,"87":87}],53:[function(_dereq_,module,exports){'use strict';var ReactUpdates=_dereq_(87);var Transaction=_dereq_(103);var assign=_dereq_(27);var emptyFunction=_dereq_(114);var RESET_BATCHED_UPDATES={initialize:emptyFunction,close:function(){ReactDefaultBatchingStrategy.isBatchingUpdates=false;}};var FLUSH_BATCHED_UPDATES={initialize:emptyFunction,close:ReactUpdates.flushBatchedUpdates.bind(ReactUpdates)};var TRANSACTION_WRAPPERS=[FLUSH_BATCHED_UPDATES,RESET_BATCHED_UPDATES];function ReactDefaultBatchingStrategyTransaction(){this.reinitializeTransaction();}
assign(ReactDefaultBatchingStrategyTransaction.prototype,Transaction.Mixin,{getTransactionWrappers:function(){return TRANSACTION_WRAPPERS;}});var transaction=new ReactDefaultBatchingStrategyTransaction();var ReactDefaultBatchingStrategy={isBatchingUpdates:false,batchedUpdates:function(callback,a,b,c,d){var alreadyBatchingUpdates=ReactDefaultBatchingStrategy.isBatchingUpdates;ReactDefaultBatchingStrategy.isBatchingUpdates=true; if(alreadyBatchingUpdates){callback(a,b,c,d);}else{transaction.perform(callback,null,a,b,c,d);}}};module.exports=ReactDefaultBatchingStrategy;},{"103":103,"114":114,"27":27,"87":87}],54:[function(_dereq_,module,exports){'use strict';var BeforeInputEventPlugin=_dereq_(3);var ChangeEventPlugin=_dereq_(7);var ClientReactRootIndex=_dereq_(8);var DefaultEventPluginOrder=_dereq_(13);var EnterLeaveEventPlugin=_dereq_(14);var ExecutionEnvironment=_dereq_(21);var HTMLDOMPropertyConfig=_dereq_(23);var MobileSafariClickEventPlugin=_dereq_(26);var ReactBrowserComponentMixin=_dereq_(29);var ReactClass=_dereq_(33);var ReactComponentBrowserEnvironment=_dereq_(35);var ReactDefaultBatchingStrategy=_dereq_(53);var ReactDOMComponent=_dereq_(42);var ReactDOMButton=_dereq_(41);var ReactDOMForm=_dereq_(43);var ReactDOMImg=_dereq_(46);var ReactDOMIDOperations=_dereq_(44);var ReactDOMIframe=_dereq_(45);var ReactDOMInput=_dereq_(47);var ReactDOMOption=_dereq_(48);var ReactDOMSelect=_dereq_(49);var ReactDOMTextarea=_dereq_(52);var ReactDOMTextComponent=_dereq_(51);var ReactElement=_dereq_(57);var ReactEventListener=_dereq_(62);var ReactInjection=_dereq_(64);var ReactInstanceHandles=_dereq_(66);var ReactMount=_dereq_(70);var ReactReconcileTransaction=_dereq_(80);var SelectEventPlugin=_dereq_(89);var ServerReactRootIndex=_dereq_(90);var SimpleEventPlugin=_dereq_(91);var SVGDOMPropertyConfig=_dereq_(88);var createFullPageComponent=_dereq_(111);function autoGenerateWrapperClass(type){return ReactClass.createClass({tagName:type.toUpperCase(),render:function(){return new ReactElement(type,null,null,null,null,this.props);}});}
function inject(){ReactInjection.EventEmitter.injectReactEventListener(ReactEventListener);ReactInjection.EventPluginHub.injectEventPluginOrder(DefaultEventPluginOrder);ReactInjection.EventPluginHub.injectInstanceHandle(ReactInstanceHandles);ReactInjection.EventPluginHub.injectMount(ReactMount);ReactInjection.EventPluginHub.injectEventPluginsByName({SimpleEventPlugin:SimpleEventPlugin,EnterLeaveEventPlugin:EnterLeaveEventPlugin,ChangeEventPlugin:ChangeEventPlugin,MobileSafariClickEventPlugin:MobileSafariClickEventPlugin,SelectEventPlugin:SelectEventPlugin,BeforeInputEventPlugin:BeforeInputEventPlugin});ReactInjection.NativeComponent.injectGenericComponentClass(ReactDOMComponent);ReactInjection.NativeComponent.injectTextComponentClass(ReactDOMTextComponent);ReactInjection.NativeComponent.injectAutoWrapper(autoGenerateWrapperClass);
ReactInjection.Class.injectMixin(ReactBrowserComponentMixin);ReactInjection.NativeComponent.injectComponentClasses({'button':ReactDOMButton,'form':ReactDOMForm,'iframe':ReactDOMIframe,'img':ReactDOMImg,'input':ReactDOMInput,'option':ReactDOMOption,'select':ReactDOMSelect,'textarea':ReactDOMTextarea,'html':createFullPageComponent('html'),'head':createFullPageComponent('head'),'body':createFullPageComponent('body')});ReactInjection.DOMProperty.injectDOMPropertyConfig(HTMLDOMPropertyConfig);ReactInjection.DOMProperty.injectDOMPropertyConfig(SVGDOMPropertyConfig);ReactInjection.EmptyComponent.injectEmptyComponent('noscript');ReactInjection.Updates.injectReconcileTransaction(ReactReconcileTransaction);ReactInjection.Updates.injectBatchingStrategy(ReactDefaultBatchingStrategy);ReactInjection.RootIndex.injectCreateReactRootIndex(ExecutionEnvironment.canUseDOM?ClientReactRootIndex.createReactRootIndex:ServerReactRootIndex.createReactRootIndex);ReactInjection.Component.injectEnvironment(ReactComponentBrowserEnvironment);ReactInjection.DOMComponent.injectIDOperations(ReactDOMIDOperations);if("production"!=="development"){var url=(ExecutionEnvironment.canUseDOM&&window.location.href)||'';if((/[?&]react_perf\b/).test(url)){var ReactDefaultPerf=_dereq_(55);ReactDefaultPerf.start();}}}
module.exports={inject:inject};},{"111":111,"13":13,"14":14,"21":21,"23":23,"26":26,"29":29,"3":3,"33":33,"35":35,"41":41,"42":42,"43":43,"44":44,"45":45,"46":46,"47":47,"48":48,"49":49,"51":51,"52":52,"53":53,"55":55,"57":57,"62":62,"64":64,"66":66,"7":7,"70":70,"8":8,"80":80,"88":88,"89":89,"90":90,"91":91}],55:[function(_dereq_,module,exports){'use strict';var DOMProperty=_dereq_(10);var ReactDefaultPerfAnalysis=_dereq_(56);var ReactMount=_dereq_(70);var ReactPerf=_dereq_(75);var performanceNow=_dereq_(146);function roundFloat(val){return Math.floor(val*100)/100;}
function addValue(obj,key,val){obj[key]=(obj[key]||0)+val;}
var ReactDefaultPerf={_allMeasurements:[], _mountStack:[0],_injected:false,start:function(){if(!ReactDefaultPerf._injected){ReactPerf.injection.injectMeasure(ReactDefaultPerf.measure);}
ReactDefaultPerf._allMeasurements.length=0;ReactPerf.enableMeasure=true;},stop:function(){ReactPerf.enableMeasure=false;},getLastMeasurements:function(){return ReactDefaultPerf._allMeasurements;},printExclusive:function(measurements){measurements=measurements||ReactDefaultPerf._allMeasurements;var summary=ReactDefaultPerfAnalysis.getExclusiveSummary(measurements);console.table(summary.map(function(item){return{'Component class name':item.componentName,'Total inclusive time (ms)':roundFloat(item.inclusive),'Exclusive mount time (ms)':roundFloat(item.exclusive),'Exclusive render time (ms)':roundFloat(item.render),'Mount time per instance (ms)':roundFloat(item.exclusive/item.count),'Render time per instance (ms)':roundFloat(item.render/item.count),'Instances':item.count};}));
},printInclusive:function(measurements){measurements=measurements||ReactDefaultPerf._allMeasurements;var summary=ReactDefaultPerfAnalysis.getInclusiveSummary(measurements);console.table(summary.map(function(item){return{'Owner > component':item.componentName,'Inclusive time (ms)':roundFloat(item.time),'Instances':item.count};}));console.log('Total time:',ReactDefaultPerfAnalysis.getTotalTime(measurements).toFixed(2)+' ms');},getMeasurementsSummaryMap:function(measurements){var summary=ReactDefaultPerfAnalysis.getInclusiveSummary(measurements,true);return summary.map(function(item){return{'Owner > component':item.componentName,'Wasted time (ms)':item.time,'Instances':item.count};});},printWasted:function(measurements){measurements=measurements||ReactDefaultPerf._allMeasurements;console.table(ReactDefaultPerf.getMeasurementsSummaryMap(measurements));console.log('Total time:',ReactDefaultPerfAnalysis.getTotalTime(measurements).toFixed(2)+' ms');},printDOM:function(measurements){measurements=measurements||ReactDefaultPerf._allMeasurements;var summary=ReactDefaultPerfAnalysis.getDOMSummary(measurements);console.table(summary.map(function(item){var result={};result[DOMProperty.ID_ATTRIBUTE_NAME]=item.id;result['type']=item.type;result['args']=JSON.stringify(item.args);return result;}));console.log('Total time:',ReactDefaultPerfAnalysis.getTotalTime(measurements).toFixed(2)+' ms');},_recordWrite:function(id,fnName,totalTime,args){ var writes=ReactDefaultPerf._allMeasurements[ReactDefaultPerf._allMeasurements.length-1].writes;writes[id]=writes[id]||[];writes[id].push({type:fnName,time:totalTime,args:args});},measure:function(moduleName,fnName,func){return function(){for(var args=[],$__0=0,$__1=arguments.length;$__0<$__1;$__0++)args.push(arguments[$__0]);var totalTime;var rv;var start;if(fnName==='_renderNewRootComponent'||fnName==='flushBatchedUpdates'){


ReactDefaultPerf._allMeasurements.push({exclusive:{},inclusive:{},render:{},counts:{},writes:{},displayNames:{},totalTime:0});start=performanceNow();rv=func.apply(this,args);ReactDefaultPerf._allMeasurements[ReactDefaultPerf._allMeasurements.length-1].totalTime=performanceNow()-start;return rv;}else if(fnName==='_mountImageIntoNode'||moduleName==='ReactDOMIDOperations'){start=performanceNow();rv=func.apply(this,args);totalTime=performanceNow()-start;if(fnName==='_mountImageIntoNode'){var mountID=ReactMount.getID(args[1]);ReactDefaultPerf._recordWrite(mountID,fnName,totalTime,args[0]);}else if(fnName==='dangerouslyProcessChildrenUpdates'){ args[0].forEach(function(update){var writeArgs={};if(update.fromIndex!==null){writeArgs.fromIndex=update.fromIndex;}
if(update.toIndex!==null){writeArgs.toIndex=update.toIndex;}
if(update.textContent!==null){writeArgs.textContent=update.textContent;}
if(update.markupIndex!==null){writeArgs.markup=args[1][update.markupIndex];}
ReactDefaultPerf._recordWrite(update.parentID,update.type,totalTime,writeArgs);});}else{ ReactDefaultPerf._recordWrite(args[0],fnName,totalTime,Array.prototype.slice.call(args,1));}
return rv;}else if(moduleName==='ReactCompositeComponent'&&(((fnName==='mountComponent'||fnName==='updateComponent'||fnName==='_renderValidatedComponent')))){if(typeof this._currentElement.type==='string'){return func.apply(this,args);}
var rootNodeID=fnName==='mountComponent'?args[0]:this._rootNodeID;var isRender=fnName==='_renderValidatedComponent';var isMount=fnName==='mountComponent';var mountStack=ReactDefaultPerf._mountStack;var entry=ReactDefaultPerf._allMeasurements[ReactDefaultPerf._allMeasurements.length-1];if(isRender){addValue(entry.counts,rootNodeID,1);}else if(isMount){mountStack.push(0);}
start=performanceNow();rv=func.apply(this,args);totalTime=performanceNow()-start;if(isRender){addValue(entry.render,rootNodeID,totalTime);}else if(isMount){var subMountTime=mountStack.pop();mountStack[mountStack.length-1]+=totalTime;addValue(entry.exclusive,rootNodeID,totalTime-subMountTime);addValue(entry.inclusive,rootNodeID,totalTime);}else{addValue(entry.inclusive,rootNodeID,totalTime);}
entry.displayNames[rootNodeID]={current:this.getName(),owner:this._currentElement._owner?this._currentElement._owner.getName():'<root>'};return rv;}else{return func.apply(this,args);}};}};module.exports=ReactDefaultPerf;},{"10":10,"146":146,"56":56,"70":70,"75":75}],56:[function(_dereq_,module,exports){var assign=_dereq_(27);var DONT_CARE_THRESHOLD=1.2;var DOM_OPERATION_TYPES={'_mountImageIntoNode':'set innerHTML',INSERT_MARKUP:'set innerHTML',MOVE_EXISTING:'move',REMOVE_NODE:'remove',TEXT_CONTENT:'set textContent','updatePropertyByID':'update attribute','deletePropertyByID':'delete attribute','updateStylesByID':'update styles','updateInnerHTMLByID':'set innerHTML','dangerouslyReplaceNodeWithMarkupByID':'replace'};function getTotalTime(measurements){
var totalTime=0;for(var i=0;i<measurements.length;i++){var measurement=measurements[i];totalTime+=measurement.totalTime;}
return totalTime;}
function getDOMSummary(measurements){var items=[];for(var i=0;i<measurements.length;i++){var measurement=measurements[i];var id;for(id in measurement.writes){measurement.writes[id].forEach(function(write){items.push({id:id,type:DOM_OPERATION_TYPES[write.type]||write.type,args:write.args});});}}
return items;}
function getExclusiveSummary(measurements){var candidates={};var displayName;for(var i=0;i<measurements.length;i++){var measurement=measurements[i];var allIDs=assign({},measurement.exclusive,measurement.inclusive);for(var id in allIDs){displayName=measurement.displayNames[id].current;candidates[displayName]=candidates[displayName]||{componentName:displayName,inclusive:0,exclusive:0,render:0,count:0};if(measurement.render[id]){candidates[displayName].render+=measurement.render[id];}
if(measurement.exclusive[id]){candidates[displayName].exclusive+=measurement.exclusive[id];}
if(measurement.inclusive[id]){candidates[displayName].inclusive+=measurement.inclusive[id];}
if(measurement.counts[id]){candidates[displayName].count+=measurement.counts[id];}}}
var arr=[];for(displayName in candidates){if(candidates[displayName].exclusive>=DONT_CARE_THRESHOLD){arr.push(candidates[displayName]);}}
arr.sort(function(a,b){return b.exclusive-a.exclusive;});return arr;}
function getInclusiveSummary(measurements,onlyClean){var candidates={};var inclusiveKey;for(var i=0;i<measurements.length;i++){var measurement=measurements[i];var allIDs=assign({},measurement.exclusive,measurement.inclusive);var cleanComponents;if(onlyClean){cleanComponents=getUnchangedComponents(measurement);}
for(var id in allIDs){if(onlyClean&&!cleanComponents[id]){continue;}
var displayName=measurement.displayNames[id];

inclusiveKey=displayName.owner+' > '+displayName.current;candidates[inclusiveKey]=candidates[inclusiveKey]||{componentName:inclusiveKey,time:0,count:0};if(measurement.inclusive[id]){candidates[inclusiveKey].time+=measurement.inclusive[id];}
if(measurement.counts[id]){candidates[inclusiveKey].count+=measurement.counts[id];}}}
var arr=[];for(inclusiveKey in candidates){if(candidates[inclusiveKey].time>=DONT_CARE_THRESHOLD){arr.push(candidates[inclusiveKey]);}}
arr.sort(function(a,b){return b.time-a.time;});return arr;}
function getUnchangedComponents(measurement){

var cleanComponents={};var dirtyLeafIDs=Object.keys(measurement.writes);var allIDs=assign({},measurement.exclusive,measurement.inclusive);for(var id in allIDs){var isDirty=false;
for(var i=0;i<dirtyLeafIDs.length;i++){if(dirtyLeafIDs[i].indexOf(id)===0){isDirty=true;break;}}
if(!isDirty&&measurement.counts[id]>0){cleanComponents[id]=true;}}
return cleanComponents;}
var ReactDefaultPerfAnalysis={getExclusiveSummary:getExclusiveSummary,getInclusiveSummary:getInclusiveSummary,getDOMSummary:getDOMSummary,getTotalTime:getTotalTime};module.exports=ReactDefaultPerfAnalysis;},{"27":27}],57:[function(_dereq_,module,exports){'use strict';var ReactContext=_dereq_(38);var ReactCurrentOwner=_dereq_(39);var assign=_dereq_(27);var warning=_dereq_(154);var RESERVED_PROPS={key:true,ref:true};function defineWarningProperty(object,key){Object.defineProperty(object,key,{configurable:false,enumerable:true,get:function(){if(!this._store){return null;}
return this._store[key];},set:function(value){("production"!=="development"?warning(false,'Don\'t set the %s property of the React element. Instead, '+'specify the correct value when initially creating the element.',key):null);this._store[key]=value;}});}
var useMutationMembrane=false;function defineMutationMembrane(prototype){try{var pseudoFrozenProperties={props:true};for(var key in pseudoFrozenProperties){defineWarningProperty(prototype,key);}
useMutationMembrane=true;}catch(x){}}
var ReactElement=function(type,key,ref,owner,context,props){ this.type=type;this.key=key;this.ref=ref;this._owner=owner;
this._context=context;if("production"!=="development"){

this._store={props:props,originalProps:assign({},props)};


try{Object.defineProperty(this._store,'validated',{configurable:false,enumerable:false,writable:true});}catch(x){}
this._store.validated=false;

if(useMutationMembrane){Object.freeze(this);return;}}
this.props=props;};ReactElement.prototype={_isReactElement:true};if("production"!=="development"){defineMutationMembrane(ReactElement.prototype);}
ReactElement.createElement=function(type,config,children){var propName; var props={};var key=null;var ref=null;if(config!=null){ref=config.ref===undefined?null:config.ref;key=config.key===undefined?null:''+config.key; for(propName in config){if(config.hasOwnProperty(propName)&&!RESERVED_PROPS.hasOwnProperty(propName)){props[propName]=config[propName];}}}

var childrenLength=arguments.length-2;if(childrenLength===1){props.children=children;}else if(childrenLength>1){var childArray=Array(childrenLength);for(var i=0;i<childrenLength;i++){childArray[i]=arguments[i+2];}
props.children=childArray;} 
if(type&&type.defaultProps){var defaultProps=type.defaultProps;for(propName in defaultProps){if(typeof props[propName]==='undefined'){props[propName]=defaultProps[propName];}}}
return new ReactElement(type,key,ref,ReactCurrentOwner.current,ReactContext.current,props);};ReactElement.createFactory=function(type){var factory=ReactElement.createElement.bind(null,type);

 factory.type=type;return factory;};ReactElement.cloneAndReplaceProps=function(oldElement,newProps){var newElement=new ReactElement(oldElement.type,oldElement.key,oldElement.ref,oldElement._owner,oldElement._context,newProps);if("production"!=="development"){ newElement._store.validated=oldElement._store.validated;}
return newElement;};ReactElement.cloneElement=function(element,config,children){var propName; var props=assign({},element.props); var key=element.key;var ref=element.ref; var owner=element._owner;if(config!=null){if(config.ref!==undefined){ref=config.ref;owner=ReactCurrentOwner.current;}
if(config.key!==undefined){key=''+config.key;} 
for(propName in config){if(config.hasOwnProperty(propName)&&!RESERVED_PROPS.hasOwnProperty(propName)){props[propName]=config[propName];}}}

var childrenLength=arguments.length-2;if(childrenLength===1){props.children=children;}else if(childrenLength>1){var childArray=Array(childrenLength);for(var i=0;i<childrenLength;i++){childArray[i]=arguments[i+2];}
props.children=childArray;}
return new ReactElement(element.type,key,ref,owner,element._context,props);};ReactElement.isValidElement=function(object){


var isElement=!!(object&&object._isReactElement);
return isElement;};module.exports=ReactElement;},{"154":154,"27":27,"38":38,"39":39}],58:[function(_dereq_,module,exports){'use strict';var ReactElement=_dereq_(57);var ReactFragment=_dereq_(63);var ReactPropTypeLocations=_dereq_(77);var ReactPropTypeLocationNames=_dereq_(76);var ReactCurrentOwner=_dereq_(39);var ReactNativeComponent=_dereq_(73);var getIteratorFn=_dereq_(126);var invariant=_dereq_(135);var warning=_dereq_(154);function getDeclarationErrorAddendum(){if(ReactCurrentOwner.current){var name=ReactCurrentOwner.current.getName();if(name){return' Check the render method of `'+name+'`.';}}
return'';}
var ownerHasKeyUseWarning={};var loggedTypeFailures={};var NUMERIC_PROPERTY_REGEX=/^\d+$/;function getName(instance){var publicInstance=instance&&instance.getPublicInstance();if(!publicInstance){return undefined;}
var constructor=publicInstance.constructor;if(!constructor){return undefined;}
return constructor.displayName||constructor.name||undefined;}
function getCurrentOwnerDisplayName(){var current=ReactCurrentOwner.current;return(current&&getName(current)||undefined);}
function validateExplicitKey(element,parentType){if(element._store.validated||element.key!=null){return;}
element._store.validated=true;warnAndMonitorForKeyUse('Each child in an array or iterator should have a unique "key" prop.',element,parentType);}
function validatePropertyKey(name,element,parentType){if(!NUMERIC_PROPERTY_REGEX.test(name)){return;}
warnAndMonitorForKeyUse('Child objects should have non-numeric keys so ordering is preserved.',element,parentType);}
function warnAndMonitorForKeyUse(message,element,parentType){var ownerName=getCurrentOwnerDisplayName();var parentName=typeof parentType==='string'?parentType:parentType.displayName||parentType.name;var useName=ownerName||parentName;var memoizer=ownerHasKeyUseWarning[message]||((ownerHasKeyUseWarning[message]={}));if(memoizer.hasOwnProperty(useName)){return;}
memoizer[useName]=true;var parentOrOwnerAddendum=ownerName?(" Check the render method of "+ownerName+"."):parentName?(" Check the React.render call using <"+parentName+">."):'';

var childOwnerAddendum='';if(element&&element._owner&&element._owner!==ReactCurrentOwner.current){var childOwnerName=getName(element._owner);childOwnerAddendum=(" It was passed a child from "+childOwnerName+".");}
("production"!=="development"?warning(false,message+'%s%s See http://fb.me/react-warning-keys for more information.',parentOrOwnerAddendum,childOwnerAddendum):null);}
function validateChildKeys(node,parentType){if(Array.isArray(node)){for(var i=0;i<node.length;i++){var child=node[i];if(ReactElement.isValidElement(child)){validateExplicitKey(child,parentType);}}}else if(ReactElement.isValidElement(node)){node._store.validated=true;}else if(node){var iteratorFn=getIteratorFn(node);if(iteratorFn){if(iteratorFn!==node.entries){var iterator=iteratorFn.call(node);var step;while(!(step=iterator.next()).done){if(ReactElement.isValidElement(step.value)){validateExplicitKey(step.value,parentType);}}}}else if(typeof node==='object'){var fragment=ReactFragment.extractIfFragment(node);for(var key in fragment){if(fragment.hasOwnProperty(key)){validatePropertyKey(key,fragment[key],parentType);}}}}}
function checkPropTypes(componentName,propTypes,props,location){for(var propName in propTypes){if(propTypes.hasOwnProperty(propName)){var error;
try{
("production"!=="development"?invariant(typeof propTypes[propName]==='function','%s: %s type `%s` is invalid; it must be a function, usually from '+'React.PropTypes.',componentName||'React class',ReactPropTypeLocationNames[location],propName):invariant(typeof propTypes[propName]==='function'));error=propTypes[propName](props,propName,componentName,location);}catch(ex){error=ex;}
if(error instanceof Error&&!(error.message in loggedTypeFailures)){
loggedTypeFailures[error.message]=true;var addendum=getDeclarationErrorAddendum(this);("production"!=="development"?warning(false,'Failed propType: %s%s',error.message,addendum):null);}}}}
var warnedPropsMutations={};function warnForPropsMutation(propName,element){var type=element.type;var elementName=typeof type==='string'?type:type.displayName;var ownerName=element._owner?element._owner.getPublicInstance().constructor.displayName:null;var warningKey=propName+'|'+elementName+'|'+ownerName;if(warnedPropsMutations.hasOwnProperty(warningKey)){return;}
warnedPropsMutations[warningKey]=true;var elementInfo='';if(elementName){elementInfo=' <'+elementName+' />';}
var ownerInfo='';if(ownerName){ownerInfo=' The element was created by '+ownerName+'.';}
("production"!=="development"?warning(false,'Don\'t set .props.%s of the React component%s. Instead, specify the '+'correct value when initially creating the element or use '+'React.cloneElement to make a new element with updated props.%s',propName,elementInfo,ownerInfo):null);}
function is(a,b){if(a!==a){ return b!==b;}
if(a===0&&b===0){  return 1 / a === 1 /b;}
return a===b;}
function checkAndWarnForMutatedProps(element){if(!element._store){
 return;}
var originalProps=element._store.originalProps;var props=element.props;for(var propName in props){if(props.hasOwnProperty(propName)){if(!originalProps.hasOwnProperty(propName)||!is(originalProps[propName],props[propName])){warnForPropsMutation(propName,element); originalProps[propName]=props[propName];}}}}
function validatePropTypes(element){if(element.type==null){return;} 
  

var componentClass=ReactNativeComponent.getComponentClassForElement(element);var name=componentClass.displayName||componentClass.name;if(componentClass.propTypes){checkPropTypes(name,componentClass.propTypes,element.props,ReactPropTypeLocations.prop);}
if(typeof componentClass.getDefaultProps==='function'){("production"!=="development"?warning(componentClass.getDefaultProps.isReactClassApproved,'getDefaultProps is only used on classic React.createClass '+'definitions. Use a static property named `defaultProps` instead.'):null);}}
var ReactElementValidator={checkAndWarnForMutatedProps:checkAndWarnForMutatedProps,createElement:function(type,props,children){
("production"!=="development"?warning(type!=null,'React.createElement: type should not be null or undefined. It should '+'be a string (for DOM elements) or a ReactClass (for composite '+'components).'):null);var element=ReactElement.createElement.apply(this,arguments);if(element==null){return element;}
for(var i=2;i<arguments.length;i++){validateChildKeys(arguments[i],type);}
validatePropTypes(element);return element;},createFactory:function(type){var validatedFactory=ReactElementValidator.createElement.bind(null,type); validatedFactory.type=type;if("production"!=="development"){try{Object.defineProperty(validatedFactory,'type',{enumerable:false,get:function(){("production"!=="development"?warning(false,'Factory.type is deprecated. Access the class directly '+'before passing it to createFactory.'):null);Object.defineProperty(this,'type',{value:type});return type;}});}catch(x){}}
return validatedFactory;},cloneElement:function(element,props,children){var newElement=ReactElement.cloneElement.apply(this,arguments);for(var i=2;i<arguments.length;i++){validateChildKeys(arguments[i],newElement.type);}
validatePropTypes(newElement);return newElement;}};module.exports=ReactElementValidator;},{"126":126,"135":135,"154":154,"39":39,"57":57,"63":63,"73":73,"76":76,"77":77}],59:[function(_dereq_,module,exports){'use strict';var ReactElement=_dereq_(57);var ReactInstanceMap=_dereq_(67);var invariant=_dereq_(135);var component;
var nullComponentIDsRegistry={};var ReactEmptyComponentInjection={injectEmptyComponent:function(emptyComponent){component=ReactElement.createFactory(emptyComponent);}};var ReactEmptyComponentType=function(){};ReactEmptyComponentType.prototype.componentDidMount=function(){var internalInstance=ReactInstanceMap.get(this);


if(!internalInstance){return;}
registerNullComponentID(internalInstance._rootNodeID);};ReactEmptyComponentType.prototype.componentWillUnmount=function(){var internalInstance=ReactInstanceMap.get(this);if(!internalInstance){return;}
deregisterNullComponentID(internalInstance._rootNodeID);};ReactEmptyComponentType.prototype.render=function(){("production"!=="development"?invariant(component,'Trying to return null from a render, but no null placeholder component '+'was injected.'):invariant(component));return component();};var emptyElement=ReactElement.createElement(ReactEmptyComponentType);function registerNullComponentID(id){nullComponentIDsRegistry[id]=true;}
function deregisterNullComponentID(id){delete nullComponentIDsRegistry[id];}
function isNullComponentID(id){return!!nullComponentIDsRegistry[id];}
var ReactEmptyComponent={emptyElement:emptyElement,injection:ReactEmptyComponentInjection,isNullComponentID:isNullComponentID};module.exports=ReactEmptyComponent;},{"135":135,"57":57,"67":67}],60:[function(_dereq_,module,exports){"use strict";var ReactErrorUtils={guard:function(func,name){return func;}};module.exports=ReactErrorUtils;},{}],61:[function(_dereq_,module,exports){'use strict';var EventPluginHub=_dereq_(17);function runEventQueueInBatch(events){EventPluginHub.enqueueEvents(events);EventPluginHub.processEventQueue();}
var ReactEventEmitterMixin={handleTopLevel:function(topLevelType,topLevelTarget,topLevelTargetID,nativeEvent){var events=EventPluginHub.extractEvents(topLevelType,topLevelTarget,topLevelTargetID,nativeEvent);runEventQueueInBatch(events);}};module.exports=ReactEventEmitterMixin;},{"17":17}],62:[function(_dereq_,module,exports){'use strict';var EventListener=_dereq_(16);var ExecutionEnvironment=_dereq_(21);var PooledClass=_dereq_(28);var ReactInstanceHandles=_dereq_(66);var ReactMount=_dereq_(70);var ReactUpdates=_dereq_(87);var assign=_dereq_(27);var getEventTarget=_dereq_(125);var getUnboundedScrollPosition=_dereq_(131);function findParent(node){

var nodeID=ReactMount.getID(node);var rootID=ReactInstanceHandles.getReactRootIDFromNodeID(nodeID);var container=ReactMount.findReactContainerForID(rootID);var parent=ReactMount.getFirstReactDOM(container);return parent;}
function TopLevelCallbackBookKeeping(topLevelType,nativeEvent){this.topLevelType=topLevelType;this.nativeEvent=nativeEvent;this.ancestors=[];}
assign(TopLevelCallbackBookKeeping.prototype,{destructor:function(){this.topLevelType=null;this.nativeEvent=null;this.ancestors.length=0;}});PooledClass.addPoolingTo(TopLevelCallbackBookKeeping,PooledClass.twoArgumentPooler);function handleTopLevelImpl(bookKeeping){var topLevelTarget=ReactMount.getFirstReactDOM(getEventTarget(bookKeeping.nativeEvent))||window;

var ancestor=topLevelTarget;while(ancestor){bookKeeping.ancestors.push(ancestor);ancestor=findParent(ancestor);}
for(var i=0,l=bookKeeping.ancestors.length;i<l;i++){topLevelTarget=bookKeeping.ancestors[i];var topLevelTargetID=ReactMount.getID(topLevelTarget)||'';ReactEventListener._handleTopLevel(bookKeeping.topLevelType,topLevelTarget,topLevelTargetID,bookKeeping.nativeEvent);}}
function scrollValueMonitor(cb){var scrollPosition=getUnboundedScrollPosition(window);cb(scrollPosition);}
var ReactEventListener={_enabled:true,_handleTopLevel:null,WINDOW_HANDLE:ExecutionEnvironment.canUseDOM?window:null,setHandleTopLevel:function(handleTopLevel){ReactEventListener._handleTopLevel=handleTopLevel;},setEnabled:function(enabled){ReactEventListener._enabled=!!enabled;},isEnabled:function(){return ReactEventListener._enabled;},trapBubbledEvent:function(topLevelType,handlerBaseName,handle){var element=handle;if(!element){return null;}
return EventListener.listen(element,handlerBaseName,ReactEventListener.dispatchEvent.bind(null,topLevelType));},trapCapturedEvent:function(topLevelType,handlerBaseName,handle){var element=handle;if(!element){return null;}
return EventListener.capture(element,handlerBaseName,ReactEventListener.dispatchEvent.bind(null,topLevelType));},monitorScrollValue:function(refresh){var callback=scrollValueMonitor.bind(null,refresh);EventListener.listen(window,'scroll',callback);},dispatchEvent:function(topLevelType,nativeEvent){if(!ReactEventListener._enabled){return;}
var bookKeeping=TopLevelCallbackBookKeeping.getPooled(topLevelType,nativeEvent);try{
ReactUpdates.batchedUpdates(handleTopLevelImpl,bookKeeping);}finally{TopLevelCallbackBookKeeping.release(bookKeeping);}}};module.exports=ReactEventListener;},{"125":125,"131":131,"16":16,"21":21,"27":27,"28":28,"66":66,"70":70,"87":87}],63:[function(_dereq_,module,exports){'use strict';var ReactElement=_dereq_(57);var warning=_dereq_(154);if("production"!=="development"){var fragmentKey='_reactFragment';var didWarnKey='_reactDidWarn';var canWarnForReactFragment=false;try{
var dummy=function(){return 1;};Object.defineProperty({},fragmentKey,{enumerable:false,value:true});Object.defineProperty({},'key',{enumerable:true,get:dummy});canWarnForReactFragment=true;}catch(x){}
var proxyPropertyAccessWithWarning=function(obj,key){Object.defineProperty(obj,key,{enumerable:true,get:function(){("production"!=="development"?warning(this[didWarnKey],'A ReactFragment is an opaque type. Accessing any of its '+'properties is deprecated. Pass it to one of the React.Children '+'helpers.'):null);this[didWarnKey]=true;return this[fragmentKey][key];},set:function(value){("production"!=="development"?warning(this[didWarnKey],'A ReactFragment is an immutable opaque type. Mutating its '+'properties is deprecated.'):null);this[didWarnKey]=true;this[fragmentKey][key]=value;}});};var issuedWarnings={};var didWarnForFragment=function(fragment){
var fragmentCacheKey='';for(var key in fragment){fragmentCacheKey+=key+':'+(typeof fragment[key])+',';}
var alreadyWarnedOnce=!!issuedWarnings[fragmentCacheKey];issuedWarnings[fragmentCacheKey]=true;return alreadyWarnedOnce;};}
var ReactFragment={
create:function(object){if("production"!=="development"){if(typeof object!=='object'||!object||Array.isArray(object)){("production"!=="development"?warning(false,'React.addons.createFragment only accepts a single object.',object):null);return object;}
if(ReactElement.isValidElement(object)){("production"!=="development"?warning(false,'React.addons.createFragment does not accept a ReactElement '+'without a wrapper object.'):null);return object;}
if(canWarnForReactFragment){var proxy={};Object.defineProperty(proxy,fragmentKey,{enumerable:false,value:object});Object.defineProperty(proxy,didWarnKey,{writable:true,enumerable:false,value:false});for(var key in object){proxyPropertyAccessWithWarning(proxy,key);}
Object.preventExtensions(proxy);return proxy;}}
return object;},
extract:function(fragment){if("production"!=="development"){if(canWarnForReactFragment){if(!fragment[fragmentKey]){("production"!=="development"?warning(didWarnForFragment(fragment),'Any use of a keyed object should be wrapped in '+'React.addons.createFragment(object) before being passed as a '+'child.'):null);return fragment;}
return fragment[fragmentKey];}}
return fragment;},

extractIfFragment:function(fragment){if("production"!=="development"){if(canWarnForReactFragment){if(fragment[fragmentKey]){return fragment[fragmentKey];}

for(var key in fragment){if(fragment.hasOwnProperty(key)&&ReactElement.isValidElement(fragment[key])){
return ReactFragment.extract(fragment);}}}}
return fragment;}};module.exports=ReactFragment;},{"154":154,"57":57}],64:[function(_dereq_,module,exports){'use strict';var DOMProperty=_dereq_(10);var EventPluginHub=_dereq_(17);var ReactComponentEnvironment=_dereq_(36);var ReactClass=_dereq_(33);var ReactEmptyComponent=_dereq_(59);var ReactBrowserEventEmitter=_dereq_(30);var ReactNativeComponent=_dereq_(73);var ReactDOMComponent=_dereq_(42);var ReactPerf=_dereq_(75);var ReactRootIndex=_dereq_(83);var ReactUpdates=_dereq_(87);var ReactInjection={Component:ReactComponentEnvironment.injection,Class:ReactClass.injection,DOMComponent:ReactDOMComponent.injection,DOMProperty:DOMProperty.injection,EmptyComponent:ReactEmptyComponent.injection,EventPluginHub:EventPluginHub.injection,EventEmitter:ReactBrowserEventEmitter.injection,NativeComponent:ReactNativeComponent.injection,Perf:ReactPerf.injection,RootIndex:ReactRootIndex.injection,Updates:ReactUpdates.injection};module.exports=ReactInjection;},{"10":10,"17":17,"30":30,"33":33,"36":36,"42":42,"59":59,"73":73,"75":75,"83":83,"87":87}],65:[function(_dereq_,module,exports){'use strict';var ReactDOMSelection=_dereq_(50);var containsNode=_dereq_(109);var focusNode=_dereq_(119);var getActiveElement=_dereq_(121);function isInDocument(node){return containsNode(document.documentElement,node);}
var ReactInputSelection={hasSelectionCapabilities:function(elem){return elem&&(((elem.nodeName==='INPUT'&&elem.type==='text')||elem.nodeName==='TEXTAREA'||elem.contentEditable==='true'));},getSelectionInformation:function(){var focusedElem=getActiveElement();return{focusedElem:focusedElem,selectionRange:ReactInputSelection.hasSelectionCapabilities(focusedElem)?ReactInputSelection.getSelection(focusedElem):null};},restoreSelection:function(priorSelectionInformation){var curFocusedElem=getActiveElement();var priorFocusedElem=priorSelectionInformation.focusedElem;var priorSelectionRange=priorSelectionInformation.selectionRange;if(curFocusedElem!==priorFocusedElem&&isInDocument(priorFocusedElem)){if(ReactInputSelection.hasSelectionCapabilities(priorFocusedElem)){ReactInputSelection.setSelection(priorFocusedElem,priorSelectionRange);}
focusNode(priorFocusedElem);}},getSelection:function(input){var selection;if('selectionStart'in input){selection={start:input.selectionStart,end:input.selectionEnd};}else if(document.selection&&input.nodeName==='INPUT'){var range=document.selection.createRange();
if(range.parentElement()===input){selection={start:-range.moveStart('character',-input.value.length),end:-range.moveEnd('character',-input.value.length)};}}else{selection=ReactDOMSelection.getOffsets(input);}
return selection||{start:0,end:0};},setSelection:function(input,offsets){var start=offsets.start;var end=offsets.end;if(typeof end==='undefined'){end=start;}
if('selectionStart'in input){input.selectionStart=start;input.selectionEnd=Math.min(end,input.value.length);}else if(document.selection&&input.nodeName==='INPUT'){var range=input.createTextRange();range.collapse(true);range.moveStart('character',start);range.moveEnd('character',end-start);range.select();}else{ReactDOMSelection.setOffsets(input,offsets);}}};module.exports=ReactInputSelection;},{"109":109,"119":119,"121":121,"50":50}],66:[function(_dereq_,module,exports){'use strict';var ReactRootIndex=_dereq_(83);var invariant=_dereq_(135);var SEPARATOR='.';var SEPARATOR_LENGTH=SEPARATOR.length;var MAX_TREE_DEPTH=100;function getReactRootIDString(index){return SEPARATOR+index.toString(36);}
function isBoundary(id,index){return id.charAt(index)===SEPARATOR||index===id.length;}
function isValidID(id){return id===''||(id.charAt(0)===SEPARATOR&&id.charAt(id.length-1)!==SEPARATOR);}
function isAncestorIDOf(ancestorID,descendantID){return(descendantID.indexOf(ancestorID)===0&&isBoundary(descendantID,ancestorID.length));}
function getParentID(id){return id?id.substr(0,id.lastIndexOf(SEPARATOR)):'';}
function getNextDescendantID(ancestorID,destinationID){("production"!=="development"?invariant(isValidID(ancestorID)&&isValidID(destinationID),'getNextDescendantID(%s, %s): Received an invalid React DOM ID.',ancestorID,destinationID):invariant(isValidID(ancestorID)&&isValidID(destinationID)));("production"!=="development"?invariant(isAncestorIDOf(ancestorID,destinationID),'getNextDescendantID(...): React has made an invalid assumption about '+'the DOM hierarchy. Expected `%s` to be an ancestor of `%s`.',ancestorID,destinationID):invariant(isAncestorIDOf(ancestorID,destinationID)));if(ancestorID===destinationID){return ancestorID;}

var start=ancestorID.length+SEPARATOR_LENGTH;var i;for(i=start;i<destinationID.length;i++){if(isBoundary(destinationID,i)){break;}}
return destinationID.substr(0,i);}
function getFirstCommonAncestorID(oneID,twoID){var minLength=Math.min(oneID.length,twoID.length);if(minLength===0){return'';}
var lastCommonMarkerIndex=0;for(var i=0;i<=minLength;i++){if(isBoundary(oneID,i)&&isBoundary(twoID,i)){lastCommonMarkerIndex=i;}else if(oneID.charAt(i)!==twoID.charAt(i)){break;}}
var longestCommonID=oneID.substr(0,lastCommonMarkerIndex);("production"!=="development"?invariant(isValidID(longestCommonID),'getFirstCommonAncestorID(%s, %s): Expected a valid React DOM ID: %s',oneID,twoID,longestCommonID):invariant(isValidID(longestCommonID)));return longestCommonID;}
function traverseParentPath(start,stop,cb,arg,skipFirst,skipLast){start=start||'';stop=stop||'';("production"!=="development"?invariant(start!==stop,'traverseParentPath(...): Cannot traverse from and to the same ID, `%s`.',start):invariant(start!==stop));var traverseUp=isAncestorIDOf(stop,start);("production"!=="development"?invariant(traverseUp||isAncestorIDOf(start,stop),'traverseParentPath(%s, %s, ...): Cannot traverse from two IDs that do '+'not have a parent path.',start,stop):invariant(traverseUp||isAncestorIDOf(start,stop)));var depth=0;var traverse=traverseUp?getParentID:getNextDescendantID;for(var id=start;;id=traverse(id,stop)){var ret;if((!skipFirst||id!==start)&&(!skipLast||id!==stop)){ret=cb(id,traverseUp,arg);}
if(ret===false||id===stop){break;}
("production"!=="development"?invariant(depth++<MAX_TREE_DEPTH,'traverseParentPath(%s, %s, ...): Detected an infinite loop while '+'traversing the React DOM ID tree. This may be due to malformed IDs: %s',start,stop):invariant(depth++<MAX_TREE_DEPTH));}}
var ReactInstanceHandles={createReactRootID:function(){return getReactRootIDString(ReactRootIndex.createReactRootIndex());},createReactID:function(rootID,name){return rootID+name;},getReactRootIDFromNodeID:function(id){if(id&&id.charAt(0)===SEPARATOR&&id.length>1){var index=id.indexOf(SEPARATOR,1);return index>-1?id.substr(0,index):id;}
return null;},traverseEnterLeave:function(leaveID,enterID,cb,upArg,downArg){var ancestorID=getFirstCommonAncestorID(leaveID,enterID);if(ancestorID!==leaveID){traverseParentPath(leaveID,ancestorID,cb,upArg,false,true);}
if(ancestorID!==enterID){traverseParentPath(ancestorID,enterID,cb,downArg,true,false);}},traverseTwoPhase:function(targetID,cb,arg){if(targetID){traverseParentPath('',targetID,cb,arg,true,false);traverseParentPath(targetID,'',cb,arg,false,true);}},traverseAncestors:function(targetID,cb,arg){traverseParentPath('',targetID,cb,arg,true,false);},_getFirstCommonAncestorID:getFirstCommonAncestorID,_getNextDescendantID:getNextDescendantID,isAncestorIDOf:isAncestorIDOf,SEPARATOR:SEPARATOR};module.exports=ReactInstanceHandles;},{"135":135,"83":83}],67:[function(_dereq_,module,exports){'use strict';var ReactInstanceMap={remove:function(key){key._reactInternalInstance=undefined;},get:function(key){return key._reactInternalInstance;},has:function(key){return key._reactInternalInstance!==undefined;},set:function(key,value){key._reactInternalInstance=value;}};module.exports=ReactInstanceMap;},{}],68:[function(_dereq_,module,exports){'use strict';var ReactLifeCycle={currentlyMountingInstance:null,currentlyUnmountingInstance:null};module.exports=ReactLifeCycle;},{}],69:[function(_dereq_,module,exports){'use strict';var adler32=_dereq_(106);var ReactMarkupChecksum={CHECKSUM_ATTR_NAME:'data-react-checksum',addChecksumToMarkup:function(markup){var checksum=adler32(markup);return markup.replace('>',' '+ReactMarkupChecksum.CHECKSUM_ATTR_NAME+'="'+checksum+'">');},canReuseMarkup:function(markup,element){var existingChecksum=element.getAttribute(ReactMarkupChecksum.CHECKSUM_ATTR_NAME);existingChecksum=existingChecksum&&parseInt(existingChecksum,10);var markupChecksum=adler32(markup);return markupChecksum===existingChecksum;}};module.exports=ReactMarkupChecksum;},{"106":106}],70:[function(_dereq_,module,exports){'use strict';var DOMProperty=_dereq_(10);var ReactBrowserEventEmitter=_dereq_(30);var ReactCurrentOwner=_dereq_(39);var ReactElement=_dereq_(57);var ReactElementValidator=_dereq_(58);var ReactEmptyComponent=_dereq_(59);var ReactInstanceHandles=_dereq_(66);var ReactInstanceMap=_dereq_(67);var ReactMarkupChecksum=_dereq_(69);var ReactPerf=_dereq_(75);var ReactReconciler=_dereq_(81);var ReactUpdateQueue=_dereq_(86);var ReactUpdates=_dereq_(87);var emptyObject=_dereq_(115);var containsNode=_dereq_(109);var getReactRootElementInContainer=_dereq_(129);var instantiateReactComponent=_dereq_(134);var invariant=_dereq_(135);var setInnerHTML=_dereq_(148);var shouldUpdateReactComponent=_dereq_(151);var warning=_dereq_(154);var SEPARATOR=ReactInstanceHandles.SEPARATOR;var ATTR_NAME=DOMProperty.ID_ATTRIBUTE_NAME;var nodeCache={};var ELEMENT_NODE_TYPE=1;var DOC_NODE_TYPE=9;var instancesByReactRootID={};var containersByReactRootID={};if("production"!=="development"){var rootElementsByReactRootID={};}
var findComponentRootReusableArray=[];function firstDifferenceIndex(string1,string2){var minLen=Math.min(string1.length,string2.length);for(var i=0;i<minLen;i++){if(string1.charAt(i)!==string2.charAt(i)){return i;}}
return string1.length===string2.length?-1:minLen;}
function getReactRootID(container){var rootElement=getReactRootElementInContainer(container);return rootElement&&ReactMount.getID(rootElement);}
function getID(node){var id=internalGetID(node);if(id){if(nodeCache.hasOwnProperty(id)){var cached=nodeCache[id];if(cached!==node){("production"!=="development"?invariant(!isValid(cached,id),'ReactMount: Two valid but unequal nodes with the same `%s`: %s',ATTR_NAME,id):invariant(!isValid(cached,id)));nodeCache[id]=node;}}else{nodeCache[id]=node;}}
return id;}
function internalGetID(node){

return node&&node.getAttribute&&node.getAttribute(ATTR_NAME)||'';}
function setID(node,id){var oldID=internalGetID(node);if(oldID!==id){delete nodeCache[oldID];}
node.setAttribute(ATTR_NAME,id);nodeCache[id]=node;}
function getNode(id){if(!nodeCache.hasOwnProperty(id)||!isValid(nodeCache[id],id)){nodeCache[id]=ReactMount.findReactNodeByID(id);}
return nodeCache[id];}
function getNodeFromInstance(instance){var id=ReactInstanceMap.get(instance)._rootNodeID;if(ReactEmptyComponent.isNullComponentID(id)){return null;}
if(!nodeCache.hasOwnProperty(id)||!isValid(nodeCache[id],id)){nodeCache[id]=ReactMount.findReactNodeByID(id);}
return nodeCache[id];}
function isValid(node,id){if(node){("production"!=="development"?invariant(internalGetID(node)===id,'ReactMount: Unexpected modification of `%s`',ATTR_NAME):invariant(internalGetID(node)===id));var container=ReactMount.findReactContainerForID(id);if(container&&containsNode(container,node)){return true;}}
return false;}
function purgeID(id){delete nodeCache[id];}
var deepestNodeSoFar=null;function findDeepestCachedAncestorImpl(ancestorID){var ancestor=nodeCache[ancestorID];if(ancestor&&isValid(ancestor,ancestorID)){deepestNodeSoFar=ancestor;}else{
return false;}}
function findDeepestCachedAncestor(targetID){deepestNodeSoFar=null;ReactInstanceHandles.traverseAncestors(targetID,findDeepestCachedAncestorImpl);var foundNode=deepestNodeSoFar;deepestNodeSoFar=null;return foundNode;}
function mountComponentIntoNode(componentInstance,rootID,container,transaction,shouldReuseMarkup){var markup=ReactReconciler.mountComponent(componentInstance,rootID,transaction,emptyObject);componentInstance._isTopLevel=true;ReactMount._mountImageIntoNode(markup,container,shouldReuseMarkup);}
function batchedMountComponentIntoNode(componentInstance,rootID,container,shouldReuseMarkup){var transaction=ReactUpdates.ReactReconcileTransaction.getPooled();transaction.perform(mountComponentIntoNode,null,componentInstance,rootID,container,transaction,shouldReuseMarkup);ReactUpdates.ReactReconcileTransaction.release(transaction);}
var ReactMount={_instancesByReactRootID:instancesByReactRootID,scrollMonitor:function(container,renderCallback){renderCallback();},_updateRootComponent:function(prevComponent,nextElement,container,callback){if("production"!=="development"){ReactElementValidator.checkAndWarnForMutatedProps(nextElement);}
ReactMount.scrollMonitor(container,function(){ReactUpdateQueue.enqueueElementInternal(prevComponent,nextElement);if(callback){ReactUpdateQueue.enqueueCallbackInternal(prevComponent,callback);}});if("production"!=="development"){rootElementsByReactRootID[getReactRootID(container)]=getReactRootElementInContainer(container);}
return prevComponent;},_registerComponent:function(nextComponent,container){("production"!=="development"?invariant(container&&((container.nodeType===ELEMENT_NODE_TYPE||container.nodeType===DOC_NODE_TYPE)),'_registerComponent(...): Target container is not a DOM element.'):invariant(container&&((container.nodeType===ELEMENT_NODE_TYPE||container.nodeType===DOC_NODE_TYPE))));ReactBrowserEventEmitter.ensureScrollValueMonitoring();var reactRootID=ReactMount.registerContainer(container);instancesByReactRootID[reactRootID]=nextComponent;return reactRootID;},_renderNewRootComponent:function(nextElement,container,shouldReuseMarkup){
("production"!=="development"?warning(ReactCurrentOwner.current==null,'_renderNewRootComponent(): Render methods should be a pure function '+'of props and state; triggering nested component updates from '+'render is not allowed. If necessary, trigger nested updates in '+'componentDidUpdate.'):null);var componentInstance=instantiateReactComponent(nextElement,null);var reactRootID=ReactMount._registerComponent(componentInstance,container);

ReactUpdates.batchedUpdates(batchedMountComponentIntoNode,componentInstance,reactRootID,container,shouldReuseMarkup);if("production"!=="development"){rootElementsByReactRootID[reactRootID]=getReactRootElementInContainer(container);}
return componentInstance;},render:function(nextElement,container,callback){("production"!=="development"?invariant(ReactElement.isValidElement(nextElement),'React.render(): Invalid component element.%s',(typeof nextElement==='string'?' Instead of passing an element string, make sure to instantiate '+'it by passing it to React.createElement.':typeof nextElement==='function'?' Instead of passing a component class, make sure to instantiate '+'it by passing it to React.createElement.': nextElement!=null&&nextElement.props!==undefined?' This may be caused by unintentionally loading two independent '+'copies of React.':'')):invariant(ReactElement.isValidElement(nextElement)));var prevComponent=instancesByReactRootID[getReactRootID(container)];if(prevComponent){var prevElement=prevComponent._currentElement;if(shouldUpdateReactComponent(prevElement,nextElement)){return ReactMount._updateRootComponent(prevComponent,nextElement,container,callback).getPublicInstance();}else{ReactMount.unmountComponentAtNode(container);}}
var reactRootElement=getReactRootElementInContainer(container);var containerHasReactMarkup=reactRootElement&&ReactMount.isRenderedByReact(reactRootElement);if("production"!=="development"){if(!containerHasReactMarkup||reactRootElement.nextSibling){var rootElementSibling=reactRootElement;while(rootElementSibling){if(ReactMount.isRenderedByReact(rootElementSibling)){("production"!=="development"?warning(false,'render(): Target node has markup rendered by React, but there '+'are unrelated nodes as well. This is most commonly caused by '+'white-space inserted around server-rendered markup.'):null);break;}
rootElementSibling=rootElementSibling.nextSibling;}}}
var shouldReuseMarkup=containerHasReactMarkup&&!prevComponent;var component=ReactMount._renderNewRootComponent(nextElement,container,shouldReuseMarkup).getPublicInstance();if(callback){callback.call(component);}
return component;},constructAndRenderComponent:function(constructor,props,container){var element=ReactElement.createElement(constructor,props);return ReactMount.render(element,container);},constructAndRenderComponentByID:function(constructor,props,id){var domNode=document.getElementById(id);("production"!=="development"?invariant(domNode,'Tried to get element with id of "%s" but it is not present on the page.',id):invariant(domNode));return ReactMount.constructAndRenderComponent(constructor,props,domNode);},registerContainer:function(container){var reactRootID=getReactRootID(container);if(reactRootID){reactRootID=ReactInstanceHandles.getReactRootIDFromNodeID(reactRootID);}
if(!reactRootID){reactRootID=ReactInstanceHandles.createReactRootID();}
containersByReactRootID[reactRootID]=container;return reactRootID;},unmountComponentAtNode:function(container){

("production"!=="development"?warning(ReactCurrentOwner.current==null,'unmountComponentAtNode(): Render methods should be a pure function of '+'props and state; triggering nested component updates from render is '+'not allowed. If necessary, trigger nested updates in '+'componentDidUpdate.'):null);("production"!=="development"?invariant(container&&((container.nodeType===ELEMENT_NODE_TYPE||container.nodeType===DOC_NODE_TYPE)),'unmountComponentAtNode(...): Target container is not a DOM element.'):invariant(container&&((container.nodeType===ELEMENT_NODE_TYPE||container.nodeType===DOC_NODE_TYPE))));var reactRootID=getReactRootID(container);var component=instancesByReactRootID[reactRootID];if(!component){return false;}
ReactMount.unmountComponentFromNode(component,container);delete instancesByReactRootID[reactRootID];delete containersByReactRootID[reactRootID];if("production"!=="development"){delete rootElementsByReactRootID[reactRootID];}
return true;},unmountComponentFromNode:function(instance,container){ReactReconciler.unmountComponent(instance);if(container.nodeType===DOC_NODE_TYPE){container=container.documentElement;} 
while(container.lastChild){container.removeChild(container.lastChild);}},findReactContainerForID:function(id){var reactRootID=ReactInstanceHandles.getReactRootIDFromNodeID(id);var container=containersByReactRootID[reactRootID];if("production"!=="development"){var rootElement=rootElementsByReactRootID[reactRootID];if(rootElement&&rootElement.parentNode!==container){("production"!=="development"?invariant(
internalGetID(rootElement)===reactRootID,'ReactMount: Root element ID differed from reactRootID.'):invariant(
internalGetID(rootElement)===reactRootID));var containerChild=container.firstChild;if(containerChild&&reactRootID===internalGetID(containerChild)){


rootElementsByReactRootID[reactRootID]=containerChild;}else{("production"!=="development"?warning(false,'ReactMount: Root element has been removed from its original '+'container. New container:',rootElement.parentNode):null);}}}
return container;},findReactNodeByID:function(id){var reactRoot=ReactMount.findReactContainerForID(id);return ReactMount.findComponentRoot(reactRoot,id);},isRenderedByReact:function(node){if(node.nodeType!==1){ return false;}
var id=ReactMount.getID(node);return id?id.charAt(0)===SEPARATOR:false;},getFirstReactDOM:function(node){var current=node;while(current&&current.parentNode!==current){if(ReactMount.isRenderedByReact(current)){return current;}
current=current.parentNode;}
return null;},findComponentRoot:function(ancestorNode,targetID){var firstChildren=findComponentRootReusableArray;var childIndex=0;var deepestAncestor=findDeepestCachedAncestor(targetID)||ancestorNode;firstChildren[0]=deepestAncestor.firstChild;firstChildren.length=1;while(childIndex<firstChildren.length){var child=firstChildren[childIndex++];var targetChild;while(child){var childID=ReactMount.getID(child);if(childID){


if(targetID===childID){targetChild=child;}else if(ReactInstanceHandles.isAncestorIDOf(childID,targetID)){

firstChildren.length=childIndex=0;firstChildren.push(child.firstChild);}}else{


firstChildren.push(child.firstChild);}
child=child.nextSibling;}
if(targetChild){

firstChildren.length=0;return targetChild;}}
firstChildren.length=0;("production"!=="development"?invariant(false,'findComponentRoot(..., %s): Unable to find element. This probably '+'means the DOM was unexpectedly mutated (e.g., by the browser), '+'usually due to forgetting a <tbody> when using tables, nesting tags '+'like <form>, <p>, or <a>, or using non-SVG elements in an <svg> '+'parent. '+'Try inspecting the child nodes of the element with React ID `%s`.',targetID,ReactMount.getID(ancestorNode)):invariant(false));},_mountImageIntoNode:function(markup,container,shouldReuseMarkup){("production"!=="development"?invariant(container&&((container.nodeType===ELEMENT_NODE_TYPE||container.nodeType===DOC_NODE_TYPE)),'mountComponentIntoNode(...): Target container is not valid.'):invariant(container&&((container.nodeType===ELEMENT_NODE_TYPE||container.nodeType===DOC_NODE_TYPE))));if(shouldReuseMarkup){var rootElement=getReactRootElementInContainer(container);if(ReactMarkupChecksum.canReuseMarkup(markup,rootElement)){return;}else{var checksum=rootElement.getAttribute(ReactMarkupChecksum.CHECKSUM_ATTR_NAME);rootElement.removeAttribute(ReactMarkupChecksum.CHECKSUM_ATTR_NAME);var rootMarkup=rootElement.outerHTML;rootElement.setAttribute(ReactMarkupChecksum.CHECKSUM_ATTR_NAME,checksum);var diffIndex=firstDifferenceIndex(markup,rootMarkup);var difference=' (client) '+
markup.substring(diffIndex-20,diffIndex+20)+'\n (server) '+rootMarkup.substring(diffIndex-20,diffIndex+20);("production"!=="development"?invariant(container.nodeType!==DOC_NODE_TYPE,'You\'re trying to render a component to the document using '+'server rendering but the checksum was invalid. This usually '+'means you rendered a different component type or props on '+'the client from the one on the server, or your render() '+'methods are impure. React cannot handle this case due to '+'cross-browser quirks by rendering at the document root. You '+'should look for environment dependent code in your components '+'and ensure the props are the same client and server side:\n%s',difference):invariant(container.nodeType!==DOC_NODE_TYPE));if("production"!=="development"){("production"!=="development"?warning(false,'React attempted to reuse markup in a container but the '+'checksum was invalid. This generally means that you are '+'using server rendering and the markup generated on the '+'server was not what the client was expecting. React injected '+'new markup to compensate which works but you have lost many '+'of the benefits of server rendering. Instead, figure out '+'why the markup being generated is different on the client '+'or server:\n%s',difference):null);}}}
("production"!=="development"?invariant(container.nodeType!==DOC_NODE_TYPE,'You\'re trying to render a component to the document but '+'you didn\'t use server rendering. We can\'t do this '+'without using server rendering due to cross-browser quirks. '+'See React.renderToString() for server rendering.'):invariant(container.nodeType!==DOC_NODE_TYPE));setInnerHTML(container,markup);},getReactRootID:getReactRootID,getID:getID,setID:setID,getNode:getNode,getNodeFromInstance:getNodeFromInstance,purgeID:purgeID};ReactPerf.measureMethods(ReactMount,'ReactMount',{_renderNewRootComponent:'_renderNewRootComponent',_mountImageIntoNode:'_mountImageIntoNode'});module.exports=ReactMount;},{"10":10,"109":109,"115":115,"129":129,"134":134,"135":135,"148":148,"151":151,"154":154,"30":30,"39":39,"57":57,"58":58,"59":59,"66":66,"67":67,"69":69,"75":75,"81":81,"86":86,"87":87}],71:[function(_dereq_,module,exports){'use strict';var ReactComponentEnvironment=_dereq_(36);var ReactMultiChildUpdateTypes=_dereq_(72);var ReactReconciler=_dereq_(81);var ReactChildReconciler=_dereq_(31);var updateDepth=0;var updateQueue=[];var markupQueue=[];function enqueueMarkup(parentID,markup,toIndex){updateQueue.push({parentID:parentID,parentNode:null,type:ReactMultiChildUpdateTypes.INSERT_MARKUP,markupIndex:markupQueue.push(markup)-1,textContent:null,fromIndex:null,toIndex:toIndex});}
function enqueueMove(parentID,fromIndex,toIndex){updateQueue.push({parentID:parentID,parentNode:null,type:ReactMultiChildUpdateTypes.MOVE_EXISTING,markupIndex:null,textContent:null,fromIndex:fromIndex,toIndex:toIndex});}
function enqueueRemove(parentID,fromIndex){updateQueue.push({parentID:parentID,parentNode:null,type:ReactMultiChildUpdateTypes.REMOVE_NODE,markupIndex:null,textContent:null,fromIndex:fromIndex,toIndex:null});}
function enqueueTextContent(parentID,textContent){updateQueue.push({parentID:parentID,parentNode:null,type:ReactMultiChildUpdateTypes.TEXT_CONTENT,markupIndex:null,textContent:textContent,fromIndex:null,toIndex:null});}
function processQueue(){if(updateQueue.length){ReactComponentEnvironment.processChildrenUpdates(updateQueue,markupQueue);clearQueue();}}
function clearQueue(){updateQueue.length=0;markupQueue.length=0;}
var ReactMultiChild={Mixin:{mountChildren:function(nestedChildren,transaction,context){var children=ReactChildReconciler.instantiateChildren(nestedChildren,transaction,context);this._renderedChildren=children;var mountImages=[];var index=0;for(var name in children){if(children.hasOwnProperty(name)){var child=children[name];var rootID=this._rootNodeID+name;var mountImage=ReactReconciler.mountComponent(child,rootID,transaction,context);child._mountIndex=index;mountImages.push(mountImage);index++;}}
return mountImages;},updateTextContent:function(nextContent){updateDepth++;var errorThrown=true;try{var prevChildren=this._renderedChildren;ReactChildReconciler.unmountChildren(prevChildren); for(var name in prevChildren){if(prevChildren.hasOwnProperty(name)){this._unmountChildByName(prevChildren[name],name);}}
this.setTextContent(nextContent);errorThrown=false;}finally{updateDepth--;if(!updateDepth){if(errorThrown){clearQueue();}else{processQueue();}}}},updateChildren:function(nextNestedChildren,transaction,context){updateDepth++;var errorThrown=true;try{this._updateChildren(nextNestedChildren,transaction,context);errorThrown=false;}finally{updateDepth--;if(!updateDepth){if(errorThrown){clearQueue();}else{processQueue();}}}},_updateChildren:function(nextNestedChildren,transaction,context){var prevChildren=this._renderedChildren;var nextChildren=ReactChildReconciler.updateChildren(prevChildren,nextNestedChildren,transaction,context);this._renderedChildren=nextChildren;if(!nextChildren&&!prevChildren){return;}
var name;
var lastIndex=0;var nextIndex=0;for(name in nextChildren){if(!nextChildren.hasOwnProperty(name)){continue;}
var prevChild=prevChildren&&prevChildren[name];var nextChild=nextChildren[name];if(prevChild===nextChild){this.moveChild(prevChild,nextIndex,lastIndex);lastIndex=Math.max(prevChild._mountIndex,lastIndex);prevChild._mountIndex=nextIndex;}else{if(prevChild){lastIndex=Math.max(prevChild._mountIndex,lastIndex);this._unmountChildByName(prevChild,name);}
this._mountChildByNameAtIndex(nextChild,name,nextIndex,transaction,context);}
nextIndex++;}
for(name in prevChildren){if(prevChildren.hasOwnProperty(name)&&!(nextChildren&&nextChildren.hasOwnProperty(name))){this._unmountChildByName(prevChildren[name],name);}}},unmountChildren:function(){var renderedChildren=this._renderedChildren;ReactChildReconciler.unmountChildren(renderedChildren);this._renderedChildren=null;},moveChild:function(child,toIndex,lastIndex){

if(child._mountIndex<lastIndex){enqueueMove(this._rootNodeID,child._mountIndex,toIndex);}},createChild:function(child,mountImage){enqueueMarkup(this._rootNodeID,mountImage,child._mountIndex);},removeChild:function(child){enqueueRemove(this._rootNodeID,child._mountIndex);},setTextContent:function(textContent){enqueueTextContent(this._rootNodeID,textContent);},_mountChildByNameAtIndex:function(child,name,index,transaction,context){var rootID=this._rootNodeID+name;var mountImage=ReactReconciler.mountComponent(child,rootID,transaction,context);child._mountIndex=index;this.createChild(child,mountImage);},_unmountChildByName:function(child,name){this.removeChild(child);child._mountIndex=null;}}};module.exports=ReactMultiChild;},{"31":31,"36":36,"72":72,"81":81}],72:[function(_dereq_,module,exports){'use strict';var keyMirror=_dereq_(140);var ReactMultiChildUpdateTypes=keyMirror({INSERT_MARKUP:null,MOVE_EXISTING:null,REMOVE_NODE:null,TEXT_CONTENT:null});module.exports=ReactMultiChildUpdateTypes;},{"140":140}],73:[function(_dereq_,module,exports){'use strict';var assign=_dereq_(27);var invariant=_dereq_(135);var autoGenerateWrapperClass=null;var genericComponentClass=null;var tagToComponentClass={};var textComponentClass=null;var ReactNativeComponentInjection={
injectGenericComponentClass:function(componentClass){genericComponentClass=componentClass;},
injectTextComponentClass:function(componentClass){textComponentClass=componentClass;},
injectComponentClasses:function(componentClasses){assign(tagToComponentClass,componentClasses);},injectAutoWrapper:function(wrapperFactory){autoGenerateWrapperClass=wrapperFactory;}};function getComponentClassForElement(element){if(typeof element.type==='function'){return element.type;}
var tag=element.type;var componentClass=tagToComponentClass[tag];if(componentClass==null){tagToComponentClass[tag]=componentClass=autoGenerateWrapperClass(tag);}
return componentClass;}
function createInternalComponent(element){("production"!=="development"?invariant(genericComponentClass,'There is no registered component for the tag %s',element.type):invariant(genericComponentClass));return new genericComponentClass(element.type,element.props);}
function createInstanceForText(text){return new textComponentClass(text);}
function isTextComponent(component){return component instanceof textComponentClass;}
var ReactNativeComponent={getComponentClassForElement:getComponentClassForElement,createInternalComponent:createInternalComponent,createInstanceForText:createInstanceForText,isTextComponent:isTextComponent,injection:ReactNativeComponentInjection};module.exports=ReactNativeComponent;},{"135":135,"27":27}],74:[function(_dereq_,module,exports){'use strict';var invariant=_dereq_(135);var ReactOwner={isValidOwner:function(object){return!!((object&&typeof object.attachRef==='function'&&typeof object.detachRef==='function'));},addComponentAsRefTo:function(component,ref,owner){("production"!=="development"?invariant(ReactOwner.isValidOwner(owner),'addComponentAsRefTo(...): Only a ReactOwner can have refs. This '+'usually means that you\'re trying to add a ref to a component that '+'doesn\'t have an owner (that is, was not created inside of another '+'component\'s `render` method). Try rendering this component inside of '+'a new top-level component which will hold the ref.'):invariant(ReactOwner.isValidOwner(owner)));owner.attachRef(ref,component);},removeComponentAsRefFrom:function(component,ref,owner){("production"!=="development"?invariant(ReactOwner.isValidOwner(owner),'removeComponentAsRefFrom(...): Only a ReactOwner can have refs. This '+'usually means that you\'re trying to remove a ref to a component that '+'doesn\'t have an owner (that is, was not created inside of another '+'component\'s `render` method). Try rendering this component inside of '+'a new top-level component which will hold the ref.'):invariant(ReactOwner.isValidOwner(owner)));
if(owner.getPublicInstance().refs[ref]===component.getPublicInstance()){owner.detachRef(ref);}}};module.exports=ReactOwner;},{"135":135}],75:[function(_dereq_,module,exports){'use strict';var ReactPerf={enableMeasure:false,storedMeasure:_noMeasure,measureMethods:function(object,objectName,methodNames){if("production"!=="development"){for(var key in methodNames){if(!methodNames.hasOwnProperty(key)){continue;}
object[key]=ReactPerf.measure(objectName,methodNames[key],object[key]);}}},measure:function(objName,fnName,func){if("production"!=="development"){var measuredFunc=null;var wrapper=function(){if(ReactPerf.enableMeasure){if(!measuredFunc){measuredFunc=ReactPerf.storedMeasure(objName,fnName,func);}
return measuredFunc.apply(this,arguments);}
return func.apply(this,arguments);};wrapper.displayName=objName+'_'+fnName;return wrapper;}
return func;},injection:{injectMeasure:function(measure){ReactPerf.storedMeasure=measure;}}};function _noMeasure(objName,fnName,func){return func;}
module.exports=ReactPerf;},{}],76:[function(_dereq_,module,exports){'use strict';var ReactPropTypeLocationNames={};if("production"!=="development"){ReactPropTypeLocationNames={prop:'prop',context:'context',childContext:'child context'};}
module.exports=ReactPropTypeLocationNames;},{}],77:[function(_dereq_,module,exports){'use strict';var keyMirror=_dereq_(140);var ReactPropTypeLocations=keyMirror({prop:null,context:null,childContext:null});module.exports=ReactPropTypeLocations;},{"140":140}],78:[function(_dereq_,module,exports){'use strict';var ReactElement=_dereq_(57);var ReactFragment=_dereq_(63);var ReactPropTypeLocationNames=_dereq_(76);var emptyFunction=_dereq_(114);var ANONYMOUS='<<anonymous>>';var elementTypeChecker=createElementTypeChecker();var nodeTypeChecker=createNodeChecker();var ReactPropTypes={array:createPrimitiveTypeChecker('array'),bool:createPrimitiveTypeChecker('boolean'),func:createPrimitiveTypeChecker('function'),number:createPrimitiveTypeChecker('number'),object:createPrimitiveTypeChecker('object'),string:createPrimitiveTypeChecker('string'),any:createAnyTypeChecker(),arrayOf:createArrayOfTypeChecker,element:elementTypeChecker,instanceOf:createInstanceTypeChecker,node:nodeTypeChecker,objectOf:createObjectOfTypeChecker,oneOf:createEnumTypeChecker,oneOfType:createUnionTypeChecker,shape:createShapeTypeChecker};function createChainableTypeChecker(validate){function checkType(isRequired,props,propName,componentName,location){componentName=componentName||ANONYMOUS;if(props[propName]==null){var locationName=ReactPropTypeLocationNames[location];if(isRequired){return new Error(("Required "+locationName+" `"+propName+"` was not specified in ")+
("`"+componentName+"`."));}
return null;}else{return validate(props,propName,componentName,location);}}
var chainedCheckType=checkType.bind(null,false);chainedCheckType.isRequired=checkType.bind(null,true);return chainedCheckType;}
function createPrimitiveTypeChecker(expectedType){function validate(props,propName,componentName,location){var propValue=props[propName];var propType=getPropType(propValue);if(propType!==expectedType){var locationName=ReactPropTypeLocationNames[location];
var preciseType=getPreciseType(propValue);return new Error(("Invalid "+locationName+" `"+propName+"` of type `"+preciseType+"` ")+
("supplied to `"+componentName+"`, expected `"+expectedType+"`."));}
return null;}
return createChainableTypeChecker(validate);}
function createAnyTypeChecker(){return createChainableTypeChecker(emptyFunction.thatReturns(null));}
function createArrayOfTypeChecker(typeChecker){function validate(props,propName,componentName,location){var propValue=props[propName];if(!Array.isArray(propValue)){var locationName=ReactPropTypeLocationNames[location];var propType=getPropType(propValue);return new Error(("Invalid "+locationName+" `"+propName+"` of type ")+
("`"+propType+"` supplied to `"+componentName+"`, expected an array."));}
for(var i=0;i<propValue.length;i++){var error=typeChecker(propValue,i,componentName,location);if(error instanceof Error){return error;}}
return null;}
return createChainableTypeChecker(validate);}
function createElementTypeChecker(){function validate(props,propName,componentName,location){if(!ReactElement.isValidElement(props[propName])){var locationName=ReactPropTypeLocationNames[location];return new Error(("Invalid "+locationName+" `"+propName+"` supplied to ")+
("`"+componentName+"`, expected a ReactElement."));}
return null;}
return createChainableTypeChecker(validate);}
function createInstanceTypeChecker(expectedClass){function validate(props,propName,componentName,location){if(!(props[propName]instanceof expectedClass)){var locationName=ReactPropTypeLocationNames[location];var expectedClassName=expectedClass.name||ANONYMOUS;return new Error(("Invalid "+locationName+" `"+propName+"` supplied to ")+
("`"+componentName+"`, expected instance of `"+expectedClassName+"`."));}
return null;}
return createChainableTypeChecker(validate);}
function createEnumTypeChecker(expectedValues){function validate(props,propName,componentName,location){var propValue=props[propName];for(var i=0;i<expectedValues.length;i++){if(propValue===expectedValues[i]){return null;}}
var locationName=ReactPropTypeLocationNames[location];var valuesString=JSON.stringify(expectedValues);return new Error(("Invalid "+locationName+" `"+propName+"` of value `"+propValue+"` ")+
("supplied to `"+componentName+"`, expected one of "+valuesString+"."));}
return createChainableTypeChecker(validate);}
function createObjectOfTypeChecker(typeChecker){function validate(props,propName,componentName,location){var propValue=props[propName];var propType=getPropType(propValue);if(propType!=='object'){var locationName=ReactPropTypeLocationNames[location];return new Error(("Invalid "+locationName+" `"+propName+"` of type ")+
("`"+propType+"` supplied to `"+componentName+"`, expected an object."));}
for(var key in propValue){if(propValue.hasOwnProperty(key)){var error=typeChecker(propValue,key,componentName,location);if(error instanceof Error){return error;}}}
return null;}
return createChainableTypeChecker(validate);}
function createUnionTypeChecker(arrayOfTypeCheckers){function validate(props,propName,componentName,location){for(var i=0;i<arrayOfTypeCheckers.length;i++){var checker=arrayOfTypeCheckers[i];if(checker(props,propName,componentName,location)==null){return null;}}
var locationName=ReactPropTypeLocationNames[location];return new Error(("Invalid "+locationName+" `"+propName+"` supplied to ")+
("`"+componentName+"`."));}
return createChainableTypeChecker(validate);}
function createNodeChecker(){function validate(props,propName,componentName,location){if(!isNode(props[propName])){var locationName=ReactPropTypeLocationNames[location];return new Error(("Invalid "+locationName+" `"+propName+"` supplied to ")+
("`"+componentName+"`, expected a ReactNode."));}
return null;}
return createChainableTypeChecker(validate);}
function createShapeTypeChecker(shapeTypes){function validate(props,propName,componentName,location){var propValue=props[propName];var propType=getPropType(propValue);if(propType!=='object'){var locationName=ReactPropTypeLocationNames[location];return new Error(("Invalid "+locationName+" `"+propName+"` of type `"+propType+"` ")+
("supplied to `"+componentName+"`, expected `object`."));}
for(var key in shapeTypes){var checker=shapeTypes[key];if(!checker){continue;}
var error=checker(propValue,key,componentName,location);if(error){return error;}}
return null;}
return createChainableTypeChecker(validate);}
function isNode(propValue){switch(typeof propValue){case'number':case'string':case'undefined':return true;case'boolean':return!propValue;case'object':if(Array.isArray(propValue)){return propValue.every(isNode);}
if(propValue===null||ReactElement.isValidElement(propValue)){return true;}
propValue=ReactFragment.extractIfFragment(propValue);for(var k in propValue){if(!isNode(propValue[k])){return false;}}
return true;default:return false;}}
function getPropType(propValue){var propType=typeof propValue;if(Array.isArray(propValue)){return'array';}
if(propValue instanceof RegExp){
return'object';}
return propType;}
function getPreciseType(propValue){var propType=getPropType(propValue);if(propType==='object'){if(propValue instanceof Date){return'date';}else if(propValue instanceof RegExp){return'regexp';}}
return propType;}
module.exports=ReactPropTypes;},{"114":114,"57":57,"63":63,"76":76}],79:[function(_dereq_,module,exports){'use strict';var PooledClass=_dereq_(28);var ReactBrowserEventEmitter=_dereq_(30);var assign=_dereq_(27);function ReactPutListenerQueue(){this.listenersToPut=[];}
assign(ReactPutListenerQueue.prototype,{enqueuePutListener:function(rootNodeID,propKey,propValue){this.listenersToPut.push({rootNodeID:rootNodeID,propKey:propKey,propValue:propValue});},putListeners:function(){for(var i=0;i<this.listenersToPut.length;i++){var listenerToPut=this.listenersToPut[i];ReactBrowserEventEmitter.putListener(listenerToPut.rootNodeID,listenerToPut.propKey,listenerToPut.propValue);}},reset:function(){this.listenersToPut.length=0;},destructor:function(){this.reset();}});PooledClass.addPoolingTo(ReactPutListenerQueue);module.exports=ReactPutListenerQueue;},{"27":27,"28":28,"30":30}],80:[function(_dereq_,module,exports){'use strict';var CallbackQueue=_dereq_(6);var PooledClass=_dereq_(28);var ReactBrowserEventEmitter=_dereq_(30);var ReactInputSelection=_dereq_(65);var ReactPutListenerQueue=_dereq_(79);var Transaction=_dereq_(103);var assign=_dereq_(27);var SELECTION_RESTORATION={initialize:ReactInputSelection.getSelectionInformation,close:ReactInputSelection.restoreSelection};var EVENT_SUPPRESSION={initialize:function(){var currentlyEnabled=ReactBrowserEventEmitter.isEnabled();ReactBrowserEventEmitter.setEnabled(false);return currentlyEnabled;},close:function(previouslyEnabled){ReactBrowserEventEmitter.setEnabled(previouslyEnabled);}};var ON_DOM_READY_QUEUEING={initialize:function(){this.reactMountReady.reset();},close:function(){this.reactMountReady.notifyAll();}};var PUT_LISTENER_QUEUEING={initialize:function(){this.putListenerQueue.reset();},close:function(){this.putListenerQueue.putListeners();}};var TRANSACTION_WRAPPERS=[PUT_LISTENER_QUEUEING,SELECTION_RESTORATION,EVENT_SUPPRESSION,ON_DOM_READY_QUEUEING];function ReactReconcileTransaction(){this.reinitializeTransaction();



this.renderToStaticMarkup=false;this.reactMountReady=CallbackQueue.getPooled(null);this.putListenerQueue=ReactPutListenerQueue.getPooled();}
var Mixin={getTransactionWrappers:function(){return TRANSACTION_WRAPPERS;}, getReactMountReady:function(){return this.reactMountReady;},getPutListenerQueue:function(){return this.putListenerQueue;},destructor:function(){CallbackQueue.release(this.reactMountReady);this.reactMountReady=null;ReactPutListenerQueue.release(this.putListenerQueue);this.putListenerQueue=null;}};assign(ReactReconcileTransaction.prototype,Transaction.Mixin,Mixin);PooledClass.addPoolingTo(ReactReconcileTransaction);module.exports=ReactReconcileTransaction;},{"103":103,"27":27,"28":28,"30":30,"6":6,"65":65,"79":79}],81:[function(_dereq_,module,exports){'use strict';var ReactRef=_dereq_(82);var ReactElementValidator=_dereq_(58);function attachRefs(){ReactRef.attachRefs(this,this._currentElement);}
var ReactReconciler={mountComponent:function(internalInstance,rootID,transaction,context){var markup=internalInstance.mountComponent(rootID,transaction,context);if("production"!=="development"){ReactElementValidator.checkAndWarnForMutatedProps(internalInstance._currentElement);}
transaction.getReactMountReady().enqueue(attachRefs,internalInstance);return markup;},unmountComponent:function(internalInstance){ReactRef.detachRefs(internalInstance,internalInstance._currentElement);internalInstance.unmountComponent();},receiveComponent:function(internalInstance,nextElement,transaction,context){var prevElement=internalInstance._currentElement;if(nextElement===prevElement&&nextElement._owner!=null){




return;}
if("production"!=="development"){ReactElementValidator.checkAndWarnForMutatedProps(nextElement);}
var refsChanged=ReactRef.shouldUpdateRefs(prevElement,nextElement);if(refsChanged){ReactRef.detachRefs(internalInstance,prevElement);}
internalInstance.receiveComponent(nextElement,transaction,context);if(refsChanged){transaction.getReactMountReady().enqueue(attachRefs,internalInstance);}},performUpdateIfNecessary:function(internalInstance,transaction){internalInstance.performUpdateIfNecessary(transaction);}};module.exports=ReactReconciler;},{"58":58,"82":82}],82:[function(_dereq_,module,exports){'use strict';var ReactOwner=_dereq_(74);var ReactRef={};function attachRef(ref,component,owner){if(typeof ref==='function'){ref(component.getPublicInstance());}else{ ReactOwner.addComponentAsRefTo(component,ref,owner);}}
function detachRef(ref,component,owner){if(typeof ref==='function'){ref(null);}else{ ReactOwner.removeComponentAsRefFrom(component,ref,owner);}}
ReactRef.attachRefs=function(instance,element){var ref=element.ref;if(ref!=null){attachRef(ref,instance,element._owner);}};ReactRef.shouldUpdateRefs=function(prevElement,nextElement){







return(nextElement._owner!==prevElement._owner||nextElement.ref!==prevElement.ref);};ReactRef.detachRefs=function(instance,element){var ref=element.ref;if(ref!=null){detachRef(ref,instance,element._owner);}};module.exports=ReactRef;},{"74":74}],83:[function(_dereq_,module,exports){'use strict';var ReactRootIndexInjection={injectCreateReactRootIndex:function(_createReactRootIndex){ReactRootIndex.createReactRootIndex=_createReactRootIndex;}};var ReactRootIndex={createReactRootIndex:null,injection:ReactRootIndexInjection};module.exports=ReactRootIndex;},{}],84:[function(_dereq_,module,exports){'use strict';var ReactElement=_dereq_(57);var ReactInstanceHandles=_dereq_(66);var ReactMarkupChecksum=_dereq_(69);var ReactServerRenderingTransaction=_dereq_(85);var emptyObject=_dereq_(115);var instantiateReactComponent=_dereq_(134);var invariant=_dereq_(135);function renderToString(element){("production"!=="development"?invariant(ReactElement.isValidElement(element),'renderToString(): You must pass a valid ReactElement.'):invariant(ReactElement.isValidElement(element)));var transaction;try{var id=ReactInstanceHandles.createReactRootID();transaction=ReactServerRenderingTransaction.getPooled(false);return transaction.perform(function(){var componentInstance=instantiateReactComponent(element,null);var markup=componentInstance.mountComponent(id,transaction,emptyObject);return ReactMarkupChecksum.addChecksumToMarkup(markup);},null);}finally{ReactServerRenderingTransaction.release(transaction);}}
function renderToStaticMarkup(element){("production"!=="development"?invariant(ReactElement.isValidElement(element),'renderToStaticMarkup(): You must pass a valid ReactElement.'):invariant(ReactElement.isValidElement(element)));var transaction;try{var id=ReactInstanceHandles.createReactRootID();transaction=ReactServerRenderingTransaction.getPooled(true);return transaction.perform(function(){var componentInstance=instantiateReactComponent(element,null);return componentInstance.mountComponent(id,transaction,emptyObject);},null);}finally{ReactServerRenderingTransaction.release(transaction);}}
module.exports={renderToString:renderToString,renderToStaticMarkup:renderToStaticMarkup};},{"115":115,"134":134,"135":135,"57":57,"66":66,"69":69,"85":85}],85:[function(_dereq_,module,exports){'use strict';var PooledClass=_dereq_(28);var CallbackQueue=_dereq_(6);var ReactPutListenerQueue=_dereq_(79);var Transaction=_dereq_(103);var assign=_dereq_(27);var emptyFunction=_dereq_(114);var ON_DOM_READY_QUEUEING={initialize:function(){this.reactMountReady.reset();},close:emptyFunction};var PUT_LISTENER_QUEUEING={initialize:function(){this.putListenerQueue.reset();},close:emptyFunction};var TRANSACTION_WRAPPERS=[PUT_LISTENER_QUEUEING,ON_DOM_READY_QUEUEING];function ReactServerRenderingTransaction(renderToStaticMarkup){this.reinitializeTransaction();this.renderToStaticMarkup=renderToStaticMarkup;this.reactMountReady=CallbackQueue.getPooled(null);this.putListenerQueue=ReactPutListenerQueue.getPooled();}
var Mixin={getTransactionWrappers:function(){return TRANSACTION_WRAPPERS;}, getReactMountReady:function(){return this.reactMountReady;},getPutListenerQueue:function(){return this.putListenerQueue;},destructor:function(){CallbackQueue.release(this.reactMountReady);this.reactMountReady=null;ReactPutListenerQueue.release(this.putListenerQueue);this.putListenerQueue=null;}};assign(ReactServerRenderingTransaction.prototype,Transaction.Mixin,Mixin);PooledClass.addPoolingTo(ReactServerRenderingTransaction);module.exports=ReactServerRenderingTransaction;},{"103":103,"114":114,"27":27,"28":28,"6":6,"79":79}],86:[function(_dereq_,module,exports){'use strict';var ReactLifeCycle=_dereq_(68);var ReactCurrentOwner=_dereq_(39);var ReactElement=_dereq_(57);var ReactInstanceMap=_dereq_(67);var ReactUpdates=_dereq_(87);var assign=_dereq_(27);var invariant=_dereq_(135);var warning=_dereq_(154);function enqueueUpdate(internalInstance){if(internalInstance!==ReactLifeCycle.currentlyMountingInstance){

ReactUpdates.enqueueUpdate(internalInstance);}}
function getInternalInstanceReadyForUpdate(publicInstance,callerName){("production"!=="development"?invariant(ReactCurrentOwner.current==null,'%s(...): Cannot update during an existing state transition '+'(such as within `render`). Render methods should be a pure function '+'of props and state.',callerName):invariant(ReactCurrentOwner.current==null));var internalInstance=ReactInstanceMap.get(publicInstance);if(!internalInstance){if("production"!=="development"){
("production"!=="development"?warning(!callerName,'%s(...): Can only update a mounted or mounting component. '+'This usually means you called %s() on an unmounted '+'component. This is a no-op.',callerName,callerName):null);}
return null;}
if(internalInstance===ReactLifeCycle.currentlyUnmountingInstance){return null;}
return internalInstance;}
var ReactUpdateQueue={enqueueCallback:function(publicInstance,callback){("production"!=="development"?invariant(typeof callback==='function','enqueueCallback(...): You called `setProps`, `replaceProps`, '+'`setState`, `replaceState`, or `forceUpdate` with a callback that '+'isn\'t callable.'):invariant(typeof callback==='function'));var internalInstance=getInternalInstanceReadyForUpdate(publicInstance);


if(!internalInstance||internalInstance===ReactLifeCycle.currentlyMountingInstance){return null;}
if(internalInstance._pendingCallbacks){internalInstance._pendingCallbacks.push(callback);}else{internalInstance._pendingCallbacks=[callback];}



enqueueUpdate(internalInstance);},enqueueCallbackInternal:function(internalInstance,callback){("production"!=="development"?invariant(typeof callback==='function','enqueueCallback(...): You called `setProps`, `replaceProps`, '+'`setState`, `replaceState`, or `forceUpdate` with a callback that '+'isn\'t callable.'):invariant(typeof callback==='function'));if(internalInstance._pendingCallbacks){internalInstance._pendingCallbacks.push(callback);}else{internalInstance._pendingCallbacks=[callback];}
enqueueUpdate(internalInstance);},enqueueForceUpdate:function(publicInstance){var internalInstance=getInternalInstanceReadyForUpdate(publicInstance,'forceUpdate');if(!internalInstance){return;}
internalInstance._pendingForceUpdate=true;enqueueUpdate(internalInstance);},enqueueReplaceState:function(publicInstance,completeState){var internalInstance=getInternalInstanceReadyForUpdate(publicInstance,'replaceState');if(!internalInstance){return;}
internalInstance._pendingStateQueue=[completeState];internalInstance._pendingReplaceState=true;enqueueUpdate(internalInstance);},enqueueSetState:function(publicInstance,partialState){var internalInstance=getInternalInstanceReadyForUpdate(publicInstance,'setState');if(!internalInstance){return;}
var queue=internalInstance._pendingStateQueue||(internalInstance._pendingStateQueue=[]);queue.push(partialState);enqueueUpdate(internalInstance);},enqueueSetProps:function(publicInstance,partialProps){var internalInstance=getInternalInstanceReadyForUpdate(publicInstance,'setProps');if(!internalInstance){return;}
("production"!=="development"?invariant(internalInstance._isTopLevel,'setProps(...): You called `setProps` on a '+'component with a parent. This is an anti-pattern since props will '+'get reactively updated when rendered. Instead, change the owner\'s '+'`render` method to pass the correct value as props to the component '+'where it is created.'):invariant(internalInstance._isTopLevel));
var element=internalInstance._pendingElement||internalInstance._currentElement;var props=assign({},element.props,partialProps);internalInstance._pendingElement=ReactElement.cloneAndReplaceProps(element,props);enqueueUpdate(internalInstance);},enqueueReplaceProps:function(publicInstance,props){var internalInstance=getInternalInstanceReadyForUpdate(publicInstance,'replaceProps');if(!internalInstance){return;}
("production"!=="development"?invariant(internalInstance._isTopLevel,'replaceProps(...): You called `replaceProps` on a '+'component with a parent. This is an anti-pattern since props will '+'get reactively updated when rendered. Instead, change the owner\'s '+'`render` method to pass the correct value as props to the component '+'where it is created.'):invariant(internalInstance._isTopLevel));
var element=internalInstance._pendingElement||internalInstance._currentElement;internalInstance._pendingElement=ReactElement.cloneAndReplaceProps(element,props);enqueueUpdate(internalInstance);},enqueueElementInternal:function(internalInstance,newElement){internalInstance._pendingElement=newElement;enqueueUpdate(internalInstance);}};module.exports=ReactUpdateQueue;},{"135":135,"154":154,"27":27,"39":39,"57":57,"67":67,"68":68,"87":87}],87:[function(_dereq_,module,exports){'use strict';var CallbackQueue=_dereq_(6);var PooledClass=_dereq_(28);var ReactCurrentOwner=_dereq_(39);var ReactPerf=_dereq_(75);var ReactReconciler=_dereq_(81);var Transaction=_dereq_(103);var assign=_dereq_(27);var invariant=_dereq_(135);var warning=_dereq_(154);var dirtyComponents=[];var asapCallbackQueue=CallbackQueue.getPooled();var asapEnqueued=false;var batchingStrategy=null;function ensureInjected(){("production"!=="development"?invariant(ReactUpdates.ReactReconcileTransaction&&batchingStrategy,'ReactUpdates: must inject a reconcile transaction class and batching '+'strategy'):invariant(ReactUpdates.ReactReconcileTransaction&&batchingStrategy));}
var NESTED_UPDATES={initialize:function(){this.dirtyComponentsLength=dirtyComponents.length;},close:function(){if(this.dirtyComponentsLength!==dirtyComponents.length){



dirtyComponents.splice(0,this.dirtyComponentsLength);flushBatchedUpdates();}else{dirtyComponents.length=0;}}};var UPDATE_QUEUEING={initialize:function(){this.callbackQueue.reset();},close:function(){this.callbackQueue.notifyAll();}};var TRANSACTION_WRAPPERS=[NESTED_UPDATES,UPDATE_QUEUEING];function ReactUpdatesFlushTransaction(){this.reinitializeTransaction();this.dirtyComponentsLength=null;this.callbackQueue=CallbackQueue.getPooled();this.reconcileTransaction=ReactUpdates.ReactReconcileTransaction.getPooled();}
assign(ReactUpdatesFlushTransaction.prototype,Transaction.Mixin,{getTransactionWrappers:function(){return TRANSACTION_WRAPPERS;},destructor:function(){this.dirtyComponentsLength=null;CallbackQueue.release(this.callbackQueue);this.callbackQueue=null;ReactUpdates.ReactReconcileTransaction.release(this.reconcileTransaction);this.reconcileTransaction=null;},perform:function(method,scope,a){return Transaction.Mixin.perform.call(this,this.reconcileTransaction.perform,this.reconcileTransaction,method,scope,a);}});PooledClass.addPoolingTo(ReactUpdatesFlushTransaction);function batchedUpdates(callback,a,b,c,d){ensureInjected();batchingStrategy.batchedUpdates(callback,a,b,c,d);}
function mountOrderComparator(c1,c2){return c1._mountOrder-c2._mountOrder;}
function runBatchedUpdates(transaction){var len=transaction.dirtyComponentsLength;("production"!=="development"?invariant(len===dirtyComponents.length,'Expected flush transaction\'s stored dirty-components length (%s) to '+'match dirty-components array length (%s).',len,dirtyComponents.length):invariant(len===dirtyComponents.length));

dirtyComponents.sort(mountOrderComparator);for(var i=0;i<len;i++){

var component=dirtyComponents[i];

 var callbacks=component._pendingCallbacks;component._pendingCallbacks=null;ReactReconciler.performUpdateIfNecessary(component,transaction.reconcileTransaction);if(callbacks){for(var j=0;j<callbacks.length;j++){transaction.callbackQueue.enqueue(callbacks[j],component.getPublicInstance());}}}}
var flushBatchedUpdates=function(){

while(dirtyComponents.length||asapEnqueued){if(dirtyComponents.length){var transaction=ReactUpdatesFlushTransaction.getPooled();transaction.perform(runBatchedUpdates,null,transaction);ReactUpdatesFlushTransaction.release(transaction);}
if(asapEnqueued){asapEnqueued=false;var queue=asapCallbackQueue;asapCallbackQueue=CallbackQueue.getPooled();queue.notifyAll();CallbackQueue.release(queue);}}};flushBatchedUpdates=ReactPerf.measure('ReactUpdates','flushBatchedUpdates',flushBatchedUpdates);function enqueueUpdate(component){ensureInjected();


("production"!=="development"?warning(ReactCurrentOwner.current==null,'enqueueUpdate(): Render methods should be a pure function of props '+'and state; triggering nested component updates from render is not '+'allowed. If necessary, trigger nested updates in '+'componentDidUpdate.'):null);if(!batchingStrategy.isBatchingUpdates){batchingStrategy.batchedUpdates(enqueueUpdate,component);return;}
dirtyComponents.push(component);}
function asap(callback,context){("production"!=="development"?invariant(batchingStrategy.isBatchingUpdates,'ReactUpdates.asap: Can\'t enqueue an asap callback in a context where'+'updates are not being batched.'):invariant(batchingStrategy.isBatchingUpdates));asapCallbackQueue.enqueue(callback,context);asapEnqueued=true;}
var ReactUpdatesInjection={injectReconcileTransaction:function(ReconcileTransaction){("production"!=="development"?invariant(ReconcileTransaction,'ReactUpdates: must provide a reconcile transaction class'):invariant(ReconcileTransaction));ReactUpdates.ReactReconcileTransaction=ReconcileTransaction;},injectBatchingStrategy:function(_batchingStrategy){("production"!=="development"?invariant(_batchingStrategy,'ReactUpdates: must provide a batching strategy'):invariant(_batchingStrategy));("production"!=="development"?invariant(typeof _batchingStrategy.batchedUpdates==='function','ReactUpdates: must provide a batchedUpdates() function'):invariant(typeof _batchingStrategy.batchedUpdates==='function'));("production"!=="development"?invariant(typeof _batchingStrategy.isBatchingUpdates==='boolean','ReactUpdates: must provide an isBatchingUpdates boolean attribute'):invariant(typeof _batchingStrategy.isBatchingUpdates==='boolean'));batchingStrategy=_batchingStrategy;}};var ReactUpdates={ReactReconcileTransaction:null,batchedUpdates:batchedUpdates,enqueueUpdate:enqueueUpdate,flushBatchedUpdates:flushBatchedUpdates,injection:ReactUpdatesInjection,asap:asap};module.exports=ReactUpdates;},{"103":103,"135":135,"154":154,"27":27,"28":28,"39":39,"6":6,"75":75,"81":81}],88:[function(_dereq_,module,exports){'use strict';var DOMProperty=_dereq_(10);var MUST_USE_ATTRIBUTE=DOMProperty.injection.MUST_USE_ATTRIBUTE;var SVGDOMPropertyConfig={Properties:{cx:MUST_USE_ATTRIBUTE,cy:MUST_USE_ATTRIBUTE,d:MUST_USE_ATTRIBUTE,dx:MUST_USE_ATTRIBUTE,dy:MUST_USE_ATTRIBUTE,fill:MUST_USE_ATTRIBUTE,fillOpacity:MUST_USE_ATTRIBUTE,fontFamily:MUST_USE_ATTRIBUTE,fontSize:MUST_USE_ATTRIBUTE,fx:MUST_USE_ATTRIBUTE,fy:MUST_USE_ATTRIBUTE,gradientTransform:MUST_USE_ATTRIBUTE,gradientUnits:MUST_USE_ATTRIBUTE,markerEnd:MUST_USE_ATTRIBUTE,markerMid:MUST_USE_ATTRIBUTE,markerStart:MUST_USE_ATTRIBUTE,offset:MUST_USE_ATTRIBUTE,opacity:MUST_USE_ATTRIBUTE,patternContentUnits:MUST_USE_ATTRIBUTE,patternUnits:MUST_USE_ATTRIBUTE,points:MUST_USE_ATTRIBUTE,preserveAspectRatio:MUST_USE_ATTRIBUTE,r:MUST_USE_ATTRIBUTE,rx:MUST_USE_ATTRIBUTE,ry:MUST_USE_ATTRIBUTE,spreadMethod:MUST_USE_ATTRIBUTE,stopColor:MUST_USE_ATTRIBUTE,stopOpacity:MUST_USE_ATTRIBUTE,stroke:MUST_USE_ATTRIBUTE,strokeDasharray:MUST_USE_ATTRIBUTE,strokeLinecap:MUST_USE_ATTRIBUTE,strokeOpacity:MUST_USE_ATTRIBUTE,strokeWidth:MUST_USE_ATTRIBUTE,textAnchor:MUST_USE_ATTRIBUTE,transform:MUST_USE_ATTRIBUTE,version:MUST_USE_ATTRIBUTE,viewBox:MUST_USE_ATTRIBUTE,x1:MUST_USE_ATTRIBUTE,x2:MUST_USE_ATTRIBUTE,x:MUST_USE_ATTRIBUTE,y1:MUST_USE_ATTRIBUTE,y2:MUST_USE_ATTRIBUTE,y:MUST_USE_ATTRIBUTE},DOMAttributeNames:{fillOpacity:'fill-opacity',fontFamily:'font-family',fontSize:'font-size',gradientTransform:'gradientTransform',gradientUnits:'gradientUnits',markerEnd:'marker-end',markerMid:'marker-mid',markerStart:'marker-start',patternContentUnits:'patternContentUnits',patternUnits:'patternUnits',preserveAspectRatio:'preserveAspectRatio',spreadMethod:'spreadMethod',stopColor:'stop-color',stopOpacity:'stop-opacity',strokeDasharray:'stroke-dasharray',strokeLinecap:'stroke-linecap',strokeOpacity:'stroke-opacity',strokeWidth:'stroke-width',textAnchor:'text-anchor',viewBox:'viewBox'}};module.exports=SVGDOMPropertyConfig;},{"10":10}],89:[function(_dereq_,module,exports){'use strict';var EventConstants=_dereq_(15);var EventPropagators=_dereq_(20);var ReactInputSelection=_dereq_(65);var SyntheticEvent=_dereq_(95);var getActiveElement=_dereq_(121);var isTextInputElement=_dereq_(138);var keyOf=_dereq_(141);var shallowEqual=_dereq_(150);var topLevelTypes=EventConstants.topLevelTypes;var eventTypes={select:{phasedRegistrationNames:{bubbled:keyOf({onSelect:null}),captured:keyOf({onSelectCapture:null})},dependencies:[topLevelTypes.topBlur,topLevelTypes.topContextMenu,topLevelTypes.topFocus,topLevelTypes.topKeyDown,topLevelTypes.topMouseDown,topLevelTypes.topMouseUp,topLevelTypes.topSelectionChange]}};var activeElement=null;var activeElementID=null;var lastSelection=null;var mouseDown=false;function getSelection(node){if('selectionStart'in node&&ReactInputSelection.hasSelectionCapabilities(node)){return{start:node.selectionStart,end:node.selectionEnd};}else if(window.getSelection){var selection=window.getSelection();return{anchorNode:selection.anchorNode,anchorOffset:selection.anchorOffset,focusNode:selection.focusNode,focusOffset:selection.focusOffset};}else if(document.selection){var range=document.selection.createRange();return{parentElement:range.parentElement(),text:range.text,top:range.boundingTop,left:range.boundingLeft};}}
function constructSelectEvent(nativeEvent){


if(mouseDown||activeElement==null||activeElement!==getActiveElement()){return null;}
var currentSelection=getSelection(activeElement);if(!lastSelection||!shallowEqual(lastSelection,currentSelection)){lastSelection=currentSelection;var syntheticEvent=SyntheticEvent.getPooled(eventTypes.select,activeElementID,nativeEvent);syntheticEvent.type='select';syntheticEvent.target=activeElement;EventPropagators.accumulateTwoPhaseDispatches(syntheticEvent);return syntheticEvent;}}
var SelectEventPlugin={eventTypes:eventTypes,extractEvents:function(topLevelType,topLevelTarget,topLevelTargetID,nativeEvent){switch(topLevelType){case topLevelTypes.topFocus:if(isTextInputElement(topLevelTarget)||topLevelTarget.contentEditable==='true'){activeElement=topLevelTarget;activeElementID=topLevelTargetID;lastSelection=null;}
break;case topLevelTypes.topBlur:activeElement=null;activeElementID=null;lastSelection=null;break;
case topLevelTypes.topMouseDown:mouseDown=true;break;case topLevelTypes.topContextMenu:case topLevelTypes.topMouseUp:mouseDown=false;return constructSelectEvent(nativeEvent);



case topLevelTypes.topSelectionChange:case topLevelTypes.topKeyDown:case topLevelTypes.topKeyUp:return constructSelectEvent(nativeEvent);}}};module.exports=SelectEventPlugin;},{"121":121,"138":138,"141":141,"15":15,"150":150,"20":20,"65":65,"95":95}],90:[function(_dereq_,module,exports){'use strict';var GLOBAL_MOUNT_POINT_MAX=Math.pow(2,53);var ServerReactRootIndex={createReactRootIndex:function(){return Math.ceil(Math.random()*GLOBAL_MOUNT_POINT_MAX);}};module.exports=ServerReactRootIndex;},{}],91:[function(_dereq_,module,exports){'use strict';var EventConstants=_dereq_(15);var EventPluginUtils=_dereq_(19);var EventPropagators=_dereq_(20);var SyntheticClipboardEvent=_dereq_(92);var SyntheticEvent=_dereq_(95);var SyntheticFocusEvent=_dereq_(96);var SyntheticKeyboardEvent=_dereq_(98);var SyntheticMouseEvent=_dereq_(99);var SyntheticDragEvent=_dereq_(94);var SyntheticTouchEvent=_dereq_(100);var SyntheticUIEvent=_dereq_(101);var SyntheticWheelEvent=_dereq_(102);var getEventCharCode=_dereq_(122);var invariant=_dereq_(135);var keyOf=_dereq_(141);var warning=_dereq_(154);var topLevelTypes=EventConstants.topLevelTypes;var eventTypes={blur:{phasedRegistrationNames:{bubbled:keyOf({onBlur:true}),captured:keyOf({onBlurCapture:true})}},click:{phasedRegistrationNames:{bubbled:keyOf({onClick:true}),captured:keyOf({onClickCapture:true})}},contextMenu:{phasedRegistrationNames:{bubbled:keyOf({onContextMenu:true}),captured:keyOf({onContextMenuCapture:true})}},copy:{phasedRegistrationNames:{bubbled:keyOf({onCopy:true}),captured:keyOf({onCopyCapture:true})}},cut:{phasedRegistrationNames:{bubbled:keyOf({onCut:true}),captured:keyOf({onCutCapture:true})}},doubleClick:{phasedRegistrationNames:{bubbled:keyOf({onDoubleClick:true}),captured:keyOf({onDoubleClickCapture:true})}},drag:{phasedRegistrationNames:{bubbled:keyOf({onDrag:true}),captured:keyOf({onDragCapture:true})}},dragEnd:{phasedRegistrationNames:{bubbled:keyOf({onDragEnd:true}),captured:keyOf({onDragEndCapture:true})}},dragEnter:{phasedRegistrationNames:{bubbled:keyOf({onDragEnter:true}),captured:keyOf({onDragEnterCapture:true})}},dragExit:{phasedRegistrationNames:{bubbled:keyOf({onDragExit:true}),captured:keyOf({onDragExitCapture:true})}},dragLeave:{phasedRegistrationNames:{bubbled:keyOf({onDragLeave:true}),captured:keyOf({onDragLeaveCapture:true})}},dragOver:{phasedRegistrationNames:{bubbled:keyOf({onDragOver:true}),captured:keyOf({onDragOverCapture:true})}},dragStart:{phasedRegistrationNames:{bubbled:keyOf({onDragStart:true}),captured:keyOf({onDragStartCapture:true})}},drop:{phasedRegistrationNames:{bubbled:keyOf({onDrop:true}),captured:keyOf({onDropCapture:true})}},focus:{phasedRegistrationNames:{bubbled:keyOf({onFocus:true}),captured:keyOf({onFocusCapture:true})}},input:{phasedRegistrationNames:{bubbled:keyOf({onInput:true}),captured:keyOf({onInputCapture:true})}},keyDown:{phasedRegistrationNames:{bubbled:keyOf({onKeyDown:true}),captured:keyOf({onKeyDownCapture:true})}},keyPress:{phasedRegistrationNames:{bubbled:keyOf({onKeyPress:true}),captured:keyOf({onKeyPressCapture:true})}},keyUp:{phasedRegistrationNames:{bubbled:keyOf({onKeyUp:true}),captured:keyOf({onKeyUpCapture:true})}},load:{phasedRegistrationNames:{bubbled:keyOf({onLoad:true}),captured:keyOf({onLoadCapture:true})}},error:{phasedRegistrationNames:{bubbled:keyOf({onError:true}),captured:keyOf({onErrorCapture:true})}},
mouseDown:{phasedRegistrationNames:{bubbled:keyOf({onMouseDown:true}),captured:keyOf({onMouseDownCapture:true})}},mouseMove:{phasedRegistrationNames:{bubbled:keyOf({onMouseMove:true}),captured:keyOf({onMouseMoveCapture:true})}},mouseOut:{phasedRegistrationNames:{bubbled:keyOf({onMouseOut:true}),captured:keyOf({onMouseOutCapture:true})}},mouseOver:{phasedRegistrationNames:{bubbled:keyOf({onMouseOver:true}),captured:keyOf({onMouseOverCapture:true})}},mouseUp:{phasedRegistrationNames:{bubbled:keyOf({onMouseUp:true}),captured:keyOf({onMouseUpCapture:true})}},paste:{phasedRegistrationNames:{bubbled:keyOf({onPaste:true}),captured:keyOf({onPasteCapture:true})}},reset:{phasedRegistrationNames:{bubbled:keyOf({onReset:true}),captured:keyOf({onResetCapture:true})}},scroll:{phasedRegistrationNames:{bubbled:keyOf({onScroll:true}),captured:keyOf({onScrollCapture:true})}},submit:{phasedRegistrationNames:{bubbled:keyOf({onSubmit:true}),captured:keyOf({onSubmitCapture:true})}},touchCancel:{phasedRegistrationNames:{bubbled:keyOf({onTouchCancel:true}),captured:keyOf({onTouchCancelCapture:true})}},touchEnd:{phasedRegistrationNames:{bubbled:keyOf({onTouchEnd:true}),captured:keyOf({onTouchEndCapture:true})}},touchMove:{phasedRegistrationNames:{bubbled:keyOf({onTouchMove:true}),captured:keyOf({onTouchMoveCapture:true})}},touchStart:{phasedRegistrationNames:{bubbled:keyOf({onTouchStart:true}),captured:keyOf({onTouchStartCapture:true})}},wheel:{phasedRegistrationNames:{bubbled:keyOf({onWheel:true}),captured:keyOf({onWheelCapture:true})}}};var topLevelEventsToDispatchConfig={topBlur:eventTypes.blur,topClick:eventTypes.click,topContextMenu:eventTypes.contextMenu,topCopy:eventTypes.copy,topCut:eventTypes.cut,topDoubleClick:eventTypes.doubleClick,topDrag:eventTypes.drag,topDragEnd:eventTypes.dragEnd,topDragEnter:eventTypes.dragEnter,topDragExit:eventTypes.dragExit,topDragLeave:eventTypes.dragLeave,topDragOver:eventTypes.dragOver,topDragStart:eventTypes.dragStart,topDrop:eventTypes.drop,topError:eventTypes.error,topFocus:eventTypes.focus,topInput:eventTypes.input,topKeyDown:eventTypes.keyDown,topKeyPress:eventTypes.keyPress,topKeyUp:eventTypes.keyUp,topLoad:eventTypes.load,topMouseDown:eventTypes.mouseDown,topMouseMove:eventTypes.mouseMove,topMouseOut:eventTypes.mouseOut,topMouseOver:eventTypes.mouseOver,topMouseUp:eventTypes.mouseUp,topPaste:eventTypes.paste,topReset:eventTypes.reset,topScroll:eventTypes.scroll,topSubmit:eventTypes.submit,topTouchCancel:eventTypes.touchCancel,topTouchEnd:eventTypes.touchEnd,topTouchMove:eventTypes.touchMove,topTouchStart:eventTypes.touchStart,topWheel:eventTypes.wheel};for(var type in topLevelEventsToDispatchConfig){topLevelEventsToDispatchConfig[type].dependencies=[type];}
var SimpleEventPlugin={eventTypes:eventTypes,executeDispatch:function(event,listener,domID){var returnValue=EventPluginUtils.executeDispatch(event,listener,domID);("production"!=="development"?warning(typeof returnValue!=='boolean','Returning `false` from an event handler is deprecated and will be '+'ignored in a future release. Instead, manually call '+'e.stopPropagation() or e.preventDefault(), as appropriate.'):null);if(returnValue===false){event.stopPropagation();event.preventDefault();}},extractEvents:function(topLevelType,topLevelTarget,topLevelTargetID,nativeEvent){var dispatchConfig=topLevelEventsToDispatchConfig[topLevelType];if(!dispatchConfig){return null;}
var EventConstructor;switch(topLevelType){case topLevelTypes.topInput:case topLevelTypes.topLoad:case topLevelTypes.topError:case topLevelTypes.topReset:case topLevelTypes.topSubmit:
 EventConstructor=SyntheticEvent;break;case topLevelTypes.topKeyPress:

if(getEventCharCode(nativeEvent)===0){return null;}
case topLevelTypes.topKeyDown:case topLevelTypes.topKeyUp:EventConstructor=SyntheticKeyboardEvent;break;case topLevelTypes.topBlur:case topLevelTypes.topFocus:EventConstructor=SyntheticFocusEvent;break;case topLevelTypes.topClick:
if(nativeEvent.button===2){return null;}
case topLevelTypes.topContextMenu:case topLevelTypes.topDoubleClick:case topLevelTypes.topMouseDown:case topLevelTypes.topMouseMove:case topLevelTypes.topMouseOut:case topLevelTypes.topMouseOver:case topLevelTypes.topMouseUp:EventConstructor=SyntheticMouseEvent;break;case topLevelTypes.topDrag:case topLevelTypes.topDragEnd:case topLevelTypes.topDragEnter:case topLevelTypes.topDragExit:case topLevelTypes.topDragLeave:case topLevelTypes.topDragOver:case topLevelTypes.topDragStart:case topLevelTypes.topDrop:EventConstructor=SyntheticDragEvent;break;case topLevelTypes.topTouchCancel:case topLevelTypes.topTouchEnd:case topLevelTypes.topTouchMove:case topLevelTypes.topTouchStart:EventConstructor=SyntheticTouchEvent;break;case topLevelTypes.topScroll:EventConstructor=SyntheticUIEvent;break;case topLevelTypes.topWheel:EventConstructor=SyntheticWheelEvent;break;case topLevelTypes.topCopy:case topLevelTypes.topCut:case topLevelTypes.topPaste:EventConstructor=SyntheticClipboardEvent;break;}
("production"!=="development"?invariant(EventConstructor,'SimpleEventPlugin: Unhandled event type, `%s`.',topLevelType):invariant(EventConstructor));var event=EventConstructor.getPooled(dispatchConfig,topLevelTargetID,nativeEvent);EventPropagators.accumulateTwoPhaseDispatches(event);return event;}};module.exports=SimpleEventPlugin;},{"100":100,"101":101,"102":102,"122":122,"135":135,"141":141,"15":15,"154":154,"19":19,"20":20,"92":92,"94":94,"95":95,"96":96,"98":98,"99":99}],92:[function(_dereq_,module,exports){'use strict';var SyntheticEvent=_dereq_(95);var ClipboardEventInterface={clipboardData:function(event){return('clipboardData'in event?event.clipboardData:window.clipboardData);}};function SyntheticClipboardEvent(dispatchConfig,dispatchMarker,nativeEvent){SyntheticEvent.call(this,dispatchConfig,dispatchMarker,nativeEvent);}
SyntheticEvent.augmentClass(SyntheticClipboardEvent,ClipboardEventInterface);module.exports=SyntheticClipboardEvent;},{"95":95}],93:[function(_dereq_,module,exports){'use strict';var SyntheticEvent=_dereq_(95);var CompositionEventInterface={data:null};function SyntheticCompositionEvent(dispatchConfig,dispatchMarker,nativeEvent){SyntheticEvent.call(this,dispatchConfig,dispatchMarker,nativeEvent);}
SyntheticEvent.augmentClass(SyntheticCompositionEvent,CompositionEventInterface);module.exports=SyntheticCompositionEvent;},{"95":95}],94:[function(_dereq_,module,exports){'use strict';var SyntheticMouseEvent=_dereq_(99);var DragEventInterface={dataTransfer:null};function SyntheticDragEvent(dispatchConfig,dispatchMarker,nativeEvent){SyntheticMouseEvent.call(this,dispatchConfig,dispatchMarker,nativeEvent);}
SyntheticMouseEvent.augmentClass(SyntheticDragEvent,DragEventInterface);module.exports=SyntheticDragEvent;},{"99":99}],95:[function(_dereq_,module,exports){'use strict';var PooledClass=_dereq_(28);var assign=_dereq_(27);var emptyFunction=_dereq_(114);var getEventTarget=_dereq_(125);var EventInterface={type:null,target:getEventTarget, currentTarget:emptyFunction.thatReturnsNull,eventPhase:null,bubbles:null,cancelable:null,timeStamp:function(event){return event.timeStamp||Date.now();},defaultPrevented:null,isTrusted:null};function SyntheticEvent(dispatchConfig,dispatchMarker,nativeEvent){this.dispatchConfig=dispatchConfig;this.dispatchMarker=dispatchMarker;this.nativeEvent=nativeEvent;var Interface=this.constructor.Interface;for(var propName in Interface){if(!Interface.hasOwnProperty(propName)){continue;}
var normalize=Interface[propName];if(normalize){this[propName]=normalize(nativeEvent);}else{this[propName]=nativeEvent[propName];}}
var defaultPrevented=nativeEvent.defaultPrevented!=null?nativeEvent.defaultPrevented:nativeEvent.returnValue===false;if(defaultPrevented){this.isDefaultPrevented=emptyFunction.thatReturnsTrue;}else{this.isDefaultPrevented=emptyFunction.thatReturnsFalse;}
this.isPropagationStopped=emptyFunction.thatReturnsFalse;}
assign(SyntheticEvent.prototype,{preventDefault:function(){this.defaultPrevented=true;var event=this.nativeEvent;if(event.preventDefault){event.preventDefault();}else{event.returnValue=false;}
this.isDefaultPrevented=emptyFunction.thatReturnsTrue;},stopPropagation:function(){var event=this.nativeEvent;if(event.stopPropagation){event.stopPropagation();}else{event.cancelBubble=true;}
this.isPropagationStopped=emptyFunction.thatReturnsTrue;},persist:function(){this.isPersistent=emptyFunction.thatReturnsTrue;},isPersistent:emptyFunction.thatReturnsFalse,destructor:function(){var Interface=this.constructor.Interface;for(var propName in Interface){this[propName]=null;}
this.dispatchConfig=null;this.dispatchMarker=null;this.nativeEvent=null;}});SyntheticEvent.Interface=EventInterface;SyntheticEvent.augmentClass=function(Class,Interface){var Super=this;var prototype=Object.create(Super.prototype);assign(prototype,Class.prototype);Class.prototype=prototype;Class.prototype.constructor=Class;Class.Interface=assign({},Super.Interface,Interface);Class.augmentClass=Super.augmentClass;PooledClass.addPoolingTo(Class,PooledClass.threeArgumentPooler);};PooledClass.addPoolingTo(SyntheticEvent,PooledClass.threeArgumentPooler);module.exports=SyntheticEvent;},{"114":114,"125":125,"27":27,"28":28}],96:[function(_dereq_,module,exports){'use strict';var SyntheticUIEvent=_dereq_(101);var FocusEventInterface={relatedTarget:null};function SyntheticFocusEvent(dispatchConfig,dispatchMarker,nativeEvent){SyntheticUIEvent.call(this,dispatchConfig,dispatchMarker,nativeEvent);}
SyntheticUIEvent.augmentClass(SyntheticFocusEvent,FocusEventInterface);module.exports=SyntheticFocusEvent;},{"101":101}],97:[function(_dereq_,module,exports){'use strict';var SyntheticEvent=_dereq_(95);var InputEventInterface={data:null};function SyntheticInputEvent(dispatchConfig,dispatchMarker,nativeEvent){SyntheticEvent.call(this,dispatchConfig,dispatchMarker,nativeEvent);}
SyntheticEvent.augmentClass(SyntheticInputEvent,InputEventInterface);module.exports=SyntheticInputEvent;},{"95":95}],98:[function(_dereq_,module,exports){'use strict';var SyntheticUIEvent=_dereq_(101);var getEventCharCode=_dereq_(122);var getEventKey=_dereq_(123);var getEventModifierState=_dereq_(124);var KeyboardEventInterface={key:getEventKey,location:null,ctrlKey:null,shiftKey:null,altKey:null,metaKey:null,repeat:null,locale:null,getModifierState:getEventModifierState, charCode:function(event){

if(event.type==='keypress'){return getEventCharCode(event);}
return 0;},keyCode:function(event){


if(event.type==='keydown'||event.type==='keyup'){return event.keyCode;}
return 0;},which:function(event){
if(event.type==='keypress'){return getEventCharCode(event);}
if(event.type==='keydown'||event.type==='keyup'){return event.keyCode;}
return 0;}};function SyntheticKeyboardEvent(dispatchConfig,dispatchMarker,nativeEvent){SyntheticUIEvent.call(this,dispatchConfig,dispatchMarker,nativeEvent);}
SyntheticUIEvent.augmentClass(SyntheticKeyboardEvent,KeyboardEventInterface);module.exports=SyntheticKeyboardEvent;},{"101":101,"122":122,"123":123,"124":124}],99:[function(_dereq_,module,exports){'use strict';var SyntheticUIEvent=_dereq_(101);var ViewportMetrics=_dereq_(104);var getEventModifierState=_dereq_(124);var MouseEventInterface={screenX:null,screenY:null,clientX:null,clientY:null,ctrlKey:null,shiftKey:null,altKey:null,metaKey:null,getModifierState:getEventModifierState,button:function(event){
var button=event.button;if('which'in event){return button;}



return button===2?2:button===4?1:0;},buttons:null,relatedTarget:function(event){return event.relatedTarget||(((event.fromElement===event.srcElement?event.toElement:event.fromElement)));},pageX:function(event){return'pageX'in event?event.pageX:event.clientX+ViewportMetrics.currentScrollLeft;},pageY:function(event){return'pageY'in event?event.pageY:event.clientY+ViewportMetrics.currentScrollTop;}};function SyntheticMouseEvent(dispatchConfig,dispatchMarker,nativeEvent){SyntheticUIEvent.call(this,dispatchConfig,dispatchMarker,nativeEvent);}
SyntheticUIEvent.augmentClass(SyntheticMouseEvent,MouseEventInterface);module.exports=SyntheticMouseEvent;},{"101":101,"104":104,"124":124}],100:[function(_dereq_,module,exports){'use strict';var SyntheticUIEvent=_dereq_(101);var getEventModifierState=_dereq_(124);var TouchEventInterface={touches:null,targetTouches:null,changedTouches:null,altKey:null,metaKey:null,ctrlKey:null,shiftKey:null,getModifierState:getEventModifierState};function SyntheticTouchEvent(dispatchConfig,dispatchMarker,nativeEvent){SyntheticUIEvent.call(this,dispatchConfig,dispatchMarker,nativeEvent);}
SyntheticUIEvent.augmentClass(SyntheticTouchEvent,TouchEventInterface);module.exports=SyntheticTouchEvent;},{"101":101,"124":124}],101:[function(_dereq_,module,exports){'use strict';var SyntheticEvent=_dereq_(95);var getEventTarget=_dereq_(125);var UIEventInterface={view:function(event){if(event.view){return event.view;}
var target=getEventTarget(event);if(target!=null&&target.window===target){ return target;}
var doc=target.ownerDocument;if(doc){return doc.defaultView||doc.parentWindow;}else{return window;}},detail:function(event){return event.detail||0;}};function SyntheticUIEvent(dispatchConfig,dispatchMarker,nativeEvent){SyntheticEvent.call(this,dispatchConfig,dispatchMarker,nativeEvent);}
SyntheticEvent.augmentClass(SyntheticUIEvent,UIEventInterface);module.exports=SyntheticUIEvent;},{"125":125,"95":95}],102:[function(_dereq_,module,exports){'use strict';var SyntheticMouseEvent=_dereq_(99);var WheelEventInterface={deltaX:function(event){return('deltaX'in event?event.deltaX:'wheelDeltaX'in event?-event.wheelDeltaX:0);},deltaY:function(event){return('deltaY'in event?event.deltaY:'wheelDeltaY'in event?-event.wheelDeltaY:'wheelDelta'in event?-event.wheelDelta:0);},deltaZ:null,

deltaMode:null};function SyntheticWheelEvent(dispatchConfig,dispatchMarker,nativeEvent){SyntheticMouseEvent.call(this,dispatchConfig,dispatchMarker,nativeEvent);}
SyntheticMouseEvent.augmentClass(SyntheticWheelEvent,WheelEventInterface);module.exports=SyntheticWheelEvent;},{"99":99}],103:[function(_dereq_,module,exports){'use strict';var invariant=_dereq_(135);var Mixin={reinitializeTransaction:function(){this.transactionWrappers=this.getTransactionWrappers();if(!this.wrapperInitData){this.wrapperInitData=[];}else{this.wrapperInitData.length=0;}
this._isInTransaction=false;},_isInTransaction:false,getTransactionWrappers:null,isInTransaction:function(){return!!this._isInTransaction;},perform:function(method,scope,a,b,c,d,e,f){("production"!=="development"?invariant(!this.isInTransaction(),'Transaction.perform(...): Cannot initialize a transaction when there '+'is already an outstanding transaction.'):invariant(!this.isInTransaction()));var errorThrown;var ret;try{this._isInTransaction=true;


errorThrown=true;this.initializeAll(0);ret=method.call(scope,a,b,c,d,e,f);errorThrown=false;}finally{try{if(errorThrown){
try{this.closeAll(0);}catch(err){}}else{
this.closeAll(0);}}finally{this._isInTransaction=false;}}
return ret;},initializeAll:function(startIndex){var transactionWrappers=this.transactionWrappers;for(var i=startIndex;i<transactionWrappers.length;i++){var wrapper=transactionWrappers[i];try{


this.wrapperInitData[i]=Transaction.OBSERVED_ERROR;this.wrapperInitData[i]=wrapper.initialize?wrapper.initialize.call(this):null;}finally{if(this.wrapperInitData[i]===Transaction.OBSERVED_ERROR){

try{this.initializeAll(i+1);}catch(err){}}}}},closeAll:function(startIndex){("production"!=="development"?invariant(this.isInTransaction(),'Transaction.closeAll(): Cannot close transaction when none are open.'):invariant(this.isInTransaction()));var transactionWrappers=this.transactionWrappers;for(var i=startIndex;i<transactionWrappers.length;i++){var wrapper=transactionWrappers[i];var initData=this.wrapperInitData[i];var errorThrown;try{


errorThrown=true;if(initData!==Transaction.OBSERVED_ERROR&&wrapper.close){wrapper.close.call(this,initData);}
errorThrown=false;}finally{if(errorThrown){

try{this.closeAll(i+1);}catch(e){}}}}
this.wrapperInitData.length=0;}};var Transaction={Mixin:Mixin,OBSERVED_ERROR:{}};module.exports=Transaction;},{"135":135}],104:[function(_dereq_,module,exports){'use strict';var ViewportMetrics={currentScrollLeft:0,currentScrollTop:0,refreshScrollValues:function(scrollPosition){ViewportMetrics.currentScrollLeft=scrollPosition.x;ViewportMetrics.currentScrollTop=scrollPosition.y;}};module.exports=ViewportMetrics;},{}],105:[function(_dereq_,module,exports){'use strict';var invariant=_dereq_(135);function accumulateInto(current,next){("production"!=="development"?invariant(next!=null,'accumulateInto(...): Accumulated items must not be null or undefined.'):invariant(next!=null));if(current==null){return next;}

var currentIsArray=Array.isArray(current);var nextIsArray=Array.isArray(next);if(currentIsArray&&nextIsArray){current.push.apply(current,next);return current;}
if(currentIsArray){current.push(next);return current;}
if(nextIsArray){return[current].concat(next);}
return[current,next];}
module.exports=accumulateInto;},{"135":135}],106:[function(_dereq_,module,exports){'use strict';var MOD=65521;


function adler32(data){var a=1;var b=0;for(var i=0;i<data.length;i++){a=(a+data.charCodeAt(i))%MOD;b=(b+a)%MOD;}
return a|(b<<16);}
module.exports=adler32;},{}],107:[function(_dereq_,module,exports){var _hyphenPattern=/-(.)/g;function camelize(string){return string.replace(_hyphenPattern,function(_,character){return character.toUpperCase();});}
module.exports=camelize;},{}],108:[function(_dereq_,module,exports){"use strict";var camelize=_dereq_(107);var msPattern=/^-ms-/;function camelizeStyleName(string){return camelize(string.replace(msPattern,'ms-'));}
module.exports=camelizeStyleName;},{"107":107}],109:[function(_dereq_,module,exports){var isTextNode=_dereq_(139);function containsNode(outerNode,innerNode){if(!outerNode||!innerNode){return false;}else if(outerNode===innerNode){return true;}else if(isTextNode(outerNode)){return false;}else if(isTextNode(innerNode)){return containsNode(outerNode,innerNode.parentNode);}else if(outerNode.contains){return outerNode.contains(innerNode);}else if(outerNode.compareDocumentPosition){return!!(outerNode.compareDocumentPosition(innerNode)&16);}else{return false;}}
module.exports=containsNode;},{"139":139}],110:[function(_dereq_,module,exports){var toArray=_dereq_(152);function hasArrayNature(obj){return( !!obj&& (typeof obj=='object'||typeof obj=='function')&&('length'in obj)&&!('setInterval'in obj)&&
(typeof obj.nodeType!='number')&&(((Array.isArray(obj)||('callee'in obj)||'item'in obj))));}
function createArrayFromMixed(obj){if(!hasArrayNature(obj)){return[obj];}else if(Array.isArray(obj)){return obj.slice();}else{return toArray(obj);}}
module.exports=createArrayFromMixed;},{"152":152}],111:[function(_dereq_,module,exports){'use strict';var ReactClass=_dereq_(33);var ReactElement=_dereq_(57);var invariant=_dereq_(135);function createFullPageComponent(tag){var elementFactory=ReactElement.createFactory(tag);var FullPageComponent=ReactClass.createClass({tagName:tag.toUpperCase(),displayName:'ReactFullPageComponent'+tag,componentWillUnmount:function(){("production"!=="development"?invariant(false,'%s tried to unmount. Because of cross-browser quirks it is '+'impossible to unmount some top-level components (eg <html>, <head>, '+'and <body>) reliably and efficiently. To fix this, have a single '+'top-level component that never unmounts render these elements.',this.constructor.displayName):invariant(false));},render:function(){return elementFactory(this.props);}});return FullPageComponent;}
module.exports=createFullPageComponent;},{"135":135,"33":33,"57":57}],112:[function(_dereq_,module,exports){var ExecutionEnvironment=_dereq_(21);var createArrayFromMixed=_dereq_(110);var getMarkupWrap=_dereq_(127);var invariant=_dereq_(135);var dummyNode=ExecutionEnvironment.canUseDOM?document.createElement('div'):null;var nodeNamePattern=/^\s*<(\w+)/;function getNodeName(markup){var nodeNameMatch=markup.match(nodeNamePattern);return nodeNameMatch&&nodeNameMatch[1].toLowerCase();}
function createNodesFromMarkup(markup,handleScript){var node=dummyNode;("production"!=="development"?invariant(!!dummyNode,'createNodesFromMarkup dummy not initialized'):invariant(!!dummyNode));var nodeName=getNodeName(markup);var wrap=nodeName&&getMarkupWrap(nodeName);if(wrap){node.innerHTML=wrap[1]+markup+wrap[2];var wrapDepth=wrap[0];while(wrapDepth--){node=node.lastChild;}}else{node.innerHTML=markup;}
var scripts=node.getElementsByTagName('script');if(scripts.length){("production"!=="development"?invariant(handleScript,'createNodesFromMarkup(...): Unexpected <script> element rendered.'):invariant(handleScript));createArrayFromMixed(scripts).forEach(handleScript);}
var nodes=createArrayFromMixed(node.childNodes);while(node.lastChild){node.removeChild(node.lastChild);}
return nodes;}
module.exports=createNodesFromMarkup;},{"110":110,"127":127,"135":135,"21":21}],113:[function(_dereq_,module,exports){'use strict';var CSSProperty=_dereq_(4);var isUnitlessNumber=CSSProperty.isUnitlessNumber;function dangerousStyleValue(name,value){





 var isEmpty=value==null||typeof value==='boolean'||value==='';if(isEmpty){return'';}
var isNonNumeric=isNaN(value);if(isNonNumeric||value===0||isUnitlessNumber.hasOwnProperty(name)&&isUnitlessNumber[name]){return''+value;}
if(typeof value==='string'){value=value.trim();}
return value+'px';}
module.exports=dangerousStyleValue;},{"4":4}],114:[function(_dereq_,module,exports){function makeEmptyFunction(arg){return function(){return arg;};}
function emptyFunction(){}
emptyFunction.thatReturns=makeEmptyFunction;emptyFunction.thatReturnsFalse=makeEmptyFunction(false);emptyFunction.thatReturnsTrue=makeEmptyFunction(true);emptyFunction.thatReturnsNull=makeEmptyFunction(null);emptyFunction.thatReturnsThis=function(){return this;};emptyFunction.thatReturnsArgument=function(arg){return arg;};module.exports=emptyFunction;},{}],115:[function(_dereq_,module,exports){"use strict";var emptyObject={};if("production"!=="development"){Object.freeze(emptyObject);}
module.exports=emptyObject;},{}],116:[function(_dereq_,module,exports){'use strict';var ESCAPE_LOOKUP={'&':'&amp;','>':'&gt;','<':'&lt;','"':'&quot;','\'':'&#x27;'};var ESCAPE_REGEX=/[&><"']/g;function escaper(match){return ESCAPE_LOOKUP[match];}
function escapeTextContentForBrowser(text){return(''+text).replace(ESCAPE_REGEX,escaper);}
module.exports=escapeTextContentForBrowser;},{}],117:[function(_dereq_,module,exports){'use strict';var ReactCurrentOwner=_dereq_(39);var ReactInstanceMap=_dereq_(67);var ReactMount=_dereq_(70);var invariant=_dereq_(135);var isNode=_dereq_(137);var warning=_dereq_(154);function findDOMNode(componentOrElement){if("production"!=="development"){var owner=ReactCurrentOwner.current;if(owner!==null){("production"!=="development"?warning(owner._warnedAboutRefsInRender,'%s is accessing getDOMNode or findDOMNode inside its render(). '+'render() should be a pure function of props and state. It should '+'never access something that requires stale data from the previous '+'render, such as refs. Move this logic to componentDidMount and '+'componentDidUpdate instead.',owner.getName()||'A component'):null);owner._warnedAboutRefsInRender=true;}}
if(componentOrElement==null){return null;}
if(isNode(componentOrElement)){return componentOrElement;}
if(ReactInstanceMap.has(componentOrElement)){return ReactMount.getNodeFromInstance(componentOrElement);}
("production"!=="development"?invariant(componentOrElement.render==null||typeof componentOrElement.render!=='function','Component (with keys: %s) contains `render` method '+'but is not mounted in the DOM',Object.keys(componentOrElement)):invariant(componentOrElement.render==null||typeof componentOrElement.render!=='function'));("production"!=="development"?invariant(false,'Element appears to be neither ReactComponent nor DOMNode (keys: %s)',Object.keys(componentOrElement)):invariant(false));}
module.exports=findDOMNode;},{"135":135,"137":137,"154":154,"39":39,"67":67,"70":70}],118:[function(_dereq_,module,exports){'use strict';var traverseAllChildren=_dereq_(153);var warning=_dereq_(154);function flattenSingleChildIntoContext(traverseContext,child,name){var result=traverseContext;var keyUnique=!result.hasOwnProperty(name);if("production"!=="development"){("production"!=="development"?warning(keyUnique,'flattenChildren(...): Encountered two children with the same key, '+'`%s`. Child keys must be unique; when two children share a key, only '+'the first child will be used.',name):null);}
if(keyUnique&&child!=null){result[name]=child;}}
function flattenChildren(children){if(children==null){return children;}
var result={};traverseAllChildren(children,flattenSingleChildIntoContext,result);return result;}
module.exports=flattenChildren;},{"153":153,"154":154}],119:[function(_dereq_,module,exports){"use strict";function focusNode(node){
try{node.focus();}catch(e){}}
module.exports=focusNode;},{}],120:[function(_dereq_,module,exports){'use strict';var forEachAccumulated=function(arr,cb,scope){if(Array.isArray(arr)){arr.forEach(cb,scope);}else if(arr){cb.call(scope,arr);}};module.exports=forEachAccumulated;},{}],121:[function(_dereq_,module,exports){function getActiveElement(){try{return document.activeElement||document.body;}catch(e){return document.body;}}
module.exports=getActiveElement;},{}],122:[function(_dereq_,module,exports){'use strict';function getEventCharCode(nativeEvent){var charCode;var keyCode=nativeEvent.keyCode;if('charCode'in nativeEvent){charCode=nativeEvent.charCode;if(charCode===0&&keyCode===13){charCode=13;}}else{charCode=keyCode;}
if(charCode>=32||charCode===13){return charCode;}
return 0;}
module.exports=getEventCharCode;},{}],123:[function(_dereq_,module,exports){'use strict';var getEventCharCode=_dereq_(122);var normalizeKey={'Esc':'Escape','Spacebar':' ','Left':'ArrowLeft','Up':'ArrowUp','Right':'ArrowRight','Down':'ArrowDown','Del':'Delete','Win':'OS','Menu':'ContextMenu','Apps':'ContextMenu','Scroll':'ScrollLock','MozPrintableKey':'Unidentified'};var translateToKey={8:'Backspace',9:'Tab',12:'Clear',13:'Enter',16:'Shift',17:'Control',18:'Alt',19:'Pause',20:'CapsLock',27:'Escape',32:' ',33:'PageUp',34:'PageDown',35:'End',36:'Home',37:'ArrowLeft',38:'ArrowUp',39:'ArrowRight',40:'ArrowDown',45:'Insert',46:'Delete',112:'F1',113:'F2',114:'F3',115:'F4',116:'F5',117:'F6',118:'F7',119:'F8',120:'F9',121:'F10',122:'F11',123:'F12',144:'NumLock',145:'ScrollLock',224:'Meta'};function getEventKey(nativeEvent){if(nativeEvent.key){

var key=normalizeKey[nativeEvent.key]||nativeEvent.key;if(key!=='Unidentified'){return key;}}
if(nativeEvent.type==='keypress'){var charCode=getEventCharCode(nativeEvent);
return charCode===13?'Enter':String.fromCharCode(charCode);}
if(nativeEvent.type==='keydown'||nativeEvent.type==='keyup'){
return translateToKey[nativeEvent.keyCode]||'Unidentified';}
return'';}
module.exports=getEventKey;},{"122":122}],124:[function(_dereq_,module,exports){'use strict';var modifierKeyToProp={'Alt':'altKey','Control':'ctrlKey','Meta':'metaKey','Shift':'shiftKey'};
function modifierStateGetter(keyArg){var syntheticEvent=this;var nativeEvent=syntheticEvent.nativeEvent;if(nativeEvent.getModifierState){return nativeEvent.getModifierState(keyArg);}
var keyProp=modifierKeyToProp[keyArg];return keyProp?!!nativeEvent[keyProp]:false;}
function getEventModifierState(nativeEvent){return modifierStateGetter;}
module.exports=getEventModifierState;},{}],125:[function(_dereq_,module,exports){'use strict';function getEventTarget(nativeEvent){var target=nativeEvent.target||nativeEvent.srcElement||window; return target.nodeType===3?target.parentNode:target;}
module.exports=getEventTarget;},{}],126:[function(_dereq_,module,exports){'use strict';var ITERATOR_SYMBOL=typeof Symbol==='function'&&Symbol.iterator;var FAUX_ITERATOR_SYMBOL='@@iterator';function getIteratorFn(maybeIterable){var iteratorFn=maybeIterable&&((ITERATOR_SYMBOL&&maybeIterable[ITERATOR_SYMBOL]||maybeIterable[FAUX_ITERATOR_SYMBOL]));if(typeof iteratorFn==='function'){return iteratorFn;}}
module.exports=getIteratorFn;},{}],127:[function(_dereq_,module,exports){var ExecutionEnvironment=_dereq_(21);var invariant=_dereq_(135);var dummyNode=ExecutionEnvironment.canUseDOM?document.createElement('div'):null;var shouldWrap={'circle':true,'defs':true,'ellipse':true,'g':true,'line':true,'linearGradient':true,'path':true,'polygon':true,'polyline':true,'radialGradient':true,'rect':true,'stop':true,'text':true};var selectWrap=[1,'<select multiple="true">','</select>'];var tableWrap=[1,'<table>','</table>'];var trWrap=[3,'<table><tbody><tr>','</tr></tbody></table>'];var svgWrap=[1,'<svg>','</svg>'];var markupWrap={'*':[1,'?<div>','</div>'],'area':[1,'<map>','</map>'],'col':[2,'<table><tbody></tbody><colgroup>','</colgroup></table>'],'legend':[1,'<fieldset>','</fieldset>'],'param':[1,'<object>','</object>'],'tr':[2,'<table><tbody>','</tbody></table>'],'optgroup':selectWrap,'option':selectWrap,'caption':tableWrap,'colgroup':tableWrap,'tbody':tableWrap,'tfoot':tableWrap,'thead':tableWrap,'td':trWrap,'th':trWrap,'circle':svgWrap,'defs':svgWrap,'ellipse':svgWrap,'g':svgWrap,'line':svgWrap,'linearGradient':svgWrap,'path':svgWrap,'polygon':svgWrap,'polyline':svgWrap,'radialGradient':svgWrap,'rect':svgWrap,'stop':svgWrap,'text':svgWrap};function getMarkupWrap(nodeName){("production"!=="development"?invariant(!!dummyNode,'Markup wrapping node not initialized'):invariant(!!dummyNode));if(!markupWrap.hasOwnProperty(nodeName)){nodeName='*';}
if(!shouldWrap.hasOwnProperty(nodeName)){if(nodeName==='*'){dummyNode.innerHTML='<link />';}else{dummyNode.innerHTML='<'+nodeName+'></'+nodeName+'>';}
shouldWrap[nodeName]=!dummyNode.firstChild;}
return shouldWrap[nodeName]?markupWrap[nodeName]:null;}
module.exports=getMarkupWrap;},{"135":135,"21":21}],128:[function(_dereq_,module,exports){'use strict';function getLeafNode(node){while(node&&node.firstChild){node=node.firstChild;}
return node;}
function getSiblingNode(node){while(node){if(node.nextSibling){return node.nextSibling;}
node=node.parentNode;}}
function getNodeForCharacterOffset(root,offset){var node=getLeafNode(root);var nodeStart=0;var nodeEnd=0;while(node){if(node.nodeType===3){nodeEnd=nodeStart+node.textContent.length;if(nodeStart<=offset&&nodeEnd>=offset){return{node:node,offset:offset-nodeStart};}
nodeStart=nodeEnd;}
node=getLeafNode(getSiblingNode(node));}}
module.exports=getNodeForCharacterOffset;},{}],129:[function(_dereq_,module,exports){'use strict';var DOC_NODE_TYPE=9;function getReactRootElementInContainer(container){if(!container){return null;}
if(container.nodeType===DOC_NODE_TYPE){return container.documentElement;}else{return container.firstChild;}}
module.exports=getReactRootElementInContainer;},{}],130:[function(_dereq_,module,exports){'use strict';var ExecutionEnvironment=_dereq_(21);var contentKey=null;function getTextContentAccessor(){if(!contentKey&&ExecutionEnvironment.canUseDOM){
contentKey='textContent'in document.documentElement?'textContent':'innerText';}
return contentKey;}
module.exports=getTextContentAccessor;},{"21":21}],131:[function(_dereq_,module,exports){"use strict";function getUnboundedScrollPosition(scrollable){if(scrollable===window){return{x:window.pageXOffset||document.documentElement.scrollLeft,y:window.pageYOffset||document.documentElement.scrollTop};}
return{x:scrollable.scrollLeft,y:scrollable.scrollTop};}
module.exports=getUnboundedScrollPosition;},{}],132:[function(_dereq_,module,exports){var _uppercasePattern=/([A-Z])/g;function hyphenate(string){return string.replace(_uppercasePattern,'-$1').toLowerCase();}
module.exports=hyphenate;},{}],133:[function(_dereq_,module,exports){"use strict";var hyphenate=_dereq_(132);var msPattern=/^ms-/;function hyphenateStyleName(string){return hyphenate(string).replace(msPattern,'-ms-');}
module.exports=hyphenateStyleName;},{"132":132}],134:[function(_dereq_,module,exports){'use strict';var ReactCompositeComponent=_dereq_(37);var ReactEmptyComponent=_dereq_(59);var ReactNativeComponent=_dereq_(73);var assign=_dereq_(27);var invariant=_dereq_(135);var warning=_dereq_(154);var ReactCompositeComponentWrapper=function(){};assign(ReactCompositeComponentWrapper.prototype,ReactCompositeComponent.Mixin,{_instantiateReactComponent:instantiateReactComponent});function isInternalComponentType(type){return(typeof type==='function'&&typeof type.prototype!=='undefined'&&typeof type.prototype.mountComponent==='function'&&typeof type.prototype.receiveComponent==='function');}
function instantiateReactComponent(node,parentCompositeType){var instance;if(node===null||node===false){node=ReactEmptyComponent.emptyElement;}
if(typeof node==='object'){var element=node;if("production"!=="development"){("production"!=="development"?warning(element&&(typeof element.type==='function'||typeof element.type==='string'),'Only functions or strings can be mounted as React components.'):null);} 
if(parentCompositeType===element.type&&typeof element.type==='string'){instance=ReactNativeComponent.createInternalComponent(element);
}else if(isInternalComponentType(element.type)){

instance=new element.type(element);}else{instance=new ReactCompositeComponentWrapper();}}else if(typeof node==='string'||typeof node==='number'){instance=ReactNativeComponent.createInstanceForText(node);}else{("production"!=="development"?invariant(false,'Encountered invalid React node of type %s',typeof node):invariant(false));}
if("production"!=="development"){("production"!=="development"?warning(typeof instance.construct==='function'&&typeof instance.mountComponent==='function'&&typeof instance.receiveComponent==='function'&&typeof instance.unmountComponent==='function','Only React Components can be mounted.'):null);}
instance.construct(node);

instance._mountIndex=0;instance._mountImage=null;if("production"!=="development"){instance._isOwnerNecessary=false;instance._warnedAboutRefsInRender=false;}

if("production"!=="development"){if(Object.preventExtensions){Object.preventExtensions(instance);}}
return instance;}
module.exports=instantiateReactComponent;},{"135":135,"154":154,"27":27,"37":37,"59":59,"73":73}],135:[function(_dereq_,module,exports){"use strict";var invariant=function(condition,format,a,b,c,d,e,f){if("production"!=="development"){if(format===undefined){throw new Error('invariant requires an error message argument');}}
if(!condition){var error;if(format===undefined){error=new Error('Minified exception occurred; use the non-minified dev environment '+'for the full error message and additional helpful warnings.');}else{var args=[a,b,c,d,e,f];var argIndex=0;error=new Error('Invariant Violation: '+
format.replace(/%s/g,function(){return args[argIndex++];}));}
error.framesToPop=1; throw error;}};module.exports=invariant;},{}],136:[function(_dereq_,module,exports){'use strict';var ExecutionEnvironment=_dereq_(21);var useHasFeature;if(ExecutionEnvironment.canUseDOM){useHasFeature=document.implementation&&document.implementation.hasFeature&& document.implementation.hasFeature('','')!==true;}
function isEventSupported(eventNameSuffix,capture){if(!ExecutionEnvironment.canUseDOM||capture&&!('addEventListener'in document)){return false;}
var eventName='on'+eventNameSuffix;var isSupported=eventName in document;if(!isSupported){var element=document.createElement('div');element.setAttribute(eventName,'return;');isSupported=typeof element[eventName]==='function';}
if(!isSupported&&useHasFeature&&eventNameSuffix==='wheel'){isSupported=document.implementation.hasFeature('Events.wheel','3.0');}
return isSupported;}
module.exports=isEventSupported;},{"21":21}],137:[function(_dereq_,module,exports){function isNode(object){return!!(object&&(((typeof Node==='function'?object instanceof Node:typeof object==='object'&&typeof object.nodeType==='number'&&typeof object.nodeName==='string'))));}
module.exports=isNode;},{}],138:[function(_dereq_,module,exports){'use strict';var supportedInputTypes={'color':true,'date':true,'datetime':true,'datetime-local':true,'email':true,'month':true,'number':true,'password':true,'range':true,'search':true,'tel':true,'text':true,'time':true,'url':true,'week':true};function isTextInputElement(elem){return elem&&((elem.nodeName==='INPUT'&&supportedInputTypes[elem.type]||elem.nodeName==='TEXTAREA'));}
module.exports=isTextInputElement;},{}],139:[function(_dereq_,module,exports){var isNode=_dereq_(137);function isTextNode(object){return isNode(object)&&object.nodeType==3;}
module.exports=isTextNode;},{"137":137}],140:[function(_dereq_,module,exports){'use strict';var invariant=_dereq_(135);var keyMirror=function(obj){var ret={};var key;("production"!=="development"?invariant(obj instanceof Object&&!Array.isArray(obj),'keyMirror(...): Argument must be an object.'):invariant(obj instanceof Object&&!Array.isArray(obj)));for(key in obj){if(!obj.hasOwnProperty(key)){continue;}
ret[key]=key;}
return ret;};module.exports=keyMirror;},{"135":135}],141:[function(_dereq_,module,exports){var keyOf=function(oneKeyObj){var key;for(key in oneKeyObj){if(!oneKeyObj.hasOwnProperty(key)){continue;}
return key;}
return null;};module.exports=keyOf;},{}],142:[function(_dereq_,module,exports){'use strict';var hasOwnProperty=Object.prototype.hasOwnProperty;function mapObject(object,callback,context){if(!object){return null;}
var result={};for(var name in object){if(hasOwnProperty.call(object,name)){result[name]=callback.call(context,object[name],name,object);}}
return result;}
module.exports=mapObject;},{}],143:[function(_dereq_,module,exports){'use strict';function memoizeStringOnly(callback){var cache={};return function(string){if(!cache.hasOwnProperty(string)){cache[string]=callback.call(this,string);}
return cache[string];};}
module.exports=memoizeStringOnly;},{}],144:[function(_dereq_,module,exports){'use strict';var ReactElement=_dereq_(57);var invariant=_dereq_(135);function onlyChild(children){("production"!=="development"?invariant(ReactElement.isValidElement(children),'onlyChild must be passed a children with exactly one child.'):invariant(ReactElement.isValidElement(children)));return children;}
module.exports=onlyChild;},{"135":135,"57":57}],145:[function(_dereq_,module,exports){"use strict";var ExecutionEnvironment=_dereq_(21);var performance;if(ExecutionEnvironment.canUseDOM){performance=window.performance||window.msPerformance||window.webkitPerformance;}
module.exports=performance||{};},{"21":21}],146:[function(_dereq_,module,exports){var performance=_dereq_(145);if(!performance||!performance.now){performance=Date;}
var performanceNow=performance.now.bind(performance);module.exports=performanceNow;},{"145":145}],147:[function(_dereq_,module,exports){'use strict';var escapeTextContentForBrowser=_dereq_(116);function quoteAttributeValueForBrowser(value){return'"'+escapeTextContentForBrowser(value)+'"';}
module.exports=quoteAttributeValueForBrowser;},{"116":116}],148:[function(_dereq_,module,exports){'use strict';var ExecutionEnvironment=_dereq_(21);var WHITESPACE_TEST=/^[ \r\n\t\f]/;var NONVISIBLE_TEST=/<(!--|link|noscript|meta|script|style)[ \r\n\t\f\/>]/;var setInnerHTML=function(node,html){node.innerHTML=html;};if(typeof MSApp!=='undefined'&&MSApp.execUnsafeLocalFunction){setInnerHTML=function(node,html){MSApp.execUnsafeLocalFunction(function(){node.innerHTML=html;});};}
if(ExecutionEnvironment.canUseDOM){


var testElement=document.createElement('div');testElement.innerHTML=' ';if(testElement.innerHTML===''){setInnerHTML=function(node,html){


if(node.parentNode){node.parentNode.replaceChild(node,node);}



if(WHITESPACE_TEST.test(html)||html[0]==='<'&&NONVISIBLE_TEST.test(html)){node.innerHTML='\uFEFF'+html;
var textNode=node.firstChild;if(textNode.data.length===1){node.removeChild(textNode);}else{textNode.deleteData(0,1);}}else{node.innerHTML=html;}};}}
module.exports=setInnerHTML;},{"21":21}],149:[function(_dereq_,module,exports){'use strict';var ExecutionEnvironment=_dereq_(21);var escapeTextContentForBrowser=_dereq_(116);var setInnerHTML=_dereq_(148);var setTextContent=function(node,text){node.textContent=text;};if(ExecutionEnvironment.canUseDOM){if(!('textContent'in document.documentElement)){setTextContent=function(node,text){setInnerHTML(node,escapeTextContentForBrowser(text));};}}
module.exports=setTextContent;},{"116":116,"148":148,"21":21}],150:[function(_dereq_,module,exports){'use strict';function shallowEqual(objA,objB){if(objA===objB){return true;}
var key;for(key in objA){if(objA.hasOwnProperty(key)&&(!objB.hasOwnProperty(key)||objA[key]!==objB[key])){return false;}}
for(key in objB){if(objB.hasOwnProperty(key)&&!objA.hasOwnProperty(key)){return false;}}
return true;}
module.exports=shallowEqual;},{}],151:[function(_dereq_,module,exports){'use strict';var warning=_dereq_(154);function shouldUpdateReactComponent(prevElement,nextElement){if(prevElement!=null&&nextElement!=null){var prevType=typeof prevElement;var nextType=typeof nextElement;if(prevType==='string'||prevType==='number'){return(nextType==='string'||nextType==='number');}else{if(nextType==='object'&&prevElement.type===nextElement.type&&prevElement.key===nextElement.key){var ownersMatch=prevElement._owner===nextElement._owner;var prevName=null;var nextName=null;var nextDisplayName=null;if("production"!=="development"){if(!ownersMatch){if(prevElement._owner!=null&&prevElement._owner.getPublicInstance()!=null&&prevElement._owner.getPublicInstance().constructor!=null){prevName=prevElement._owner.getPublicInstance().constructor.displayName;}
if(nextElement._owner!=null&&nextElement._owner.getPublicInstance()!=null&&nextElement._owner.getPublicInstance().constructor!=null){nextName=nextElement._owner.getPublicInstance().constructor.displayName;}
if(nextElement.type!=null&&nextElement.type.displayName!=null){nextDisplayName=nextElement.type.displayName;}
if(nextElement.type!=null&&typeof nextElement.type==='string'){nextDisplayName=nextElement.type;}
if(typeof nextElement.type!=='string'||nextElement.type==='input'||nextElement.type==='textarea'){if((prevElement._owner!=null&&prevElement._owner._isOwnerNecessary===false)||(nextElement._owner!=null&&nextElement._owner._isOwnerNecessary===false)){if(prevElement._owner!=null){prevElement._owner._isOwnerNecessary=true;}
if(nextElement._owner!=null){nextElement._owner._isOwnerNecessary=true;}
("production"!=="development"?warning(false,'<%s /> is being rendered by both %s and %s using the same '+'key (%s) in the same place. Currently, this means that '+'they don\'t preserve state. This behavior should be very '+'rare so we\'re considering deprecating it. Please contact '+'the React team and explain your use case so that we can '+'take that into consideration.',nextDisplayName||'Unknown Component',prevName||'[Unknown]',nextName||'[Unknown]',prevElement.key):null);}}}}
return ownersMatch;}}}
return false;}
module.exports=shouldUpdateReactComponent;},{"154":154}],152:[function(_dereq_,module,exports){var invariant=_dereq_(135);function toArray(obj){var length=obj.length;
("production"!=="development"?invariant(!Array.isArray(obj)&&(typeof obj==='object'||typeof obj==='function'),'toArray: Array-like object expected'):invariant(!Array.isArray(obj)&&(typeof obj==='object'||typeof obj==='function')));("production"!=="development"?invariant(typeof length==='number','toArray: Object needs a length property'):invariant(typeof length==='number'));("production"!=="development"?invariant(length===0||(length-1)in obj,'toArray: Object should have keys for indices'):invariant(length===0||(length-1)in obj));

if(obj.hasOwnProperty){try{return Array.prototype.slice.call(obj);}catch(e){}}
var ret=Array(length);for(var ii=0;ii<length;ii++){ret[ii]=obj[ii];}
return ret;}
module.exports=toArray;},{"135":135}],153:[function(_dereq_,module,exports){'use strict';var ReactElement=_dereq_(57);var ReactFragment=_dereq_(63);var ReactInstanceHandles=_dereq_(66);var getIteratorFn=_dereq_(126);var invariant=_dereq_(135);var warning=_dereq_(154);var SEPARATOR=ReactInstanceHandles.SEPARATOR;var SUBSEPARATOR=':';var userProvidedKeyEscaperLookup={'=':'=0','.':'=1',':':'=2'};var userProvidedKeyEscapeRegex=/[=.:]/g;var didWarnAboutMaps=false;function userProvidedKeyEscaper(match){return userProvidedKeyEscaperLookup[match];}
function getComponentKey(component,index){if(component&&component.key!=null){ return wrapUserProvidedKey(component.key);} 
return index.toString(36);}
function escapeUserProvidedKey(text){return(''+text).replace(userProvidedKeyEscapeRegex,userProvidedKeyEscaper);}
function wrapUserProvidedKey(key){return'$'+escapeUserProvidedKey(key);}
function traverseAllChildrenImpl(children,nameSoFar,indexSoFar,callback,traverseContext){var type=typeof children;if(type==='undefined'||type==='boolean'){children=null;}
if(children===null||type==='string'||type==='number'||ReactElement.isValidElement(children)){callback(traverseContext,children,
nameSoFar===''?SEPARATOR+getComponentKey(children,0):nameSoFar,indexSoFar);return 1;}
var child,nextName,nextIndex;var subtreeCount=0;if(Array.isArray(children)){for(var i=0;i<children.length;i++){child=children[i];nextName=((nameSoFar!==''?nameSoFar+SUBSEPARATOR:SEPARATOR)+
getComponentKey(child,i));nextIndex=indexSoFar+subtreeCount;subtreeCount+=traverseAllChildrenImpl(child,nextName,nextIndex,callback,traverseContext);}}else{var iteratorFn=getIteratorFn(children);if(iteratorFn){var iterator=iteratorFn.call(children);var step;if(iteratorFn!==children.entries){var ii=0;while(!(step=iterator.next()).done){child=step.value;nextName=((nameSoFar!==''?nameSoFar+SUBSEPARATOR:SEPARATOR)+
getComponentKey(child,ii++));nextIndex=indexSoFar+subtreeCount;subtreeCount+=traverseAllChildrenImpl(child,nextName,nextIndex,callback,traverseContext);}}else{if("production"!=="development"){("production"!=="development"?warning(didWarnAboutMaps,'Using Maps as children is not yet fully supported. It is an '+'experimental feature that might be removed. Convert it to a '+'sequence / iterable of keyed ReactElements instead.'):null);didWarnAboutMaps=true;}
while(!(step=iterator.next()).done){var entry=step.value;if(entry){child=entry[1];nextName=((nameSoFar!==''?nameSoFar+SUBSEPARATOR:SEPARATOR)+
wrapUserProvidedKey(entry[0])+SUBSEPARATOR+
getComponentKey(child,0));nextIndex=indexSoFar+subtreeCount;subtreeCount+=traverseAllChildrenImpl(child,nextName,nextIndex,callback,traverseContext);}}}}else if(type==='object'){("production"!=="development"?invariant(children.nodeType!==1,'traverseAllChildren(...): Encountered an invalid child; DOM '+'elements are not valid children of React components.'):invariant(children.nodeType!==1));var fragment=ReactFragment.extract(children);for(var key in fragment){if(fragment.hasOwnProperty(key)){child=fragment[key];nextName=((nameSoFar!==''?nameSoFar+SUBSEPARATOR:SEPARATOR)+
wrapUserProvidedKey(key)+SUBSEPARATOR+
getComponentKey(child,0));nextIndex=indexSoFar+subtreeCount;subtreeCount+=traverseAllChildrenImpl(child,nextName,nextIndex,callback,traverseContext);}}}}
return subtreeCount;}
function traverseAllChildren(children,callback,traverseContext){if(children==null){return 0;}
return traverseAllChildrenImpl(children,'',0,callback,traverseContext);}
module.exports=traverseAllChildren;},{"126":126,"135":135,"154":154,"57":57,"63":63,"66":66}],154:[function(_dereq_,module,exports){"use strict";var emptyFunction=_dereq_(114);var warning=emptyFunction;if("production"!=="development"){warning=function(condition,format){for(var args=[],$__0=2,$__1=arguments.length;$__0<$__1;$__0++)args.push(arguments[$__0]);if(format===undefined){throw new Error('`warning(condition, format, ...args)` requires a warning '+'message argument');}
if(format.length<10||/^[s\W]*$/.test(format)){throw new Error('The warning format should be able to uniquely identify this '+'warning. Please, use a more descriptive format than: '+format);}
if(format.indexOf('Failed Composite propType: ')===0){return; }
if(!condition){var argIndex=0;var message='Warning: '+format.replace(/%s/g,function(){return args[argIndex++];});console.warn(message);try{
throw new Error(message);}catch(x){}}};}
module.exports=warning;},{"114":114}]},{},[1])(1)});(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.JSXTransformer=f()}})(function(){var define,module,exports;return(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(_dereq_,module,exports){'use strict';var ReactTools=_dereq_('../main');var inlineSourceMap=_dereq_('./inline-source-map');var headEl;var dummyAnchor;var inlineScriptCount=0;


var supportsAccessors=Object.prototype.hasOwnProperty('__defineGetter__');function transformReact(source,options){options=options||{};


if(options.sourceMap){options.sourceMap=supportsAccessors;}
return ReactTools.transformWithDetails(source,options);}
function exec(source,options){return eval(transformReact(source,options).code);}
function createSourceCodeErrorMessage(code,e){var sourceLines=code.split('\n');


if(!e.lineNumber||e.lineNumber>sourceLines.length){return'';}
var erroneousLine=sourceLines[e.lineNumber-1];
var indentation=0;erroneousLine=erroneousLine.replace(/^\s+/,function(leadingSpaces){indentation=leadingSpaces.length;return'';});  
 var LIMIT=30;var errorColumn=e.column-indentation;if(errorColumn>LIMIT){erroneousLine='... '+erroneousLine.slice(errorColumn-LIMIT);errorColumn=4+LIMIT;}
if(erroneousLine.length-errorColumn>LIMIT){erroneousLine=erroneousLine.slice(0,errorColumn+LIMIT)+' ...';}
var message='\n\n'+erroneousLine+'\n';message+=new Array(errorColumn-1).join(' ')+'^';return message;}
function transformCode(code,url,options){try{var transformed=transformReact(code,options);}catch(e){e.message+='\n    at ';if(url){if('fileName'in e){
e.fileName=url;}
e.message+=url+':'+e.lineNumber+':'+e.columnNumber;}else{e.message+=location.href;}
e.message+=createSourceCodeErrorMessage(code,e);throw e;}
if(!transformed.sourceMap){return transformed.code;}
var source;if(url==null){source='Inline JSX script';inlineScriptCount++;if(inlineScriptCount>1){source+=' ('+inlineScriptCount+')';}}else if(dummyAnchor){


dummyAnchor.href=url;source=dummyAnchor.pathname.substr(1);}
return(transformed.code+'\n'+
inlineSourceMap(transformed.sourceMap,code,source));}
function run(code,url,options){var scriptEl=document.createElement('script');scriptEl.text=transformCode(code,url,options);headEl.appendChild(scriptEl);}
function load(url,successCallback,errorCallback){var xhr;xhr=window.ActiveXObject?new window.ActiveXObject('Microsoft.XMLHTTP'):new XMLHttpRequest();
xhr.open('GET',url,true);if('overrideMimeType'in xhr){xhr.overrideMimeType('text/plain');}
xhr.onreadystatechange=function(){if(xhr.readyState===4){if(xhr.status===0||xhr.status===200){successCallback(xhr.responseText);}else{errorCallback();throw new Error('Could not load '+url);}}};return xhr.send(null);}
function loadScripts(scripts){var result=[];var count=scripts.length;function check(){var script,i;for(i=0;i<count;i++){script=result[i];if(script.loaded&&!script.executed){script.executed=true;run(script.content,script.url,script.options);}else if(!script.loaded&&!script.error&&!script.async){break;}}}
scripts.forEach(function(script,i){var options={sourceMap:true};if(/;harmony=true(;|$)/.test(script.type)){options.harmony=true;}
if(/;stripTypes=true(;|$)/.test(script.type)){options.stripTypes=true;} 
var async=script.hasAttribute('async');if(script.src){result[i]={async:async,error:false,executed:false,content:null,loaded:false,url:script.src,options:options};load(script.src,function(content){result[i].loaded=true;result[i].content=content;check();},function(){result[i].error=true;check();});}else{result[i]={async:async,error:false,executed:false,content:script.innerHTML,loaded:true,url:null,options:options};}});check();}
function runScripts(){var scripts=document.getElementsByTagName('script'); var jsxScripts=[];for(var i=0;i<scripts.length;i++){if(/^text\/jsx(;|$)/.test(scripts.item(i).type)){jsxScripts.push(scripts.item(i));}}
if(jsxScripts.length<1){return;}
console.warn('You are using the in-browser JSX transformer. Be sure to precompile '+'your JSX for production - '+'http://facebook.github.io/react/docs/tooling-integration.html#jsx');loadScripts(jsxScripts);}

if(typeof window!=='undefined'&&window!==null){headEl=document.getElementsByTagName('head')[0];dummyAnchor=document.createElement('a');if(window.addEventListener){window.addEventListener('DOMContentLoaded',runScripts,false);}else{window.attachEvent('onload',runScripts);}}
module.exports={transform:transformReact,exec:exec};},{"../main":2,"./inline-source-map":41}],2:[function(_dereq_,module,exports){'use strict';var visitors=_dereq_('./vendor/fbtransform/visitors');var transform=_dereq_('jstransform').transform;var typesSyntax=_dereq_('jstransform/visitors/type-syntax');var inlineSourceMap=_dereq_('./vendor/inline-source-map');module.exports={transform:function(input,options){options=processOptions(options);var output=innerTransform(input,options);var result=output.code;if(options.sourceMap){var map=inlineSourceMap(output.sourceMap,input,options.filename);result+='\n'+map;}
return result;},transformWithDetails:function(input,options){options=processOptions(options);var output=innerTransform(input,options);var result={};result.code=output.code;if(options.sourceMap){result.sourceMap=output.sourceMap.toJSON();}
if(options.filename){result.sourceMap.sources=[options.filename];}
return result;}};function processOptions(opts){opts=opts||{};var options={};options.harmony=opts.harmony;options.stripTypes=opts.stripTypes;options.sourceMap=opts.sourceMap;options.filename=opts.sourceFilename;if(opts.es6module){options.sourceType='module';}
if(opts.nonStrictEs6module){options.sourceType='nonStrictModule';}

options.es3=opts.target==='es3';options.es5=!options.es3;return options;}
function innerTransform(input,options){var visitorSets=['react'];if(options.harmony){visitorSets.push('harmony');}
if(options.es3){visitorSets.push('es3');}
if(options.stripTypes){

 input=transform(typesSyntax.visitorList,input,options).code;}
var visitorList=visitors.getVisitorsBySet(visitorSets);return transform(visitorList,input,options);}},{"./vendor/fbtransform/visitors":40,"./vendor/inline-source-map":41,"jstransform":22,"jstransform/visitors/type-syntax":36}],3:[function(_dereq_,module,exports){var base64=_dereq_('base64-js')
var ieee754=_dereq_('ieee754')
var isArray=_dereq_('is-array')
exports.Buffer=Buffer
exports.SlowBuffer=SlowBuffer
exports.INSPECT_MAX_BYTES=50
Buffer.poolSize=8192
var kMaxLength=0x3fffffff
var rootParent={}
Buffer.TYPED_ARRAY_SUPPORT=(function(){try{var buf=new ArrayBuffer(0)
var arr=new Uint8Array(buf)
arr.foo=function(){return 42}
return arr.foo()===42&& typeof arr.subarray==='function'&&new Uint8Array(1).subarray(1,1).byteLength===0
}catch(e){return false}})()
function Buffer(subject,encoding){var self=this
if(!(self instanceof Buffer))return new Buffer(subject,encoding)
var type=typeof subject
var length
if(type==='number'){length=+subject}else if(type==='string'){length=Buffer.byteLength(subject,encoding)}else if(type==='object'&&subject!==null){ if(subject.type==='Buffer'&&isArray(subject.data))subject=subject.data
length=+subject.length}else{throw new TypeError('must start with number, buffer, array or string')}
if(length>kMaxLength){throw new RangeError('Attempt to allocate Buffer larger than maximum size: 0x'+
kMaxLength.toString(16)+' bytes')}
if(length<0)length=0
else length>>>=0
 if(Buffer.TYPED_ARRAY_SUPPORT){ self=Buffer._augment(new Uint8Array(length))
}else{self.length=length
self._isBuffer=true}
var i
if(Buffer.TYPED_ARRAY_SUPPORT&&typeof subject.byteLength==='number'){ self._set(subject)}else if(isArrayish(subject)){ if(Buffer.isBuffer(subject)){for(i=0;i<length;i++){self[i]=subject.readUInt8(i)}}else{for(i=0;i<length;i++){self[i]=((subject[i]%256)+256)%256}}}else if(type==='string'){self.write(subject,0,encoding)}else if(type==='number'&&!Buffer.TYPED_ARRAY_SUPPORT){for(i=0;i<length;i++){self[i]=0}}
if(length>0&&length<=Buffer.poolSize)self.parent=rootParent
return self}
function SlowBuffer(subject,encoding){if(!(this instanceof SlowBuffer))return new SlowBuffer(subject,encoding)
var buf=new Buffer(subject,encoding)
delete buf.parent
return buf}
Buffer.isBuffer=function isBuffer(b){return!!(b!=null&&b._isBuffer)}
Buffer.compare=function compare(a,b){if(!Buffer.isBuffer(a)||!Buffer.isBuffer(b)){throw new TypeError('Arguments must be Buffers')}
if(a===b)return 0
var x=a.length
var y=b.length
for(var i=0,len=Math.min(x,y);i<len&&a[i]===b[i];i++){}
if(i!==len){x=a[i]
y=b[i]}
if(x<y)return-1
if(y<x)return 1
return 0}
Buffer.isEncoding=function isEncoding(encoding){switch(String(encoding).toLowerCase()){case'hex':case'utf8':case'utf-8':case'ascii':case'binary':case'base64':case'raw':case'ucs2':case'ucs-2':case'utf16le':case'utf-16le':return true
default:return false}}
Buffer.concat=function concat(list,totalLength){if(!isArray(list))throw new TypeError('list argument must be an Array of Buffers.')
if(list.length===0){return new Buffer(0)}else if(list.length===1){return list[0]}
var i
if(totalLength===undefined){totalLength=0
for(i=0;i<list.length;i++){totalLength+=list[i].length}}
var buf=new Buffer(totalLength)
var pos=0
for(i=0;i<list.length;i++){var item=list[i]
item.copy(buf,pos)
pos+=item.length}
return buf}
Buffer.byteLength=function byteLength(str,encoding){var ret
str=str+''
switch(encoding||'utf8'){case'ascii':case'binary':case'raw':ret=str.length
break
case'ucs2':case'ucs-2':case'utf16le':case'utf-16le':ret=str.length*2
break
case'hex':ret=str.length>>>1
break
case'utf8':case'utf-8':ret=utf8ToBytes(str).length
break
case'base64':ret=base64ToBytes(str).length
break
default:ret=str.length}
return ret}
Buffer.prototype.length=undefined
Buffer.prototype.parent=undefined

Buffer.prototype.toString=function toString(encoding,start,end){var loweredCase=false
start=start>>>0
end=end===undefined||end===Infinity?this.length:end>>>0
if(!encoding)encoding='utf8'
if(start<0)start=0
if(end>this.length)end=this.length
if(end<=start)return''
while(true){switch(encoding){case'hex':return hexSlice(this,start,end)
case'utf8':case'utf-8':return utf8Slice(this,start,end)
case'ascii':return asciiSlice(this,start,end)
case'binary':return binarySlice(this,start,end)
case'base64':return base64Slice(this,start,end)
case'ucs2':case'ucs-2':case'utf16le':case'utf-16le':return utf16leSlice(this,start,end)
default:if(loweredCase)throw new TypeError('Unknown encoding: '+encoding)
encoding=(encoding+'').toLowerCase()
loweredCase=true}}}
Buffer.prototype.equals=function equals(b){if(!Buffer.isBuffer(b))throw new TypeError('Argument must be a Buffer')
if(this===b)return true
return Buffer.compare(this,b)===0}
Buffer.prototype.inspect=function inspect(){var str=''
var max=exports.INSPECT_MAX_BYTES
if(this.length>0){str=this.toString('hex',0,max).match(/.{2}/g).join(' ')
if(this.length>max)str+=' ... '}
return'<Buffer '+str+'>'}
Buffer.prototype.compare=function compare(b){if(!Buffer.isBuffer(b))throw new TypeError('Argument must be a Buffer')
if(this===b)return 0
return Buffer.compare(this,b)}
Buffer.prototype.indexOf=function indexOf(val,byteOffset){if(byteOffset>0x7fffffff)byteOffset=0x7fffffff
else if(byteOffset<-0x80000000)byteOffset=-0x80000000
byteOffset>>=0
if(this.length===0)return-1
if(byteOffset>=this.length)return-1
 
if(byteOffset<0)byteOffset=Math.max(this.length+byteOffset,0)
if(typeof val==='string'){if(val.length===0)return-1 
 return String.prototype.indexOf.call(this,val,byteOffset)}
if(Buffer.isBuffer(val)){return arrayIndexOf(this,val,byteOffset)}
if(typeof val==='number'){if(Buffer.TYPED_ARRAY_SUPPORT&&Uint8Array.prototype.indexOf==='function'){return Uint8Array.prototype.indexOf.call(this,val,byteOffset)}
return arrayIndexOf(this,[val],byteOffset)}
function arrayIndexOf(arr,val,byteOffset){var foundIndex=-1
for(var i=0;byteOffset+i<arr.length;i++){if(arr[byteOffset+i]===val[foundIndex===-1?0:i-foundIndex]){if(foundIndex===-1)foundIndex=i
if(i-foundIndex+1===val.length)return byteOffset+foundIndex}else{foundIndex=-1}}
return-1}
throw new TypeError('val must be string, number or Buffer')}
Buffer.prototype.get=function get(offset){console.log('.get() is deprecated. Access using array indexes instead.')
return this.readUInt8(offset)}
Buffer.prototype.set=function set(v,offset){console.log('.set() is deprecated. Access using array indexes instead.')
return this.writeUInt8(v,offset)}
function hexWrite(buf,string,offset,length){offset=Number(offset)||0
var remaining=buf.length-offset
if(!length){length=remaining}else{length=Number(length)
if(length>remaining){length=remaining}} 
var strLen=string.length
if(strLen%2!==0)throw new Error('Invalid hex string')
if(length>strLen/2){length=strLen/2}
for(var i=0;i<length;i++){var parsed=parseInt(string.substr(i*2,2),16)
if(isNaN(parsed))throw new Error('Invalid hex string')
buf[offset+i]=parsed}
return i}
function utf8Write(buf,string,offset,length){var charsWritten=blitBuffer(utf8ToBytes(string,buf.length-offset),buf,offset,length)
return charsWritten}
function asciiWrite(buf,string,offset,length){var charsWritten=blitBuffer(asciiToBytes(string),buf,offset,length)
return charsWritten}
function binaryWrite(buf,string,offset,length){return asciiWrite(buf,string,offset,length)}
function base64Write(buf,string,offset,length){var charsWritten=blitBuffer(base64ToBytes(string),buf,offset,length)
return charsWritten}
function utf16leWrite(buf,string,offset,length){var charsWritten=blitBuffer(utf16leToBytes(string,buf.length-offset),buf,offset,length)
return charsWritten}
Buffer.prototype.write=function write(string,offset,length,encoding){
if(isFinite(offset)){if(!isFinite(length)){encoding=length
length=undefined}}else{ var swap=encoding
encoding=offset
offset=length
length=swap}
offset=Number(offset)||0
if(length<0||offset<0||offset>this.length){throw new RangeError('attempt to write outside buffer bounds')}
var remaining=this.length-offset
if(!length){length=remaining}else{length=Number(length)
if(length>remaining){length=remaining}}
encoding=String(encoding||'utf8').toLowerCase()
var ret
switch(encoding){case'hex':ret=hexWrite(this,string,offset,length)
break
case'utf8':case'utf-8':ret=utf8Write(this,string,offset,length)
break
case'ascii':ret=asciiWrite(this,string,offset,length)
break
case'binary':ret=binaryWrite(this,string,offset,length)
break
case'base64':ret=base64Write(this,string,offset,length)
break
case'ucs2':case'ucs-2':case'utf16le':case'utf-16le':ret=utf16leWrite(this,string,offset,length)
break
default:throw new TypeError('Unknown encoding: '+encoding)}
return ret}
Buffer.prototype.toJSON=function toJSON(){return{type:'Buffer',data:Array.prototype.slice.call(this._arr||this,0)}}
function base64Slice(buf,start,end){if(start===0&&end===buf.length){return base64.fromByteArray(buf)}else{return base64.fromByteArray(buf.slice(start,end))}}
function utf8Slice(buf,start,end){var res=''
var tmp=''
end=Math.min(buf.length,end)
for(var i=start;i<end;i++){if(buf[i]<=0x7F){res+=decodeUtf8Char(tmp)+String.fromCharCode(buf[i])
tmp=''}else{tmp+='%'+buf[i].toString(16)}}
return res+decodeUtf8Char(tmp)}
function asciiSlice(buf,start,end){var ret=''
end=Math.min(buf.length,end)
for(var i=start;i<end;i++){ret+=String.fromCharCode(buf[i]&0x7F)}
return ret}
function binarySlice(buf,start,end){var ret=''
end=Math.min(buf.length,end)
for(var i=start;i<end;i++){ret+=String.fromCharCode(buf[i])}
return ret}
function hexSlice(buf,start,end){var len=buf.length
if(!start||start<0)start=0
if(!end||end<0||end>len)end=len
var out=''
for(var i=start;i<end;i++){out+=toHex(buf[i])}
return out}
function utf16leSlice(buf,start,end){var bytes=buf.slice(start,end)
var res=''
for(var i=0;i<bytes.length;i+=2){res+=String.fromCharCode(bytes[i]+bytes[i+1]*256)}
return res}
Buffer.prototype.slice=function slice(start,end){var len=this.length
start=~~start
end=end===undefined?len:~~end
if(start<0){start+=len
if(start<0)start=0}else if(start>len){start=len}
if(end<0){end+=len
if(end<0)end=0}else if(end>len){end=len}
if(end<start)end=start
var newBuf
if(Buffer.TYPED_ARRAY_SUPPORT){newBuf=Buffer._augment(this.subarray(start,end))}else{var sliceLen=end-start
newBuf=new Buffer(sliceLen,undefined)
for(var i=0;i<sliceLen;i++){newBuf[i]=this[i+start]}}
if(newBuf.length)newBuf.parent=this.parent||this
return newBuf}
function checkOffset(offset,ext,length){if((offset%1)!==0||offset<0)throw new RangeError('offset is not uint')
if(offset+ext>length)throw new RangeError('Trying to access beyond buffer length')}
Buffer.prototype.readUIntLE=function readUIntLE(offset,byteLength,noAssert){offset=offset>>>0
byteLength=byteLength>>>0
if(!noAssert)checkOffset(offset,byteLength,this.length)
var val=this[offset]
var mul=1
var i=0
while(++i<byteLength&&(mul*=0x100)){val+=this[offset+i]*mul}
return val}
Buffer.prototype.readUIntBE=function readUIntBE(offset,byteLength,noAssert){offset=offset>>>0
byteLength=byteLength>>>0
if(!noAssert){checkOffset(offset,byteLength,this.length)}
var val=this[offset+--byteLength]
var mul=1
while(byteLength>0&&(mul*=0x100)){val+=this[offset+--byteLength]*mul}
return val}
Buffer.prototype.readUInt8=function readUInt8(offset,noAssert){if(!noAssert)checkOffset(offset,1,this.length)
return this[offset]}
Buffer.prototype.readUInt16LE=function readUInt16LE(offset,noAssert){if(!noAssert)checkOffset(offset,2,this.length)
return this[offset]|(this[offset+1]<<8)}
Buffer.prototype.readUInt16BE=function readUInt16BE(offset,noAssert){if(!noAssert)checkOffset(offset,2,this.length)
return(this[offset]<<8)|this[offset+1]}
Buffer.prototype.readUInt32LE=function readUInt32LE(offset,noAssert){if(!noAssert)checkOffset(offset,4,this.length)
return((this[offset])|(this[offset+1]<<8)|(this[offset+2]<<16))+
(this[offset+3]*0x1000000)}
Buffer.prototype.readUInt32BE=function readUInt32BE(offset,noAssert){if(!noAssert)checkOffset(offset,4,this.length)
return(this[offset]*0x1000000)+
((this[offset+1]<<16)|(this[offset+2]<<8)|this[offset+3])}
Buffer.prototype.readIntLE=function readIntLE(offset,byteLength,noAssert){offset=offset>>>0
byteLength=byteLength>>>0
if(!noAssert)checkOffset(offset,byteLength,this.length)
var val=this[offset]
var mul=1
var i=0
while(++i<byteLength&&(mul*=0x100)){val+=this[offset+i]*mul}
mul*=0x80
if(val>=mul)val-=Math.pow(2,8*byteLength)
return val}
Buffer.prototype.readIntBE=function readIntBE(offset,byteLength,noAssert){offset=offset>>>0
byteLength=byteLength>>>0
if(!noAssert)checkOffset(offset,byteLength,this.length)
var i=byteLength
var mul=1
var val=this[offset+--i]
while(i>0&&(mul*=0x100)){val+=this[offset+--i]*mul}
mul*=0x80
if(val>=mul)val-=Math.pow(2,8*byteLength)
return val}
Buffer.prototype.readInt8=function readInt8(offset,noAssert){if(!noAssert)checkOffset(offset,1,this.length)
if(!(this[offset]&0x80))return(this[offset])
return((0xff-this[offset]+1)*-1)}
Buffer.prototype.readInt16LE=function readInt16LE(offset,noAssert){if(!noAssert)checkOffset(offset,2,this.length)
var val=this[offset]|(this[offset+1]<<8)
return(val&0x8000)?val|0xFFFF0000:val}
Buffer.prototype.readInt16BE=function readInt16BE(offset,noAssert){if(!noAssert)checkOffset(offset,2,this.length)
var val=this[offset+1]|(this[offset]<<8)
return(val&0x8000)?val|0xFFFF0000:val}
Buffer.prototype.readInt32LE=function readInt32LE(offset,noAssert){if(!noAssert)checkOffset(offset,4,this.length)
return(this[offset])|(this[offset+1]<<8)|(this[offset+2]<<16)|(this[offset+3]<<24)}
Buffer.prototype.readInt32BE=function readInt32BE(offset,noAssert){if(!noAssert)checkOffset(offset,4,this.length)
return(this[offset]<<24)|(this[offset+1]<<16)|(this[offset+2]<<8)|(this[offset+3])}
Buffer.prototype.readFloatLE=function readFloatLE(offset,noAssert){if(!noAssert)checkOffset(offset,4,this.length)
return ieee754.read(this,offset,true,23,4)}
Buffer.prototype.readFloatBE=function readFloatBE(offset,noAssert){if(!noAssert)checkOffset(offset,4,this.length)
return ieee754.read(this,offset,false,23,4)}
Buffer.prototype.readDoubleLE=function readDoubleLE(offset,noAssert){if(!noAssert)checkOffset(offset,8,this.length)
return ieee754.read(this,offset,true,52,8)}
Buffer.prototype.readDoubleBE=function readDoubleBE(offset,noAssert){if(!noAssert)checkOffset(offset,8,this.length)
return ieee754.read(this,offset,false,52,8)}
function checkInt(buf,value,offset,ext,max,min){if(!Buffer.isBuffer(buf))throw new TypeError('buffer must be a Buffer instance')
if(value>max||value<min)throw new RangeError('value is out of bounds')
if(offset+ext>buf.length)throw new RangeError('index out of range')}
Buffer.prototype.writeUIntLE=function writeUIntLE(value,offset,byteLength,noAssert){value=+value
offset=offset>>>0
byteLength=byteLength>>>0
if(!noAssert)checkInt(this,value,offset,byteLength,Math.pow(2,8*byteLength),0)
var mul=1
var i=0
this[offset]=value&0xFF
while(++i<byteLength&&(mul*=0x100)){this[offset+i]=(value/mul)>>>0&0xFF}
return offset+byteLength}
Buffer.prototype.writeUIntBE=function writeUIntBE(value,offset,byteLength,noAssert){value=+value
offset=offset>>>0
byteLength=byteLength>>>0
if(!noAssert)checkInt(this,value,offset,byteLength,Math.pow(2,8*byteLength),0)
var i=byteLength-1
var mul=1
this[offset+i]=value&0xFF
while(--i>=0&&(mul*=0x100)){this[offset+i]=(value/mul)>>>0&0xFF}
return offset+byteLength}
Buffer.prototype.writeUInt8=function writeUInt8(value,offset,noAssert){value=+value
offset=offset>>>0
if(!noAssert)checkInt(this,value,offset,1,0xff,0)
if(!Buffer.TYPED_ARRAY_SUPPORT)value=Math.floor(value)
this[offset]=value
return offset+1}
function objectWriteUInt16(buf,value,offset,littleEndian){if(value<0)value=0xffff+value+1
for(var i=0,j=Math.min(buf.length-offset,2);i<j;i++){buf[offset+i]=(value&(0xff<<(8*(littleEndian?i:1-i))))>>>(littleEndian?i:1-i)*8}}
Buffer.prototype.writeUInt16LE=function writeUInt16LE(value,offset,noAssert){value=+value
offset=offset>>>0
if(!noAssert)checkInt(this,value,offset,2,0xffff,0)
if(Buffer.TYPED_ARRAY_SUPPORT){this[offset]=value
this[offset+1]=(value>>>8)}else{objectWriteUInt16(this,value,offset,true)}
return offset+2}
Buffer.prototype.writeUInt16BE=function writeUInt16BE(value,offset,noAssert){value=+value
offset=offset>>>0
if(!noAssert)checkInt(this,value,offset,2,0xffff,0)
if(Buffer.TYPED_ARRAY_SUPPORT){this[offset]=(value>>>8)
this[offset+1]=value}else{objectWriteUInt16(this,value,offset,false)}
return offset+2}
function objectWriteUInt32(buf,value,offset,littleEndian){if(value<0)value=0xffffffff+value+1
for(var i=0,j=Math.min(buf.length-offset,4);i<j;i++){buf[offset+i]=(value>>>(littleEndian?i:3-i)*8)&0xff}}
Buffer.prototype.writeUInt32LE=function writeUInt32LE(value,offset,noAssert){value=+value
offset=offset>>>0
if(!noAssert)checkInt(this,value,offset,4,0xffffffff,0)
if(Buffer.TYPED_ARRAY_SUPPORT){this[offset+3]=(value>>>24)
this[offset+2]=(value>>>16)
this[offset+1]=(value>>>8)
this[offset]=value}else{objectWriteUInt32(this,value,offset,true)}
return offset+4}
Buffer.prototype.writeUInt32BE=function writeUInt32BE(value,offset,noAssert){value=+value
offset=offset>>>0
if(!noAssert)checkInt(this,value,offset,4,0xffffffff,0)
if(Buffer.TYPED_ARRAY_SUPPORT){this[offset]=(value>>>24)
this[offset+1]=(value>>>16)
this[offset+2]=(value>>>8)
this[offset+3]=value}else{objectWriteUInt32(this,value,offset,false)}
return offset+4}
Buffer.prototype.writeIntLE=function writeIntLE(value,offset,byteLength,noAssert){value=+value
offset=offset>>>0
if(!noAssert){checkInt(this,value,offset,byteLength,Math.pow(2,8*byteLength-1)-1,-Math.pow(2,8*byteLength-1))}
var i=0
var mul=1
var sub=value<0?1:0
this[offset]=value&0xFF
while(++i<byteLength&&(mul*=0x100)){this[offset+i]=((value/mul)>>0)-sub&0xFF}
return offset+byteLength}
Buffer.prototype.writeIntBE=function writeIntBE(value,offset,byteLength,noAssert){value=+value
offset=offset>>>0
if(!noAssert){checkInt(this,value,offset,byteLength,Math.pow(2,8*byteLength-1)-1,-Math.pow(2,8*byteLength-1))}
var i=byteLength-1
var mul=1
var sub=value<0?1:0
this[offset+i]=value&0xFF
while(--i>=0&&(mul*=0x100)){this[offset+i]=((value/mul)>>0)-sub&0xFF}
return offset+byteLength}
Buffer.prototype.writeInt8=function writeInt8(value,offset,noAssert){value=+value
offset=offset>>>0
if(!noAssert)checkInt(this,value,offset,1,0x7f,-0x80)
if(!Buffer.TYPED_ARRAY_SUPPORT)value=Math.floor(value)
if(value<0)value=0xff+value+1
this[offset]=value
return offset+1}
Buffer.prototype.writeInt16LE=function writeInt16LE(value,offset,noAssert){value=+value
offset=offset>>>0
if(!noAssert)checkInt(this,value,offset,2,0x7fff,-0x8000)
if(Buffer.TYPED_ARRAY_SUPPORT){this[offset]=value
this[offset+1]=(value>>>8)}else{objectWriteUInt16(this,value,offset,true)}
return offset+2}
Buffer.prototype.writeInt16BE=function writeInt16BE(value,offset,noAssert){value=+value
offset=offset>>>0
if(!noAssert)checkInt(this,value,offset,2,0x7fff,-0x8000)
if(Buffer.TYPED_ARRAY_SUPPORT){this[offset]=(value>>>8)
this[offset+1]=value}else{objectWriteUInt16(this,value,offset,false)}
return offset+2}
Buffer.prototype.writeInt32LE=function writeInt32LE(value,offset,noAssert){value=+value
offset=offset>>>0
if(!noAssert)checkInt(this,value,offset,4,0x7fffffff,-0x80000000)
if(Buffer.TYPED_ARRAY_SUPPORT){this[offset]=value
this[offset+1]=(value>>>8)
this[offset+2]=(value>>>16)
this[offset+3]=(value>>>24)}else{objectWriteUInt32(this,value,offset,true)}
return offset+4}
Buffer.prototype.writeInt32BE=function writeInt32BE(value,offset,noAssert){value=+value
offset=offset>>>0
if(!noAssert)checkInt(this,value,offset,4,0x7fffffff,-0x80000000)
if(value<0)value=0xffffffff+value+1
if(Buffer.TYPED_ARRAY_SUPPORT){this[offset]=(value>>>24)
this[offset+1]=(value>>>16)
this[offset+2]=(value>>>8)
this[offset+3]=value}else{objectWriteUInt32(this,value,offset,false)}
return offset+4}
function checkIEEE754(buf,value,offset,ext,max,min){if(value>max||value<min)throw new RangeError('value is out of bounds')
if(offset+ext>buf.length)throw new RangeError('index out of range')
if(offset<0)throw new RangeError('index out of range')}
function writeFloat(buf,value,offset,littleEndian,noAssert){if(!noAssert){checkIEEE754(buf,value,offset,4,3.4028234663852886e+38,-3.4028234663852886e+38)}
ieee754.write(buf,value,offset,littleEndian,23,4)
return offset+4}
Buffer.prototype.writeFloatLE=function writeFloatLE(value,offset,noAssert){return writeFloat(this,value,offset,true,noAssert)}
Buffer.prototype.writeFloatBE=function writeFloatBE(value,offset,noAssert){return writeFloat(this,value,offset,false,noAssert)}
function writeDouble(buf,value,offset,littleEndian,noAssert){if(!noAssert){checkIEEE754(buf,value,offset,8,1.7976931348623157E+308,-1.7976931348623157E+308)}
ieee754.write(buf,value,offset,littleEndian,52,8)
return offset+8}
Buffer.prototype.writeDoubleLE=function writeDoubleLE(value,offset,noAssert){return writeDouble(this,value,offset,true,noAssert)}
Buffer.prototype.writeDoubleBE=function writeDoubleBE(value,offset,noAssert){return writeDouble(this,value,offset,false,noAssert)}
Buffer.prototype.copy=function copy(target,target_start,start,end){if(!start)start=0
if(!end&&end!==0)end=this.length
if(target_start>=target.length)target_start=target.length
if(!target_start)target_start=0
if(end>0&&end<start)end=start
 
if(end===start)return 0
if(target.length===0||this.length===0)return 0
 
if(target_start<0){throw new RangeError('targetStart out of bounds')}
if(start<0||start>=this.length)throw new RangeError('sourceStart out of bounds')
if(end<0)throw new RangeError('sourceEnd out of bounds')
if(end>this.length)end=this.length
if(target.length-target_start<end-start){end=target.length-target_start+start}
var len=end-start
if(len<1000||!Buffer.TYPED_ARRAY_SUPPORT){for(var i=0;i<len;i++){target[i+target_start]=this[i+start]}}else{target._set(this.subarray(start,start+len),target_start)}
return len}
Buffer.prototype.fill=function fill(value,start,end){if(!value)value=0
if(!start)start=0
if(!end)end=this.length
if(end<start)throw new RangeError('end < start') 
if(end===start)return
if(this.length===0)return
if(start<0||start>=this.length)throw new RangeError('start out of bounds')
if(end<0||end>this.length)throw new RangeError('end out of bounds')
var i
if(typeof value==='number'){for(i=start;i<end;i++){this[i]=value}}else{var bytes=utf8ToBytes(value.toString())
var len=bytes.length
for(i=start;i<end;i++){this[i]=bytes[i%len]}}
return this}
Buffer.prototype.toArrayBuffer=function toArrayBuffer(){if(typeof Uint8Array!=='undefined'){if(Buffer.TYPED_ARRAY_SUPPORT){return(new Buffer(this)).buffer}else{var buf=new Uint8Array(this.length)
for(var i=0,len=buf.length;i<len;i+=1){buf[i]=this[i]}
return buf.buffer}}else{throw new TypeError('Buffer.toArrayBuffer not supported in this browser')}}

var BP=Buffer.prototype 
Buffer._augment=function _augment(arr){arr.constructor=Buffer
arr._isBuffer=true
 
arr._set=arr.set

arr.get=BP.get
arr.set=BP.set
arr.write=BP.write
arr.toString=BP.toString
arr.toLocaleString=BP.toString
arr.toJSON=BP.toJSON
arr.equals=BP.equals
arr.compare=BP.compare
arr.indexOf=BP.indexOf
arr.copy=BP.copy
arr.slice=BP.slice
arr.readUIntLE=BP.readUIntLE
arr.readUIntBE=BP.readUIntBE
arr.readUInt8=BP.readUInt8
arr.readUInt16LE=BP.readUInt16LE
arr.readUInt16BE=BP.readUInt16BE
arr.readUInt32LE=BP.readUInt32LE
arr.readUInt32BE=BP.readUInt32BE
arr.readIntLE=BP.readIntLE
arr.readIntBE=BP.readIntBE
arr.readInt8=BP.readInt8
arr.readInt16LE=BP.readInt16LE
arr.readInt16BE=BP.readInt16BE
arr.readInt32LE=BP.readInt32LE
arr.readInt32BE=BP.readInt32BE
arr.readFloatLE=BP.readFloatLE
arr.readFloatBE=BP.readFloatBE
arr.readDoubleLE=BP.readDoubleLE
arr.readDoubleBE=BP.readDoubleBE
arr.writeUInt8=BP.writeUInt8
arr.writeUIntLE=BP.writeUIntLE
arr.writeUIntBE=BP.writeUIntBE
arr.writeUInt16LE=BP.writeUInt16LE
arr.writeUInt16BE=BP.writeUInt16BE
arr.writeUInt32LE=BP.writeUInt32LE
arr.writeUInt32BE=BP.writeUInt32BE
arr.writeIntLE=BP.writeIntLE
arr.writeIntBE=BP.writeIntBE
arr.writeInt8=BP.writeInt8
arr.writeInt16LE=BP.writeInt16LE
arr.writeInt16BE=BP.writeInt16BE
arr.writeInt32LE=BP.writeInt32LE
arr.writeInt32BE=BP.writeInt32BE
arr.writeFloatLE=BP.writeFloatLE
arr.writeFloatBE=BP.writeFloatBE
arr.writeDoubleLE=BP.writeDoubleLE
arr.writeDoubleBE=BP.writeDoubleBE
arr.fill=BP.fill
arr.inspect=BP.inspect
arr.toArrayBuffer=BP.toArrayBuffer
return arr}
var INVALID_BASE64_RE=/[^+\/0-9A-z\-]/g
function base64clean(str){ str=stringtrim(str).replace(INVALID_BASE64_RE,'')
if(str.length<2)return'' 
while(str.length%4!==0){str=str+'='}
return str}
function stringtrim(str){if(str.trim)return str.trim()
return str.replace(/^\s+|\s+$/g,'')}
function isArrayish(subject){return isArray(subject)||Buffer.isBuffer(subject)||subject&&typeof subject==='object'&&typeof subject.length==='number'}
function toHex(n){if(n<16)return'0'+n.toString(16)
return n.toString(16)}
function utf8ToBytes(string,units){units=units||Infinity
var codePoint
var length=string.length
var leadSurrogate=null
var bytes=[]
var i=0
for(;i<length;i++){codePoint=string.charCodeAt(i) 
if(codePoint>0xD7FF&&codePoint<0xE000){ if(leadSurrogate){ if(codePoint<0xDC00){if((units-=3)>-1)bytes.push(0xEF,0xBF,0xBD)
leadSurrogate=codePoint
continue}else{ codePoint=leadSurrogate-0xD800<<10|codePoint-0xDC00|0x10000
leadSurrogate=null}}else{ if(codePoint>0xDBFF){ if((units-=3)>-1)bytes.push(0xEF,0xBF,0xBD)
continue}else if(i+1===length){ if((units-=3)>-1)bytes.push(0xEF,0xBF,0xBD)
continue}else{ leadSurrogate=codePoint
continue}}}else if(leadSurrogate){ if((units-=3)>-1)bytes.push(0xEF,0xBF,0xBD)
leadSurrogate=null} 
if(codePoint<0x80){if((units-=1)<0)break
bytes.push(codePoint)}else if(codePoint<0x800){if((units-=2)<0)break
bytes.push(codePoint>>0x6|0xC0,codePoint&0x3F|0x80)}else if(codePoint<0x10000){if((units-=3)<0)break
bytes.push(codePoint>>0xC|0xE0,codePoint>>0x6&0x3F|0x80,codePoint&0x3F|0x80)}else if(codePoint<0x200000){if((units-=4)<0)break
bytes.push(codePoint>>0x12|0xF0,codePoint>>0xC&0x3F|0x80,codePoint>>0x6&0x3F|0x80,codePoint&0x3F|0x80)}else{throw new Error('Invalid code point')}}
return bytes}
function asciiToBytes(str){var byteArray=[]
for(var i=0;i<str.length;i++){byteArray.push(str.charCodeAt(i)&0xFF)}
return byteArray}
function utf16leToBytes(str,units){var c,hi,lo
var byteArray=[]
for(var i=0;i<str.length;i++){if((units-=2)<0)break
c=str.charCodeAt(i)
hi=c>>8
lo=c%256
byteArray.push(lo)
byteArray.push(hi)}
return byteArray}
function base64ToBytes(str){return base64.toByteArray(base64clean(str))}
function blitBuffer(src,dst,offset,length){for(var i=0;i<length;i++){if((i+offset>=dst.length)||(i>=src.length))break
dst[i+offset]=src[i]}
return i}
function decodeUtf8Char(str){try{return decodeURIComponent(str)}catch(err){return String.fromCharCode(0xFFFD)
}}},{"base64-js":4,"ieee754":5,"is-array":6}],4:[function(_dereq_,module,exports){var lookup='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';;(function(exports){'use strict';var Arr=(typeof Uint8Array!=='undefined')?Uint8Array:Array
var PLUS='+'.charCodeAt(0)
var SLASH='/'.charCodeAt(0)
var NUMBER='0'.charCodeAt(0)
var LOWER='a'.charCodeAt(0)
var UPPER='A'.charCodeAt(0)
var PLUS_URL_SAFE='-'.charCodeAt(0)
var SLASH_URL_SAFE='_'.charCodeAt(0)
function decode(elt){var code=elt.charCodeAt(0)
if(code===PLUS||code===PLUS_URL_SAFE)
return 62 
if(code===SLASH||code===SLASH_URL_SAFE)
return 63 
if(code<NUMBER)
return-1 
 if(code<NUMBER+10)
return code-NUMBER+26+26
if(code<UPPER+26)
return code-UPPER
if(code<LOWER+26)
return code-LOWER+26}
function b64ToByteArray(b64){var i,j,l,tmp,placeHolders,arr
if(b64.length%4>0){throw new Error('Invalid string. Length must be a multiple of 4')}


 
var len=b64.length
placeHolders='='===b64.charAt(len-2)?2:'='===b64.charAt(len-1)?1:0
 
arr=new Arr(b64.length*3/4-placeHolders) 
l=placeHolders>0?b64.length-4:b64.length
var L=0
function push(v){arr[L++]=v}
for(i=0,j=0;i<l;i+=4,j+=3){tmp=(decode(b64.charAt(i))<<18)|(decode(b64.charAt(i+1))<<12)|(decode(b64.charAt(i+2))<<6)|decode(b64.charAt(i+3))
push((tmp&0xFF0000)>>16)
push((tmp&0xFF00)>>8)
push(tmp&0xFF)}
if(placeHolders===2){tmp=(decode(b64.charAt(i))<<2)|(decode(b64.charAt(i+1))>>4)
push(tmp&0xFF)}else if(placeHolders===1){tmp=(decode(b64.charAt(i))<<10)|(decode(b64.charAt(i+1))<<4)|(decode(b64.charAt(i+2))>>2)
push((tmp>>8)&0xFF)
push(tmp&0xFF)}
return arr}
function uint8ToBase64(uint8){var i,extraBytes=uint8.length%3, output="",temp,length
function encode(num){return lookup.charAt(num)}
function tripletToBase64(num){return encode(num>>18&0x3F)+encode(num>>12&0x3F)+encode(num>>6&0x3F)+encode(num&0x3F)} 
for(i=0,length=uint8.length-extraBytes;i<length;i+=3){temp=(uint8[i]<<16)+(uint8[i+1]<<8)+(uint8[i+2])
output+=tripletToBase64(temp)} 
switch(extraBytes){case 1:temp=uint8[uint8.length-1]
output+=encode(temp>>2)
output+=encode((temp<<4)&0x3F)
output+='=='
break
case 2:temp=(uint8[uint8.length-2]<<8)+(uint8[uint8.length-1])
output+=encode(temp>>10)
output+=encode((temp>>4)&0x3F)
output+=encode((temp<<2)&0x3F)
output+='='
break}
return output}
exports.toByteArray=b64ToByteArray
exports.fromByteArray=uint8ToBase64}(typeof exports==='undefined'?(this.base64js={}):exports))},{}],5:[function(_dereq_,module,exports){exports.read=function(buffer,offset,isLE,mLen,nBytes){var e,m,eLen=nBytes*8-mLen-1,eMax=(1<<eLen)-1,eBias=eMax>>1,nBits=-7,i=isLE?(nBytes-1):0,d=isLE?-1:1,s=buffer[offset+i];i+=d;e=s&((1<<(-nBits))-1);s>>=(-nBits);nBits+=eLen;for(;nBits>0;e=e*256+buffer[offset+i],i+=d,nBits-=8);m=e&((1<<(-nBits))-1);e>>=(-nBits);nBits+=mLen;for(;nBits>0;m=m*256+buffer[offset+i],i+=d,nBits-=8);if(e===0){e=1-eBias;}else if(e===eMax){return m?NaN:((s?-1:1)*Infinity);}else{m=m+Math.pow(2,mLen);e=e-eBias;}
return(s?-1:1)*m*Math.pow(2,e-mLen);};exports.write=function(buffer,value,offset,isLE,mLen,nBytes){var e,m,c,eLen=nBytes*8-mLen-1,eMax=(1<<eLen)-1,eBias=eMax>>1,rt=(mLen===23?Math.pow(2,-24)-Math.pow(2,-77):0),i=isLE?0:(nBytes-1),d=isLE?1:-1,s=value<0||(value===0&&1/value<0)?1:0;value=Math.abs(value);if(isNaN(value)||value===Infinity){m=isNaN(value)?1:0;e=eMax;}else{e=Math.floor(Math.log(value)/Math.LN2);if(value*(c=Math.pow(2,-e))<1){e--;c*=2;}
if(e+eBias>=1){value+=rt/c;}else{value+=rt*Math.pow(2,1-eBias);}
if(value*c>=2){e++;c/=2;}
if(e+eBias>=eMax){m=0;e=eMax;}else if(e+eBias>=1){m=(value*c-1)*Math.pow(2,mLen);e=e+eBias;}else{m=value*Math.pow(2,eBias-1)*Math.pow(2,mLen);e=0;}}
for(;mLen>=8;buffer[offset+i]=m&0xff,i+=d,m/=256,mLen-=8);e=(e<<mLen)|m;eLen+=mLen;for(;eLen>0;buffer[offset+i]=e&0xff,i+=d,e/=256,eLen-=8);buffer[offset+i-d]|=s*128;};},{}],6:[function(_dereq_,module,exports){var isArray=Array.isArray;var str=Object.prototype.toString;module.exports=isArray||function(val){return!!val&&'[object Array]'==str.call(val);};},{}],7:[function(_dereq_,module,exports){(function(process){













function normalizeArray(parts,allowAboveRoot){ var up=0;for(var i=parts.length-1;i>=0;i--){var last=parts[i];if(last==='.'){parts.splice(i,1);}else if(last==='..'){parts.splice(i,1);up++;}else if(up){parts.splice(i,1);up--;}} 
if(allowAboveRoot){for(;up--;up){parts.unshift('..');}}
return parts;}

var splitPathRe=/^(\/?|)([\s\S]*?)((?:\.{1,2}|[^\/]+?|)(\.[^.\/]*|))(?:[\/]*)$/;var splitPath=function(filename){return splitPathRe.exec(filename).slice(1);};
exports.resolve=function(){var resolvedPath='',resolvedAbsolute=false;for(var i=arguments.length-1;i>=-1&&!resolvedAbsolute;i--){var path=(i>=0)?arguments[i]:process.cwd(); if(typeof path!=='string'){throw new TypeError('Arguments to path.resolve must be strings');}else if(!path){continue;}
resolvedPath=path+'/'+resolvedPath;resolvedAbsolute=path.charAt(0)==='/';}
 
resolvedPath=normalizeArray(filter(resolvedPath.split('/'),function(p){return!!p;}),!resolvedAbsolute).join('/');return((resolvedAbsolute?'/':'')+resolvedPath)||'.';};
exports.normalize=function(path){var isAbsolute=exports.isAbsolute(path),trailingSlash=substr(path,-1)==='/'; path=normalizeArray(filter(path.split('/'),function(p){return!!p;}),!isAbsolute).join('/');if(!path&&!isAbsolute){path='.';}
if(path&&trailingSlash){path+='/';}
return(isAbsolute?'/':'')+path;};exports.isAbsolute=function(path){return path.charAt(0)==='/';};exports.join=function(){var paths=Array.prototype.slice.call(arguments,0);return exports.normalize(filter(paths,function(p,index){if(typeof p!=='string'){throw new TypeError('Arguments to path.join must be strings');}
return p;}).join('/'));};
exports.relative=function(from,to){from=exports.resolve(from).substr(1);to=exports.resolve(to).substr(1);function trim(arr){var start=0;for(;start<arr.length;start++){if(arr[start]!=='')break;}
var end=arr.length-1;for(;end>=0;end--){if(arr[end]!=='')break;}
if(start>end)return[];return arr.slice(start,end-start+1);}
var fromParts=trim(from.split('/'));var toParts=trim(to.split('/'));var length=Math.min(fromParts.length,toParts.length);var samePartsLength=length;for(var i=0;i<length;i++){if(fromParts[i]!==toParts[i]){samePartsLength=i;break;}}
var outputParts=[];for(var i=samePartsLength;i<fromParts.length;i++){outputParts.push('..');}
outputParts=outputParts.concat(toParts.slice(samePartsLength));return outputParts.join('/');};exports.sep='/';exports.delimiter=':';exports.dirname=function(path){var result=splitPath(path),root=result[0],dir=result[1];if(!root&&!dir){ return'.';}
if(dir){ dir=dir.substr(0,dir.length-1);}
return root+dir;};exports.basename=function(path,ext){var f=splitPath(path)[2];if(ext&&f.substr(-1*ext.length)===ext){f=f.substr(0,f.length-ext.length);}
return f;};exports.extname=function(path){return splitPath(path)[3];};function filter(xs,f){if(xs.filter)return xs.filter(f);var res=[];for(var i=0;i<xs.length;i++){if(f(xs[i],i,xs))res.push(xs[i]);}
return res;}
var substr='ab'.substr(-1)==='b'?function(str,start,len){return str.substr(start,len)}:function(str,start,len){if(start<0)start=str.length+start;return str.substr(start,len);};}).call(this,_dereq_('_process'))},{"_process":8}],8:[function(_dereq_,module,exports){var process=module.exports={};var queue=[];var draining=false;function drainQueue(){if(draining){return;}
draining=true;var currentQueue;var len=queue.length;while(len){currentQueue=queue;queue=[];var i=-1;while(++i<len){currentQueue[i]();}
len=queue.length;}
draining=false;}
process.nextTick=function(fun){queue.push(fun);if(!draining){setTimeout(drainQueue,0);}};process.title='browser';process.browser=true;process.env={};process.argv=[];process.version='';process.versions={};function noop(){}
process.on=noop;process.addListener=noop;process.once=noop;process.off=noop;process.removeListener=noop;process.removeAllListeners=noop;process.emit=noop;process.binding=function(name){throw new Error('process.binding is not supported');};process.cwd=function(){return'/'};process.chdir=function(dir){throw new Error('process.chdir is not supported');};process.umask=function(){return 0;};},{}],9:[function(_dereq_,module,exports){(function(root,factory){'use strict';if(typeof define==='function'&&define.amd){define(['exports'],factory);}else if(typeof exports!=='undefined'){factory(exports);}else{factory((root.esprima={}));}}(this,function(exports){'use strict';var Token,TokenName,FnExprTokens,Syntax,PropertyKind,Messages,Regex,SyntaxTreeDelegate,XHTMLEntities,ClassPropertyType,source,strict,index,lineNumber,lineStart,length,delegate,lookahead,state,extra;Token={BooleanLiteral:1,EOF:2,Identifier:3,Keyword:4,NullLiteral:5,NumericLiteral:6,Punctuator:7,StringLiteral:8,RegularExpression:9,Template:10,JSXIdentifier:11,JSXText:12};TokenName={};TokenName[Token.BooleanLiteral]='Boolean';TokenName[Token.EOF]='<end>';TokenName[Token.Identifier]='Identifier';TokenName[Token.Keyword]='Keyword';TokenName[Token.NullLiteral]='Null';TokenName[Token.NumericLiteral]='Numeric';TokenName[Token.Punctuator]='Punctuator';TokenName[Token.StringLiteral]='String';TokenName[Token.JSXIdentifier]='JSXIdentifier';TokenName[Token.JSXText]='JSXText';TokenName[Token.RegularExpression]='RegularExpression';FnExprTokens=['(','{','[','in','typeof','instanceof','new','return','case','delete','throw','void','=','+=','-=','*=','/=','%=','<<=','>>=','>>>=','&=','|=','^=',',','+','-','*','/','%','++','--','<<','>>','>>>','&','|','^','!','~','&&','||','?',':','===','==','>=','<=','<','>','!=','!=='];Syntax={AnyTypeAnnotation:'AnyTypeAnnotation',ArrayExpression:'ArrayExpression',ArrayPattern:'ArrayPattern',ArrayTypeAnnotation:'ArrayTypeAnnotation',ArrowFunctionExpression:'ArrowFunctionExpression',AssignmentExpression:'AssignmentExpression',BinaryExpression:'BinaryExpression',BlockStatement:'BlockStatement',BooleanTypeAnnotation:'BooleanTypeAnnotation',BreakStatement:'BreakStatement',CallExpression:'CallExpression',CatchClause:'CatchClause',ClassBody:'ClassBody',ClassDeclaration:'ClassDeclaration',ClassExpression:'ClassExpression',ClassImplements:'ClassImplements',ClassProperty:'ClassProperty',ComprehensionBlock:'ComprehensionBlock',ComprehensionExpression:'ComprehensionExpression',ConditionalExpression:'ConditionalExpression',ContinueStatement:'ContinueStatement',DebuggerStatement:'DebuggerStatement',DeclareClass:'DeclareClass',DeclareFunction:'DeclareFunction',DeclareModule:'DeclareModule',DeclareVariable:'DeclareVariable',DoWhileStatement:'DoWhileStatement',EmptyStatement:'EmptyStatement',ExportDeclaration:'ExportDeclaration',ExportBatchSpecifier:'ExportBatchSpecifier',ExportSpecifier:'ExportSpecifier',ExpressionStatement:'ExpressionStatement',ForInStatement:'ForInStatement',ForOfStatement:'ForOfStatement',ForStatement:'ForStatement',FunctionDeclaration:'FunctionDeclaration',FunctionExpression:'FunctionExpression',FunctionTypeAnnotation:'FunctionTypeAnnotation',FunctionTypeParam:'FunctionTypeParam',GenericTypeAnnotation:'GenericTypeAnnotation',Identifier:'Identifier',IfStatement:'IfStatement',ImportDeclaration:'ImportDeclaration',ImportDefaultSpecifier:'ImportDefaultSpecifier',ImportNamespaceSpecifier:'ImportNamespaceSpecifier',ImportSpecifier:'ImportSpecifier',InterfaceDeclaration:'InterfaceDeclaration',InterfaceExtends:'InterfaceExtends',IntersectionTypeAnnotation:'IntersectionTypeAnnotation',LabeledStatement:'LabeledStatement',Literal:'Literal',LogicalExpression:'LogicalExpression',MemberExpression:'MemberExpression',MethodDefinition:'MethodDefinition',ModuleSpecifier:'ModuleSpecifier',NewExpression:'NewExpression',NullableTypeAnnotation:'NullableTypeAnnotation',NumberTypeAnnotation:'NumberTypeAnnotation',ObjectExpression:'ObjectExpression',ObjectPattern:'ObjectPattern',ObjectTypeAnnotation:'ObjectTypeAnnotation',ObjectTypeCallProperty:'ObjectTypeCallProperty',ObjectTypeIndexer:'ObjectTypeIndexer',ObjectTypeProperty:'ObjectTypeProperty',Program:'Program',Property:'Property',QualifiedTypeIdentifier:'QualifiedTypeIdentifier',ReturnStatement:'ReturnStatement',SequenceExpression:'SequenceExpression',SpreadElement:'SpreadElement',SpreadProperty:'SpreadProperty',StringLiteralTypeAnnotation:'StringLiteralTypeAnnotation',StringTypeAnnotation:'StringTypeAnnotation',SwitchCase:'SwitchCase',SwitchStatement:'SwitchStatement',TaggedTemplateExpression:'TaggedTemplateExpression',TemplateElement:'TemplateElement',TemplateLiteral:'TemplateLiteral',ThisExpression:'ThisExpression',ThrowStatement:'ThrowStatement',TupleTypeAnnotation:'TupleTypeAnnotation',TryStatement:'TryStatement',TypeAlias:'TypeAlias',TypeAnnotation:'TypeAnnotation',TypeCastExpression:'TypeCastExpression',TypeofTypeAnnotation:'TypeofTypeAnnotation',TypeParameterDeclaration:'TypeParameterDeclaration',TypeParameterInstantiation:'TypeParameterInstantiation',UnaryExpression:'UnaryExpression',UnionTypeAnnotation:'UnionTypeAnnotation',UpdateExpression:'UpdateExpression',VariableDeclaration:'VariableDeclaration',VariableDeclarator:'VariableDeclarator',VoidTypeAnnotation:'VoidTypeAnnotation',WhileStatement:'WhileStatement',WithStatement:'WithStatement',JSXIdentifier:'JSXIdentifier',JSXNamespacedName:'JSXNamespacedName',JSXMemberExpression:'JSXMemberExpression',JSXEmptyExpression:'JSXEmptyExpression',JSXExpressionContainer:'JSXExpressionContainer',JSXElement:'JSXElement',JSXClosingElement:'JSXClosingElement',JSXOpeningElement:'JSXOpeningElement',JSXAttribute:'JSXAttribute',JSXSpreadAttribute:'JSXSpreadAttribute',JSXText:'JSXText',YieldExpression:'YieldExpression',AwaitExpression:'AwaitExpression'};PropertyKind={Data:1,Get:2,Set:4};ClassPropertyType={'static':'static',prototype:'prototype'};Messages={UnexpectedToken:'Unexpected token %0',UnexpectedNumber:'Unexpected number',UnexpectedString:'Unexpected string',UnexpectedIdentifier:'Unexpected identifier',UnexpectedReserved:'Unexpected reserved word',UnexpectedTemplate:'Unexpected quasi %0',UnexpectedEOS:'Unexpected end of input',NewlineAfterThrow:'Illegal newline after throw',InvalidRegExp:'Invalid regular expression',UnterminatedRegExp:'Invalid regular expression: missing /',InvalidLHSInAssignment:'Invalid left-hand side in assignment',InvalidLHSInFormalsList:'Invalid left-hand side in formals list',InvalidLHSInForIn:'Invalid left-hand side in for-in',MultipleDefaultsInSwitch:'More than one default clause in switch statement',NoCatchOrFinally:'Missing catch or finally after try',UnknownLabel:'Undefined label \'%0\'',Redeclaration:'%0 \'%1\' has already been declared',IllegalContinue:'Illegal continue statement',IllegalBreak:'Illegal break statement',IllegalDuplicateClassProperty:'Illegal duplicate property in class definition',IllegalClassConstructorProperty:'Illegal constructor property in class definition',IllegalReturn:'Illegal return statement',IllegalSpread:'Illegal spread element',StrictModeWith:'Strict mode code may not include a with statement',StrictCatchVariable:'Catch variable may not be eval or arguments in strict mode',StrictVarName:'Variable name may not be eval or arguments in strict mode',StrictParamName:'Parameter name eval or arguments is not allowed in strict mode',StrictParamDupe:'Strict mode function may not have duplicate parameter names',ParameterAfterRestParameter:'Rest parameter must be final parameter of an argument list',DefaultRestParameter:'Rest parameter can not have a default value',ElementAfterSpreadElement:'Spread must be the final element of an element list',PropertyAfterSpreadProperty:'A rest property must be the final property of an object literal',ObjectPatternAsRestParameter:'Invalid rest parameter',ObjectPatternAsSpread:'Invalid spread argument',StrictFunctionName:'Function name may not be eval or arguments in strict mode',StrictOctalLiteral:'Octal literals are not allowed in strict mode.',StrictDelete:'Delete of an unqualified identifier in strict mode.',StrictDuplicateProperty:'Duplicate data property in object literal not allowed in strict mode',AccessorDataProperty:'Object literal may not have data and accessor property with the same name',AccessorGetSet:'Object literal may not have multiple get/set accessors with the same name',StrictLHSAssignment:'Assignment to eval or arguments is not allowed in strict mode',StrictLHSPostfix:'Postfix increment/decrement may not have eval or arguments operand in strict mode',StrictLHSPrefix:'Prefix increment/decrement may not have eval or arguments operand in strict mode',StrictReservedWord:'Use of future reserved word in strict mode',MissingFromClause:'Missing from clause',NoAsAfterImportNamespace:'Missing as after import *',InvalidModuleSpecifier:'Invalid module specifier',IllegalImportDeclaration:'Illegal import declaration',IllegalExportDeclaration:'Illegal export declaration',NoUninitializedConst:'Const must be initialized',ComprehensionRequiresBlock:'Comprehension must have at least one block',ComprehensionError:'Comprehension Error',EachNotAllowed:'Each is not supported',InvalidJSXAttributeValue:'JSX value should be either an expression or a quoted JSX text',ExpectedJSXClosingTag:'Expected corresponding JSX closing tag for %0',AdjacentJSXElements:'Adjacent JSX elements must be wrapped in an enclosing tag',ConfusedAboutFunctionType:'Unexpected token =>. It looks like '+'you are trying to write a function type, but you ended up '+'writing a grouped type followed by an =>, which is a syntax '+'error. Remember, function type parameters are named so function '+'types look like (name1: type1, name2: type2) => returnType. You '+'probably wrote (type1) => returnType'};Regex={NonAsciiIdentifierStart:new RegExp('[\xaa\xb5\xba\xc0-\xd6\xd8-\xf6\xf8-\u02c1\u02c6-\u02d1\u02e0-\u02e4\u02ec\u02ee\u0370-\u0374\u0376\u0377\u037a-\u037d\u0386\u0388-\u038a\u038c\u038e-\u03a1\u03a3-\u03f5\u03f7-\u0481\u048a-\u0527\u0531-\u0556\u0559\u0561-\u0587\u05d0-\u05ea\u05f0-\u05f2\u0620-\u064a\u066e\u066f\u0671-\u06d3\u06d5\u06e5\u06e6\u06ee\u06ef\u06fa-\u06fc\u06ff\u0710\u0712-\u072f\u074d-\u07a5\u07b1\u07ca-\u07ea\u07f4\u07f5\u07fa\u0800-\u0815\u081a\u0824\u0828\u0840-\u0858\u08a0\u08a2-\u08ac\u0904-\u0939\u093d\u0950\u0958-\u0961\u0971-\u0977\u0979-\u097f\u0985-\u098c\u098f\u0990\u0993-\u09a8\u09aa-\u09b0\u09b2\u09b6-\u09b9\u09bd\u09ce\u09dc\u09dd\u09df-\u09e1\u09f0\u09f1\u0a05-\u0a0a\u0a0f\u0a10\u0a13-\u0a28\u0a2a-\u0a30\u0a32\u0a33\u0a35\u0a36\u0a38\u0a39\u0a59-\u0a5c\u0a5e\u0a72-\u0a74\u0a85-\u0a8d\u0a8f-\u0a91\u0a93-\u0aa8\u0aaa-\u0ab0\u0ab2\u0ab3\u0ab5-\u0ab9\u0abd\u0ad0\u0ae0\u0ae1\u0b05-\u0b0c\u0b0f\u0b10\u0b13-\u0b28\u0b2a-\u0b30\u0b32\u0b33\u0b35-\u0b39\u0b3d\u0b5c\u0b5d\u0b5f-\u0b61\u0b71\u0b83\u0b85-\u0b8a\u0b8e-\u0b90\u0b92-\u0b95\u0b99\u0b9a\u0b9c\u0b9e\u0b9f\u0ba3\u0ba4\u0ba8-\u0baa\u0bae-\u0bb9\u0bd0\u0c05-\u0c0c\u0c0e-\u0c10\u0c12-\u0c28\u0c2a-\u0c33\u0c35-\u0c39\u0c3d\u0c58\u0c59\u0c60\u0c61\u0c85-\u0c8c\u0c8e-\u0c90\u0c92-\u0ca8\u0caa-\u0cb3\u0cb5-\u0cb9\u0cbd\u0cde\u0ce0\u0ce1\u0cf1\u0cf2\u0d05-\u0d0c\u0d0e-\u0d10\u0d12-\u0d3a\u0d3d\u0d4e\u0d60\u0d61\u0d7a-\u0d7f\u0d85-\u0d96\u0d9a-\u0db1\u0db3-\u0dbb\u0dbd\u0dc0-\u0dc6\u0e01-\u0e30\u0e32\u0e33\u0e40-\u0e46\u0e81\u0e82\u0e84\u0e87\u0e88\u0e8a\u0e8d\u0e94-\u0e97\u0e99-\u0e9f\u0ea1-\u0ea3\u0ea5\u0ea7\u0eaa\u0eab\u0ead-\u0eb0\u0eb2\u0eb3\u0ebd\u0ec0-\u0ec4\u0ec6\u0edc-\u0edf\u0f00\u0f40-\u0f47\u0f49-\u0f6c\u0f88-\u0f8c\u1000-\u102a\u103f\u1050-\u1055\u105a-\u105d\u1061\u1065\u1066\u106e-\u1070\u1075-\u1081\u108e\u10a0-\u10c5\u10c7\u10cd\u10d0-\u10fa\u10fc-\u1248\u124a-\u124d\u1250-\u1256\u1258\u125a-\u125d\u1260-\u1288\u128a-\u128d\u1290-\u12b0\u12b2-\u12b5\u12b8-\u12be\u12c0\u12c2-\u12c5\u12c8-\u12d6\u12d8-\u1310\u1312-\u1315\u1318-\u135a\u1380-\u138f\u13a0-\u13f4\u1401-\u166c\u166f-\u167f\u1681-\u169a\u16a0-\u16ea\u16ee-\u16f0\u1700-\u170c\u170e-\u1711\u1720-\u1731\u1740-\u1751\u1760-\u176c\u176e-\u1770\u1780-\u17b3\u17d7\u17dc\u1820-\u1877\u1880-\u18a8\u18aa\u18b0-\u18f5\u1900-\u191c\u1950-\u196d\u1970-\u1974\u1980-\u19ab\u19c1-\u19c7\u1a00-\u1a16\u1a20-\u1a54\u1aa7\u1b05-\u1b33\u1b45-\u1b4b\u1b83-\u1ba0\u1bae\u1baf\u1bba-\u1be5\u1c00-\u1c23\u1c4d-\u1c4f\u1c5a-\u1c7d\u1ce9-\u1cec\u1cee-\u1cf1\u1cf5\u1cf6\u1d00-\u1dbf\u1e00-\u1f15\u1f18-\u1f1d\u1f20-\u1f45\u1f48-\u1f4d\u1f50-\u1f57\u1f59\u1f5b\u1f5d\u1f5f-\u1f7d\u1f80-\u1fb4\u1fb6-\u1fbc\u1fbe\u1fc2-\u1fc4\u1fc6-\u1fcc\u1fd0-\u1fd3\u1fd6-\u1fdb\u1fe0-\u1fec\u1ff2-\u1ff4\u1ff6-\u1ffc\u2071\u207f\u2090-\u209c\u2102\u2107\u210a-\u2113\u2115\u2119-\u211d\u2124\u2126\u2128\u212a-\u212d\u212f-\u2139\u213c-\u213f\u2145-\u2149\u214e\u2160-\u2188\u2c00-\u2c2e\u2c30-\u2c5e\u2c60-\u2ce4\u2ceb-\u2cee\u2cf2\u2cf3\u2d00-\u2d25\u2d27\u2d2d\u2d30-\u2d67\u2d6f\u2d80-\u2d96\u2da0-\u2da6\u2da8-\u2dae\u2db0-\u2db6\u2db8-\u2dbe\u2dc0-\u2dc6\u2dc8-\u2dce\u2dd0-\u2dd6\u2dd8-\u2dde\u2e2f\u3005-\u3007\u3021-\u3029\u3031-\u3035\u3038-\u303c\u3041-\u3096\u309d-\u309f\u30a1-\u30fa\u30fc-\u30ff\u3105-\u312d\u3131-\u318e\u31a0-\u31ba\u31f0-\u31ff\u3400-\u4db5\u4e00-\u9fcc\ua000-\ua48c\ua4d0-\ua4fd\ua500-\ua60c\ua610-\ua61f\ua62a\ua62b\ua640-\ua66e\ua67f-\ua697\ua6a0-\ua6ef\ua717-\ua71f\ua722-\ua788\ua78b-\ua78e\ua790-\ua793\ua7a0-\ua7aa\ua7f8-\ua801\ua803-\ua805\ua807-\ua80a\ua80c-\ua822\ua840-\ua873\ua882-\ua8b3\ua8f2-\ua8f7\ua8fb\ua90a-\ua925\ua930-\ua946\ua960-\ua97c\ua984-\ua9b2\ua9cf\uaa00-\uaa28\uaa40-\uaa42\uaa44-\uaa4b\uaa60-\uaa76\uaa7a\uaa80-\uaaaf\uaab1\uaab5\uaab6\uaab9-\uaabd\uaac0\uaac2\uaadb-\uaadd\uaae0-\uaaea\uaaf2-\uaaf4\uab01-\uab06\uab09-\uab0e\uab11-\uab16\uab20-\uab26\uab28-\uab2e\uabc0-\uabe2\uac00-\ud7a3\ud7b0-\ud7c6\ud7cb-\ud7fb\uf900-\ufa6d\ufa70-\ufad9\ufb00-\ufb06\ufb13-\ufb17\ufb1d\ufb1f-\ufb28\ufb2a-\ufb36\ufb38-\ufb3c\ufb3e\ufb40\ufb41\ufb43\ufb44\ufb46-\ufbb1\ufbd3-\ufd3d\ufd50-\ufd8f\ufd92-\ufdc7\ufdf0-\ufdfb\ufe70-\ufe74\ufe76-\ufefc\uff21-\uff3a\uff41-\uff5a\uff66-\uffbe\uffc2-\uffc7\uffca-\uffcf\uffd2-\uffd7\uffda-\uffdc]'),NonAsciiIdentifierPart:new RegExp('[\xaa\xb5\xba\xc0-\xd6\xd8-\xf6\xf8-\u02c1\u02c6-\u02d1\u02e0-\u02e4\u02ec\u02ee\u0300-\u0374\u0376\u0377\u037a-\u037d\u0386\u0388-\u038a\u038c\u038e-\u03a1\u03a3-\u03f5\u03f7-\u0481\u0483-\u0487\u048a-\u0527\u0531-\u0556\u0559\u0561-\u0587\u0591-\u05bd\u05bf\u05c1\u05c2\u05c4\u05c5\u05c7\u05d0-\u05ea\u05f0-\u05f2\u0610-\u061a\u0620-\u0669\u066e-\u06d3\u06d5-\u06dc\u06df-\u06e8\u06ea-\u06fc\u06ff\u0710-\u074a\u074d-\u07b1\u07c0-\u07f5\u07fa\u0800-\u082d\u0840-\u085b\u08a0\u08a2-\u08ac\u08e4-\u08fe\u0900-\u0963\u0966-\u096f\u0971-\u0977\u0979-\u097f\u0981-\u0983\u0985-\u098c\u098f\u0990\u0993-\u09a8\u09aa-\u09b0\u09b2\u09b6-\u09b9\u09bc-\u09c4\u09c7\u09c8\u09cb-\u09ce\u09d7\u09dc\u09dd\u09df-\u09e3\u09e6-\u09f1\u0a01-\u0a03\u0a05-\u0a0a\u0a0f\u0a10\u0a13-\u0a28\u0a2a-\u0a30\u0a32\u0a33\u0a35\u0a36\u0a38\u0a39\u0a3c\u0a3e-\u0a42\u0a47\u0a48\u0a4b-\u0a4d\u0a51\u0a59-\u0a5c\u0a5e\u0a66-\u0a75\u0a81-\u0a83\u0a85-\u0a8d\u0a8f-\u0a91\u0a93-\u0aa8\u0aaa-\u0ab0\u0ab2\u0ab3\u0ab5-\u0ab9\u0abc-\u0ac5\u0ac7-\u0ac9\u0acb-\u0acd\u0ad0\u0ae0-\u0ae3\u0ae6-\u0aef\u0b01-\u0b03\u0b05-\u0b0c\u0b0f\u0b10\u0b13-\u0b28\u0b2a-\u0b30\u0b32\u0b33\u0b35-\u0b39\u0b3c-\u0b44\u0b47\u0b48\u0b4b-\u0b4d\u0b56\u0b57\u0b5c\u0b5d\u0b5f-\u0b63\u0b66-\u0b6f\u0b71\u0b82\u0b83\u0b85-\u0b8a\u0b8e-\u0b90\u0b92-\u0b95\u0b99\u0b9a\u0b9c\u0b9e\u0b9f\u0ba3\u0ba4\u0ba8-\u0baa\u0bae-\u0bb9\u0bbe-\u0bc2\u0bc6-\u0bc8\u0bca-\u0bcd\u0bd0\u0bd7\u0be6-\u0bef\u0c01-\u0c03\u0c05-\u0c0c\u0c0e-\u0c10\u0c12-\u0c28\u0c2a-\u0c33\u0c35-\u0c39\u0c3d-\u0c44\u0c46-\u0c48\u0c4a-\u0c4d\u0c55\u0c56\u0c58\u0c59\u0c60-\u0c63\u0c66-\u0c6f\u0c82\u0c83\u0c85-\u0c8c\u0c8e-\u0c90\u0c92-\u0ca8\u0caa-\u0cb3\u0cb5-\u0cb9\u0cbc-\u0cc4\u0cc6-\u0cc8\u0cca-\u0ccd\u0cd5\u0cd6\u0cde\u0ce0-\u0ce3\u0ce6-\u0cef\u0cf1\u0cf2\u0d02\u0d03\u0d05-\u0d0c\u0d0e-\u0d10\u0d12-\u0d3a\u0d3d-\u0d44\u0d46-\u0d48\u0d4a-\u0d4e\u0d57\u0d60-\u0d63\u0d66-\u0d6f\u0d7a-\u0d7f\u0d82\u0d83\u0d85-\u0d96\u0d9a-\u0db1\u0db3-\u0dbb\u0dbd\u0dc0-\u0dc6\u0dca\u0dcf-\u0dd4\u0dd6\u0dd8-\u0ddf\u0df2\u0df3\u0e01-\u0e3a\u0e40-\u0e4e\u0e50-\u0e59\u0e81\u0e82\u0e84\u0e87\u0e88\u0e8a\u0e8d\u0e94-\u0e97\u0e99-\u0e9f\u0ea1-\u0ea3\u0ea5\u0ea7\u0eaa\u0eab\u0ead-\u0eb9\u0ebb-\u0ebd\u0ec0-\u0ec4\u0ec6\u0ec8-\u0ecd\u0ed0-\u0ed9\u0edc-\u0edf\u0f00\u0f18\u0f19\u0f20-\u0f29\u0f35\u0f37\u0f39\u0f3e-\u0f47\u0f49-\u0f6c\u0f71-\u0f84\u0f86-\u0f97\u0f99-\u0fbc\u0fc6\u1000-\u1049\u1050-\u109d\u10a0-\u10c5\u10c7\u10cd\u10d0-\u10fa\u10fc-\u1248\u124a-\u124d\u1250-\u1256\u1258\u125a-\u125d\u1260-\u1288\u128a-\u128d\u1290-\u12b0\u12b2-\u12b5\u12b8-\u12be\u12c0\u12c2-\u12c5\u12c8-\u12d6\u12d8-\u1310\u1312-\u1315\u1318-\u135a\u135d-\u135f\u1380-\u138f\u13a0-\u13f4\u1401-\u166c\u166f-\u167f\u1681-\u169a\u16a0-\u16ea\u16ee-\u16f0\u1700-\u170c\u170e-\u1714\u1720-\u1734\u1740-\u1753\u1760-\u176c\u176e-\u1770\u1772\u1773\u1780-\u17d3\u17d7\u17dc\u17dd\u17e0-\u17e9\u180b-\u180d\u1810-\u1819\u1820-\u1877\u1880-\u18aa\u18b0-\u18f5\u1900-\u191c\u1920-\u192b\u1930-\u193b\u1946-\u196d\u1970-\u1974\u1980-\u19ab\u19b0-\u19c9\u19d0-\u19d9\u1a00-\u1a1b\u1a20-\u1a5e\u1a60-\u1a7c\u1a7f-\u1a89\u1a90-\u1a99\u1aa7\u1b00-\u1b4b\u1b50-\u1b59\u1b6b-\u1b73\u1b80-\u1bf3\u1c00-\u1c37\u1c40-\u1c49\u1c4d-\u1c7d\u1cd0-\u1cd2\u1cd4-\u1cf6\u1d00-\u1de6\u1dfc-\u1f15\u1f18-\u1f1d\u1f20-\u1f45\u1f48-\u1f4d\u1f50-\u1f57\u1f59\u1f5b\u1f5d\u1f5f-\u1f7d\u1f80-\u1fb4\u1fb6-\u1fbc\u1fbe\u1fc2-\u1fc4\u1fc6-\u1fcc\u1fd0-\u1fd3\u1fd6-\u1fdb\u1fe0-\u1fec\u1ff2-\u1ff4\u1ff6-\u1ffc\u200c\u200d\u203f\u2040\u2054\u2071\u207f\u2090-\u209c\u20d0-\u20dc\u20e1\u20e5-\u20f0\u2102\u2107\u210a-\u2113\u2115\u2119-\u211d\u2124\u2126\u2128\u212a-\u212d\u212f-\u2139\u213c-\u213f\u2145-\u2149\u214e\u2160-\u2188\u2c00-\u2c2e\u2c30-\u2c5e\u2c60-\u2ce4\u2ceb-\u2cf3\u2d00-\u2d25\u2d27\u2d2d\u2d30-\u2d67\u2d6f\u2d7f-\u2d96\u2da0-\u2da6\u2da8-\u2dae\u2db0-\u2db6\u2db8-\u2dbe\u2dc0-\u2dc6\u2dc8-\u2dce\u2dd0-\u2dd6\u2dd8-\u2dde\u2de0-\u2dff\u2e2f\u3005-\u3007\u3021-\u302f\u3031-\u3035\u3038-\u303c\u3041-\u3096\u3099\u309a\u309d-\u309f\u30a1-\u30fa\u30fc-\u30ff\u3105-\u312d\u3131-\u318e\u31a0-\u31ba\u31f0-\u31ff\u3400-\u4db5\u4e00-\u9fcc\ua000-\ua48c\ua4d0-\ua4fd\ua500-\ua60c\ua610-\ua62b\ua640-\ua66f\ua674-\ua67d\ua67f-\ua697\ua69f-\ua6f1\ua717-\ua71f\ua722-\ua788\ua78b-\ua78e\ua790-\ua793\ua7a0-\ua7aa\ua7f8-\ua827\ua840-\ua873\ua880-\ua8c4\ua8d0-\ua8d9\ua8e0-\ua8f7\ua8fb\ua900-\ua92d\ua930-\ua953\ua960-\ua97c\ua980-\ua9c0\ua9cf-\ua9d9\uaa00-\uaa36\uaa40-\uaa4d\uaa50-\uaa59\uaa60-\uaa76\uaa7a\uaa7b\uaa80-\uaac2\uaadb-\uaadd\uaae0-\uaaef\uaaf2-\uaaf6\uab01-\uab06\uab09-\uab0e\uab11-\uab16\uab20-\uab26\uab28-\uab2e\uabc0-\uabea\uabec\uabed\uabf0-\uabf9\uac00-\ud7a3\ud7b0-\ud7c6\ud7cb-\ud7fb\uf900-\ufa6d\ufa70-\ufad9\ufb00-\ufb06\ufb13-\ufb17\ufb1d-\ufb28\ufb2a-\ufb36\ufb38-\ufb3c\ufb3e\ufb40\ufb41\ufb43\ufb44\ufb46-\ufbb1\ufbd3-\ufd3d\ufd50-\ufd8f\ufd92-\ufdc7\ufdf0-\ufdfb\ufe00-\ufe0f\ufe20-\ufe26\ufe33\ufe34\ufe4d-\ufe4f\ufe70-\ufe74\ufe76-\ufefc\uff10-\uff19\uff21-\uff3a\uff3f\uff41-\uff5a\uff66-\uffbe\uffc2-\uffc7\uffca-\uffcf\uffd2-\uffd7\uffda-\uffdc]'),LeadingZeros:new RegExp('^0+(?!$)')};
function assert(condition,message){if(!condition){throw new Error('ASSERT: '+message);}}
function StringMap(){this.$data={};}
StringMap.prototype.get=function(key){key='$'+key;return this.$data[key];};StringMap.prototype.set=function(key,value){key='$'+key;this.$data[key]=value;return this;};StringMap.prototype.has=function(key){key='$'+key;return Object.prototype.hasOwnProperty.call(this.$data,key);};StringMap.prototype["delete"]=function(key){key='$'+key;return delete this.$data[key];};function isDecimalDigit(ch){return(ch>=48&&ch<=57); }
function isHexDigit(ch){return'0123456789abcdefABCDEF'.indexOf(ch)>=0;}
function isOctalDigit(ch){return'01234567'.indexOf(ch)>=0;} 
function isWhiteSpace(ch){return(ch===32)|| (ch===9)|| (ch===0xB)||(ch===0xC)||(ch===0xA0)||(ch>=0x1680&&'\u1680\u180E\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u202F\u205F\u3000\uFEFF'.indexOf(String.fromCharCode(ch))>0);} 
function isLineTerminator(ch){return(ch===10)||(ch===13)||(ch===0x2028)||(ch===0x2029);} 
function isIdentifierStart(ch){return(ch===36)||(ch===95)|| (ch>=65&&ch<=90)|| (ch>=97&&ch<=122)|| (ch===92)|| ((ch>=0x80)&&Regex.NonAsciiIdentifierStart.test(String.fromCharCode(ch)));}
function isIdentifierPart(ch){return(ch===36)||(ch===95)|| (ch>=65&&ch<=90)|| (ch>=97&&ch<=122)|| (ch>=48&&ch<=57)|| (ch===92)|| ((ch>=0x80)&&Regex.NonAsciiIdentifierPart.test(String.fromCharCode(ch)));} 
function isFutureReservedWord(id){switch(id){case'class':case'enum':case'export':case'extends':case'import':case'super':return true;default:return false;}}
function isStrictModeReservedWord(id){switch(id){case'implements':case'interface':case'package':case'private':case'protected':case'public':case'static':case'yield':case'let':return true;default:return false;}}
function isRestrictedWord(id){return id==='eval'||id==='arguments';} 
function isKeyword(id){if(strict&&isStrictModeReservedWord(id)){return true;}
switch(id.length){case 2:return(id==='if')||(id==='in')||(id==='do');case 3:return(id==='var')||(id==='for')||(id==='new')||(id==='try')||(id==='let');case 4:return(id==='this')||(id==='else')||(id==='case')||(id==='void')||(id==='with')||(id==='enum');case 5:return(id==='while')||(id==='break')||(id==='catch')||(id==='throw')||(id==='const')||(id==='class')||(id==='super');case 6:return(id==='return')||(id==='typeof')||(id==='delete')||(id==='switch')||(id==='export')||(id==='import');case 7:return(id==='default')||(id==='finally')||(id==='extends');case 8:return(id==='function')||(id==='continue')||(id==='debugger');case 10:return(id==='instanceof');default:return false;}} 
function addComment(type,value,start,end,loc){var comment;assert(typeof start==='number','Comment must have valid position');

if(state.lastCommentStart>=start){return;}
state.lastCommentStart=start;comment={type:type,value:value};if(extra.range){comment.range=[start,end];}
if(extra.loc){comment.loc=loc;}
extra.comments.push(comment);if(extra.attachComment){extra.leadingComments.push(comment);extra.trailingComments.push(comment);}}
function skipSingleLineComment(){var start,loc,ch,comment;start=index-2;loc={start:{line:lineNumber,column:index-lineStart-2}};while(index<length){ch=source.charCodeAt(index);++index;if(isLineTerminator(ch)){if(extra.comments){comment=source.slice(start+2,index-1);loc.end={line:lineNumber,column:index-lineStart-1};addComment('Line',comment,start,index-1,loc);}
if(ch===13&&source.charCodeAt(index)===10){++index;}
++lineNumber;lineStart=index;return;}}
if(extra.comments){comment=source.slice(start+2,index);loc.end={line:lineNumber,column:index-lineStart};addComment('Line',comment,start,index,loc);}}
function skipMultiLineComment(){var start,loc,ch,comment;if(extra.comments){start=index-2;loc={start:{line:lineNumber,column:index-lineStart-2}};}
while(index<length){ch=source.charCodeAt(index);if(isLineTerminator(ch)){if(ch===13&&source.charCodeAt(index+1)===10){++index;}
++lineNumber;++index;lineStart=index;if(index>=length){throwError({},Messages.UnexpectedToken,'ILLEGAL');}}else if(ch===42){if(source.charCodeAt(index+1)===47){++index;++index;if(extra.comments){comment=source.slice(start+2,index-2);loc.end={line:lineNumber,column:index-lineStart};addComment('Block',comment,start,index,loc);}
return;}
++index;}else{++index;}}
throwError({},Messages.UnexpectedToken,'ILLEGAL');}
function skipComment(){var ch;while(index<length){ch=source.charCodeAt(index);if(isWhiteSpace(ch)){++index;}else if(isLineTerminator(ch)){++index;if(ch===13&&source.charCodeAt(index)===10){++index;}
++lineNumber;lineStart=index;}else if(ch===47){ch=source.charCodeAt(index+1);if(ch===47){++index;++index;skipSingleLineComment();}else if(ch===42){++index;++index;skipMultiLineComment();}else{break;}}else{break;}}}
function scanHexEscape(prefix){var i,len,ch,code=0;len=(prefix==='u')?4:2;for(i=0;i<len;++i){if(index<length&&isHexDigit(source[index])){ch=source[index++];code=code*16+'0123456789abcdef'.indexOf(ch.toLowerCase());}else{return'';}}
return String.fromCharCode(code);}
function scanUnicodeCodePointEscape(){var ch,code,cu1,cu2;ch=source[index];code=0;if(ch==='}'){throwError({},Messages.UnexpectedToken,'ILLEGAL');}
while(index<length){ch=source[index++];if(!isHexDigit(ch)){break;}
code=code*16+'0123456789abcdef'.indexOf(ch.toLowerCase());}
if(code>0x10FFFF||ch!=='}'){throwError({},Messages.UnexpectedToken,'ILLEGAL');} 
if(code<=0xFFFF){return String.fromCharCode(code);}
cu1=((code-0x10000)>>10)+0xD800;cu2=((code-0x10000)&1023)+0xDC00;return String.fromCharCode(cu1,cu2);}
function getEscapedIdentifier(){var ch,id;ch=source.charCodeAt(index++);id=String.fromCharCode(ch);if(ch===92){if(source.charCodeAt(index)!==117){throwError({},Messages.UnexpectedToken,'ILLEGAL');}
++index;ch=scanHexEscape('u');if(!ch||ch==='\\'||!isIdentifierStart(ch.charCodeAt(0))){throwError({},Messages.UnexpectedToken,'ILLEGAL');}
id=ch;}
while(index<length){ch=source.charCodeAt(index);if(!isIdentifierPart(ch)){break;}
++index;id+=String.fromCharCode(ch);if(ch===92){id=id.substr(0,id.length-1);if(source.charCodeAt(index)!==117){throwError({},Messages.UnexpectedToken,'ILLEGAL');}
++index;ch=scanHexEscape('u');if(!ch||ch==='\\'||!isIdentifierPart(ch.charCodeAt(0))){throwError({},Messages.UnexpectedToken,'ILLEGAL');}
id+=ch;}}
return id;}
function getIdentifier(){var start,ch;start=index++;while(index<length){ch=source.charCodeAt(index);if(ch===92){index=start;return getEscapedIdentifier();}
if(isIdentifierPart(ch)){++index;}else{break;}}
return source.slice(start,index);}
function scanIdentifier(){var start,id,type;start=index;id=(source.charCodeAt(index)===92)?getEscapedIdentifier():getIdentifier();if(id.length===1){type=Token.Identifier;}else if(isKeyword(id)){type=Token.Keyword;}else if(id==='null'){type=Token.NullLiteral;}else if(id==='true'||id==='false'){type=Token.BooleanLiteral;}else{type=Token.Identifier;}
return{type:type,value:id,lineNumber:lineNumber,lineStart:lineStart,range:[start,index]};} 
function scanPunctuator(){var start=index,code=source.charCodeAt(index),code2,ch1=source[index],ch2,ch3,ch4;if(state.inJSXTag||state.inJSXChild){
switch(code){case 60:case 62:++index;return{type:Token.Punctuator,value:String.fromCharCode(code),lineNumber:lineNumber,lineStart:lineStart,range:[start,index]};}}
switch(code){case 40: case 41: case 59: case 44: case 123: case 125: case 91:case 93:case 58:case 63:case 126:++index;if(extra.tokenize){if(code===40){extra.openParenToken=extra.tokens.length;}else if(code===123){extra.openCurlyToken=extra.tokens.length;}}
return{type:Token.Punctuator,value:String.fromCharCode(code),lineNumber:lineNumber,lineStart:lineStart,range:[start,index]};default:code2=source.charCodeAt(index+1);if(code2===61){switch(code){case 37:case 38:case 42:case 43:case 45:case 47:case 60:case 62:case 94:case 124:index+=2;return{type:Token.Punctuator,value:String.fromCharCode(code)+String.fromCharCode(code2),lineNumber:lineNumber,lineStart:lineStart,range:[start,index]};case 33:case 61:index+=2;if(source.charCodeAt(index)===61){++index;}
return{type:Token.Punctuator,value:source.slice(start,index),lineNumber:lineNumber,lineStart:lineStart,range:[start,index]};default:break;}}
break;}
ch2=source[index+1];ch3=source[index+2];ch4=source[index+3];if(ch1==='>'&&ch2==='>'&&ch3==='>'){if(ch4==='='){index+=4;return{type:Token.Punctuator,value:'>>>=',lineNumber:lineNumber,lineStart:lineStart,range:[start,index]};}}
if(ch1==='>'&&ch2==='>'&&ch3==='>'&&!state.inType){index+=3;return{type:Token.Punctuator,value:'>>>',lineNumber:lineNumber,lineStart:lineStart,range:[start,index]};}
if(ch1==='<'&&ch2==='<'&&ch3==='='){index+=3;return{type:Token.Punctuator,value:'<<=',lineNumber:lineNumber,lineStart:lineStart,range:[start,index]};}
if(ch1==='>'&&ch2==='>'&&ch3==='='){index+=3;return{type:Token.Punctuator,value:'>>=',lineNumber:lineNumber,lineStart:lineStart,range:[start,index]};}
if(ch1==='.'&&ch2==='.'&&ch3==='.'){index+=3;return{type:Token.Punctuator,value:'...',lineNumber:lineNumber,lineStart:lineStart,range:[start,index]};}

if(ch1===ch2&&('+-<>&|'.indexOf(ch1)>=0)&&!state.inType){index+=2;return{type:Token.Punctuator,value:ch1+ch2,lineNumber:lineNumber,lineStart:lineStart,range:[start,index]};}
if(ch1==='='&&ch2==='>'){index+=2;return{type:Token.Punctuator,value:'=>',lineNumber:lineNumber,lineStart:lineStart,range:[start,index]};}
if('<>=!+-*%&|^/'.indexOf(ch1)>=0){++index;return{type:Token.Punctuator,value:ch1,lineNumber:lineNumber,lineStart:lineStart,range:[start,index]};}
if(ch1==='.'){++index;return{type:Token.Punctuator,value:ch1,lineNumber:lineNumber,lineStart:lineStart,range:[start,index]};}
throwError({},Messages.UnexpectedToken,'ILLEGAL');} 
function scanHexLiteral(start){var number='';while(index<length){if(!isHexDigit(source[index])){break;}
number+=source[index++];}
if(number.length===0){throwError({},Messages.UnexpectedToken,'ILLEGAL');}
if(isIdentifierStart(source.charCodeAt(index))){throwError({},Messages.UnexpectedToken,'ILLEGAL');}
return{type:Token.NumericLiteral,value:parseInt('0x'+number,16),lineNumber:lineNumber,lineStart:lineStart,range:[start,index]};}
function scanBinaryLiteral(start){var ch,number;number='';while(index<length){ch=source[index];if(ch!=='0'&&ch!=='1'){break;}
number+=source[index++];}
if(number.length===0){ throwError({},Messages.UnexpectedToken,'ILLEGAL');}
if(index<length){ch=source.charCodeAt(index);if(isIdentifierStart(ch)||isDecimalDigit(ch)){throwError({},Messages.UnexpectedToken,'ILLEGAL');}}
return{type:Token.NumericLiteral,value:parseInt(number,2),lineNumber:lineNumber,lineStart:lineStart,range:[start,index]};}
function scanOctalLiteral(prefix,start){var number,octal;if(isOctalDigit(prefix)){octal=true;number='0'+source[index++];}else{octal=false;++index;number='';}
while(index<length){if(!isOctalDigit(source[index])){break;}
number+=source[index++];}
if(!octal&&number.length===0){ throwError({},Messages.UnexpectedToken,'ILLEGAL');}
if(isIdentifierStart(source.charCodeAt(index))||isDecimalDigit(source.charCodeAt(index))){throwError({},Messages.UnexpectedToken,'ILLEGAL');}
return{type:Token.NumericLiteral,value:parseInt(number,8),octal:octal,lineNumber:lineNumber,lineStart:lineStart,range:[start,index]};}
function scanNumericLiteral(){var number,start,ch;ch=source[index];assert(isDecimalDigit(ch.charCodeAt(0))||(ch==='.'),'Numeric literal must start with a decimal digit or a decimal point');start=index;number='';if(ch!=='.'){number=source[index++];ch=source[index];if(number==='0'){if(ch==='x'||ch==='X'){++index;return scanHexLiteral(start);}
if(ch==='b'||ch==='B'){++index;return scanBinaryLiteral(start);}
if(ch==='o'||ch==='O'||isOctalDigit(ch)){return scanOctalLiteral(ch,start);}
if(ch&&isDecimalDigit(ch.charCodeAt(0))){throwError({},Messages.UnexpectedToken,'ILLEGAL');}}
while(isDecimalDigit(source.charCodeAt(index))){number+=source[index++];}
ch=source[index];}
if(ch==='.'){number+=source[index++];while(isDecimalDigit(source.charCodeAt(index))){number+=source[index++];}
ch=source[index];}
if(ch==='e'||ch==='E'){number+=source[index++];ch=source[index];if(ch==='+'||ch==='-'){number+=source[index++];}
if(isDecimalDigit(source.charCodeAt(index))){while(isDecimalDigit(source.charCodeAt(index))){number+=source[index++];}}else{throwError({},Messages.UnexpectedToken,'ILLEGAL');}}
if(isIdentifierStart(source.charCodeAt(index))){throwError({},Messages.UnexpectedToken,'ILLEGAL');}
return{type:Token.NumericLiteral,value:parseFloat(number),lineNumber:lineNumber,lineStart:lineStart,range:[start,index]};} 
function scanStringLiteral(){var str='',quote,start,ch,code,unescaped,restore,octal=false;quote=source[index];assert((quote==='\''||quote==='"'),'String literal must starts with a quote');start=index;++index;while(index<length){ch=source[index++];if(ch===quote){quote='';break;}else if(ch==='\\'){ch=source[index++];if(!ch||!isLineTerminator(ch.charCodeAt(0))){switch(ch){case'n':str+='\n';break;case'r':str+='\r';break;case't':str+='\t';break;case'u':case'x':if(source[index]==='{'){++index;str+=scanUnicodeCodePointEscape();}else{restore=index;unescaped=scanHexEscape(ch);if(unescaped){str+=unescaped;}else{index=restore;str+=ch;}}
break;case'b':str+='\b';break;case'f':str+='\f';break;case'v':str+='\x0B';break;default:if(isOctalDigit(ch)){code='01234567'.indexOf(ch); if(code!==0){octal=true;}
if(index<length&&isOctalDigit(source[index])){octal=true;code=code*8+'01234567'.indexOf(source[index++]);
 if('0123'.indexOf(ch)>=0&&index<length&&isOctalDigit(source[index])){code=code*8+'01234567'.indexOf(source[index++]);}}
str+=String.fromCharCode(code);}else{str+=ch;}
break;}}else{++lineNumber;if(ch==='\r'&&source[index]==='\n'){++index;}
lineStart=index;}}else if(isLineTerminator(ch.charCodeAt(0))){break;}else{str+=ch;}}
if(quote!==''){throwError({},Messages.UnexpectedToken,'ILLEGAL');}
return{type:Token.StringLiteral,value:str,octal:octal,lineNumber:lineNumber,lineStart:lineStart,range:[start,index]};}
function scanTemplate(){var cooked='',ch,start,terminated,tail,restore,unescaped,code,octal;terminated=false;tail=false;start=index;++index;while(index<length){ch=source[index++];if(ch==='`'){tail=true;terminated=true;break;}else if(ch==='$'){if(source[index]==='{'){++index;terminated=true;break;}
cooked+=ch;}else if(ch==='\\'){ch=source[index++];if(!isLineTerminator(ch.charCodeAt(0))){switch(ch){case'n':cooked+='\n';break;case'r':cooked+='\r';break;case't':cooked+='\t';break;case'u':case'x':if(source[index]==='{'){++index;cooked+=scanUnicodeCodePointEscape();}else{restore=index;unescaped=scanHexEscape(ch);if(unescaped){cooked+=unescaped;}else{index=restore;cooked+=ch;}}
break;case'b':cooked+='\b';break;case'f':cooked+='\f';break;case'v':cooked+='\v';break;default:if(isOctalDigit(ch)){code='01234567'.indexOf(ch); if(code!==0){octal=true;}
if(index<length&&isOctalDigit(source[index])){octal=true;code=code*8+'01234567'.indexOf(source[index++]);
 if('0123'.indexOf(ch)>=0&&index<length&&isOctalDigit(source[index])){code=code*8+'01234567'.indexOf(source[index++]);}}
cooked+=String.fromCharCode(code);}else{cooked+=ch;}
break;}}else{++lineNumber;if(ch==='\r'&&source[index]==='\n'){++index;}
lineStart=index;}}else if(isLineTerminator(ch.charCodeAt(0))){++lineNumber;if(ch==='\r'&&source[index]==='\n'){++index;}
lineStart=index;cooked+='\n';}else{cooked+=ch;}}
if(!terminated){throwError({},Messages.UnexpectedToken,'ILLEGAL');}
return{type:Token.Template,value:{cooked:cooked,raw:source.slice(start+1,index-((tail)?1:2))},tail:tail,octal:octal,lineNumber:lineNumber,lineStart:lineStart,range:[start,index]};}
function scanTemplateElement(option){var startsWith,template;lookahead=null;skipComment();startsWith=(option.head)?'`':'}';if(source[index]!==startsWith){throwError({},Messages.UnexpectedToken,'ILLEGAL');}
template=scanTemplate();peek();return template;}
function testRegExp(pattern,flags){var tmp=pattern,value;if(flags.indexOf('u')>=0){





tmp=tmp.replace(/\\u\{([0-9a-fA-F]+)\}/g,function($0,$1){if(parseInt($1,16)<=0x10FFFF){return'x';}
throwError({},Messages.InvalidRegExp);}).replace(/[\uD800-\uDBFF][\uDC00-\uDFFF]/g,'x');}
try{value=new RegExp(tmp);}catch(e){throwError({},Messages.InvalidRegExp);}


try{return new RegExp(pattern,flags);}catch(exception){return null;}}
function scanRegExpBody(){var ch,str,classMarker,terminated,body;ch=source[index];assert(ch==='/','Regular expression literal must start with a slash');str=source[index++];classMarker=false;terminated=false;while(index<length){ch=source[index++];str+=ch;if(ch==='\\'){ch=source[index++]; if(isLineTerminator(ch.charCodeAt(0))){throwError({},Messages.UnterminatedRegExp);}
str+=ch;}else if(isLineTerminator(ch.charCodeAt(0))){throwError({},Messages.UnterminatedRegExp);}else if(classMarker){if(ch===']'){classMarker=false;}}else{if(ch==='/'){terminated=true;break;}else if(ch==='['){classMarker=true;}}}
if(!terminated){throwError({},Messages.UnterminatedRegExp);}
body=str.substr(1,str.length-2);return{value:body,literal:str};}
function scanRegExpFlags(){var ch,str,flags,restore;str='';flags='';while(index<length){ch=source[index];if(!isIdentifierPart(ch.charCodeAt(0))){break;}
++index;if(ch==='\\'&&index<length){ch=source[index];if(ch==='u'){++index;restore=index;ch=scanHexEscape('u');if(ch){flags+=ch;for(str+='\\u';restore<index;++restore){str+=source[restore];}}else{index=restore;flags+='u';str+='\\u';}
throwErrorTolerant({},Messages.UnexpectedToken,'ILLEGAL');}else{str+='\\';throwErrorTolerant({},Messages.UnexpectedToken,'ILLEGAL');}}else{flags+=ch;str+=ch;}}
return{value:flags,literal:str};}
function scanRegExp(){var start,body,flags,value;lookahead=null;skipComment();start=index;body=scanRegExpBody();flags=scanRegExpFlags();value=testRegExp(body.value,flags.value);if(extra.tokenize){return{type:Token.RegularExpression,value:value,regex:{pattern:body.value,flags:flags.value},lineNumber:lineNumber,lineStart:lineStart,range:[start,index]};}
return{literal:body.literal+flags.literal,value:value,regex:{pattern:body.value,flags:flags.value},range:[start,index]};}
function isIdentifierName(token){return token.type===Token.Identifier||token.type===Token.Keyword||token.type===Token.BooleanLiteral||token.type===Token.NullLiteral;}
function advanceSlash(){var prevToken,checkToken; prevToken=extra.tokens[extra.tokens.length-1];if(!prevToken){return scanRegExp();}
if(prevToken.type==='Punctuator'){if(prevToken.value===')'){checkToken=extra.tokens[extra.openParenToken-1];if(checkToken&&checkToken.type==='Keyword'&&(checkToken.value==='if'||checkToken.value==='while'||checkToken.value==='for'||checkToken.value==='with')){return scanRegExp();}
return scanPunctuator();}
if(prevToken.value==='}'){if(extra.tokens[extra.openCurlyToken-3]&&extra.tokens[extra.openCurlyToken-3].type==='Keyword'){checkToken=extra.tokens[extra.openCurlyToken-4];if(!checkToken){return scanPunctuator();}}else if(extra.tokens[extra.openCurlyToken-4]&&extra.tokens[extra.openCurlyToken-4].type==='Keyword'){checkToken=extra.tokens[extra.openCurlyToken-5];if(!checkToken){return scanRegExp();}}else{return scanPunctuator();}

if(FnExprTokens.indexOf(checkToken.value)>=0){return scanPunctuator();}
return scanRegExp();}
return scanRegExp();}
if(prevToken.type==='Keyword'&&prevToken.value!=='this'){return scanRegExp();}
return scanPunctuator();}
function advance(){var ch;if(!state.inJSXChild){skipComment();}
if(index>=length){return{type:Token.EOF,lineNumber:lineNumber,lineStart:lineStart,range:[index,index]};}
if(state.inJSXChild){return advanceJSXChild();}
ch=source.charCodeAt(index);if(ch===40||ch===41||ch===58){return scanPunctuator();}
if(ch===39||ch===34){if(state.inJSXTag){return scanJSXStringLiteral();}
return scanStringLiteral();}
if(state.inJSXTag&&isJSXIdentifierStart(ch)){return scanJSXIdentifier();}
if(ch===96){return scanTemplate();}
if(isIdentifierStart(ch)){return scanIdentifier();}

if(ch===46){if(isDecimalDigit(source.charCodeAt(index+1))){return scanNumericLiteral();}
return scanPunctuator();}
if(isDecimalDigit(ch)){return scanNumericLiteral();}
if(extra.tokenize&&ch===47){return advanceSlash();}
return scanPunctuator();}
function lex(){var token;token=lookahead;index=token.range[1];lineNumber=token.lineNumber;lineStart=token.lineStart;lookahead=advance();index=token.range[1];lineNumber=token.lineNumber;lineStart=token.lineStart;return token;}
function peek(){var pos,line,start;pos=index;line=lineNumber;start=lineStart;lookahead=advance();index=pos;lineNumber=line;lineStart=start;}
function lookahead2(){var adv,pos,line,start,result;adv=(typeof extra.advance==='function')?extra.advance:advance;pos=index;line=lineNumber;start=lineStart;if(lookahead===null){lookahead=adv();}
index=lookahead.range[1];lineNumber=lookahead.lineNumber;lineStart=lookahead.lineStart;result=adv();index=pos;lineNumber=line;lineStart=start;return result;}
function rewind(token){index=token.range[0];lineNumber=token.lineNumber;lineStart=token.lineStart;lookahead=token;}
function markerCreate(){if(!extra.loc&&!extra.range){return undefined;}
skipComment();return{offset:index,line:lineNumber,col:index-lineStart};}
function markerCreatePreserveWhitespace(){if(!extra.loc&&!extra.range){return undefined;}
return{offset:index,line:lineNumber,col:index-lineStart};}
function processComment(node){var lastChild,trailingComments,bottomRight=extra.bottomRightStack,last=bottomRight[bottomRight.length-1];if(node.type===Syntax.Program){if(node.body.length>0){return;}}
if(extra.trailingComments.length>0){if(extra.trailingComments[0].range[0]>=node.range[1]){trailingComments=extra.trailingComments;extra.trailingComments=[];}else{extra.trailingComments.length=0;}}else{if(last&&last.trailingComments&&last.trailingComments[0].range[0]>=node.range[1]){trailingComments=last.trailingComments;delete last.trailingComments;}}
if(last){while(last&&last.range[0]>=node.range[0]){lastChild=last;last=bottomRight.pop();}}
if(lastChild){if(lastChild.leadingComments&&lastChild.leadingComments[lastChild.leadingComments.length-1].range[1]<=node.range[0]){node.leadingComments=lastChild.leadingComments;delete lastChild.leadingComments;}}else if(extra.leadingComments.length>0&&extra.leadingComments[extra.leadingComments.length-1].range[1]<=node.range[0]){node.leadingComments=extra.leadingComments;extra.leadingComments=[];}
if(trailingComments){node.trailingComments=trailingComments;}
bottomRight.push(node);}
function markerApply(marker,node){if(extra.range){node.range=[marker.offset,index];}
if(extra.loc){node.loc={start:{line:marker.line,column:marker.col},end:{line:lineNumber,column:index-lineStart}};node=delegate.postProcess(node);}
if(extra.attachComment){processComment(node);}
return node;}
SyntaxTreeDelegate={name:'SyntaxTree',postProcess:function(node){return node;},createArrayExpression:function(elements){return{type:Syntax.ArrayExpression,elements:elements};},createAssignmentExpression:function(operator,left,right){return{type:Syntax.AssignmentExpression,operator:operator,left:left,right:right};},createBinaryExpression:function(operator,left,right){var type=(operator==='||'||operator==='&&')?Syntax.LogicalExpression:Syntax.BinaryExpression;return{type:type,operator:operator,left:left,right:right};},createBlockStatement:function(body){return{type:Syntax.BlockStatement,body:body};},createBreakStatement:function(label){return{type:Syntax.BreakStatement,label:label};},createCallExpression:function(callee,args){return{type:Syntax.CallExpression,callee:callee,'arguments':args};},createCatchClause:function(param,body){return{type:Syntax.CatchClause,param:param,body:body};},createConditionalExpression:function(test,consequent,alternate){return{type:Syntax.ConditionalExpression,test:test,consequent:consequent,alternate:alternate};},createContinueStatement:function(label){return{type:Syntax.ContinueStatement,label:label};},createDebuggerStatement:function(){return{type:Syntax.DebuggerStatement};},createDoWhileStatement:function(body,test){return{type:Syntax.DoWhileStatement,body:body,test:test};},createEmptyStatement:function(){return{type:Syntax.EmptyStatement};},createExpressionStatement:function(expression){return{type:Syntax.ExpressionStatement,expression:expression};},createForStatement:function(init,test,update,body){return{type:Syntax.ForStatement,init:init,test:test,update:update,body:body};},createForInStatement:function(left,right,body){return{type:Syntax.ForInStatement,left:left,right:right,body:body,each:false};},createForOfStatement:function(left,right,body){return{type:Syntax.ForOfStatement,left:left,right:right,body:body};},createFunctionDeclaration:function(id,params,defaults,body,rest,generator,expression,isAsync,returnType,typeParameters){var funDecl={type:Syntax.FunctionDeclaration,id:id,params:params,defaults:defaults,body:body,rest:rest,generator:generator,expression:expression,returnType:returnType,typeParameters:typeParameters};if(isAsync){funDecl.async=true;}
return funDecl;},createFunctionExpression:function(id,params,defaults,body,rest,generator,expression,isAsync,returnType,typeParameters){var funExpr={type:Syntax.FunctionExpression,id:id,params:params,defaults:defaults,body:body,rest:rest,generator:generator,expression:expression,returnType:returnType,typeParameters:typeParameters};if(isAsync){funExpr.async=true;}
return funExpr;},createIdentifier:function(name){return{type:Syntax.Identifier,name:name,



typeAnnotation:undefined,optional:undefined};},createTypeAnnotation:function(typeAnnotation){return{type:Syntax.TypeAnnotation,typeAnnotation:typeAnnotation};},createTypeCast:function(expression,typeAnnotation){return{type:Syntax.TypeCastExpression,expression:expression,typeAnnotation:typeAnnotation};},createFunctionTypeAnnotation:function(params,returnType,rest,typeParameters){return{type:Syntax.FunctionTypeAnnotation,params:params,returnType:returnType,rest:rest,typeParameters:typeParameters};},createFunctionTypeParam:function(name,typeAnnotation,optional){return{type:Syntax.FunctionTypeParam,name:name,typeAnnotation:typeAnnotation,optional:optional};},createNullableTypeAnnotation:function(typeAnnotation){return{type:Syntax.NullableTypeAnnotation,typeAnnotation:typeAnnotation};},createArrayTypeAnnotation:function(elementType){return{type:Syntax.ArrayTypeAnnotation,elementType:elementType};},createGenericTypeAnnotation:function(id,typeParameters){return{type:Syntax.GenericTypeAnnotation,id:id,typeParameters:typeParameters};},createQualifiedTypeIdentifier:function(qualification,id){return{type:Syntax.QualifiedTypeIdentifier,qualification:qualification,id:id};},createTypeParameterDeclaration:function(params){return{type:Syntax.TypeParameterDeclaration,params:params};},createTypeParameterInstantiation:function(params){return{type:Syntax.TypeParameterInstantiation,params:params};},createAnyTypeAnnotation:function(){return{type:Syntax.AnyTypeAnnotation};},createBooleanTypeAnnotation:function(){return{type:Syntax.BooleanTypeAnnotation};},createNumberTypeAnnotation:function(){return{type:Syntax.NumberTypeAnnotation};},createStringTypeAnnotation:function(){return{type:Syntax.StringTypeAnnotation};},createStringLiteralTypeAnnotation:function(token){return{type:Syntax.StringLiteralTypeAnnotation,value:token.value,raw:source.slice(token.range[0],token.range[1])};},createVoidTypeAnnotation:function(){return{type:Syntax.VoidTypeAnnotation};},createTypeofTypeAnnotation:function(argument){return{type:Syntax.TypeofTypeAnnotation,argument:argument};},createTupleTypeAnnotation:function(types){return{type:Syntax.TupleTypeAnnotation,types:types};},createObjectTypeAnnotation:function(properties,indexers,callProperties){return{type:Syntax.ObjectTypeAnnotation,properties:properties,indexers:indexers,callProperties:callProperties};},createObjectTypeIndexer:function(id,key,value,isStatic){return{type:Syntax.ObjectTypeIndexer,id:id,key:key,value:value,"static":isStatic};},createObjectTypeCallProperty:function(value,isStatic){return{type:Syntax.ObjectTypeCallProperty,value:value,"static":isStatic};},createObjectTypeProperty:function(key,value,optional,isStatic){return{type:Syntax.ObjectTypeProperty,key:key,value:value,optional:optional,"static":isStatic};},createUnionTypeAnnotation:function(types){return{type:Syntax.UnionTypeAnnotation,types:types};},createIntersectionTypeAnnotation:function(types){return{type:Syntax.IntersectionTypeAnnotation,types:types};},createTypeAlias:function(id,typeParameters,right){return{type:Syntax.TypeAlias,id:id,typeParameters:typeParameters,right:right};},createInterface:function(id,typeParameters,body,extended){return{type:Syntax.InterfaceDeclaration,id:id,typeParameters:typeParameters,body:body,"extends":extended};},createInterfaceExtends:function(id,typeParameters){return{type:Syntax.InterfaceExtends,id:id,typeParameters:typeParameters};},createDeclareFunction:function(id){return{type:Syntax.DeclareFunction,id:id};},createDeclareVariable:function(id){return{type:Syntax.DeclareVariable,id:id};},createDeclareModule:function(id,body){return{type:Syntax.DeclareModule,id:id,body:body};},createJSXAttribute:function(name,value){return{type:Syntax.JSXAttribute,name:name,value:value||null};},createJSXSpreadAttribute:function(argument){return{type:Syntax.JSXSpreadAttribute,argument:argument};},createJSXIdentifier:function(name){return{type:Syntax.JSXIdentifier,name:name};},createJSXNamespacedName:function(namespace,name){return{type:Syntax.JSXNamespacedName,namespace:namespace,name:name};},createJSXMemberExpression:function(object,property){return{type:Syntax.JSXMemberExpression,object:object,property:property};},createJSXElement:function(openingElement,closingElement,children){return{type:Syntax.JSXElement,openingElement:openingElement,closingElement:closingElement,children:children};},createJSXEmptyExpression:function(){return{type:Syntax.JSXEmptyExpression};},createJSXExpressionContainer:function(expression){return{type:Syntax.JSXExpressionContainer,expression:expression};},createJSXOpeningElement:function(name,attributes,selfClosing){return{type:Syntax.JSXOpeningElement,name:name,selfClosing:selfClosing,attributes:attributes};},createJSXClosingElement:function(name){return{type:Syntax.JSXClosingElement,name:name};},createIfStatement:function(test,consequent,alternate){return{type:Syntax.IfStatement,test:test,consequent:consequent,alternate:alternate};},createLabeledStatement:function(label,body){return{type:Syntax.LabeledStatement,label:label,body:body};},createLiteral:function(token){var object={type:Syntax.Literal,value:token.value,raw:source.slice(token.range[0],token.range[1])};if(token.regex){object.regex=token.regex;}
return object;},createMemberExpression:function(accessor,object,property){return{type:Syntax.MemberExpression,computed:accessor==='[',object:object,property:property};},createNewExpression:function(callee,args){return{type:Syntax.NewExpression,callee:callee,'arguments':args};},createObjectExpression:function(properties){return{type:Syntax.ObjectExpression,properties:properties};},createPostfixExpression:function(operator,argument){return{type:Syntax.UpdateExpression,operator:operator,argument:argument,prefix:false};},createProgram:function(body){return{type:Syntax.Program,body:body};},createProperty:function(kind,key,value,method,shorthand,computed){return{type:Syntax.Property,key:key,value:value,kind:kind,method:method,shorthand:shorthand,computed:computed};},createReturnStatement:function(argument){return{type:Syntax.ReturnStatement,argument:argument};},createSequenceExpression:function(expressions){return{type:Syntax.SequenceExpression,expressions:expressions};},createSwitchCase:function(test,consequent){return{type:Syntax.SwitchCase,test:test,consequent:consequent};},createSwitchStatement:function(discriminant,cases){return{type:Syntax.SwitchStatement,discriminant:discriminant,cases:cases};},createThisExpression:function(){return{type:Syntax.ThisExpression};},createThrowStatement:function(argument){return{type:Syntax.ThrowStatement,argument:argument};},createTryStatement:function(block,guardedHandlers,handlers,finalizer){return{type:Syntax.TryStatement,block:block,guardedHandlers:guardedHandlers,handlers:handlers,finalizer:finalizer};},createUnaryExpression:function(operator,argument){if(operator==='++'||operator==='--'){return{type:Syntax.UpdateExpression,operator:operator,argument:argument,prefix:true};}
return{type:Syntax.UnaryExpression,operator:operator,argument:argument,prefix:true};},createVariableDeclaration:function(declarations,kind){return{type:Syntax.VariableDeclaration,declarations:declarations,kind:kind};},createVariableDeclarator:function(id,init){return{type:Syntax.VariableDeclarator,id:id,init:init};},createWhileStatement:function(test,body){return{type:Syntax.WhileStatement,test:test,body:body};},createWithStatement:function(object,body){return{type:Syntax.WithStatement,object:object,body:body};},createTemplateElement:function(value,tail){return{type:Syntax.TemplateElement,value:value,tail:tail};},createTemplateLiteral:function(quasis,expressions){return{type:Syntax.TemplateLiteral,quasis:quasis,expressions:expressions};},createSpreadElement:function(argument){return{type:Syntax.SpreadElement,argument:argument};},createSpreadProperty:function(argument){return{type:Syntax.SpreadProperty,argument:argument};},createTaggedTemplateExpression:function(tag,quasi){return{type:Syntax.TaggedTemplateExpression,tag:tag,quasi:quasi};},createArrowFunctionExpression:function(params,defaults,body,rest,expression,isAsync){var arrowExpr={type:Syntax.ArrowFunctionExpression,id:null,params:params,defaults:defaults,body:body,rest:rest,generator:false,expression:expression};if(isAsync){arrowExpr.async=true;}
return arrowExpr;},createMethodDefinition:function(propertyType,kind,key,value,computed){return{type:Syntax.MethodDefinition,key:key,value:value,kind:kind,'static':propertyType===ClassPropertyType["static"],computed:computed};},createClassProperty:function(key,typeAnnotation,computed,isStatic){return{type:Syntax.ClassProperty,key:key,typeAnnotation:typeAnnotation,computed:computed,"static":isStatic};},createClassBody:function(body){return{type:Syntax.ClassBody,body:body};},createClassImplements:function(id,typeParameters){return{type:Syntax.ClassImplements,id:id,typeParameters:typeParameters};},createClassExpression:function(id,superClass,body,typeParameters,superTypeParameters,implemented){return{type:Syntax.ClassExpression,id:id,superClass:superClass,body:body,typeParameters:typeParameters,superTypeParameters:superTypeParameters,"implements":implemented};},createClassDeclaration:function(id,superClass,body,typeParameters,superTypeParameters,implemented){return{type:Syntax.ClassDeclaration,id:id,superClass:superClass,body:body,typeParameters:typeParameters,superTypeParameters:superTypeParameters,"implements":implemented};},createModuleSpecifier:function(token){return{type:Syntax.ModuleSpecifier,value:token.value,raw:source.slice(token.range[0],token.range[1])};},createExportSpecifier:function(id,name){return{type:Syntax.ExportSpecifier,id:id,name:name};},createExportBatchSpecifier:function(){return{type:Syntax.ExportBatchSpecifier};},createImportDefaultSpecifier:function(id){return{type:Syntax.ImportDefaultSpecifier,id:id};},createImportNamespaceSpecifier:function(id){return{type:Syntax.ImportNamespaceSpecifier,id:id};},createExportDeclaration:function(isDefault,declaration,specifiers,src){return{type:Syntax.ExportDeclaration,'default':!!isDefault,declaration:declaration,specifiers:specifiers,source:src};},createImportSpecifier:function(id,name){return{type:Syntax.ImportSpecifier,id:id,name:name};},createImportDeclaration:function(specifiers,src,isType){return{type:Syntax.ImportDeclaration,specifiers:specifiers,source:src,isType:isType};},createYieldExpression:function(argument,dlg){return{type:Syntax.YieldExpression,argument:argument,delegate:dlg};},createAwaitExpression:function(argument){return{type:Syntax.AwaitExpression,argument:argument};},createComprehensionExpression:function(filter,blocks,body){return{type:Syntax.ComprehensionExpression,filter:filter,blocks:blocks,body:body};}};function peekLineTerminator(){var pos,line,start,found;pos=index;line=lineNumber;start=lineStart;skipComment();found=lineNumber!==line;index=pos;lineNumber=line;lineStart=start;return found;} 
function throwError(token,messageFormat){var error,args=Array.prototype.slice.call(arguments,2),msg=messageFormat.replace(/%(\d)/g,function(whole,idx){assert(idx<args.length,'Message reference must be in range');return args[idx];});if(typeof token.lineNumber==='number'){error=new Error('Line '+token.lineNumber+': '+msg);error.index=token.range[0];error.lineNumber=token.lineNumber;error.column=token.range[0]-lineStart+1;}else{error=new Error('Line '+lineNumber+': '+msg);error.index=index;error.lineNumber=lineNumber;error.column=index-lineStart+1;}
error.description=msg;throw error;}
function throwErrorTolerant(){try{throwError.apply(null,arguments);}catch(e){if(extra.errors){extra.errors.push(e);}else{throw e;}}}
function throwUnexpected(token){if(token.type===Token.EOF){throwError(token,Messages.UnexpectedEOS);}
if(token.type===Token.NumericLiteral){throwError(token,Messages.UnexpectedNumber);}
if(token.type===Token.StringLiteral||token.type===Token.JSXText){throwError(token,Messages.UnexpectedString);}
if(token.type===Token.Identifier){throwError(token,Messages.UnexpectedIdentifier);}
if(token.type===Token.Keyword){if(isFutureReservedWord(token.value)){throwError(token,Messages.UnexpectedReserved);}else if(strict&&isStrictModeReservedWord(token.value)){throwErrorTolerant(token,Messages.StrictReservedWord);return;}
throwError(token,Messages.UnexpectedToken,token.value);}
if(token.type===Token.Template){throwError(token,Messages.UnexpectedTemplate,token.value.raw);}
throwError(token,Messages.UnexpectedToken,token.value);}
function expect(value){var token=lex();if(token.type!==Token.Punctuator||token.value!==value){throwUnexpected(token);}}
function expectKeyword(keyword,contextual){var token=lex();if(token.type!==(contextual?Token.Identifier:Token.Keyword)||token.value!==keyword){throwUnexpected(token);}}
function expectContextualKeyword(keyword){return expectKeyword(keyword,true);}
function match(value){return lookahead.type===Token.Punctuator&&lookahead.value===value;} 
function matchKeyword(keyword,contextual){var expectedType=contextual?Token.Identifier:Token.Keyword;return lookahead.type===expectedType&&lookahead.value===keyword;} 
function matchContextualKeyword(keyword){return matchKeyword(keyword,true);} 
function matchAssign(){var op;if(lookahead.type!==Token.Punctuator){return false;}
op=lookahead.value;return op==='='||op==='*='||op==='/='||op==='%='||op==='+='||op==='-='||op==='<<='||op==='>>='||op==='>>>='||op==='&='||op==='^='||op==='|=';} 
 
 
function matchYield(){return state.yieldAllowed&&matchKeyword('yield',!strict);}
function matchAsync(){var backtrackToken=lookahead,matches=false;if(matchContextualKeyword('async')){lex();matches=!peekLineTerminator();rewind(backtrackToken);}
return matches;}
function matchAwait(){return state.awaitAllowed&&matchContextualKeyword('await');}
function consumeSemicolon(){var line,oldIndex=index,oldLineNumber=lineNumber,oldLineStart=lineStart,oldLookahead=lookahead;if(source.charCodeAt(index)===59){lex();return;}
line=lineNumber;skipComment();if(lineNumber!==line){index=oldIndex;lineNumber=oldLineNumber;lineStart=oldLineStart;lookahead=oldLookahead;return;}
if(match(';')){lex();return;}
if(lookahead.type!==Token.EOF&&!match('}')){throwUnexpected(lookahead);}} 
function isLeftHandSide(expr){return expr.type===Syntax.Identifier||expr.type===Syntax.MemberExpression;}
function isAssignableLeftHandSide(expr){return isLeftHandSide(expr)||expr.type===Syntax.ObjectPattern||expr.type===Syntax.ArrayPattern;} 
function parseArrayInitialiser(){var elements=[],blocks=[],filter=null,tmp,possiblecomprehension=true,marker=markerCreate();expect('[');while(!match(']')){if(lookahead.value==='for'&&lookahead.type===Token.Keyword){if(!possiblecomprehension){throwError({},Messages.ComprehensionError);}
matchKeyword('for');tmp=parseForStatement({ignoreBody:true});tmp.of=tmp.type===Syntax.ForOfStatement;tmp.type=Syntax.ComprehensionBlock;if(tmp.left.kind){ throwError({},Messages.ComprehensionError);}
blocks.push(tmp);}else if(lookahead.value==='if'&&lookahead.type===Token.Keyword){if(!possiblecomprehension){throwError({},Messages.ComprehensionError);}
expectKeyword('if');expect('(');filter=parseExpression();expect(')');}else if(lookahead.value===','&&lookahead.type===Token.Punctuator){possiblecomprehension=false;lex();elements.push(null);}else{tmp=parseSpreadOrAssignmentExpression();elements.push(tmp);if(tmp&&tmp.type===Syntax.SpreadElement){if(!match(']')){throwError({},Messages.ElementAfterSpreadElement);}}else if(!(match(']')||matchKeyword('for')||matchKeyword('if'))){expect(',');possiblecomprehension=false;}}}
expect(']');if(filter&&!blocks.length){throwError({},Messages.ComprehensionRequiresBlock);}
if(blocks.length){if(elements.length!==1){throwError({},Messages.ComprehensionError);}
return markerApply(marker,delegate.createComprehensionExpression(filter,blocks,elements[0]));}
return markerApply(marker,delegate.createArrayExpression(elements));} 
function parsePropertyFunction(options){var previousStrict,previousYieldAllowed,previousAwaitAllowed,params,defaults,body,marker=markerCreate();previousStrict=strict;previousYieldAllowed=state.yieldAllowed;state.yieldAllowed=options.generator;previousAwaitAllowed=state.awaitAllowed;state.awaitAllowed=options.async;params=options.params||[];defaults=options.defaults||[];body=parseConciseBody();if(options.name&&strict&&isRestrictedWord(params[0].name)){throwErrorTolerant(options.name,Messages.StrictParamName);}
strict=previousStrict;state.yieldAllowed=previousYieldAllowed;state.awaitAllowed=previousAwaitAllowed;return markerApply(marker,delegate.createFunctionExpression(null,params,defaults,body,options.rest||null,options.generator,body.type!==Syntax.BlockStatement,options.async,options.returnType,options.typeParameters));}
function parsePropertyMethodFunction(options){var previousStrict,tmp,method;previousStrict=strict;strict=true;tmp=parseParams();if(tmp.stricted){throwErrorTolerant(tmp.stricted,tmp.message);}
method=parsePropertyFunction({params:tmp.params,defaults:tmp.defaults,rest:tmp.rest,generator:options.generator,async:options.async,returnType:tmp.returnType,typeParameters:options.typeParameters});strict=previousStrict;return method;}
function parseObjectPropertyKey(){var marker=markerCreate(),token=lex(),propertyKey,result;
if(token.type===Token.StringLiteral||token.type===Token.NumericLiteral){if(strict&&token.octal){throwErrorTolerant(token,Messages.StrictOctalLiteral);}
return markerApply(marker,delegate.createLiteral(token));}
if(token.type===Token.Punctuator&&token.value==='['){
marker=markerCreate();propertyKey=parseAssignmentExpression();result=markerApply(marker,propertyKey);expect(']');return result;}
return markerApply(marker,delegate.createIdentifier(token.value));}
function parseObjectProperty(){var token,key,id,param,computed,marker=markerCreate(),returnType,typeParameters;token=lookahead;computed=(token.value==='['&&token.type===Token.Punctuator);if(token.type===Token.Identifier||computed||matchAsync()){id=parseObjectPropertyKey();if(match(':')){lex();return markerApply(marker,delegate.createProperty('init',id,parseAssignmentExpression(),false,false,computed));}
if(match('(')||match('<')){if(match('<')){typeParameters=parseTypeParameterDeclaration();}
return markerApply(marker,delegate.createProperty('init',id,parsePropertyMethodFunction({generator:false,async:false,typeParameters:typeParameters}),true,false,computed));}
if(token.value==='get'){computed=(lookahead.value==='[');key=parseObjectPropertyKey();expect('(');expect(')');if(match(':')){returnType=parseTypeAnnotation();}
return markerApply(marker,delegate.createProperty('get',key,parsePropertyFunction({generator:false,async:false,returnType:returnType}),false,false,computed));}
if(token.value==='set'){computed=(lookahead.value==='[');key=parseObjectPropertyKey();expect('(');token=lookahead;param=[parseTypeAnnotatableIdentifier()];expect(')');if(match(':')){returnType=parseTypeAnnotation();}
return markerApply(marker,delegate.createProperty('set',key,parsePropertyFunction({params:param,generator:false,async:false,name:token,returnType:returnType}),false,false,computed));}
if(token.value==='async'){computed=(lookahead.value==='[');key=parseObjectPropertyKey();if(match('<')){typeParameters=parseTypeParameterDeclaration();}
return markerApply(marker,delegate.createProperty('init',key,parsePropertyMethodFunction({generator:false,async:true,typeParameters:typeParameters}),true,false,computed));}
if(computed){throwUnexpected(lookahead);}
return markerApply(marker,delegate.createProperty('init',id,id,false,true,false));}
if(token.type===Token.EOF||token.type===Token.Punctuator){if(!match('*')){throwUnexpected(token);}
lex();computed=(lookahead.type===Token.Punctuator&&lookahead.value==='[');id=parseObjectPropertyKey();if(match('<')){typeParameters=parseTypeParameterDeclaration();}
if(!match('(')){throwUnexpected(lex());}
return markerApply(marker,delegate.createProperty('init',id,parsePropertyMethodFunction({generator:true,typeParameters:typeParameters}),true,false,computed));}
key=parseObjectPropertyKey();if(match(':')){lex();return markerApply(marker,delegate.createProperty('init',key,parseAssignmentExpression(),false,false,false));}
if(match('(')||match('<')){if(match('<')){typeParameters=parseTypeParameterDeclaration();}
return markerApply(marker,delegate.createProperty('init',key,parsePropertyMethodFunction({generator:false,typeParameters:typeParameters}),true,false,false));}
throwUnexpected(lex());}
function parseObjectSpreadProperty(){var marker=markerCreate();expect('...');return markerApply(marker,delegate.createSpreadProperty(parseAssignmentExpression()));}
function getFieldName(key){var toString=String;if(key.type===Syntax.Identifier){return key.name;}
return toString(key.value);}
function parseObjectInitialiser(){var properties=[],property,name,kind,storedKind,map=new StringMap(),marker=markerCreate(),toString=String;expect('{');while(!match('}')){if(match('...')){property=parseObjectSpreadProperty();}else{property=parseObjectProperty();if(property.key.type===Syntax.Identifier){name=property.key.name;}else{name=toString(property.key.value);}
kind=(property.kind==='init')?PropertyKind.Data:(property.kind==='get')?PropertyKind.Get:PropertyKind.Set;if(map.has(name)){storedKind=map.get(name);if(storedKind===PropertyKind.Data){if(strict&&kind===PropertyKind.Data){throwErrorTolerant({},Messages.StrictDuplicateProperty);}else if(kind!==PropertyKind.Data){throwErrorTolerant({},Messages.AccessorDataProperty);}}else{if(kind===PropertyKind.Data){throwErrorTolerant({},Messages.AccessorDataProperty);}else if(storedKind&kind){throwErrorTolerant({},Messages.AccessorGetSet);}}
map.set(name,storedKind|kind);}else{map.set(name,kind);}}
properties.push(property);if(!match('}')){expect(',');}}
expect('}');return markerApply(marker,delegate.createObjectExpression(properties));}
function parseTemplateElement(option){var marker=markerCreate(),token=scanTemplateElement(option);if(strict&&token.octal){throwError(token,Messages.StrictOctalLiteral);}
return markerApply(marker,delegate.createTemplateElement({raw:token.value.raw,cooked:token.value.cooked},token.tail));}
function parseTemplateLiteral(){var quasi,quasis,expressions,marker=markerCreate();quasi=parseTemplateElement({head:true});quasis=[quasi];expressions=[];while(!quasi.tail){expressions.push(parseExpression());quasi=parseTemplateElement({head:false});quasis.push(quasi);}
return markerApply(marker,delegate.createTemplateLiteral(quasis,expressions));} 
function parseGroupExpression(){var expr,marker,typeAnnotation;expect('(');++state.parenthesizedCount;marker=markerCreate();expr=parseExpression();if(match(':')){typeAnnotation=parseTypeAnnotation();expr=markerApply(marker,delegate.createTypeCast(expr,typeAnnotation));}
expect(')');return expr;}
function matchAsyncFuncExprOrDecl(){var token;if(matchAsync()){token=lookahead2();if(token.type===Token.Keyword&&token.value==='function'){return true;}}
return false;} 
function parsePrimaryExpression(){var marker,type,token,expr;type=lookahead.type;if(type===Token.Identifier){marker=markerCreate();return markerApply(marker,delegate.createIdentifier(lex().value));}
if(type===Token.StringLiteral||type===Token.NumericLiteral){if(strict&&lookahead.octal){throwErrorTolerant(lookahead,Messages.StrictOctalLiteral);}
marker=markerCreate();return markerApply(marker,delegate.createLiteral(lex()));}
if(type===Token.Keyword){if(matchKeyword('this')){marker=markerCreate();lex();return markerApply(marker,delegate.createThisExpression());}
if(matchKeyword('function')){return parseFunctionExpression();}
if(matchKeyword('class')){return parseClassExpression();}
if(matchKeyword('super')){marker=markerCreate();lex();return markerApply(marker,delegate.createIdentifier('super'));}}
if(type===Token.BooleanLiteral){marker=markerCreate();token=lex();token.value=(token.value==='true');return markerApply(marker,delegate.createLiteral(token));}
if(type===Token.NullLiteral){marker=markerCreate();token=lex();token.value=null;return markerApply(marker,delegate.createLiteral(token));}
if(match('[')){return parseArrayInitialiser();}
if(match('{')){return parseObjectInitialiser();}
if(match('(')){return parseGroupExpression();}
if(match('/')||match('/=')){marker=markerCreate();expr=delegate.createLiteral(scanRegExp());peek();return markerApply(marker,expr);}
if(type===Token.Template){return parseTemplateLiteral();}
if(match('<')){return parseJSXElement();}
throwUnexpected(lex());} 
function parseArguments(){var args=[],arg;expect('(');if(!match(')')){while(index<length){arg=parseSpreadOrAssignmentExpression();args.push(arg);if(match(')')){break;}else if(arg.type===Syntax.SpreadElement){throwError({},Messages.ElementAfterSpreadElement);}
expect(',');}}
expect(')');return args;}
function parseSpreadOrAssignmentExpression(){if(match('...')){var marker=markerCreate();lex();return markerApply(marker,delegate.createSpreadElement(parseAssignmentExpression()));}
return parseAssignmentExpression();}
function parseNonComputedProperty(){var marker=markerCreate(),token=lex();if(!isIdentifierName(token)){throwUnexpected(token);}
return markerApply(marker,delegate.createIdentifier(token.value));}
function parseNonComputedMember(){expect('.');return parseNonComputedProperty();}
function parseComputedMember(){var expr;expect('[');expr=parseExpression();expect(']');return expr;}
function parseNewExpression(){var callee,args,marker=markerCreate();expectKeyword('new');callee=parseLeftHandSideExpression();args=match('(')?parseArguments():[];return markerApply(marker,delegate.createNewExpression(callee,args));}
function parseLeftHandSideExpressionAllowCall(){var expr,args,marker=markerCreate();expr=matchKeyword('new')?parseNewExpression():parsePrimaryExpression();while(match('.')||match('[')||match('(')||lookahead.type===Token.Template){if(match('(')){args=parseArguments();expr=markerApply(marker,delegate.createCallExpression(expr,args));}else if(match('[')){expr=markerApply(marker,delegate.createMemberExpression('[',expr,parseComputedMember()));}else if(match('.')){expr=markerApply(marker,delegate.createMemberExpression('.',expr,parseNonComputedMember()));}else{expr=markerApply(marker,delegate.createTaggedTemplateExpression(expr,parseTemplateLiteral()));}}
return expr;}
function parseLeftHandSideExpression(){var expr,marker=markerCreate();expr=matchKeyword('new')?parseNewExpression():parsePrimaryExpression();while(match('.')||match('[')||lookahead.type===Token.Template){if(match('[')){expr=markerApply(marker,delegate.createMemberExpression('[',expr,parseComputedMember()));}else if(match('.')){expr=markerApply(marker,delegate.createMemberExpression('.',expr,parseNonComputedMember()));}else{expr=markerApply(marker,delegate.createTaggedTemplateExpression(expr,parseTemplateLiteral()));}}
return expr;} 
function parsePostfixExpression(){var marker=markerCreate(),expr=parseLeftHandSideExpressionAllowCall(),token;if(lookahead.type!==Token.Punctuator){return expr;}
if((match('++')||match('--'))&&!peekLineTerminator()){ if(strict&&expr.type===Syntax.Identifier&&isRestrictedWord(expr.name)){throwErrorTolerant({},Messages.StrictLHSPostfix);}
if(!isLeftHandSide(expr)){throwError({},Messages.InvalidLHSInAssignment);}
token=lex();expr=markerApply(marker,delegate.createPostfixExpression(token.value,expr));}
return expr;} 
function parseUnaryExpression(){var marker,token,expr;if(lookahead.type!==Token.Punctuator&&lookahead.type!==Token.Keyword){return parsePostfixExpression();}
if(match('++')||match('--')){marker=markerCreate();token=lex();expr=parseUnaryExpression(); if(strict&&expr.type===Syntax.Identifier&&isRestrictedWord(expr.name)){throwErrorTolerant({},Messages.StrictLHSPrefix);}
if(!isLeftHandSide(expr)){throwError({},Messages.InvalidLHSInAssignment);}
return markerApply(marker,delegate.createUnaryExpression(token.value,expr));}
if(match('+')||match('-')||match('~')||match('!')){marker=markerCreate();token=lex();expr=parseUnaryExpression();return markerApply(marker,delegate.createUnaryExpression(token.value,expr));}
if(matchKeyword('delete')||matchKeyword('void')||matchKeyword('typeof')){marker=markerCreate();token=lex();expr=parseUnaryExpression();expr=markerApply(marker,delegate.createUnaryExpression(token.value,expr));if(strict&&expr.operator==='delete'&&expr.argument.type===Syntax.Identifier){throwErrorTolerant({},Messages.StrictDelete);}
return expr;}
return parsePostfixExpression();}
function binaryPrecedence(token,allowIn){var prec=0;if(token.type!==Token.Punctuator&&token.type!==Token.Keyword){return 0;}
switch(token.value){case'||':prec=1;break;case'&&':prec=2;break;case'|':prec=3;break;case'^':prec=4;break;case'&':prec=5;break;case'==':case'!=':case'===':case'!==':prec=6;break;case'<':case'>':case'<=':case'>=':case'instanceof':prec=7;break;case'in':prec=allowIn?7:0;break;case'<<':case'>>':case'>>>':prec=8;break;case'+':case'-':prec=9;break;case'*':case'/':case'%':prec=11;break;default:break;}
return prec;}





 
function parseBinaryExpression(){var expr,token,prec,previousAllowIn,stack,right,operator,left,i,marker,markers;previousAllowIn=state.allowIn;state.allowIn=true;marker=markerCreate();left=parseUnaryExpression();token=lookahead;prec=binaryPrecedence(token,previousAllowIn);if(prec===0){return left;}
token.prec=prec;lex();markers=[marker,markerCreate()];right=parseUnaryExpression();stack=[left,token,right];while((prec=binaryPrecedence(lookahead,previousAllowIn))>0){while((stack.length>2)&&(prec<=stack[stack.length-2].prec)){right=stack.pop();operator=stack.pop().value;left=stack.pop();expr=delegate.createBinaryExpression(operator,left,right);markers.pop();marker=markers.pop();markerApply(marker,expr);stack.push(expr);markers.push(marker);}
token=lex();token.prec=prec;stack.push(token);markers.push(markerCreate());expr=parseUnaryExpression();stack.push(expr);}
state.allowIn=previousAllowIn;i=stack.length-1;expr=stack[i];markers.pop();while(i>1){expr=delegate.createBinaryExpression(stack[i-1].value,stack[i-2],expr);i-=2;marker=markers.pop();markerApply(marker,expr);}
return expr;} 
function parseConditionalExpression(){var expr,previousAllowIn,consequent,alternate,marker=markerCreate();expr=parseBinaryExpression();if(match('?')){lex();previousAllowIn=state.allowIn;state.allowIn=true;consequent=parseAssignmentExpression();state.allowIn=previousAllowIn;expect(':');alternate=parseAssignmentExpression();expr=markerApply(marker,delegate.createConditionalExpression(expr,consequent,alternate));}
return expr;}
 
function reinterpretAsAssignmentBindingPattern(expr){var i,len,property,element;if(expr.type===Syntax.ObjectExpression){expr.type=Syntax.ObjectPattern;for(i=0,len=expr.properties.length;i<len;i+=1){property=expr.properties[i];if(property.type===Syntax.SpreadProperty){if(i<len-1){throwError({},Messages.PropertyAfterSpreadProperty);}
reinterpretAsAssignmentBindingPattern(property.argument);}else{if(property.kind!=='init'){throwError({},Messages.InvalidLHSInAssignment);}
reinterpretAsAssignmentBindingPattern(property.value);}}}else if(expr.type===Syntax.ArrayExpression){expr.type=Syntax.ArrayPattern;for(i=0,len=expr.elements.length;i<len;i+=1){element=expr.elements[i];if(element){reinterpretAsAssignmentBindingPattern(element);}}}else if(expr.type===Syntax.Identifier){if(isRestrictedWord(expr.name)){throwError({},Messages.InvalidLHSInAssignment);}}else if(expr.type===Syntax.SpreadElement){reinterpretAsAssignmentBindingPattern(expr.argument);if(expr.argument.type===Syntax.ObjectPattern){throwError({},Messages.ObjectPatternAsSpread);}}else{if(expr.type!==Syntax.MemberExpression&&expr.type!==Syntax.CallExpression&&expr.type!==Syntax.NewExpression){throwError({},Messages.InvalidLHSInAssignment);}}} 
function reinterpretAsDestructuredParameter(options,expr){var i,len,property,element;if(expr.type===Syntax.ObjectExpression){expr.type=Syntax.ObjectPattern;for(i=0,len=expr.properties.length;i<len;i+=1){property=expr.properties[i];if(property.type===Syntax.SpreadProperty){if(i<len-1){throwError({},Messages.PropertyAfterSpreadProperty);}
reinterpretAsDestructuredParameter(options,property.argument);}else{if(property.kind!=='init'){throwError({},Messages.InvalidLHSInFormalsList);}
reinterpretAsDestructuredParameter(options,property.value);}}}else if(expr.type===Syntax.ArrayExpression){expr.type=Syntax.ArrayPattern;for(i=0,len=expr.elements.length;i<len;i+=1){element=expr.elements[i];if(element){reinterpretAsDestructuredParameter(options,element);}}}else if(expr.type===Syntax.Identifier){validateParam(options,expr,expr.name);}else if(expr.type===Syntax.SpreadElement){ if(expr.argument.type!==Syntax.Identifier){throwError({},Messages.InvalidLHSInFormalsList);}
validateParam(options,expr.argument,expr.argument.name);}else{throwError({},Messages.InvalidLHSInFormalsList);}}
function reinterpretAsCoverFormalsList(expressions){var i,len,param,params,defaults,defaultCount,options,rest;params=[];defaults=[];defaultCount=0;rest=null;options={paramSet:new StringMap()};for(i=0,len=expressions.length;i<len;i+=1){param=expressions[i];if(param.type===Syntax.Identifier){params.push(param);defaults.push(null);validateParam(options,param,param.name);}else if(param.type===Syntax.ObjectExpression||param.type===Syntax.ArrayExpression){reinterpretAsDestructuredParameter(options,param);params.push(param);defaults.push(null);}else if(param.type===Syntax.SpreadElement){assert(i===len-1,'It is guaranteed that SpreadElement is last element by parseExpression');if(param.argument.type!==Syntax.Identifier){throwError({},Messages.InvalidLHSInFormalsList);}
reinterpretAsDestructuredParameter(options,param.argument);rest=param.argument;}else if(param.type===Syntax.AssignmentExpression){params.push(param.left);defaults.push(param.right);++defaultCount;validateParam(options,param.left,param.left.name);}else{return null;}}
if(options.message===Messages.StrictParamDupe){throwError(strict?options.stricted:options.firstRestricted,options.message);}
if(defaultCount===0){defaults=[];}
return{params:params,defaults:defaults,rest:rest,stricted:options.stricted,firstRestricted:options.firstRestricted,message:options.message};}
function parseArrowFunctionExpression(options,marker){var previousStrict,previousYieldAllowed,previousAwaitAllowed,body;expect('=>');previousStrict=strict;previousYieldAllowed=state.yieldAllowed;state.yieldAllowed=false;previousAwaitAllowed=state.awaitAllowed;state.awaitAllowed=!!options.async;body=parseConciseBody();if(strict&&options.firstRestricted){throwError(options.firstRestricted,options.message);}
if(strict&&options.stricted){throwErrorTolerant(options.stricted,options.message);}
strict=previousStrict;state.yieldAllowed=previousYieldAllowed;state.awaitAllowed=previousAwaitAllowed;return markerApply(marker,delegate.createArrowFunctionExpression(options.params,options.defaults,body,options.rest,body.type!==Syntax.BlockStatement,!!options.async));}
function parseAssignmentExpression(){var marker,expr,token,params,oldParenthesizedCount,startsWithParen=false,backtrackToken=lookahead,possiblyAsync=false;if(matchYield()){return parseYieldExpression();}
if(matchAwait()){return parseAwaitExpression();}
oldParenthesizedCount=state.parenthesizedCount;marker=markerCreate();if(matchAsyncFuncExprOrDecl()){return parseFunctionExpression();}
if(matchAsync()){


possiblyAsync=true;lex();}
if(match('(')){token=lookahead2();if((token.type===Token.Punctuator&&token.value===')')||token.value==='...'){params=parseParams();if(!match('=>')){throwUnexpected(lex());}
params.async=possiblyAsync;return parseArrowFunctionExpression(params,marker);}
startsWithParen=true;}
token=lookahead;

if(possiblyAsync&&!match('(')&&token.type!==Token.Identifier){possiblyAsync=false;rewind(backtrackToken);}
expr=parseConditionalExpression();if(match('=>')&&(state.parenthesizedCount===oldParenthesizedCount||state.parenthesizedCount===(oldParenthesizedCount+1))){if(expr.type===Syntax.Identifier){params=reinterpretAsCoverFormalsList([expr]);}else if(expr.type===Syntax.AssignmentExpression||expr.type===Syntax.ArrayExpression||expr.type===Syntax.ObjectExpression){if(!startsWithParen){throwUnexpected(lex());}
params=reinterpretAsCoverFormalsList([expr]);}else if(expr.type===Syntax.SequenceExpression){params=reinterpretAsCoverFormalsList(expr.expressions);}
if(params){params.async=possiblyAsync;return parseArrowFunctionExpression(params,marker);}}


if(possiblyAsync){possiblyAsync=false;rewind(backtrackToken);expr=parseConditionalExpression();}
if(matchAssign()){ if(strict&&expr.type===Syntax.Identifier&&isRestrictedWord(expr.name)){throwErrorTolerant(token,Messages.StrictLHSAssignment);} 
if(match('=')&&(expr.type===Syntax.ObjectExpression||expr.type===Syntax.ArrayExpression)){reinterpretAsAssignmentBindingPattern(expr);}else if(!isLeftHandSide(expr)){throwError({},Messages.InvalidLHSInAssignment);}
expr=markerApply(marker,delegate.createAssignmentExpression(lex().value,expr,parseAssignmentExpression()));}
return expr;} 
function parseExpression(){var marker,expr,expressions,sequence,spreadFound;marker=markerCreate();expr=parseAssignmentExpression();expressions=[expr];if(match(',')){while(index<length){if(!match(',')){break;}
lex();expr=parseSpreadOrAssignmentExpression();expressions.push(expr);if(expr.type===Syntax.SpreadElement){spreadFound=true;if(!match(')')){throwError({},Messages.ElementAfterSpreadElement);}
break;}}
sequence=markerApply(marker,delegate.createSequenceExpression(expressions));}
if(spreadFound&&lookahead2().value!=='=>'){throwError({},Messages.IllegalSpread);}
return sequence||expr;} 
function parseStatementList(){var list=[],statement;while(index<length){if(match('}')){break;}
statement=parseSourceElement();if(typeof statement==='undefined'){break;}
list.push(statement);}
return list;}
function parseBlock(){var block,marker=markerCreate();expect('{');block=parseStatementList();expect('}');return markerApply(marker,delegate.createBlockStatement(block));} 
function parseTypeParameterDeclaration(){var marker=markerCreate(),paramTypes=[];expect('<');while(!match('>')){paramTypes.push(parseTypeAnnotatableIdentifier());if(!match('>')){expect(',');}}
expect('>');return markerApply(marker,delegate.createTypeParameterDeclaration(paramTypes));}
function parseTypeParameterInstantiation(){var marker=markerCreate(),oldInType=state.inType,paramTypes=[];state.inType=true;expect('<');while(!match('>')){paramTypes.push(parseType());if(!match('>')){expect(',');}}
expect('>');state.inType=oldInType;return markerApply(marker,delegate.createTypeParameterInstantiation(paramTypes));}
function parseObjectTypeIndexer(marker,isStatic){var id,key,value;expect('[');id=parseObjectPropertyKey();expect(':');key=parseType();expect(']');expect(':');value=parseType();return markerApply(marker,delegate.createObjectTypeIndexer(id,key,value,isStatic));}
function parseObjectTypeMethodish(marker){var params=[],rest=null,returnType,typeParameters=null;if(match('<')){typeParameters=parseTypeParameterDeclaration();}
expect('(');while(lookahead.type===Token.Identifier){params.push(parseFunctionTypeParam());if(!match(')')){expect(',');}}
if(match('...')){lex();rest=parseFunctionTypeParam();}
expect(')');expect(':');returnType=parseType();return markerApply(marker,delegate.createFunctionTypeAnnotation(params,returnType,rest,typeParameters));}
function parseObjectTypeMethod(marker,isStatic,key){var optional=false,value;value=parseObjectTypeMethodish(marker);return markerApply(marker,delegate.createObjectTypeProperty(key,value,optional,isStatic));}
function parseObjectTypeCallProperty(marker,isStatic){var valueMarker=markerCreate();return markerApply(marker,delegate.createObjectTypeCallProperty(parseObjectTypeMethodish(valueMarker),isStatic));}
function parseObjectType(allowStatic){var callProperties=[],indexers=[],marker,optional=false,properties=[],propertyKey,propertyTypeAnnotation,token,isStatic,matchStatic;expect('{');while(!match('}')){marker=markerCreate();matchStatic=strict?matchKeyword('static'):matchContextualKeyword('static');if(allowStatic&&matchStatic){token=lex();isStatic=true;}
if(match('[')){indexers.push(parseObjectTypeIndexer(marker,isStatic));}else if(match('(')||match('<')){callProperties.push(parseObjectTypeCallProperty(marker,allowStatic));}else{if(isStatic&&match(':')){propertyKey=markerApply(marker,delegate.createIdentifier(token));throwErrorTolerant(token,Messages.StrictReservedWord);}else{propertyKey=parseObjectPropertyKey();}
if(match('<')||match('(')){ properties.push(parseObjectTypeMethod(marker,isStatic,propertyKey));}else{if(match('?')){lex();optional=true;}
expect(':');propertyTypeAnnotation=parseType();properties.push(markerApply(marker,delegate.createObjectTypeProperty(propertyKey,propertyTypeAnnotation,optional,isStatic)));}}
if(match(';')){lex();}else if(!match('}')){throwUnexpected(lookahead);}}
expect('}');return delegate.createObjectTypeAnnotation(properties,indexers,callProperties);}
function parseGenericType(){var marker=markerCreate(),typeParameters=null,typeIdentifier;typeIdentifier=parseVariableIdentifier();while(match('.')){expect('.');typeIdentifier=markerApply(marker,delegate.createQualifiedTypeIdentifier(typeIdentifier,parseVariableIdentifier()));}
if(match('<')){typeParameters=parseTypeParameterInstantiation();}
return markerApply(marker,delegate.createGenericTypeAnnotation(typeIdentifier,typeParameters));}
function parseVoidType(){var marker=markerCreate();expectKeyword('void');return markerApply(marker,delegate.createVoidTypeAnnotation());}
function parseTypeofType(){var argument,marker=markerCreate();expectKeyword('typeof');argument=parsePrimaryType();return markerApply(marker,delegate.createTypeofTypeAnnotation(argument));}
function parseTupleType(){var marker=markerCreate(),types=[];expect('['); while(index<length&&!match(']')){types.push(parseType());if(match(']')){break;}
expect(',');}
expect(']');return markerApply(marker,delegate.createTupleTypeAnnotation(types));}
function parseFunctionTypeParam(){var marker=markerCreate(),name,optional=false,typeAnnotation;name=parseVariableIdentifier();if(match('?')){lex();optional=true;}
expect(':');typeAnnotation=parseType();return markerApply(marker,delegate.createFunctionTypeParam(name,typeAnnotation,optional));}
function parseFunctionTypeParams(){var ret={params:[],rest:null};while(lookahead.type===Token.Identifier){ret.params.push(parseFunctionTypeParam());if(!match(')')){expect(',');}}
if(match('...')){lex();ret.rest=parseFunctionTypeParam();}
return ret;}


function parsePrimaryType(){var params=null,returnType=null,marker=markerCreate(),rest=null,tmp,typeParameters,token,type,isGroupedType=false;switch(lookahead.type){case Token.Identifier:switch(lookahead.value){case'any':lex();return markerApply(marker,delegate.createAnyTypeAnnotation());case'bool': case'boolean':lex();return markerApply(marker,delegate.createBooleanTypeAnnotation());case'number':lex();return markerApply(marker,delegate.createNumberTypeAnnotation());case'string':lex();return markerApply(marker,delegate.createStringTypeAnnotation());}
return markerApply(marker,parseGenericType());case Token.Punctuator:switch(lookahead.value){case'{':return markerApply(marker,parseObjectType());case'[':return parseTupleType();case'<':typeParameters=parseTypeParameterDeclaration();expect('(');tmp=parseFunctionTypeParams();params=tmp.params;rest=tmp.rest;expect(')');expect('=>');returnType=parseType();return markerApply(marker,delegate.createFunctionTypeAnnotation(params,returnType,rest,typeParameters));case'(':lex(); if(!match(')')&&!match('...')){if(lookahead.type===Token.Identifier){token=lookahead2();isGroupedType=token.value!=='?'&&token.value!==':';}else{isGroupedType=true;}}
if(isGroupedType){type=parseType();expect(')');
 if(match('=>')){throwError({},Messages.ConfusedAboutFunctionType);}
return type;}
tmp=parseFunctionTypeParams();params=tmp.params;rest=tmp.rest;expect(')');expect('=>');returnType=parseType();return markerApply(marker,delegate.createFunctionTypeAnnotation(params,returnType,rest,null ));}
break;case Token.Keyword:switch(lookahead.value){case'void':return markerApply(marker,parseVoidType());case'typeof':return markerApply(marker,parseTypeofType());}
break;case Token.StringLiteral:token=lex();if(token.octal){throwError(token,Messages.StrictOctalLiteral);}
return markerApply(marker,delegate.createStringLiteralTypeAnnotation(token));}
throwUnexpected(lookahead);}
function parsePostfixType(){var marker=markerCreate(),t=parsePrimaryType();if(match('[')){expect('[');expect(']');return markerApply(marker,delegate.createArrayTypeAnnotation(t));}
return t;}
function parsePrefixType(){var marker=markerCreate();if(match('?')){lex();return markerApply(marker,delegate.createNullableTypeAnnotation(parsePrefixType()));}
return parsePostfixType();}
function parseIntersectionType(){var marker=markerCreate(),type,types;type=parsePrefixType();types=[type];while(match('&')){lex();types.push(parsePrefixType());}
return types.length===1?type:markerApply(marker,delegate.createIntersectionTypeAnnotation(types));}
function parseUnionType(){var marker=markerCreate(),type,types;type=parseIntersectionType();types=[type];while(match('|')){lex();types.push(parseIntersectionType());}
return types.length===1?type:markerApply(marker,delegate.createUnionTypeAnnotation(types));}
function parseType(){var oldInType=state.inType,type;state.inType=true;type=parseUnionType();state.inType=oldInType;return type;}
function parseTypeAnnotation(){var marker=markerCreate(),type;expect(':');type=parseType();return markerApply(marker,delegate.createTypeAnnotation(type));}
function parseVariableIdentifier(){var marker=markerCreate(),token=lex();if(token.type!==Token.Identifier){throwUnexpected(token);}
return markerApply(marker,delegate.createIdentifier(token.value));}
function parseTypeAnnotatableIdentifier(requireTypeAnnotation,canBeOptionalParam){var marker=markerCreate(),ident=parseVariableIdentifier(),isOptionalParam=false;if(canBeOptionalParam&&match('?')){expect('?');isOptionalParam=true;}
if(requireTypeAnnotation||match(':')){ident.typeAnnotation=parseTypeAnnotation();ident=markerApply(marker,ident);}
if(isOptionalParam){ident.optional=true;ident=markerApply(marker,ident);}
return ident;}
function parseVariableDeclaration(kind){var id,marker=markerCreate(),init=null,typeAnnotationMarker=markerCreate();if(match('{')){id=parseObjectInitialiser();reinterpretAsAssignmentBindingPattern(id);if(match(':')){id.typeAnnotation=parseTypeAnnotation();markerApply(typeAnnotationMarker,id);}}else if(match('[')){id=parseArrayInitialiser();reinterpretAsAssignmentBindingPattern(id);if(match(':')){id.typeAnnotation=parseTypeAnnotation();markerApply(typeAnnotationMarker,id);}}else{id=state.allowKeyword?parseNonComputedProperty():parseTypeAnnotatableIdentifier(); if(strict&&isRestrictedWord(id.name)){throwErrorTolerant({},Messages.StrictVarName);}}
if(kind==='const'){if(!match('=')){throwError({},Messages.NoUninitializedConst);}
expect('=');init=parseAssignmentExpression();}else if(match('=')){lex();init=parseAssignmentExpression();}
return markerApply(marker,delegate.createVariableDeclarator(id,init));}
function parseVariableDeclarationList(kind){var list=[];do{list.push(parseVariableDeclaration(kind));if(!match(',')){break;}
lex();}while(index<length);return list;}
function parseVariableStatement(){var declarations,marker=markerCreate();expectKeyword('var');declarations=parseVariableDeclarationList();consumeSemicolon();return markerApply(marker,delegate.createVariableDeclaration(declarations,'var'));}
 
function parseConstLetDeclaration(kind){var declarations,marker=markerCreate();expectKeyword(kind);declarations=parseVariableDeclarationList(kind);consumeSemicolon();return markerApply(marker,delegate.createVariableDeclaration(declarations,kind));} 
function parseModuleSpecifier(){var marker=markerCreate(),specifier;if(lookahead.type!==Token.StringLiteral){throwError({},Messages.InvalidModuleSpecifier);}
specifier=delegate.createModuleSpecifier(lookahead);lex();return markerApply(marker,specifier);}
function parseExportBatchSpecifier(){var marker=markerCreate();expect('*');return markerApply(marker,delegate.createExportBatchSpecifier());}
function parseExportSpecifier(){var id,name=null,marker=markerCreate(),from;if(matchKeyword('default')){lex();id=markerApply(marker,delegate.createIdentifier('default'));}else{id=parseVariableIdentifier();}
if(matchContextualKeyword('as')){lex();name=parseNonComputedProperty();}
return markerApply(marker,delegate.createExportSpecifier(id,name));}
function parseExportDeclaration(){var declaration=null,possibleIdentifierToken,sourceElement,isExportFromIdentifier,src=null,specifiers=[],marker=markerCreate();expectKeyword('export');if(matchKeyword('default')){lex();if(matchKeyword('function')||matchKeyword('class')){possibleIdentifierToken=lookahead2();if(isIdentifierName(possibleIdentifierToken)){
sourceElement=parseSourceElement();return markerApply(marker,delegate.createExportDeclaration(true,sourceElement,[sourceElement.id],null));}
switch(lookahead.value){case'class':return markerApply(marker,delegate.createExportDeclaration(true,parseClassExpression(),[],null));case'function':return markerApply(marker,delegate.createExportDeclaration(true,parseFunctionExpression(),[],null));}}
if(matchContextualKeyword('from')){throwError({},Messages.UnexpectedToken,lookahead.value);}
if(match('{')){declaration=parseObjectInitialiser();}else if(match('[')){declaration=parseArrayInitialiser();}else{declaration=parseAssignmentExpression();}
consumeSemicolon();return markerApply(marker,delegate.createExportDeclaration(true,declaration,[],null));} 
if(lookahead.type===Token.Keyword||matchContextualKeyword('type')){switch(lookahead.value){case'type':case'let':case'const':case'var':case'class':case'function':return markerApply(marker,delegate.createExportDeclaration(false,parseSourceElement(),specifiers,null));}}
if(match('*')){specifiers.push(parseExportBatchSpecifier());if(!matchContextualKeyword('from')){throwError({},lookahead.value?Messages.UnexpectedToken:Messages.MissingFromClause,lookahead.value);}
lex();src=parseModuleSpecifier();consumeSemicolon();return markerApply(marker,delegate.createExportDeclaration(false,null,specifiers,src));}
expect('{');if(!match('}')){do{isExportFromIdentifier=isExportFromIdentifier||matchKeyword('default');specifiers.push(parseExportSpecifier());}while(match(',')&&lex());}
expect('}');if(matchContextualKeyword('from')){lex();src=parseModuleSpecifier();consumeSemicolon();}else if(isExportFromIdentifier){ throwError({},lookahead.value?Messages.UnexpectedToken:Messages.MissingFromClause,lookahead.value);}else{
consumeSemicolon();}
return markerApply(marker,delegate.createExportDeclaration(false,declaration,specifiers,src));}
function parseImportSpecifier(){var id,name=null,marker=markerCreate();id=parseNonComputedProperty();if(matchContextualKeyword('as')){lex();name=parseVariableIdentifier();}
return markerApply(marker,delegate.createImportSpecifier(id,name));}
function parseNamedImports(){var specifiers=[];expect('{');if(!match('}')){do{specifiers.push(parseImportSpecifier());}while(match(',')&&lex());}
expect('}');return specifiers;}
function parseImportDefaultSpecifier(){var id,marker=markerCreate();id=parseNonComputedProperty();return markerApply(marker,delegate.createImportDefaultSpecifier(id));}
function parseImportNamespaceSpecifier(){var id,marker=markerCreate();expect('*');if(!matchContextualKeyword('as')){throwError({},Messages.NoAsAfterImportNamespace);}
lex();id=parseNonComputedProperty();return markerApply(marker,delegate.createImportNamespaceSpecifier(id));}
function parseImportDeclaration(){var specifiers,src,marker=markerCreate(),isType=false,token2;expectKeyword('import');if(matchContextualKeyword('type')){token2=lookahead2();if((token2.type===Token.Identifier&&token2.value!=='from')||(token2.type===Token.Punctuator&&(token2.value==='{'||token2.value==='*'))){isType=true;lex();}}
specifiers=[];if(lookahead.type===Token.StringLiteral){src=parseModuleSpecifier();consumeSemicolon();return markerApply(marker,delegate.createImportDeclaration(specifiers,src,isType));}
if(!matchKeyword('default')&&isIdentifierName(lookahead)){
specifiers.push(parseImportDefaultSpecifier());if(match(',')){lex();}}
if(match('*')){
 specifiers.push(parseImportNamespaceSpecifier());}else if(match('{')){
specifiers=specifiers.concat(parseNamedImports());}
if(!matchContextualKeyword('from')){throwError({},lookahead.value?Messages.UnexpectedToken:Messages.MissingFromClause,lookahead.value);}
lex();src=parseModuleSpecifier();consumeSemicolon();return markerApply(marker,delegate.createImportDeclaration(specifiers,src,isType));} 
function parseEmptyStatement(){var marker=markerCreate();expect(';');return markerApply(marker,delegate.createEmptyStatement());} 
function parseExpressionStatement(){var marker=markerCreate(),expr=parseExpression();consumeSemicolon();return markerApply(marker,delegate.createExpressionStatement(expr));} 
function parseIfStatement(){var test,consequent,alternate,marker=markerCreate();expectKeyword('if');expect('(');test=parseExpression();expect(')');consequent=parseStatement();if(matchKeyword('else')){lex();alternate=parseStatement();}else{alternate=null;}
return markerApply(marker,delegate.createIfStatement(test,consequent,alternate));} 
function parseDoWhileStatement(){var body,test,oldInIteration,marker=markerCreate();expectKeyword('do');oldInIteration=state.inIteration;state.inIteration=true;body=parseStatement();state.inIteration=oldInIteration;expectKeyword('while');expect('(');test=parseExpression();expect(')');if(match(';')){lex();}
return markerApply(marker,delegate.createDoWhileStatement(body,test));}
function parseWhileStatement(){var test,body,oldInIteration,marker=markerCreate();expectKeyword('while');expect('(');test=parseExpression();expect(')');oldInIteration=state.inIteration;state.inIteration=true;body=parseStatement();state.inIteration=oldInIteration;return markerApply(marker,delegate.createWhileStatement(test,body));}
function parseForVariableDeclaration(){var marker=markerCreate(),token=lex(),declarations=parseVariableDeclarationList();return markerApply(marker,delegate.createVariableDeclaration(declarations,token.value));}
function parseForStatement(opts){var init,test,update,left,right,body,operator,oldInIteration,marker=markerCreate();init=test=update=null;expectKeyword('for'); if(matchContextualKeyword('each')){throwError({},Messages.EachNotAllowed);}
expect('(');if(match(';')){lex();}else{if(matchKeyword('var')||matchKeyword('let')||matchKeyword('const')){state.allowIn=false;init=parseForVariableDeclaration();state.allowIn=true;if(init.declarations.length===1){if(matchKeyword('in')||matchContextualKeyword('of')){operator=lookahead;if(!((operator.value==='in'||init.kind!=='var')&&init.declarations[0].init)){lex();left=init;right=parseExpression();init=null;}}}}else{state.allowIn=false;init=parseExpression();state.allowIn=true;if(matchContextualKeyword('of')){operator=lex();left=init;right=parseExpression();init=null;}else if(matchKeyword('in')){ if(!isAssignableLeftHandSide(init)){throwError({},Messages.InvalidLHSInForIn);}
operator=lex();left=init;right=parseExpression();init=null;}}
if(typeof left==='undefined'){expect(';');}}
if(typeof left==='undefined'){if(!match(';')){test=parseExpression();}
expect(';');if(!match(')')){update=parseExpression();}}
expect(')');oldInIteration=state.inIteration;state.inIteration=true;if(!(opts!==undefined&&opts.ignoreBody)){body=parseStatement();}
state.inIteration=oldInIteration;if(typeof left==='undefined'){return markerApply(marker,delegate.createForStatement(init,test,update,body));}
if(operator.value==='in'){return markerApply(marker,delegate.createForInStatement(left,right,body));}
return markerApply(marker,delegate.createForOfStatement(left,right,body));} 
function parseContinueStatement(){var label=null,marker=markerCreate();expectKeyword('continue');if(source.charCodeAt(index)===59){lex();if(!state.inIteration){throwError({},Messages.IllegalContinue);}
return markerApply(marker,delegate.createContinueStatement(null));}
if(peekLineTerminator()){if(!state.inIteration){throwError({},Messages.IllegalContinue);}
return markerApply(marker,delegate.createContinueStatement(null));}
if(lookahead.type===Token.Identifier){label=parseVariableIdentifier();if(!state.labelSet.has(label.name)){throwError({},Messages.UnknownLabel,label.name);}}
consumeSemicolon();if(label===null&&!state.inIteration){throwError({},Messages.IllegalContinue);}
return markerApply(marker,delegate.createContinueStatement(label));} 
function parseBreakStatement(){var label=null,marker=markerCreate();expectKeyword('break');if(source.charCodeAt(index)===59){lex();if(!(state.inIteration||state.inSwitch)){throwError({},Messages.IllegalBreak);}
return markerApply(marker,delegate.createBreakStatement(null));}
if(peekLineTerminator()){if(!(state.inIteration||state.inSwitch)){throwError({},Messages.IllegalBreak);}
return markerApply(marker,delegate.createBreakStatement(null));}
if(lookahead.type===Token.Identifier){label=parseVariableIdentifier();if(!state.labelSet.has(label.name)){throwError({},Messages.UnknownLabel,label.name);}}
consumeSemicolon();if(label===null&&!(state.inIteration||state.inSwitch)){throwError({},Messages.IllegalBreak);}
return markerApply(marker,delegate.createBreakStatement(label));} 
function parseReturnStatement(){var argument=null,marker=markerCreate();expectKeyword('return');if(!state.inFunctionBody){throwErrorTolerant({},Messages.IllegalReturn);}
if(source.charCodeAt(index)===32){if(isIdentifierStart(source.charCodeAt(index+1))){argument=parseExpression();consumeSemicolon();return markerApply(marker,delegate.createReturnStatement(argument));}}
if(peekLineTerminator()){return markerApply(marker,delegate.createReturnStatement(null));}
if(!match(';')){if(!match('}')&&lookahead.type!==Token.EOF){argument=parseExpression();}}
consumeSemicolon();return markerApply(marker,delegate.createReturnStatement(argument));} 
function parseWithStatement(){var object,body,marker=markerCreate();if(strict){throwErrorTolerant({},Messages.StrictModeWith);}
expectKeyword('with');expect('(');object=parseExpression();expect(')');body=parseStatement();return markerApply(marker,delegate.createWithStatement(object,body));} 
function parseSwitchCase(){var test,consequent=[],sourceElement,marker=markerCreate();if(matchKeyword('default')){lex();test=null;}else{expectKeyword('case');test=parseExpression();}
expect(':');while(index<length){if(match('}')||matchKeyword('default')||matchKeyword('case')){break;}
sourceElement=parseSourceElement();if(typeof sourceElement==='undefined'){break;}
consequent.push(sourceElement);}
return markerApply(marker,delegate.createSwitchCase(test,consequent));}
function parseSwitchStatement(){var discriminant,cases,clause,oldInSwitch,defaultFound,marker=markerCreate();expectKeyword('switch');expect('(');discriminant=parseExpression();expect(')');expect('{');cases=[];if(match('}')){lex();return markerApply(marker,delegate.createSwitchStatement(discriminant,cases));}
oldInSwitch=state.inSwitch;state.inSwitch=true;defaultFound=false;while(index<length){if(match('}')){break;}
clause=parseSwitchCase();if(clause.test===null){if(defaultFound){throwError({},Messages.MultipleDefaultsInSwitch);}
defaultFound=true;}
cases.push(clause);}
state.inSwitch=oldInSwitch;expect('}');return markerApply(marker,delegate.createSwitchStatement(discriminant,cases));} 
function parseThrowStatement(){var argument,marker=markerCreate();expectKeyword('throw');if(peekLineTerminator()){throwError({},Messages.NewlineAfterThrow);}
argument=parseExpression();consumeSemicolon();return markerApply(marker,delegate.createThrowStatement(argument));} 
function parseCatchClause(){var param,body,marker=markerCreate();expectKeyword('catch');expect('(');if(match(')')){throwUnexpected(lookahead);}
param=parseExpression(); if(strict&&param.type===Syntax.Identifier&&isRestrictedWord(param.name)){throwErrorTolerant({},Messages.StrictCatchVariable);}
expect(')');body=parseBlock();return markerApply(marker,delegate.createCatchClause(param,body));}
function parseTryStatement(){var block,handlers=[],finalizer=null,marker=markerCreate();expectKeyword('try');block=parseBlock();if(matchKeyword('catch')){handlers.push(parseCatchClause());}
if(matchKeyword('finally')){lex();finalizer=parseBlock();}
if(handlers.length===0&&!finalizer){throwError({},Messages.NoCatchOrFinally);}
return markerApply(marker,delegate.createTryStatement(block,[],handlers,finalizer));} 
function parseDebuggerStatement(){var marker=markerCreate();expectKeyword('debugger');consumeSemicolon();return markerApply(marker,delegate.createDebuggerStatement());} 
function parseStatement(){var type=lookahead.type,marker,expr,labeledBody;if(type===Token.EOF){throwUnexpected(lookahead);}
if(type===Token.Punctuator){switch(lookahead.value){case';':return parseEmptyStatement();case'{':return parseBlock();case'(':return parseExpressionStatement();default:break;}}
if(type===Token.Keyword){switch(lookahead.value){case'break':return parseBreakStatement();case'continue':return parseContinueStatement();case'debugger':return parseDebuggerStatement();case'do':return parseDoWhileStatement();case'for':return parseForStatement();case'function':return parseFunctionDeclaration();case'class':return parseClassDeclaration();case'if':return parseIfStatement();case'return':return parseReturnStatement();case'switch':return parseSwitchStatement();case'throw':return parseThrowStatement();case'try':return parseTryStatement();case'var':return parseVariableStatement();case'while':return parseWhileStatement();case'with':return parseWithStatement();default:break;}}
if(matchAsyncFuncExprOrDecl()){return parseFunctionDeclaration();}
marker=markerCreate();expr=parseExpression(); if((expr.type===Syntax.Identifier)&&match(':')){lex();if(state.labelSet.has(expr.name)){throwError({},Messages.Redeclaration,'Label',expr.name);}
state.labelSet.set(expr.name,true);labeledBody=parseStatement();state.labelSet["delete"](expr.name);return markerApply(marker,delegate.createLabeledStatement(expr,labeledBody));}
consumeSemicolon();return markerApply(marker,delegate.createExpressionStatement(expr));} 
function parseConciseBody(){if(match('{')){return parseFunctionSourceElements();}
return parseAssignmentExpression();}
function parseFunctionSourceElements(){var sourceElement,sourceElements=[],token,directive,firstRestricted,oldLabelSet,oldInIteration,oldInSwitch,oldInFunctionBody,oldParenthesizedCount,marker=markerCreate();expect('{');while(index<length){if(lookahead.type!==Token.StringLiteral){break;}
token=lookahead;sourceElement=parseSourceElement();sourceElements.push(sourceElement);if(sourceElement.expression.type!==Syntax.Literal){ break;}
directive=source.slice(token.range[0]+1,token.range[1]-1);if(directive==='use strict'){strict=true;if(firstRestricted){throwErrorTolerant(firstRestricted,Messages.StrictOctalLiteral);}}else{if(!firstRestricted&&token.octal){firstRestricted=token;}}}
oldLabelSet=state.labelSet;oldInIteration=state.inIteration;oldInSwitch=state.inSwitch;oldInFunctionBody=state.inFunctionBody;oldParenthesizedCount=state.parenthesizedCount;state.labelSet=new StringMap();state.inIteration=false;state.inSwitch=false;state.inFunctionBody=true;state.parenthesizedCount=0;while(index<length){if(match('}')){break;}
sourceElement=parseSourceElement();if(typeof sourceElement==='undefined'){break;}
sourceElements.push(sourceElement);}
expect('}');state.labelSet=oldLabelSet;state.inIteration=oldInIteration;state.inSwitch=oldInSwitch;state.inFunctionBody=oldInFunctionBody;state.parenthesizedCount=oldParenthesizedCount;return markerApply(marker,delegate.createBlockStatement(sourceElements));}
function validateParam(options,param,name){if(strict){if(isRestrictedWord(name)){options.stricted=param;options.message=Messages.StrictParamName;}
if(options.paramSet.has(name)){options.stricted=param;options.message=Messages.StrictParamDupe;}}else if(!options.firstRestricted){if(isRestrictedWord(name)){options.firstRestricted=param;options.message=Messages.StrictParamName;}else if(isStrictModeReservedWord(name)){options.firstRestricted=param;options.message=Messages.StrictReservedWord;}else if(options.paramSet.has(name)){options.firstRestricted=param;options.message=Messages.StrictParamDupe;}}
options.paramSet.set(name,true);}
function parseParam(options){var marker,token,rest,param,def;token=lookahead;if(token.value==='...'){token=lex();rest=true;}
if(match('[')){marker=markerCreate();param=parseArrayInitialiser();reinterpretAsDestructuredParameter(options,param);if(match(':')){param.typeAnnotation=parseTypeAnnotation();markerApply(marker,param);}}else if(match('{')){marker=markerCreate();if(rest){throwError({},Messages.ObjectPatternAsRestParameter);}
param=parseObjectInitialiser();reinterpretAsDestructuredParameter(options,param);if(match(':')){param.typeAnnotation=parseTypeAnnotation();markerApply(marker,param);}}else{param=rest?parseTypeAnnotatableIdentifier(false,false ):parseTypeAnnotatableIdentifier(false,true );validateParam(options,token,token.value);}
if(match('=')){if(rest){throwErrorTolerant(lookahead,Messages.DefaultRestParameter);}
lex();def=parseAssignmentExpression();++options.defaultCount;}
if(rest){if(!match(')')){throwError({},Messages.ParameterAfterRestParameter);}
options.rest=param;return false;}
options.params.push(param);options.defaults.push(def);return!match(')');}
function parseParams(firstRestricted){var options,marker=markerCreate();options={params:[],defaultCount:0,defaults:[],rest:null,firstRestricted:firstRestricted};expect('(');if(!match(')')){options.paramSet=new StringMap();while(index<length){if(!parseParam(options)){break;}
expect(',');}}
expect(')');if(options.defaultCount===0){options.defaults=[];}
if(match(':')){options.returnType=parseTypeAnnotation();}
return markerApply(marker,options);}
function parseFunctionDeclaration(){var id,body,token,tmp,firstRestricted,message,generator,isAsync,previousStrict,previousYieldAllowed,previousAwaitAllowed,marker=markerCreate(),typeParameters;isAsync=false;if(matchAsync()){lex();isAsync=true;}
expectKeyword('function');generator=false;if(match('*')){lex();generator=true;}
token=lookahead;id=parseVariableIdentifier();if(match('<')){typeParameters=parseTypeParameterDeclaration();}
if(strict){if(isRestrictedWord(token.value)){throwErrorTolerant(token,Messages.StrictFunctionName);}}else{if(isRestrictedWord(token.value)){firstRestricted=token;message=Messages.StrictFunctionName;}else if(isStrictModeReservedWord(token.value)){firstRestricted=token;message=Messages.StrictReservedWord;}}
tmp=parseParams(firstRestricted);firstRestricted=tmp.firstRestricted;if(tmp.message){message=tmp.message;}
previousStrict=strict;previousYieldAllowed=state.yieldAllowed;state.yieldAllowed=generator;previousAwaitAllowed=state.awaitAllowed;state.awaitAllowed=isAsync;body=parseFunctionSourceElements();if(strict&&firstRestricted){throwError(firstRestricted,message);}
if(strict&&tmp.stricted){throwErrorTolerant(tmp.stricted,message);}
strict=previousStrict;state.yieldAllowed=previousYieldAllowed;state.awaitAllowed=previousAwaitAllowed;return markerApply(marker,delegate.createFunctionDeclaration(id,tmp.params,tmp.defaults,body,tmp.rest,generator,false,isAsync,tmp.returnType,typeParameters));}
function parseFunctionExpression(){var token,id=null,firstRestricted,message,tmp,body,generator,isAsync,previousStrict,previousYieldAllowed,previousAwaitAllowed,marker=markerCreate(),typeParameters;isAsync=false;if(matchAsync()){lex();isAsync=true;}
expectKeyword('function');generator=false;if(match('*')){lex();generator=true;}
if(!match('(')){if(!match('<')){token=lookahead;id=parseVariableIdentifier();if(strict){if(isRestrictedWord(token.value)){throwErrorTolerant(token,Messages.StrictFunctionName);}}else{if(isRestrictedWord(token.value)){firstRestricted=token;message=Messages.StrictFunctionName;}else if(isStrictModeReservedWord(token.value)){firstRestricted=token;message=Messages.StrictReservedWord;}}}
if(match('<')){typeParameters=parseTypeParameterDeclaration();}}
tmp=parseParams(firstRestricted);firstRestricted=tmp.firstRestricted;if(tmp.message){message=tmp.message;}
previousStrict=strict;previousYieldAllowed=state.yieldAllowed;state.yieldAllowed=generator;previousAwaitAllowed=state.awaitAllowed;state.awaitAllowed=isAsync;body=parseFunctionSourceElements();if(strict&&firstRestricted){throwError(firstRestricted,message);}
if(strict&&tmp.stricted){throwErrorTolerant(tmp.stricted,message);}
strict=previousStrict;state.yieldAllowed=previousYieldAllowed;state.awaitAllowed=previousAwaitAllowed;return markerApply(marker,delegate.createFunctionExpression(id,tmp.params,tmp.defaults,body,tmp.rest,generator,false,isAsync,tmp.returnType,typeParameters));}
function parseYieldExpression(){var delegateFlag,expr,marker=markerCreate();expectKeyword('yield',!strict);delegateFlag=false;if(match('*')){lex();delegateFlag=true;}
expr=parseAssignmentExpression();return markerApply(marker,delegate.createYieldExpression(expr,delegateFlag));}
function parseAwaitExpression(){var expr,marker=markerCreate();expectContextualKeyword('await');expr=parseAssignmentExpression();return markerApply(marker,delegate.createAwaitExpression(expr));}

 
function specialMethod(methodDefinition){return methodDefinition.kind==='get'||methodDefinition.kind==='set'||methodDefinition.value.generator;}
function parseMethodDefinition(key,isStatic,generator,computed){var token,param,propType,isAsync,typeParameters,tokenValue,returnType;propType=isStatic?ClassPropertyType["static"]:ClassPropertyType.prototype;if(generator){return delegate.createMethodDefinition(propType,'',key,parsePropertyMethodFunction({generator:true}),computed);}
tokenValue=key.type==='Identifier'&&key.name;if(tokenValue==='get'&&!match('(')){key=parseObjectPropertyKey();expect('(');expect(')');if(match(':')){returnType=parseTypeAnnotation();}
return delegate.createMethodDefinition(propType,'get',key,parsePropertyFunction({generator:false,returnType:returnType}),computed);}
if(tokenValue==='set'&&!match('(')){key=parseObjectPropertyKey();expect('(');token=lookahead;param=[parseTypeAnnotatableIdentifier()];expect(')');if(match(':')){returnType=parseTypeAnnotation();}
return delegate.createMethodDefinition(propType,'set',key,parsePropertyFunction({params:param,generator:false,name:token,returnType:returnType}),computed);}
if(match('<')){typeParameters=parseTypeParameterDeclaration();}
isAsync=tokenValue==='async'&&!match('(');if(isAsync){key=parseObjectPropertyKey();}
return delegate.createMethodDefinition(propType,'',key,parsePropertyMethodFunction({generator:false,async:isAsync,typeParameters:typeParameters}),computed);}
function parseClassProperty(key,computed,isStatic){var typeAnnotation;typeAnnotation=parseTypeAnnotation();expect(';');return delegate.createClassProperty(key,typeAnnotation,computed,isStatic);}
function parseClassElement(){var computed=false,generator=false,key,marker=markerCreate(),isStatic=false,possiblyOpenBracketToken;if(match(';')){lex();return undefined;}
if(lookahead.value==='static'){lex();isStatic=true;}
if(match('*')){lex();generator=true;}
possiblyOpenBracketToken=lookahead;if(matchContextualKeyword('get')||matchContextualKeyword('set')){possiblyOpenBracketToken=lookahead2();}
if(possiblyOpenBracketToken.type===Token.Punctuator&&possiblyOpenBracketToken.value==='['){computed=true;}
key=parseObjectPropertyKey();if(!generator&&lookahead.value===':'){return markerApply(marker,parseClassProperty(key,computed,isStatic));}
return markerApply(marker,parseMethodDefinition(key,isStatic,generator,computed));}
function parseClassBody(){var classElement,classElements=[],existingProps={},marker=markerCreate(),propName,propType;existingProps[ClassPropertyType["static"]]=new StringMap();existingProps[ClassPropertyType.prototype]=new StringMap();expect('{');while(index<length){if(match('}')){break;}
classElement=parseClassElement(existingProps);if(typeof classElement!=='undefined'){classElements.push(classElement);propName=!classElement.computed&&getFieldName(classElement.key);if(propName!==false){propType=classElement["static"]?ClassPropertyType["static"]:ClassPropertyType.prototype;if(classElement.type===Syntax.MethodDefinition){if(propName==='constructor'&&!classElement["static"]){if(specialMethod(classElement)){throwError(classElement,Messages.IllegalClassConstructorProperty);}
if(existingProps[ClassPropertyType.prototype].has('constructor')){throwError(classElement.key,Messages.IllegalDuplicateClassProperty);}}
existingProps[propType].set(propName,true);}}}}
expect('}');return markerApply(marker,delegate.createClassBody(classElements));}
function parseClassImplements(){var id,implemented=[],marker,typeParameters;if(strict){expectKeyword('implements');}else{expectContextualKeyword('implements');}
while(index<length){marker=markerCreate();id=parseVariableIdentifier();if(match('<')){typeParameters=parseTypeParameterInstantiation();}else{typeParameters=null;}
implemented.push(markerApply(marker,delegate.createClassImplements(id,typeParameters)));if(!match(',')){break;}
expect(',');}
return implemented;}
function parseClassExpression(){var id,implemented,previousYieldAllowed,superClass=null,superTypeParameters,marker=markerCreate(),typeParameters,matchImplements;expectKeyword('class');matchImplements=strict?matchKeyword('implements'):matchContextualKeyword('implements');if(!matchKeyword('extends')&&!matchImplements&&!match('{')){id=parseVariableIdentifier();}
if(match('<')){typeParameters=parseTypeParameterDeclaration();}
if(matchKeyword('extends')){expectKeyword('extends');previousYieldAllowed=state.yieldAllowed;state.yieldAllowed=false;superClass=parseLeftHandSideExpressionAllowCall();if(match('<')){superTypeParameters=parseTypeParameterInstantiation();}
state.yieldAllowed=previousYieldAllowed;}
if(strict?matchKeyword('implements'):matchContextualKeyword('implements')){implemented=parseClassImplements();}
return markerApply(marker,delegate.createClassExpression(id,superClass,parseClassBody(),typeParameters,superTypeParameters,implemented));}
function parseClassDeclaration(){var id,implemented,previousYieldAllowed,superClass=null,superTypeParameters,marker=markerCreate(),typeParameters;expectKeyword('class');id=parseVariableIdentifier();if(match('<')){typeParameters=parseTypeParameterDeclaration();}
if(matchKeyword('extends')){expectKeyword('extends');previousYieldAllowed=state.yieldAllowed;state.yieldAllowed=false;superClass=parseLeftHandSideExpressionAllowCall();if(match('<')){superTypeParameters=parseTypeParameterInstantiation();}
state.yieldAllowed=previousYieldAllowed;}
if(strict?matchKeyword('implements'):matchContextualKeyword('implements')){implemented=parseClassImplements();}
return markerApply(marker,delegate.createClassDeclaration(id,superClass,parseClassBody(),typeParameters,superTypeParameters,implemented));} 
function parseSourceElement(){var token;if(lookahead.type===Token.Keyword){switch(lookahead.value){case'const':case'let':return parseConstLetDeclaration(lookahead.value);case'function':return parseFunctionDeclaration();case'export':throwErrorTolerant({},Messages.IllegalExportDeclaration);return parseExportDeclaration();case'import':throwErrorTolerant({},Messages.IllegalImportDeclaration);return parseImportDeclaration();case'interface':if(lookahead2().type===Token.Identifier){return parseInterface();}
return parseStatement();default:return parseStatement();}}
if(matchContextualKeyword('type')&&lookahead2().type===Token.Identifier){return parseTypeAlias();}
if(matchContextualKeyword('interface')&&lookahead2().type===Token.Identifier){return parseInterface();}
if(matchContextualKeyword('declare')){token=lookahead2();if(token.type===Token.Keyword){switch(token.value){case'class':return parseDeclareClass();case'function':return parseDeclareFunction();case'var':return parseDeclareVariable();}}else if(token.type===Token.Identifier&&token.value==='module'){return parseDeclareModule();}}
if(lookahead.type!==Token.EOF){return parseStatement();}}
function parseProgramElement(){var isModule=extra.sourceType==='module'||extra.sourceType==='nonStrictModule';if(isModule&&lookahead.type===Token.Keyword){switch(lookahead.value){case'export':return parseExportDeclaration();case'import':return parseImportDeclaration();}}
return parseSourceElement();}
function parseProgramElements(){var sourceElement,sourceElements=[],token,directive,firstRestricted;while(index<length){token=lookahead;if(token.type!==Token.StringLiteral){break;}
sourceElement=parseProgramElement();sourceElements.push(sourceElement);if(sourceElement.expression.type!==Syntax.Literal){ break;}
directive=source.slice(token.range[0]+1,token.range[1]-1);if(directive==='use strict'){strict=true;if(firstRestricted){throwErrorTolerant(firstRestricted,Messages.StrictOctalLiteral);}}else{if(!firstRestricted&&token.octal){firstRestricted=token;}}}
while(index<length){sourceElement=parseProgramElement();if(typeof sourceElement==='undefined'){break;}
sourceElements.push(sourceElement);}
return sourceElements;}
function parseProgram(){var body,marker=markerCreate();strict=extra.sourceType==='module';peek();body=parseProgramElements();return markerApply(marker,delegate.createProgram(body));} 
XHTMLEntities={quot:'\u0022',amp:'&',apos:'\u0027',lt:'<',gt:'>',nbsp:'\u00A0',iexcl:'\u00A1',cent:'\u00A2',pound:'\u00A3',curren:'\u00A4',yen:'\u00A5',brvbar:'\u00A6',sect:'\u00A7',uml:'\u00A8',copy:'\u00A9',ordf:'\u00AA',laquo:'\u00AB',not:'\u00AC',shy:'\u00AD',reg:'\u00AE',macr:'\u00AF',deg:'\u00B0',plusmn:'\u00B1',sup2:'\u00B2',sup3:'\u00B3',acute:'\u00B4',micro:'\u00B5',para:'\u00B6',middot:'\u00B7',cedil:'\u00B8',sup1:'\u00B9',ordm:'\u00BA',raquo:'\u00BB',frac14:'\u00BC',frac12:'\u00BD',frac34:'\u00BE',iquest:'\u00BF',Agrave:'\u00C0',Aacute:'\u00C1',Acirc:'\u00C2',Atilde:'\u00C3',Auml:'\u00C4',Aring:'\u00C5',AElig:'\u00C6',Ccedil:'\u00C7',Egrave:'\u00C8',Eacute:'\u00C9',Ecirc:'\u00CA',Euml:'\u00CB',Igrave:'\u00CC',Iacute:'\u00CD',Icirc:'\u00CE',Iuml:'\u00CF',ETH:'\u00D0',Ntilde:'\u00D1',Ograve:'\u00D2',Oacute:'\u00D3',Ocirc:'\u00D4',Otilde:'\u00D5',Ouml:'\u00D6',times:'\u00D7',Oslash:'\u00D8',Ugrave:'\u00D9',Uacute:'\u00DA',Ucirc:'\u00DB',Uuml:'\u00DC',Yacute:'\u00DD',THORN:'\u00DE',szlig:'\u00DF',agrave:'\u00E0',aacute:'\u00E1',acirc:'\u00E2',atilde:'\u00E3',auml:'\u00E4',aring:'\u00E5',aelig:'\u00E6',ccedil:'\u00E7',egrave:'\u00E8',eacute:'\u00E9',ecirc:'\u00EA',euml:'\u00EB',igrave:'\u00EC',iacute:'\u00ED',icirc:'\u00EE',iuml:'\u00EF',eth:'\u00F0',ntilde:'\u00F1',ograve:'\u00F2',oacute:'\u00F3',ocirc:'\u00F4',otilde:'\u00F5',ouml:'\u00F6',divide:'\u00F7',oslash:'\u00F8',ugrave:'\u00F9',uacute:'\u00FA',ucirc:'\u00FB',uuml:'\u00FC',yacute:'\u00FD',thorn:'\u00FE',yuml:'\u00FF',OElig:'\u0152',oelig:'\u0153',Scaron:'\u0160',scaron:'\u0161',Yuml:'\u0178',fnof:'\u0192',circ:'\u02C6',tilde:'\u02DC',Alpha:'\u0391',Beta:'\u0392',Gamma:'\u0393',Delta:'\u0394',Epsilon:'\u0395',Zeta:'\u0396',Eta:'\u0397',Theta:'\u0398',Iota:'\u0399',Kappa:'\u039A',Lambda:'\u039B',Mu:'\u039C',Nu:'\u039D',Xi:'\u039E',Omicron:'\u039F',Pi:'\u03A0',Rho:'\u03A1',Sigma:'\u03A3',Tau:'\u03A4',Upsilon:'\u03A5',Phi:'\u03A6',Chi:'\u03A7',Psi:'\u03A8',Omega:'\u03A9',alpha:'\u03B1',beta:'\u03B2',gamma:'\u03B3',delta:'\u03B4',epsilon:'\u03B5',zeta:'\u03B6',eta:'\u03B7',theta:'\u03B8',iota:'\u03B9',kappa:'\u03BA',lambda:'\u03BB',mu:'\u03BC',nu:'\u03BD',xi:'\u03BE',omicron:'\u03BF',pi:'\u03C0',rho:'\u03C1',sigmaf:'\u03C2',sigma:'\u03C3',tau:'\u03C4',upsilon:'\u03C5',phi:'\u03C6',chi:'\u03C7',psi:'\u03C8',omega:'\u03C9',thetasym:'\u03D1',upsih:'\u03D2',piv:'\u03D6',ensp:'\u2002',emsp:'\u2003',thinsp:'\u2009',zwnj:'\u200C',zwj:'\u200D',lrm:'\u200E',rlm:'\u200F',ndash:'\u2013',mdash:'\u2014',lsquo:'\u2018',rsquo:'\u2019',sbquo:'\u201A',ldquo:'\u201C',rdquo:'\u201D',bdquo:'\u201E',dagger:'\u2020',Dagger:'\u2021',bull:'\u2022',hellip:'\u2026',permil:'\u2030',prime:'\u2032',Prime:'\u2033',lsaquo:'\u2039',rsaquo:'\u203A',oline:'\u203E',frasl:'\u2044',euro:'\u20AC',image:'\u2111',weierp:'\u2118',real:'\u211C',trade:'\u2122',alefsym:'\u2135',larr:'\u2190',uarr:'\u2191',rarr:'\u2192',darr:'\u2193',harr:'\u2194',crarr:'\u21B5',lArr:'\u21D0',uArr:'\u21D1',rArr:'\u21D2',dArr:'\u21D3',hArr:'\u21D4',forall:'\u2200',part:'\u2202',exist:'\u2203',empty:'\u2205',nabla:'\u2207',isin:'\u2208',notin:'\u2209',ni:'\u220B',prod:'\u220F',sum:'\u2211',minus:'\u2212',lowast:'\u2217',radic:'\u221A',prop:'\u221D',infin:'\u221E',ang:'\u2220',and:'\u2227',or:'\u2228',cap:'\u2229',cup:'\u222A','int':'\u222B',there4:'\u2234',sim:'\u223C',cong:'\u2245',asymp:'\u2248',ne:'\u2260',equiv:'\u2261',le:'\u2264',ge:'\u2265',sub:'\u2282',sup:'\u2283',nsub:'\u2284',sube:'\u2286',supe:'\u2287',oplus:'\u2295',otimes:'\u2297',perp:'\u22A5',sdot:'\u22C5',lceil:'\u2308',rceil:'\u2309',lfloor:'\u230A',rfloor:'\u230B',lang:'\u2329',rang:'\u232A',loz:'\u25CA',spades:'\u2660',clubs:'\u2663',hearts:'\u2665',diams:'\u2666'};function getQualifiedJSXName(object){if(object.type===Syntax.JSXIdentifier){return object.name;}
if(object.type===Syntax.JSXNamespacedName){return object.namespace.name+':'+object.name.name;}
if(object.type===Syntax.JSXMemberExpression){return(getQualifiedJSXName(object.object)+'.'+
getQualifiedJSXName(object.property));}
throwUnexpected(object);}
function isJSXIdentifierStart(ch){return(ch!==92)&&isIdentifierStart(ch);}
function isJSXIdentifierPart(ch){return(ch!==92)&&(ch===45||isIdentifierPart(ch));}
function scanJSXIdentifier(){var ch,start,value='';start=index;while(index<length){ch=source.charCodeAt(index);if(!isJSXIdentifierPart(ch)){break;}
value+=source[index++];}
return{type:Token.JSXIdentifier,value:value,lineNumber:lineNumber,lineStart:lineStart,range:[start,index]};}
function scanJSXEntity(){var ch,str='',start=index,count=0,code;ch=source[index];assert(ch==='&','Entity must start with an ampersand');index++;while(index<length&&count++<10){ch=source[index++];if(ch===';'){break;}
str+=ch;}
if(ch===';'){if(str[0]==='#'){if(str[1]==='x'){code=+('0'+str.substr(1));}else{code=+str.substr(1).replace(Regex.LeadingZeros,'');}
if(!isNaN(code)){return String.fromCharCode(code);}
}else if(XHTMLEntities[str]){return XHTMLEntities[str];}}
index=start+1;return'&';}
function scanJSXText(stopChars){var ch,str='',start;start=index;while(index<length){ch=source[index];if(stopChars.indexOf(ch)!==-1){break;}
if(ch==='&'){str+=scanJSXEntity();}else{index++;if(ch==='\r'&&source[index]==='\n'){str+=ch;ch=source[index];index++;}
if(isLineTerminator(ch.charCodeAt(0))){++lineNumber;lineStart=index;}
str+=ch;}}
return{type:Token.JSXText,value:str,lineNumber:lineNumber,lineStart:lineStart,range:[start,index]};}
function scanJSXStringLiteral(){var innerToken,quote,start;quote=source[index];assert((quote==='\''||quote==='"'),'String literal must starts with a quote');start=index;++index;innerToken=scanJSXText([quote]);if(quote!==source[index]){throwError({},Messages.UnexpectedToken,'ILLEGAL');}
++index;innerToken.range=[start,index];return innerToken;}
function advanceJSXChild(){var ch=source.charCodeAt(index); if(ch!==60&&ch!==62&&ch!==123&&ch!==125){return scanJSXText(['<','>','{','}']);}
return scanPunctuator();}
function parseJSXIdentifier(){var token,marker=markerCreate();if(lookahead.type!==Token.JSXIdentifier){throwUnexpected(lookahead);}
token=lex();return markerApply(marker,delegate.createJSXIdentifier(token.value));}
function parseJSXNamespacedName(){var namespace,name,marker=markerCreate();namespace=parseJSXIdentifier();expect(':');name=parseJSXIdentifier();return markerApply(marker,delegate.createJSXNamespacedName(namespace,name));}
function parseJSXMemberExpression(){var marker=markerCreate(),expr=parseJSXIdentifier();while(match('.')){lex();expr=markerApply(marker,delegate.createJSXMemberExpression(expr,parseJSXIdentifier()));}
return expr;}
function parseJSXElementName(){if(lookahead2().value===':'){return parseJSXNamespacedName();}
if(lookahead2().value==='.'){return parseJSXMemberExpression();}
return parseJSXIdentifier();}
function parseJSXAttributeName(){if(lookahead2().value===':'){return parseJSXNamespacedName();}
return parseJSXIdentifier();}
function parseJSXAttributeValue(){var value,marker;if(match('{')){value=parseJSXExpressionContainer();if(value.expression.type===Syntax.JSXEmptyExpression){throwError(value,'JSX attributes must only be assigned a non-empty '+'expression');}}else if(match('<')){value=parseJSXElement();}else if(lookahead.type===Token.JSXText){marker=markerCreate();value=markerApply(marker,delegate.createLiteral(lex()));}else{throwError({},Messages.InvalidJSXAttributeValue);}
return value;}
function parseJSXEmptyExpression(){var marker=markerCreatePreserveWhitespace();while(source.charAt(index)!=='}'){index++;}
return markerApply(marker,delegate.createJSXEmptyExpression());}
function parseJSXExpressionContainer(){var expression,origInJSXChild,origInJSXTag,marker=markerCreate();origInJSXChild=state.inJSXChild;origInJSXTag=state.inJSXTag;state.inJSXChild=false;state.inJSXTag=false;expect('{');if(match('}')){expression=parseJSXEmptyExpression();}else{expression=parseExpression();}
state.inJSXChild=origInJSXChild;state.inJSXTag=origInJSXTag;expect('}');return markerApply(marker,delegate.createJSXExpressionContainer(expression));}
function parseJSXSpreadAttribute(){var expression,origInJSXChild,origInJSXTag,marker=markerCreate();origInJSXChild=state.inJSXChild;origInJSXTag=state.inJSXTag;state.inJSXChild=false;state.inJSXTag=false;expect('{');expect('...');expression=parseAssignmentExpression();state.inJSXChild=origInJSXChild;state.inJSXTag=origInJSXTag;expect('}');return markerApply(marker,delegate.createJSXSpreadAttribute(expression));}
function parseJSXAttribute(){var name,marker;if(match('{')){return parseJSXSpreadAttribute();}
marker=markerCreate();name=parseJSXAttributeName(); if(match('=')){lex();return markerApply(marker,delegate.createJSXAttribute(name,parseJSXAttributeValue()));}
return markerApply(marker,delegate.createJSXAttribute(name));}
function parseJSXChild(){var token,marker;if(match('{')){token=parseJSXExpressionContainer();}else if(lookahead.type===Token.JSXText){marker=markerCreatePreserveWhitespace();token=markerApply(marker,delegate.createLiteral(lex()));}else if(match('<')){token=parseJSXElement();}else{throwUnexpected(lookahead);}
return token;}
function parseJSXClosingElement(){var name,origInJSXChild,origInJSXTag,marker=markerCreate();origInJSXChild=state.inJSXChild;origInJSXTag=state.inJSXTag;state.inJSXChild=false;state.inJSXTag=true;expect('<');expect('/');name=parseJSXElementName();

 state.inJSXChild=origInJSXChild;state.inJSXTag=origInJSXTag;expect('>');return markerApply(marker,delegate.createJSXClosingElement(name));}
function parseJSXOpeningElement(){var name,attributes=[],selfClosing=false,origInJSXChild,origInJSXTag,marker=markerCreate();origInJSXChild=state.inJSXChild;origInJSXTag=state.inJSXTag;state.inJSXChild=false;state.inJSXTag=true;expect('<');name=parseJSXElementName();while(index<length&&lookahead.value!=='/'&&lookahead.value!=='>'){attributes.push(parseJSXAttribute());}
state.inJSXTag=origInJSXTag;if(lookahead.value==='/'){expect('/');

 state.inJSXChild=origInJSXChild;expect('>');selfClosing=true;}else{state.inJSXChild=true;expect('>');}
return markerApply(marker,delegate.createJSXOpeningElement(name,attributes,selfClosing));}
function parseJSXElement(){var openingElement,closingElement=null,children=[],origInJSXChild,origInJSXTag,marker=markerCreate();origInJSXChild=state.inJSXChild;origInJSXTag=state.inJSXTag;openingElement=parseJSXOpeningElement();if(!openingElement.selfClosing){while(index<length){state.inJSXChild=false; if(lookahead.value==='<'&&lookahead2().value==='/'){break;}
state.inJSXChild=true;children.push(parseJSXChild());}
state.inJSXChild=origInJSXChild;state.inJSXTag=origInJSXTag;closingElement=parseJSXClosingElement();if(getQualifiedJSXName(closingElement.name)!==getQualifiedJSXName(openingElement.name)){throwError({},Messages.ExpectedJSXClosingTag,getQualifiedJSXName(openingElement.name));}}





if(!origInJSXChild&&match('<')){throwError(lookahead,Messages.AdjacentJSXElements);}
return markerApply(marker,delegate.createJSXElement(openingElement,closingElement,children));}
function parseTypeAlias(){var id,marker=markerCreate(),typeParameters=null,right;expectContextualKeyword('type');id=parseVariableIdentifier();if(match('<')){typeParameters=parseTypeParameterDeclaration();}
expect('=');right=parseType();consumeSemicolon();return markerApply(marker,delegate.createTypeAlias(id,typeParameters,right));}
function parseInterfaceExtends(){var marker=markerCreate(),id,typeParameters=null;id=parseVariableIdentifier();if(match('<')){typeParameters=parseTypeParameterInstantiation();}
return markerApply(marker,delegate.createInterfaceExtends(id,typeParameters));}
function parseInterfaceish(marker,allowStatic){var body,bodyMarker,extended=[],id,typeParameters=null;id=parseVariableIdentifier();if(match('<')){typeParameters=parseTypeParameterDeclaration();}
if(matchKeyword('extends')){expectKeyword('extends');while(index<length){extended.push(parseInterfaceExtends());if(!match(',')){break;}
expect(',');}}
bodyMarker=markerCreate();body=markerApply(bodyMarker,parseObjectType(allowStatic));return markerApply(marker,delegate.createInterface(id,typeParameters,body,extended));}
function parseInterface(){var marker=markerCreate();if(strict){expectKeyword('interface');}else{expectContextualKeyword('interface');}
return parseInterfaceish(marker,false);}
function parseDeclareClass(){var marker=markerCreate(),ret;expectContextualKeyword('declare');expectKeyword('class');ret=parseInterfaceish(marker,true);ret.type=Syntax.DeclareClass;return ret;}
function parseDeclareFunction(){var id,idMarker,marker=markerCreate(),params,returnType,rest,tmp,typeParameters=null,value,valueMarker;expectContextualKeyword('declare');expectKeyword('function');idMarker=markerCreate();id=parseVariableIdentifier();valueMarker=markerCreate();if(match('<')){typeParameters=parseTypeParameterDeclaration();}
expect('(');tmp=parseFunctionTypeParams();params=tmp.params;rest=tmp.rest;expect(')');expect(':');returnType=parseType();value=markerApply(valueMarker,delegate.createFunctionTypeAnnotation(params,returnType,rest,typeParameters));id.typeAnnotation=markerApply(valueMarker,delegate.createTypeAnnotation(value));markerApply(idMarker,id);consumeSemicolon();return markerApply(marker,delegate.createDeclareFunction(id));}
function parseDeclareVariable(){var id,marker=markerCreate();expectContextualKeyword('declare');expectKeyword('var');id=parseTypeAnnotatableIdentifier();consumeSemicolon();return markerApply(marker,delegate.createDeclareVariable(id));}
function parseDeclareModule(){var body=[],bodyMarker,id,idMarker,marker=markerCreate(),token;expectContextualKeyword('declare');expectContextualKeyword('module');if(lookahead.type===Token.StringLiteral){if(strict&&lookahead.octal){throwErrorTolerant(lookahead,Messages.StrictOctalLiteral);}
idMarker=markerCreate();id=markerApply(idMarker,delegate.createLiteral(lex()));}else{id=parseVariableIdentifier();}
bodyMarker=markerCreate();expect('{');while(index<length&&!match('}')){token=lookahead2();switch(token.value){case'class':body.push(parseDeclareClass());break;case'function':body.push(parseDeclareFunction());break;case'var':body.push(parseDeclareVariable());break;default:throwUnexpected(lookahead);}}
expect('}');return markerApply(marker,delegate.createDeclareModule(id,markerApply(bodyMarker,delegate.createBlockStatement(body))));}
function collectToken(){var loc,token,range,value,entry;if(!state.inJSXChild){skipComment();}
loc={start:{line:lineNumber,column:index-lineStart}};token=extra.advance();loc.end={line:lineNumber,column:index-lineStart};if(token.type!==Token.EOF){range=[token.range[0],token.range[1]];value=source.slice(token.range[0],token.range[1]);entry={type:TokenName[token.type],value:value,range:range,loc:loc};if(token.regex){entry.regex={pattern:token.regex.pattern,flags:token.regex.flags};}
extra.tokens.push(entry);}
return token;}
function collectRegex(){var pos,loc,regex,token;skipComment();pos=index;loc={start:{line:lineNumber,column:index-lineStart}};regex=extra.scanRegExp();loc.end={line:lineNumber,column:index-lineStart};if(!extra.tokenize){if(extra.tokens.length>0){token=extra.tokens[extra.tokens.length-1];if(token.range[0]===pos&&token.type==='Punctuator'){if(token.value==='/'||token.value==='/='){extra.tokens.pop();}}}
extra.tokens.push({type:'RegularExpression',value:regex.literal,regex:regex.regex,range:[pos,index],loc:loc});}
return regex;}
function filterTokenLocation(){var i,entry,token,tokens=[];for(i=0;i<extra.tokens.length;++i){entry=extra.tokens[i];token={type:entry.type,value:entry.value};if(entry.regex){token.regex={pattern:entry.regex.pattern,flags:entry.regex.flags};}
if(extra.range){token.range=entry.range;}
if(extra.loc){token.loc=entry.loc;}
tokens.push(token);}
extra.tokens=tokens;}
function patch(){if(typeof extra.tokens!=='undefined'){extra.advance=advance;extra.scanRegExp=scanRegExp;advance=collectToken;scanRegExp=collectRegex;}}
function unpatch(){if(typeof extra.scanRegExp==='function'){advance=extra.advance;scanRegExp=extra.scanRegExp;}}
function extend(object,properties){var entry,result={};for(entry in object){if(object.hasOwnProperty(entry)){result[entry]=object[entry];}}
for(entry in properties){if(properties.hasOwnProperty(entry)){result[entry]=properties[entry];}}
return result;}
function tokenize(code,options){var toString,token,tokens;toString=String;if(typeof code!=='string'&&!(code instanceof String)){code=toString(code);}
delegate=SyntaxTreeDelegate;source=code;index=0;lineNumber=(source.length>0)?1:0;lineStart=0;length=source.length;lookahead=null;state={allowKeyword:true,allowIn:true,labelSet:new StringMap(),inFunctionBody:false,inIteration:false,inSwitch:false,lastCommentStart:-1};extra={};options=options||{};options.tokens=true;extra.tokens=[];extra.tokenize=true;extra.openParenToken=-1;extra.openCurlyToken=-1;extra.range=(typeof options.range==='boolean')&&options.range;extra.loc=(typeof options.loc==='boolean')&&options.loc;if(typeof options.comment==='boolean'&&options.comment){extra.comments=[];}
if(typeof options.tolerant==='boolean'&&options.tolerant){extra.errors=[];}
patch();try{peek();if(lookahead.type===Token.EOF){return extra.tokens;}
token=lex();while(lookahead.type!==Token.EOF){try{token=lex();}catch(lexError){token=lookahead;if(extra.errors){extra.errors.push(lexError);
break;}else{throw lexError;}}}
filterTokenLocation();tokens=extra.tokens;if(typeof extra.comments!=='undefined'){tokens.comments=extra.comments;}
if(typeof extra.errors!=='undefined'){tokens.errors=extra.errors;}}catch(e){throw e;}finally{unpatch();extra={};}
return tokens;}
function parse(code,options){var program,toString;toString=String;if(typeof code!=='string'&&!(code instanceof String)){code=toString(code);}
delegate=SyntaxTreeDelegate;source=code;index=0;lineNumber=(source.length>0)?1:0;lineStart=0;length=source.length;lookahead=null;state={allowKeyword:false,allowIn:true,labelSet:new StringMap(),parenthesizedCount:0,inFunctionBody:false,inIteration:false,inSwitch:false,inJSXChild:false,inJSXTag:false,inType:false,lastCommentStart:-1,yieldAllowed:false,awaitAllowed:false};extra={};if(typeof options!=='undefined'){extra.range=(typeof options.range==='boolean')&&options.range;extra.loc=(typeof options.loc==='boolean')&&options.loc;extra.attachComment=(typeof options.attachComment==='boolean')&&options.attachComment;if(extra.loc&&options.source!==null&&options.source!==undefined){delegate=extend(delegate,{'postProcess':function(node){node.loc.source=toString(options.source);return node;}});}
extra.sourceType=options.sourceType;if(typeof options.tokens==='boolean'&&options.tokens){extra.tokens=[];}
if(typeof options.comment==='boolean'&&options.comment){extra.comments=[];}
if(typeof options.tolerant==='boolean'&&options.tolerant){extra.errors=[];}
if(extra.attachComment){extra.range=true;extra.comments=[];extra.bottomRightStack=[];extra.trailingComments=[];extra.leadingComments=[];}}
patch();try{program=parseProgram();if(typeof extra.comments!=='undefined'){program.comments=extra.comments;}
if(typeof extra.tokens!=='undefined'){filterTokenLocation();program.tokens=extra.tokens;}
if(typeof extra.errors!=='undefined'){program.errors=extra.errors;}}catch(e){throw e;}finally{unpatch();extra={};}
return program;}
exports.version='13001.1001.0-dev-harmony-fb';exports.tokenize=tokenize;exports.parse=parse;exports.Syntax=(function(){var name,types={};if(typeof Object.create==='function'){types=Object.create(null);}
for(name in Syntax){if(Syntax.hasOwnProperty(name)){types[name]=Syntax[name];}}
if(typeof Object.freeze==='function'){Object.freeze(types);}
return types;}());}));},{}],10:[function(_dereq_,module,exports){var Base62=(function(my){my.chars=["0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
my.encode=function(i){if(i===0){return'0'}
var s=''
while(i>0){s=this.chars[i%62]+s
i=Math.floor(i/62)}
return s};my.decode=function(a,b,c,d){for(b=c=(a===(/\W|_|^$/.test(a+="")||a))-1;d=a.charCodeAt(c++);)
b=b*62+d-[,48,29,87][d>>5];return b};return my;}({}));module.exports=Base62},{}],11:[function(_dereq_,module,exports){exports.SourceMapGenerator=_dereq_('./source-map/source-map-generator').SourceMapGenerator;exports.SourceMapConsumer=_dereq_('./source-map/source-map-consumer').SourceMapConsumer;exports.SourceNode=_dereq_('./source-map/source-node').SourceNode;},{"./source-map/source-map-consumer":16,"./source-map/source-map-generator":17,"./source-map/source-node":18}],12:[function(_dereq_,module,exports){if(typeof define!=='function'){var define=_dereq_('amdefine')(module,_dereq_);}
define(function(_dereq_,exports,module){var util=_dereq_('./util');function ArraySet(){this._array=[];this._set={};}
ArraySet.fromArray=function ArraySet_fromArray(aArray,aAllowDuplicates){var set=new ArraySet();for(var i=0,len=aArray.length;i<len;i++){set.add(aArray[i],aAllowDuplicates);}
return set;};ArraySet.prototype.add=function ArraySet_add(aStr,aAllowDuplicates){var isDuplicate=this.has(aStr);var idx=this._array.length;if(!isDuplicate||aAllowDuplicates){this._array.push(aStr);}
if(!isDuplicate){this._set[util.toSetString(aStr)]=idx;}};ArraySet.prototype.has=function ArraySet_has(aStr){return Object.prototype.hasOwnProperty.call(this._set,util.toSetString(aStr));};ArraySet.prototype.indexOf=function ArraySet_indexOf(aStr){if(this.has(aStr)){return this._set[util.toSetString(aStr)];}
throw new Error('"'+aStr+'" is not in the set.');};ArraySet.prototype.at=function ArraySet_at(aIdx){if(aIdx>=0&&aIdx<this._array.length){return this._array[aIdx];}
throw new Error('No element indexed by '+aIdx);};ArraySet.prototype.toArray=function ArraySet_toArray(){return this._array.slice();};exports.ArraySet=ArraySet;});},{"./util":19,"amdefine":20}],13:[function(_dereq_,module,exports){if(typeof define!=='function'){var define=_dereq_('amdefine')(module,_dereq_);}
define(function(_dereq_,exports,module){var base64=_dereq_('./base64');





 var VLQ_BASE_SHIFT=5; var VLQ_BASE=1<<VLQ_BASE_SHIFT; var VLQ_BASE_MASK=VLQ_BASE-1; var VLQ_CONTINUATION_BIT=VLQ_BASE;function toVLQSigned(aValue){return aValue<0?((-aValue)<<1)+1:(aValue<<1)+0;}
function fromVLQSigned(aValue){var isNegative=(aValue&1)===1;var shifted=aValue>>1;return isNegative?-shifted:shifted;}
exports.encode=function base64VLQ_encode(aValue){var encoded="";var digit;var vlq=toVLQSigned(aValue);do{digit=vlq&VLQ_BASE_MASK;vlq>>>=VLQ_BASE_SHIFT;if(vlq>0){
digit|=VLQ_CONTINUATION_BIT;}
encoded+=base64.encode(digit);}while(vlq>0);return encoded;};exports.decode=function base64VLQ_decode(aStr){var i=0;var strLen=aStr.length;var result=0;var shift=0;var continuation,digit;do{if(i>=strLen){throw new Error("Expected more digits in base 64 VLQ value.");}
digit=base64.decode(aStr.charAt(i++));continuation=!!(digit&VLQ_CONTINUATION_BIT);digit&=VLQ_BASE_MASK;result=result+(digit<<shift);shift+=VLQ_BASE_SHIFT;}while(continuation);return{value:fromVLQSigned(result),rest:aStr.slice(i)};};});},{"./base64":14,"amdefine":20}],14:[function(_dereq_,module,exports){if(typeof define!=='function'){var define=_dereq_('amdefine')(module,_dereq_);}
define(function(_dereq_,exports,module){var charToIntMap={};var intToCharMap={};'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'.split('').forEach(function(ch,index){charToIntMap[ch]=index;intToCharMap[index]=ch;});exports.encode=function base64_encode(aNumber){if(aNumber in intToCharMap){return intToCharMap[aNumber];}
throw new TypeError("Must be between 0 and 63: "+aNumber);};exports.decode=function base64_decode(aChar){if(aChar in charToIntMap){return charToIntMap[aChar];}
throw new TypeError("Not a valid base 64 digit: "+aChar);};});},{"amdefine":20}],15:[function(_dereq_,module,exports){if(typeof define!=='function'){var define=_dereq_('amdefine')(module,_dereq_);}
define(function(_dereq_,exports,module){function recursiveSearch(aLow,aHigh,aNeedle,aHaystack,aCompare){


var mid=Math.floor((aHigh-aLow)/2)+aLow;var cmp=aCompare(aNeedle,aHaystack[mid],true);if(cmp===0){return aHaystack[mid];}
else if(cmp>0){if(aHigh-mid>1){return recursiveSearch(mid,aHigh,aNeedle,aHaystack,aCompare);}

return aHaystack[mid];}
else{if(mid-aLow>1){return recursiveSearch(aLow,mid,aNeedle,aHaystack,aCompare);}

return aLow<0?null:aHaystack[aLow];}}
exports.search=function search(aNeedle,aHaystack,aCompare){return aHaystack.length>0?recursiveSearch(-1,aHaystack.length,aNeedle,aHaystack,aCompare):null;};});},{"amdefine":20}],16:[function(_dereq_,module,exports){if(typeof define!=='function'){var define=_dereq_('amdefine')(module,_dereq_);}
define(function(_dereq_,exports,module){var util=_dereq_('./util');var binarySearch=_dereq_('./binary-search');var ArraySet=_dereq_('./array-set').ArraySet;var base64VLQ=_dereq_('./base64-vlq');function SourceMapConsumer(aSourceMap){var sourceMap=aSourceMap;if(typeof aSourceMap==='string'){sourceMap=JSON.parse(aSourceMap.replace(/^\)\]\}'/,''));}
var version=util.getArg(sourceMap,'version');var sources=util.getArg(sourceMap,'sources');
var names=util.getArg(sourceMap,'names',[]);var sourceRoot=util.getArg(sourceMap,'sourceRoot',null);var sourcesContent=util.getArg(sourceMap,'sourcesContent',null);var mappings=util.getArg(sourceMap,'mappings');var file=util.getArg(sourceMap,'file',null);
if(version!=this._version){throw new Error('Unsupported version: '+version);}



this._names=ArraySet.fromArray(names,true);this._sources=ArraySet.fromArray(sources,true);this.sourceRoot=sourceRoot;this.sourcesContent=sourcesContent;this._mappings=mappings;this.file=file;}
SourceMapConsumer.fromSourceMap=function SourceMapConsumer_fromSourceMap(aSourceMap){var smc=Object.create(SourceMapConsumer.prototype);smc._names=ArraySet.fromArray(aSourceMap._names.toArray(),true);smc._sources=ArraySet.fromArray(aSourceMap._sources.toArray(),true);smc.sourceRoot=aSourceMap._sourceRoot;smc.sourcesContent=aSourceMap._generateSourcesContent(smc._sources.toArray(),smc.sourceRoot);smc.file=aSourceMap._file;smc.__generatedMappings=aSourceMap._mappings.slice().sort(util.compareByGeneratedPositions);smc.__originalMappings=aSourceMap._mappings.slice().sort(util.compareByOriginalPositions);return smc;}; SourceMapConsumer.prototype._version=3;Object.defineProperty(SourceMapConsumer.prototype,'sources',{get:function(){return this._sources.toArray().map(function(s){return this.sourceRoot?util.join(this.sourceRoot,s):s;},this);}});











SourceMapConsumer.prototype.__generatedMappings=null;Object.defineProperty(SourceMapConsumer.prototype,'_generatedMappings',{get:function(){if(!this.__generatedMappings){this.__generatedMappings=[];this.__originalMappings=[];this._parseMappings(this._mappings,this.sourceRoot);}
return this.__generatedMappings;}});SourceMapConsumer.prototype.__originalMappings=null;Object.defineProperty(SourceMapConsumer.prototype,'_originalMappings',{get:function(){if(!this.__originalMappings){this.__generatedMappings=[];this.__originalMappings=[];this._parseMappings(this._mappings,this.sourceRoot);}
return this.__originalMappings;}});SourceMapConsumer.prototype._parseMappings=function SourceMapConsumer_parseMappings(aStr,aSourceRoot){var generatedLine=1;var previousGeneratedColumn=0;var previousOriginalLine=0;var previousOriginalColumn=0;var previousSource=0;var previousName=0;var mappingSeparator=/^[,;]/;var str=aStr;var mapping;var temp;while(str.length>0){if(str.charAt(0)===';'){generatedLine++;str=str.slice(1);previousGeneratedColumn=0;}
else if(str.charAt(0)===','){str=str.slice(1);}
else{mapping={};mapping.generatedLine=generatedLine;temp=base64VLQ.decode(str);mapping.generatedColumn=previousGeneratedColumn+temp.value;previousGeneratedColumn=mapping.generatedColumn;str=temp.rest;if(str.length>0&&!mappingSeparator.test(str.charAt(0))){temp=base64VLQ.decode(str);mapping.source=this._sources.at(previousSource+temp.value);previousSource+=temp.value;str=temp.rest;if(str.length===0||mappingSeparator.test(str.charAt(0))){throw new Error('Found a source, but no line and column');}
temp=base64VLQ.decode(str);mapping.originalLine=previousOriginalLine+temp.value;previousOriginalLine=mapping.originalLine; mapping.originalLine+=1;str=temp.rest;if(str.length===0||mappingSeparator.test(str.charAt(0))){throw new Error('Found a source and line, but no column');}
temp=base64VLQ.decode(str);mapping.originalColumn=previousOriginalColumn+temp.value;previousOriginalColumn=mapping.originalColumn;str=temp.rest;if(str.length>0&&!mappingSeparator.test(str.charAt(0))){temp=base64VLQ.decode(str);mapping.name=this._names.at(previousName+temp.value);previousName+=temp.value;str=temp.rest;}}
this.__generatedMappings.push(mapping);if(typeof mapping.originalLine==='number'){this.__originalMappings.push(mapping);}}}
this.__originalMappings.sort(util.compareByOriginalPositions);};SourceMapConsumer.prototype._findMapping=function SourceMapConsumer_findMapping(aNeedle,aMappings,aLineName,aColumnName,aComparator){


if(aNeedle[aLineName]<=0){throw new TypeError('Line must be greater than or equal to 1, got '
+aNeedle[aLineName]);}
if(aNeedle[aColumnName]<0){throw new TypeError('Column must be greater than or equal to 0, got '
+aNeedle[aColumnName]);}
return binarySearch.search(aNeedle,aMappings,aComparator);};SourceMapConsumer.prototype.originalPositionFor=function SourceMapConsumer_originalPositionFor(aArgs){var needle={generatedLine:util.getArg(aArgs,'line'),generatedColumn:util.getArg(aArgs,'column')};var mapping=this._findMapping(needle,this._generatedMappings,"generatedLine","generatedColumn",util.compareByGeneratedPositions);if(mapping){var source=util.getArg(mapping,'source',null);if(source&&this.sourceRoot){source=util.join(this.sourceRoot,source);}
return{source:source,line:util.getArg(mapping,'originalLine',null),column:util.getArg(mapping,'originalColumn',null),name:util.getArg(mapping,'name',null)};}
return{source:null,line:null,column:null,name:null};};SourceMapConsumer.prototype.sourceContentFor=function SourceMapConsumer_sourceContentFor(aSource){if(!this.sourcesContent){return null;}
if(this.sourceRoot){aSource=util.relative(this.sourceRoot,aSource);}
if(this._sources.has(aSource)){return this.sourcesContent[this._sources.indexOf(aSource)];}
var url;if(this.sourceRoot&&(url=util.urlParse(this.sourceRoot))){


var fileUriAbsPath=aSource.replace(/^file:\/\//,"");if(url.scheme=="file"&&this._sources.has(fileUriAbsPath)){return this.sourcesContent[this._sources.indexOf(fileUriAbsPath)]}
if((!url.path||url.path=="/")&&this._sources.has("/"+aSource)){return this.sourcesContent[this._sources.indexOf("/"+aSource)];}}
throw new Error('"'+aSource+'" is not in the SourceMap.');};SourceMapConsumer.prototype.generatedPositionFor=function SourceMapConsumer_generatedPositionFor(aArgs){var needle={source:util.getArg(aArgs,'source'),originalLine:util.getArg(aArgs,'line'),originalColumn:util.getArg(aArgs,'column')};if(this.sourceRoot){needle.source=util.relative(this.sourceRoot,needle.source);}
var mapping=this._findMapping(needle,this._originalMappings,"originalLine","originalColumn",util.compareByOriginalPositions);if(mapping){return{line:util.getArg(mapping,'generatedLine',null),column:util.getArg(mapping,'generatedColumn',null)};}
return{line:null,column:null};};SourceMapConsumer.GENERATED_ORDER=1;SourceMapConsumer.ORIGINAL_ORDER=2;SourceMapConsumer.prototype.eachMapping=function SourceMapConsumer_eachMapping(aCallback,aContext,aOrder){var context=aContext||null;var order=aOrder||SourceMapConsumer.GENERATED_ORDER;var mappings;switch(order){case SourceMapConsumer.GENERATED_ORDER:mappings=this._generatedMappings;break;case SourceMapConsumer.ORIGINAL_ORDER:mappings=this._originalMappings;break;default:throw new Error("Unknown order of iteration.");}
var sourceRoot=this.sourceRoot;mappings.map(function(mapping){var source=mapping.source;if(source&&sourceRoot){source=util.join(sourceRoot,source);}
return{source:source,generatedLine:mapping.generatedLine,generatedColumn:mapping.generatedColumn,originalLine:mapping.originalLine,originalColumn:mapping.originalColumn,name:mapping.name};}).forEach(aCallback,context);};exports.SourceMapConsumer=SourceMapConsumer;});},{"./array-set":12,"./base64-vlq":13,"./binary-search":15,"./util":19,"amdefine":20}],17:[function(_dereq_,module,exports){if(typeof define!=='function'){var define=_dereq_('amdefine')(module,_dereq_);}
define(function(_dereq_,exports,module){var base64VLQ=_dereq_('./base64-vlq');var util=_dereq_('./util');var ArraySet=_dereq_('./array-set').ArraySet;function SourceMapGenerator(aArgs){this._file=util.getArg(aArgs,'file');this._sourceRoot=util.getArg(aArgs,'sourceRoot',null);this._sources=new ArraySet();this._names=new ArraySet();this._mappings=[];this._sourcesContents=null;}
SourceMapGenerator.prototype._version=3;SourceMapGenerator.fromSourceMap=function SourceMapGenerator_fromSourceMap(aSourceMapConsumer){var sourceRoot=aSourceMapConsumer.sourceRoot;var generator=new SourceMapGenerator({file:aSourceMapConsumer.file,sourceRoot:sourceRoot});aSourceMapConsumer.eachMapping(function(mapping){var newMapping={generated:{line:mapping.generatedLine,column:mapping.generatedColumn}};if(mapping.source){newMapping.source=mapping.source;if(sourceRoot){newMapping.source=util.relative(sourceRoot,newMapping.source);}
newMapping.original={line:mapping.originalLine,column:mapping.originalColumn};if(mapping.name){newMapping.name=mapping.name;}}
generator.addMapping(newMapping);});aSourceMapConsumer.sources.forEach(function(sourceFile){var content=aSourceMapConsumer.sourceContentFor(sourceFile);if(content){generator.setSourceContent(sourceFile,content);}});return generator;};SourceMapGenerator.prototype.addMapping=function SourceMapGenerator_addMapping(aArgs){var generated=util.getArg(aArgs,'generated');var original=util.getArg(aArgs,'original',null);var source=util.getArg(aArgs,'source',null);var name=util.getArg(aArgs,'name',null);this._validateMapping(generated,original,source,name);if(source&&!this._sources.has(source)){this._sources.add(source);}
if(name&&!this._names.has(name)){this._names.add(name);}
this._mappings.push({generatedLine:generated.line,generatedColumn:generated.column,originalLine:original!=null&&original.line,originalColumn:original!=null&&original.column,source:source,name:name});};SourceMapGenerator.prototype.setSourceContent=function SourceMapGenerator_setSourceContent(aSourceFile,aSourceContent){var source=aSourceFile;if(this._sourceRoot){source=util.relative(this._sourceRoot,source);}
if(aSourceContent!==null){if(!this._sourcesContents){this._sourcesContents={};}
this._sourcesContents[util.toSetString(source)]=aSourceContent;}else{delete this._sourcesContents[util.toSetString(source)];if(Object.keys(this._sourcesContents).length===0){this._sourcesContents=null;}}};SourceMapGenerator.prototype.applySourceMap=function SourceMapGenerator_applySourceMap(aSourceMapConsumer,aSourceFile){ if(!aSourceFile){aSourceFile=aSourceMapConsumer.file;}
var sourceRoot=this._sourceRoot;if(sourceRoot){aSourceFile=util.relative(sourceRoot,aSourceFile);}

var newSources=new ArraySet();var newNames=new ArraySet();this._mappings.forEach(function(mapping){if(mapping.source===aSourceFile&&mapping.originalLine){var original=aSourceMapConsumer.originalPositionFor({line:mapping.originalLine,column:mapping.originalColumn});if(original.source!==null){ if(sourceRoot){mapping.source=util.relative(sourceRoot,original.source);}else{mapping.source=original.source;}
mapping.originalLine=original.line;mapping.originalColumn=original.column;if(original.name!==null&&mapping.name!==null){
 mapping.name=original.name;}}}
var source=mapping.source;if(source&&!newSources.has(source)){newSources.add(source);}
var name=mapping.name;if(name&&!newNames.has(name)){newNames.add(name);}},this);this._sources=newSources;this._names=newNames;aSourceMapConsumer.sources.forEach(function(sourceFile){var content=aSourceMapConsumer.sourceContentFor(sourceFile);if(content){if(sourceRoot){sourceFile=util.relative(sourceRoot,sourceFile);}
this.setSourceContent(sourceFile,content);}},this);};SourceMapGenerator.prototype._validateMapping=function SourceMapGenerator_validateMapping(aGenerated,aOriginal,aSource,aName){if(aGenerated&&'line'in aGenerated&&'column'in aGenerated&&aGenerated.line>0&&aGenerated.column>=0&&!aOriginal&&!aSource&&!aName){return;}
else if(aGenerated&&'line'in aGenerated&&'column'in aGenerated&&aOriginal&&'line'in aOriginal&&'column'in aOriginal&&aGenerated.line>0&&aGenerated.column>=0&&aOriginal.line>0&&aOriginal.column>=0&&aSource){return;}
else{throw new Error('Invalid mapping: '+JSON.stringify({generated:aGenerated,source:aSource,orginal:aOriginal,name:aName}));}};SourceMapGenerator.prototype._serializeMappings=function SourceMapGenerator_serializeMappings(){var previousGeneratedColumn=0;var previousGeneratedLine=1;var previousOriginalColumn=0;var previousOriginalLine=0;var previousName=0;var previousSource=0;var result='';var mapping;



this._mappings.sort(util.compareByGeneratedPositions);for(var i=0,len=this._mappings.length;i<len;i++){mapping=this._mappings[i];if(mapping.generatedLine!==previousGeneratedLine){previousGeneratedColumn=0;while(mapping.generatedLine!==previousGeneratedLine){result+=';';previousGeneratedLine++;}}
else{if(i>0){if(!util.compareByGeneratedPositions(mapping,this._mappings[i-1])){continue;}
result+=',';}}
result+=base64VLQ.encode(mapping.generatedColumn
-previousGeneratedColumn);previousGeneratedColumn=mapping.generatedColumn;if(mapping.source){result+=base64VLQ.encode(this._sources.indexOf(mapping.source)
-previousSource);previousSource=this._sources.indexOf(mapping.source); result+=base64VLQ.encode(mapping.originalLine-1
-previousOriginalLine);previousOriginalLine=mapping.originalLine-1;result+=base64VLQ.encode(mapping.originalColumn
-previousOriginalColumn);previousOriginalColumn=mapping.originalColumn;if(mapping.name){result+=base64VLQ.encode(this._names.indexOf(mapping.name)
-previousName);previousName=this._names.indexOf(mapping.name);}}}
return result;};SourceMapGenerator.prototype._generateSourcesContent=function SourceMapGenerator_generateSourcesContent(aSources,aSourceRoot){return aSources.map(function(source){if(!this._sourcesContents){return null;}
if(aSourceRoot){source=util.relative(aSourceRoot,source);}
var key=util.toSetString(source);return Object.prototype.hasOwnProperty.call(this._sourcesContents,key)?this._sourcesContents[key]:null;},this);};SourceMapGenerator.prototype.toJSON=function SourceMapGenerator_toJSON(){var map={version:this._version,file:this._file,sources:this._sources.toArray(),names:this._names.toArray(),mappings:this._serializeMappings()};if(this._sourceRoot){map.sourceRoot=this._sourceRoot;}
if(this._sourcesContents){map.sourcesContent=this._generateSourcesContent(map.sources,map.sourceRoot);}
return map;}; SourceMapGenerator.prototype.toString=function SourceMapGenerator_toString(){return JSON.stringify(this);};exports.SourceMapGenerator=SourceMapGenerator;});},{"./array-set":12,"./base64-vlq":13,"./util":19,"amdefine":20}],18:[function(_dereq_,module,exports){if(typeof define!=='function'){var define=_dereq_('amdefine')(module,_dereq_);}
define(function(_dereq_,exports,module){var SourceMapGenerator=_dereq_('./source-map-generator').SourceMapGenerator;var util=_dereq_('./util');function SourceNode(aLine,aColumn,aSource,aChunks,aName){this.children=[];this.sourceContents={};this.line=aLine===undefined?null:aLine;this.column=aColumn===undefined?null:aColumn;this.source=aSource===undefined?null:aSource;this.name=aName===undefined?null:aName;if(aChunks!=null)this.add(aChunks);}
SourceNode.fromStringWithSourceMap=function SourceNode_fromStringWithSourceMap(aGeneratedCode,aSourceMapConsumer){
 var node=new SourceNode();
var remainingLines=aGeneratedCode.split('\n');var lastGeneratedLine=1,lastGeneratedColumn=0;var lastMapping=null;aSourceMapConsumer.eachMapping(function(mapping){if(lastMapping===null){
while(lastGeneratedLine<mapping.generatedLine){node.add(remainingLines.shift()+"\n");lastGeneratedLine++;}
if(lastGeneratedColumn<mapping.generatedColumn){var nextLine=remainingLines[0];node.add(nextLine.substr(0,mapping.generatedColumn));remainingLines[0]=nextLine.substr(mapping.generatedColumn);lastGeneratedColumn=mapping.generatedColumn;}}else{if(lastGeneratedLine<mapping.generatedLine){var code="";do{code+=remainingLines.shift()+"\n";lastGeneratedLine++;lastGeneratedColumn=0;}while(lastGeneratedLine<mapping.generatedLine);
if(lastGeneratedColumn<mapping.generatedColumn){var nextLine=remainingLines[0];code+=nextLine.substr(0,mapping.generatedColumn);remainingLines[0]=nextLine.substr(mapping.generatedColumn);lastGeneratedColumn=mapping.generatedColumn;}
addMappingWithCode(lastMapping,code);}else{
var nextLine=remainingLines[0];var code=nextLine.substr(0,mapping.generatedColumn-
lastGeneratedColumn);remainingLines[0]=nextLine.substr(mapping.generatedColumn-
lastGeneratedColumn);lastGeneratedColumn=mapping.generatedColumn;addMappingWithCode(lastMapping,code);}}
lastMapping=mapping;},this); addMappingWithCode(lastMapping,remainingLines.join("\n")); aSourceMapConsumer.sources.forEach(function(sourceFile){var content=aSourceMapConsumer.sourceContentFor(sourceFile);if(content){node.setSourceContent(sourceFile,content);}});return node;function addMappingWithCode(mapping,code){if(mapping===null||mapping.source===undefined){node.add(code);}else{node.add(new SourceNode(mapping.originalLine,mapping.originalColumn,mapping.source,code,mapping.name));}}};SourceNode.prototype.add=function SourceNode_add(aChunk){if(Array.isArray(aChunk)){aChunk.forEach(function(chunk){this.add(chunk);},this);}
else if(aChunk instanceof SourceNode||typeof aChunk==="string"){if(aChunk){this.children.push(aChunk);}}
else{throw new TypeError("Expected a SourceNode, string, or an array of SourceNodes and strings. Got "+aChunk);}
return this;};SourceNode.prototype.prepend=function SourceNode_prepend(aChunk){if(Array.isArray(aChunk)){for(var i=aChunk.length-1;i>=0;i--){this.prepend(aChunk[i]);}}
else if(aChunk instanceof SourceNode||typeof aChunk==="string"){this.children.unshift(aChunk);}
else{throw new TypeError("Expected a SourceNode, string, or an array of SourceNodes and strings. Got "+aChunk);}
return this;};SourceNode.prototype.walk=function SourceNode_walk(aFn){var chunk;for(var i=0,len=this.children.length;i<len;i++){chunk=this.children[i];if(chunk instanceof SourceNode){chunk.walk(aFn);}
else{if(chunk!==''){aFn(chunk,{source:this.source,line:this.line,column:this.column,name:this.name});}}}};SourceNode.prototype.join=function SourceNode_join(aSep){var newChildren;var i;var len=this.children.length;if(len>0){newChildren=[];for(i=0;i<len-1;i++){newChildren.push(this.children[i]);newChildren.push(aSep);}
newChildren.push(this.children[i]);this.children=newChildren;}
return this;};SourceNode.prototype.replaceRight=function SourceNode_replaceRight(aPattern,aReplacement){var lastChild=this.children[this.children.length-1];if(lastChild instanceof SourceNode){lastChild.replaceRight(aPattern,aReplacement);}
else if(typeof lastChild==='string'){this.children[this.children.length-1]=lastChild.replace(aPattern,aReplacement);}
else{this.children.push(''.replace(aPattern,aReplacement));}
return this;};SourceNode.prototype.setSourceContent=function SourceNode_setSourceContent(aSourceFile,aSourceContent){this.sourceContents[util.toSetString(aSourceFile)]=aSourceContent;};SourceNode.prototype.walkSourceContents=function SourceNode_walkSourceContents(aFn){for(var i=0,len=this.children.length;i<len;i++){if(this.children[i]instanceof SourceNode){this.children[i].walkSourceContents(aFn);}}
var sources=Object.keys(this.sourceContents);for(var i=0,len=sources.length;i<len;i++){aFn(util.fromSetString(sources[i]),this.sourceContents[sources[i]]);}};SourceNode.prototype.toString=function SourceNode_toString(){var str="";this.walk(function(chunk){str+=chunk;});return str;};SourceNode.prototype.toStringWithSourceMap=function SourceNode_toStringWithSourceMap(aArgs){var generated={code:"",line:1,column:0};var map=new SourceMapGenerator(aArgs);var sourceMappingActive=false;var lastOriginalSource=null;var lastOriginalLine=null;var lastOriginalColumn=null;var lastOriginalName=null;this.walk(function(chunk,original){generated.code+=chunk;if(original.source!==null&&original.line!==null&&original.column!==null){if(lastOriginalSource!==original.source||lastOriginalLine!==original.line||lastOriginalColumn!==original.column||lastOriginalName!==original.name){map.addMapping({source:original.source,original:{line:original.line,column:original.column},generated:{line:generated.line,column:generated.column},name:original.name});}
lastOriginalSource=original.source;lastOriginalLine=original.line;lastOriginalColumn=original.column;lastOriginalName=original.name;sourceMappingActive=true;}else if(sourceMappingActive){map.addMapping({generated:{line:generated.line,column:generated.column}});lastOriginalSource=null;sourceMappingActive=false;}
chunk.split('').forEach(function(ch){if(ch==='\n'){generated.line++;generated.column=0;}else{generated.column++;}});});this.walkSourceContents(function(sourceFile,sourceContent){map.setSourceContent(sourceFile,sourceContent);});return{code:generated.code,map:map};};exports.SourceNode=SourceNode;});},{"./source-map-generator":17,"./util":19,"amdefine":20}],19:[function(_dereq_,module,exports){if(typeof define!=='function'){var define=_dereq_('amdefine')(module,_dereq_);}
define(function(_dereq_,exports,module){function getArg(aArgs,aName,aDefaultValue){if(aName in aArgs){return aArgs[aName];}else if(arguments.length===3){return aDefaultValue;}else{throw new Error('"'+aName+'" is a required argument.');}}
exports.getArg=getArg;var urlRegexp=/([\w+\-.]+):\/\/((\w+:\w+)@)?([\w.]+)?(:(\d+))?(\S+)?/;var dataUrlRegexp=/^data:.+\,.+/;function urlParse(aUrl){var match=aUrl.match(urlRegexp);if(!match){return null;}
return{scheme:match[1],auth:match[3],host:match[4],port:match[6],path:match[7]};}
exports.urlParse=urlParse;function urlGenerate(aParsedUrl){var url=aParsedUrl.scheme+"://";if(aParsedUrl.auth){url+=aParsedUrl.auth+"@"}
if(aParsedUrl.host){url+=aParsedUrl.host;}
if(aParsedUrl.port){url+=":"+aParsedUrl.port}
if(aParsedUrl.path){url+=aParsedUrl.path;}
return url;}
exports.urlGenerate=urlGenerate;function join(aRoot,aPath){var url;if(aPath.match(urlRegexp)||aPath.match(dataUrlRegexp)){return aPath;}
if(aPath.charAt(0)==='/'&&(url=urlParse(aRoot))){url.path=aPath;return urlGenerate(url);}
return aRoot.replace(/\/$/,'')+'/'+aPath;}
exports.join=join;function toSetString(aStr){return'$'+aStr;}
exports.toSetString=toSetString;function fromSetString(aStr){return aStr.substr(1);}
exports.fromSetString=fromSetString;function relative(aRoot,aPath){aRoot=aRoot.replace(/\/$/,'');var url=urlParse(aRoot);if(aPath.charAt(0)=="/"&&url&&url.path=="/"){return aPath.slice(1);}
return aPath.indexOf(aRoot+'/')===0?aPath.substr(aRoot.length+1):aPath;}
exports.relative=relative;function strcmp(aStr1,aStr2){var s1=aStr1||"";var s2=aStr2||"";return(s1>s2)-(s1<s2);}
function compareByOriginalPositions(mappingA,mappingB,onlyCompareOriginal){var cmp;cmp=strcmp(mappingA.source,mappingB.source);if(cmp){return cmp;}
cmp=mappingA.originalLine-mappingB.originalLine;if(cmp){return cmp;}
cmp=mappingA.originalColumn-mappingB.originalColumn;if(cmp||onlyCompareOriginal){return cmp;}
cmp=strcmp(mappingA.name,mappingB.name);if(cmp){return cmp;}
cmp=mappingA.generatedLine-mappingB.generatedLine;if(cmp){return cmp;}
return mappingA.generatedColumn-mappingB.generatedColumn;};exports.compareByOriginalPositions=compareByOriginalPositions;function compareByGeneratedPositions(mappingA,mappingB,onlyCompareGenerated){var cmp;cmp=mappingA.generatedLine-mappingB.generatedLine;if(cmp){return cmp;}
cmp=mappingA.generatedColumn-mappingB.generatedColumn;if(cmp||onlyCompareGenerated){return cmp;}
cmp=strcmp(mappingA.source,mappingB.source);if(cmp){return cmp;}
cmp=mappingA.originalLine-mappingB.originalLine;if(cmp){return cmp;}
cmp=mappingA.originalColumn-mappingB.originalColumn;if(cmp){return cmp;}
return strcmp(mappingA.name,mappingB.name);};exports.compareByGeneratedPositions=compareByGeneratedPositions;});},{"amdefine":20}],20:[function(_dereq_,module,exports){(function(process,__filename){'use strict';function amdefine(module,requireFn){'use strict';var defineCache={},loaderCache={},alreadyCalled=false,path=_dereq_('path'),makeRequire,stringRequire;function trimDots(ary){var i,part;for(i=0;ary[i];i+=1){part=ary[i];if(part==='.'){ary.splice(i,1);i-=1;}else if(part==='..'){if(i===1&&(ary[2]==='..'||ary[0]==='..')){



break;}else if(i>0){ary.splice(i-1,2);i-=2;}}}}
function normalize(name,baseName){var baseParts;if(name&&name.charAt(0)==='.'){
if(baseName){baseParts=baseName.split('/');baseParts=baseParts.slice(0,baseParts.length-1);baseParts=baseParts.concat(name.split('/'));trimDots(baseParts);name=baseParts.join('/');}}
return name;}
function makeNormalize(relName){return function(name){return normalize(name,relName);};}
function makeLoad(id){function load(value){loaderCache[id]=value;}
load.fromText=function(id,text){


throw new Error('amdefine does not implement load.fromText');};return load;}
makeRequire=function(systemRequire,exports,module,relId){function amdRequire(deps,callback){if(typeof deps==='string'){return stringRequire(systemRequire,exports,module,deps,relId);}else{deps=deps.map(function(depName){return stringRequire(systemRequire,exports,module,depName,relId);});process.nextTick(function(){callback.apply(null,deps);});}}
amdRequire.toUrl=function(filePath){if(filePath.indexOf('.')===0){return normalize(filePath,path.dirname(module.filename));}else{return filePath;}};return amdRequire;};requireFn=requireFn||function req(){return module.require.apply(module,arguments);};function runFactory(id,deps,factory){var r,e,m,result;if(id){e=loaderCache[id]={};m={id:id,uri:__filename,exports:e};r=makeRequire(requireFn,e,m,id);}else{ if(alreadyCalled){throw new Error('amdefine with no module ID cannot be called more than once per file.');}
alreadyCalled=true;

e=module.exports;m=module;r=makeRequire(requireFn,e,m,module.id);}

if(deps){deps=deps.map(function(depName){return r(depName);});}
if(typeof factory==='function'){result=factory.apply(m.exports,deps);}else{result=factory;}
if(result!==undefined){m.exports=result;if(id){loaderCache[id]=m.exports;}}}
stringRequire=function(systemRequire,exports,module,id,relId){ var index=id.indexOf('!'),originalId=id,prefix,plugin;if(index===-1){id=normalize(id,relId);if(id==='require'){return makeRequire(systemRequire,exports,module,relId);}else if(id==='exports'){return exports;}else if(id==='module'){return module;}else if(loaderCache.hasOwnProperty(id)){return loaderCache[id];}else if(defineCache[id]){runFactory.apply(null,defineCache[id]);return loaderCache[id];}else{if(systemRequire){return systemRequire(originalId);}else{throw new Error('No module with ID: '+id);}}}else{prefix=id.substring(0,index);id=id.substring(index+1,id.length);plugin=stringRequire(systemRequire,exports,module,prefix,relId);if(plugin.normalize){id=plugin.normalize(id,makeNormalize(relId));}else{id=normalize(id,relId);}
if(loaderCache[id]){return loaderCache[id];}else{plugin.load(id,makeRequire(systemRequire,exports,module,relId),makeLoad(id),{});return loaderCache[id];}}};function define(id,deps,factory){if(Array.isArray(id)){factory=deps;deps=id;id=undefined;}else if(typeof id!=='string'){factory=id;id=deps=undefined;}
if(deps&&!Array.isArray(deps)){factory=deps;deps=undefined;}
if(!deps){deps=['require','exports','module'];}


if(id){
defineCache[id]=[id,deps,factory];}else{runFactory(id,deps,factory);}}


define.require=function(id){if(loaderCache[id]){return loaderCache[id];}
if(defineCache[id]){runFactory.apply(null,defineCache[id]);return loaderCache[id];}};define.amd={};return define;}
module.exports=amdefine;}).call(this,_dereq_('_process'),"/node_modules/jstransform/node_modules/source-map/node_modules/amdefine/amdefine.js")},{"_process":8,"path":7}],21:[function(_dereq_,module,exports){var docblockRe=/^\s*(\/\*\*(.|\r?\n)*?\*\/)/;var ltrimRe=/^\s*/;function extract(contents){var match=contents.match(docblockRe);if(match){return match[0].replace(ltrimRe,'')||'';}
return'';}
var commentStartRe=/^\/\*\*?/;var commentEndRe=/\*+\/$/;var wsRe=/[\t ]+/g;var stringStartRe=/(\r?\n|^) *\*/g;var multilineRe=/(?:^|\r?\n) *(@[^\r\n]*?) *\r?\n *([^@\r\n\s][^@\r\n]+?) *\r?\n/g;var propertyRe=/(?:^|\r?\n) *@(\S+) *([^\r\n]*)/g;function parse(docblock){docblock=docblock.replace(commentStartRe,'').replace(commentEndRe,'').replace(wsRe,' ').replace(stringStartRe,'$1'); var prev='';while(prev!=docblock){prev=docblock;docblock=docblock.replace(multilineRe,"\n$1 $2\n");}
docblock=docblock.trim();var result=[];var match;while(match=propertyRe.exec(docblock)){result.push([match[1],match[2]]);}
return result;}
function parseAsObject(docblock){var pairs=parse(docblock);var result={};for(var i=0;i<pairs.length;i++){result[pairs[i][0]]=pairs[i][1];}
return result;}
exports.extract=extract;exports.parse=parse;exports.parseAsObject=parseAsObject;},{}],22:[function(_dereq_,module,exports){"use strict";var esprima=_dereq_('esprima-fb');var utils=_dereq_('./utils');var getBoundaryNode=utils.getBoundaryNode;var declareIdentInScope=utils.declareIdentInLocalScope;var initScopeMetadata=utils.initScopeMetadata;var Syntax=esprima.Syntax;function _nodeIsClosureScopeBoundary(node,parentNode){if(node.type===Syntax.Program){return true;}
var parentIsFunction=parentNode.type===Syntax.FunctionDeclaration||parentNode.type===Syntax.FunctionExpression||parentNode.type===Syntax.ArrowFunctionExpression;var parentIsCurlylessArrowFunc=parentNode.type===Syntax.ArrowFunctionExpression&&node===parentNode.body;return parentIsFunction&&(node.type===Syntax.BlockStatement||parentIsCurlylessArrowFunc);}
function _nodeIsBlockScopeBoundary(node,parentNode){if(node.type===Syntax.Program){return false;}
return node.type===Syntax.BlockStatement&&parentNode.type===Syntax.CatchClause;}
function traverse(node,path,state){
 var startIndex=null;var parentNode=path[0];if(!Array.isArray(node)&&state.localScope.parentNode!==parentNode){if(_nodeIsClosureScopeBoundary(node,parentNode)){var scopeIsStrict=state.scopeIsStrict;if(!scopeIsStrict&&(node.type===Syntax.BlockStatement||node.type===Syntax.Program)){scopeIsStrict=node.body.length>0&&node.body[0].type===Syntax.ExpressionStatement&&node.body[0].expression.type===Syntax.Literal&&node.body[0].expression.value==='use strict';}
if(node.type===Syntax.Program){startIndex=state.g.buffer.length;state=utils.updateState(state,{scopeIsStrict:scopeIsStrict});}else{startIndex=state.g.buffer.length+1;state=utils.updateState(state,{localScope:{parentNode:parentNode,parentScope:state.localScope,identifiers:{},tempVarIndex:0,tempVars:[]},scopeIsStrict:scopeIsStrict}); declareIdentInScope('arguments',initScopeMetadata(node),state);
 if(parentNode.params.length>0){var param;var metadata=initScopeMetadata(parentNode,path.slice(1),path[0]);for(var i=0;i<parentNode.params.length;i++){param=parentNode.params[i];if(param.type===Syntax.Identifier){declareIdentInScope(param.name,metadata,state);}}}
 
if(parentNode.rest){var metadata=initScopeMetadata(parentNode,path.slice(1),path[0]);declareIdentInScope(parentNode.rest.name,metadata,state);}
 
if(parentNode.type===Syntax.FunctionExpression&&parentNode.id){var metaData=initScopeMetadata(parentNode,path.parentNodeslice,parentNode);declareIdentInScope(parentNode.id.name,metaData,state);}}
 
collectClosureIdentsAndTraverse(node,path,state);}
if(_nodeIsBlockScopeBoundary(node,parentNode)){startIndex=state.g.buffer.length;state=utils.updateState(state,{localScope:{parentNode:parentNode,parentScope:state.localScope,identifiers:{},tempVarIndex:0,tempVars:[]}});if(parentNode.type===Syntax.CatchClause){var metadata=initScopeMetadata(parentNode,path.slice(1),parentNode);declareIdentInScope(parentNode.param.name,metadata,state);}
collectBlockIdentsAndTraverse(node,path,state);}} 
function traverser(node,path,state){node.range&&utils.catchup(node.range[0],state);traverse(node,path,state);node.range&&utils.catchup(node.range[1],state);}
utils.analyzeAndTraverse(walker,traverser,node,path,state);if(startIndex!==null){utils.injectTempVarDeclarations(state,startIndex);}}
function collectClosureIdentsAndTraverse(node,path,state){utils.analyzeAndTraverse(visitLocalClosureIdentifiers,collectClosureIdentsAndTraverse,node,path,state);}
function collectBlockIdentsAndTraverse(node,path,state){utils.analyzeAndTraverse(visitLocalBlockIdentifiers,collectBlockIdentsAndTraverse,node,path,state);}
function visitLocalClosureIdentifiers(node,path,state){var metaData;switch(node.type){case Syntax.ArrowFunctionExpression:case Syntax.FunctionExpression:
 return false;case Syntax.ClassDeclaration:case Syntax.ClassExpression:case Syntax.FunctionDeclaration:if(node.id){metaData=initScopeMetadata(getBoundaryNode(path),path.slice(),node);declareIdentInScope(node.id.name,metaData,state);}
return false;case Syntax.VariableDeclarator: if(path[0].kind==='var'){metaData=initScopeMetadata(getBoundaryNode(path),path.slice(),node);declareIdentInScope(node.id.name,metaData,state);}
break;}}
function visitLocalBlockIdentifiers(node,path,state){if(node.type===Syntax.CatchClause){return false;}}
function walker(node,path,state){var visitors=state.g.visitors;for(var i=0;i<visitors.length;i++){if(visitors[i].test(node,path,state)){return visitors[i](traverse,node,path,state);}}}
var _astCache={};function getAstForSource(source,options){if(_astCache[source]&&!options.disableAstCache){return _astCache[source];}
var ast=esprima.parse(source,{comment:true,loc:true,range:true,sourceType:options.sourceType});if(!options.disableAstCache){_astCache[source]=ast;}
return ast;}
function transform(visitors,source,options){options=options||{};var ast;try{ast=getAstForSource(source,options);}catch(e){e.message='Parse Error: '+e.message;throw e;}
var state=utils.createState(source,ast,options);state.g.visitors=visitors;if(options.sourceMap){var SourceMapGenerator=_dereq_('source-map').SourceMapGenerator;state.g.sourceMap=new SourceMapGenerator({file:options.filename||'transformed.js'});}
traverse(ast,[],state);utils.catchup(source.length,state);var ret={code:state.g.buffer,extra:state.g.extra};if(options.sourceMap){ret.sourceMap=state.g.sourceMap;ret.sourceMapFilename=options.filename||'source.js';}
return ret;}
exports.transform=transform;exports.Syntax=Syntax;},{"./utils":23,"esprima-fb":9,"source-map":11}],23:[function(_dereq_,module,exports){var Syntax=_dereq_('esprima-fb').Syntax;var leadingIndentRegexp=/(^|\n)( {2}|\t)/g;var nonWhiteRegexp=/(\S)/g;function createState(source,rootNode,transformOptions){return{ localScope:{parentNode:rootNode,parentScope:null,identifiers:{},tempVarIndex:0,tempVars:[]},superClass:null,mungeNamespace:'',methodNode:null,methodFuncNode:null,className:null,scopeIsStrict:null,indentBy:0,g:{opts:transformOptions,position:0,extra:{},buffer:'',source:source,docblock:null,tagNamespaceUsed:false,isBolt:undefined,sourceMap:null,sourceMapFilename:'source.js',sourceLine:1,bufferLine:1,originalProgramAST:null,sourceColumn:0,bufferColumn:0}};}
function updateState(state,update){var ret=Object.create(state);Object.keys(update).forEach(function(updatedKey){ret[updatedKey]=update[updatedKey];});return ret;}
function catchup(end,state,contentTransformer){if(end<state.g.position){ return;}
var source=state.g.source.substring(state.g.position,end);var transformed=updateIndent(source,state);if(state.g.sourceMap&&transformed){ state.g.sourceMap.addMapping({generated:{line:state.g.bufferLine,column:state.g.bufferColumn},original:{line:state.g.sourceLine,column:state.g.sourceColumn},source:state.g.sourceMapFilename}); var sourceLines=source.split('\n');var transformedLines=transformed.split('\n');

for(var i=1;i<sourceLines.length-1;i++){state.g.sourceMap.addMapping({generated:{line:state.g.bufferLine,column:0},original:{line:state.g.sourceLine,column:0},source:state.g.sourceMapFilename});state.g.sourceLine++;state.g.bufferLine++;} 
if(sourceLines.length>1){state.g.sourceLine++;state.g.bufferLine++;state.g.sourceColumn=0;state.g.bufferColumn=0;}
state.g.sourceColumn+=sourceLines[sourceLines.length-1].length;state.g.bufferColumn+=transformedLines[transformedLines.length-1].length;}
state.g.buffer+=contentTransformer?contentTransformer(transformed):transformed;state.g.position=end;}
function getNodeSourceText(node,state){return state.g.source.substring(node.range[0],node.range[1]);}
function _replaceNonWhite(value){return value.replace(nonWhiteRegexp,' ');}
function _stripNonWhite(value){return value.replace(nonWhiteRegexp,'');}
function getNextSyntacticCharOffset(char,state){var pendingSource=state.g.source.substring(state.g.position);var pendingSourceLines=pendingSource.split('\n');var charOffset=0;var line;var withinBlockComment=false;var withinString=false;lineLoop:while((line=pendingSourceLines.shift())!==undefined){var lineEndPos=charOffset+line.length;charLoop:for(;charOffset<lineEndPos;charOffset++){var currChar=pendingSource[charOffset];if(currChar==='"'||currChar==='\''){withinString=!withinString;continue charLoop;}else if(withinString){continue charLoop;}else if(charOffset+1<lineEndPos){var nextTwoChars=currChar+line[charOffset+1];if(nextTwoChars==='//'){charOffset=lineEndPos+1;continue lineLoop;}else if(nextTwoChars==='/*'){withinBlockComment=true;charOffset+=1;continue charLoop;}else if(nextTwoChars==='*/'){withinBlockComment=false;charOffset+=1;continue charLoop;}}
if(!withinBlockComment&&currChar===char){return charOffset+state.g.position;}}
charOffset++;withinString=false;}
throw new Error('`'+char+'` not found!');}
function catchupWhiteOut(end,state){catchup(end,state,_replaceNonWhite);}
function catchupWhiteSpace(end,state){catchup(end,state,_stripNonWhite);}
var reNonNewline=/[^\n]/g;function stripNonNewline(value){return value.replace(reNonNewline,function(){return'';});}
function catchupNewlines(end,state){catchup(end,state,stripNonNewline);}
function move(end,state){ if(state.g.sourceMap){if(end<state.g.position){state.g.position=0;state.g.sourceLine=1;state.g.sourceColumn=0;}
var source=state.g.source.substring(state.g.position,end);var sourceLines=source.split('\n');if(sourceLines.length>1){state.g.sourceLine+=sourceLines.length-1;state.g.sourceColumn=0;}
state.g.sourceColumn+=sourceLines[sourceLines.length-1].length;}
state.g.position=end;}
function append(str,state){if(state.g.sourceMap&&str){state.g.sourceMap.addMapping({generated:{line:state.g.bufferLine,column:state.g.bufferColumn},original:{line:state.g.sourceLine,column:state.g.sourceColumn},source:state.g.sourceMapFilename});var transformedLines=str.split('\n');if(transformedLines.length>1){state.g.bufferLine+=transformedLines.length-1;state.g.bufferColumn=0;}
state.g.bufferColumn+=transformedLines[transformedLines.length-1].length;}
state.g.buffer+=str;}
function updateIndent(str,state){var indentBy=state.indentBy;if(indentBy<0){for(var i=0;i<-indentBy;i++){str=str.replace(leadingIndentRegexp,'$1');}}else{for(var i=0;i<indentBy;i++){str=str.replace(leadingIndentRegexp,'$1$2$2');}}
return str;}
function indentBefore(start,state){var end=start;start=start-1;while(start>0&&state.g.source[start]!='\n'){if(!state.g.source[start].match(/[ \t]/)){end=start;}
start--;}
return state.g.source.substring(start+1,end);}
function getDocblock(state){if(!state.g.docblock){var docblock=_dereq_('./docblock');state.g.docblock=docblock.parseAsObject(docblock.extract(state.g.source));}
return state.g.docblock;}
function identWithinLexicalScope(identName,state,stopBeforeNode){var currScope=state.localScope;while(currScope){if(currScope.identifiers[identName]!==undefined){return true;}
if(stopBeforeNode&&currScope.parentNode===stopBeforeNode){break;}
currScope=currScope.parentScope;}
return false;}
function identInLocalScope(identName,state){return state.localScope.identifiers[identName]!==undefined;}
function initScopeMetadata(boundaryNode,path,node){return{boundaryNode:boundaryNode,bindingPath:path,bindingNode:node};}
function declareIdentInLocalScope(identName,metaData,state){state.localScope.identifiers[identName]={boundaryNode:metaData.boundaryNode,path:metaData.bindingPath,node:metaData.bindingNode,state:Object.create(state)};}
function getLexicalBindingMetadata(identName,state){var currScope=state.localScope;while(currScope){if(currScope.identifiers[identName]!==undefined){return currScope.identifiers[identName];}
currScope=currScope.parentScope;}}
function getLocalBindingMetadata(identName,state){return state.localScope.identifiers[identName];}
function analyzeAndTraverse(analyzer,traverser,node,path,state){if(node.type){if(analyzer(node,path,state)===false){return;}
path.unshift(node);}
getOrderedChildren(node).forEach(function(child){traverser(child,path,state);});node.type&&path.shift();}
function getOrderedChildren(node){var queue=[];for(var key in node){if(node.hasOwnProperty(key)){enqueueNodeWithStartIndex(queue,node[key]);}}
queue.sort(function(a,b){return a[1]-b[1];});return queue.map(function(pair){return pair[0];});}
function enqueueNodeWithStartIndex(queue,node){if(typeof node!=='object'||node===null){return;}
if(node.range){queue.push([node,node.range[0]]);}else if(Array.isArray(node)){for(var ii=0;ii<node.length;ii++){enqueueNodeWithStartIndex(queue,node[ii]);}}}
function containsChildOfType(node,type){return containsChildMatching(node,function(node){return node.type===type;});}
function containsChildMatching(node,matcher){var foundMatchingChild=false;function nodeTypeAnalyzer(node){if(matcher(node)===true){foundMatchingChild=true;return false;}}
function nodeTypeTraverser(child,path,state){if(!foundMatchingChild){foundMatchingChild=containsChildMatching(child,matcher);}}
analyzeAndTraverse(nodeTypeAnalyzer,nodeTypeTraverser,node,[]);return foundMatchingChild;}
var scopeTypes={};scopeTypes[Syntax.ArrowFunctionExpression]=true;scopeTypes[Syntax.FunctionExpression]=true;scopeTypes[Syntax.FunctionDeclaration]=true;scopeTypes[Syntax.Program]=true;function getBoundaryNode(path){for(var ii=0;ii<path.length;++ii){if(scopeTypes[path[ii].type]){return path[ii];}}
throw new Error('Expected to find a node with one of the following types in path:\n'+
JSON.stringify(Object.keys(scopeTypes)));}
function getTempVar(tempVarIndex){return'$__'+tempVarIndex;}
function injectTempVar(state){var tempVar='$__'+(state.localScope.tempVarIndex++);state.localScope.tempVars.push(tempVar);return tempVar;}
function injectTempVarDeclarations(state,index){if(state.localScope.tempVars.length){state.g.buffer=state.g.buffer.slice(0,index)+'var '+state.localScope.tempVars.join(', ')+';'+
state.g.buffer.slice(index);state.localScope.tempVars=[];}}
exports.analyzeAndTraverse=analyzeAndTraverse;exports.append=append;exports.catchup=catchup;exports.catchupNewlines=catchupNewlines;exports.catchupWhiteOut=catchupWhiteOut;exports.catchupWhiteSpace=catchupWhiteSpace;exports.containsChildMatching=containsChildMatching;exports.containsChildOfType=containsChildOfType;exports.createState=createState;exports.declareIdentInLocalScope=declareIdentInLocalScope;exports.getBoundaryNode=getBoundaryNode;exports.getDocblock=getDocblock;exports.getLexicalBindingMetadata=getLexicalBindingMetadata;exports.getLocalBindingMetadata=getLocalBindingMetadata;exports.getNextSyntacticCharOffset=getNextSyntacticCharOffset;exports.getNodeSourceText=getNodeSourceText;exports.getOrderedChildren=getOrderedChildren;exports.getTempVar=getTempVar;exports.identInLocalScope=identInLocalScope;exports.identWithinLexicalScope=identWithinLexicalScope;exports.indentBefore=indentBefore;exports.initScopeMetadata=initScopeMetadata;exports.injectTempVar=injectTempVar;exports.injectTempVarDeclarations=injectTempVarDeclarations;exports.move=move;exports.scopeTypes=scopeTypes;exports.updateIndent=updateIndent;exports.updateState=updateState;},{"./docblock":21,"esprima-fb":9}],24:[function(_dereq_,module,exports){var restParamVisitors=_dereq_('./es6-rest-param-visitors');var destructuringVisitors=_dereq_('./es6-destructuring-visitors');var Syntax=_dereq_('esprima-fb').Syntax;var utils=_dereq_('../src/utils');function visitArrowFunction(traverse,node,path,state){var notInExpression=(path[0].type===Syntax.ExpressionStatement);
if(notInExpression){utils.append('(',state);}
utils.append('function',state);renderParams(traverse,node,path,state);utils.catchupWhiteSpace(node.body.range[0],state);var renderBody=node.body.type==Syntax.BlockStatement?renderStatementBody:renderExpressionBody;path.unshift(node);renderBody(traverse,node,path,state);path.shift();
var containsBindingSyntax=utils.containsChildMatching(node.body,function(node){return node.type===Syntax.ThisExpression||(node.type===Syntax.Identifier&&node.name==="super");});if(containsBindingSyntax){utils.append('.bind(this)',state);}
utils.catchupWhiteSpace(node.range[1],state);if(notInExpression){utils.append(')',state);}
return false;}
function renderParams(traverse,node,path,state){
if(isParensFreeSingleParam(node,state)||!node.params.length){utils.append('(',state);}
if(node.params.length!==0){path.unshift(node);traverse(node.params,path,state);path.unshift();}
utils.append(')',state);}
function isParensFreeSingleParam(node,state){return node.params.length===1&&state.g.source[state.g.position]!=='(';}
function renderExpressionBody(traverse,node,path,state){
utils.append('{',state);if(node.rest){utils.append(restParamVisitors.renderRestParamSetup(node,state),state);}
destructuringVisitors.renderDestructuredComponents(node,utils.updateState(state,{localScope:{parentNode:state.parentNode,parentScope:state.parentScope,identifiers:state.identifiers,tempVarIndex:0}}));utils.append('return ',state);renderStatementBody(traverse,node,path,state);utils.append(';}',state);}
function renderStatementBody(traverse,node,path,state){traverse(node.body,path,state);utils.catchup(node.body.range[1],state);}
visitArrowFunction.test=function(node,path,state){return node.type===Syntax.ArrowFunctionExpression;};exports.visitorList=[visitArrowFunction];},{"../src/utils":23,"./es6-destructuring-visitors":27,"./es6-rest-param-visitors":30,"esprima-fb":9}],25:[function(_dereq_,module,exports){var Syntax=_dereq_('esprima-fb').Syntax;var utils=_dereq_('../src/utils');function process(traverse,node,path,state){utils.move(node.range[0],state);traverse(node,path,state);utils.catchup(node.range[1],state);}
function visitCallSpread(traverse,node,path,state){utils.catchup(node.range[0],state);if(node.type===Syntax.NewExpression){
utils.append('new (Function.prototype.bind.apply(',state);process(traverse,node.callee,path,state);}else if(node.callee.type===Syntax.MemberExpression){
var tempVar=utils.injectTempVar(state);utils.append('('+tempVar+' = ',state);process(traverse,node.callee.object,path,state);utils.append(')',state);if(node.callee.property.type===Syntax.Identifier){utils.append('.',state);process(traverse,node.callee.property,path,state);}else{utils.append('[',state);process(traverse,node.callee.property,path,state);utils.append(']',state);}
utils.append('.apply('+tempVar,state);}else{
var needsToBeWrappedInParenthesis=node.callee.type===Syntax.FunctionDeclaration||node.callee.type===Syntax.FunctionExpression;if(needsToBeWrappedInParenthesis){utils.append('(',state);}
process(traverse,node.callee,path,state);if(needsToBeWrappedInParenthesis){utils.append(')',state);}
utils.append('.apply(null',state);}
utils.append(', ',state);var args=node.arguments.slice();var spread=args.pop();if(args.length||node.type===Syntax.NewExpression){utils.append('[',state);if(node.type===Syntax.NewExpression){utils.append('null'+(args.length?', ':''),state);}
while(args.length){var arg=args.shift();utils.move(arg.range[0],state);traverse(arg,path,state);if(args.length){utils.catchup(args[0].range[0],state);}else{utils.catchup(arg.range[1],state);}}
utils.append('].concat(',state);process(traverse,spread.argument,path,state);utils.append(')',state);}else{process(traverse,spread.argument,path,state);}
utils.append(node.type===Syntax.NewExpression?'))':')',state);utils.move(node.range[1],state);return false;}
visitCallSpread.test=function(node,path,state){return((node.type===Syntax.CallExpression||node.type===Syntax.NewExpression)&&node.arguments.length>0&&node.arguments[node.arguments.length-1].type===Syntax.SpreadElement);};exports.visitorList=[visitCallSpread];},{"../src/utils":23,"esprima-fb":9}],26:[function(_dereq_,module,exports){'use strict';var base62=_dereq_('base62');var Syntax=_dereq_('esprima-fb').Syntax;var utils=_dereq_('../src/utils');var reservedWordsHelper=_dereq_('./reserved-words-helper');var declareIdentInLocalScope=utils.declareIdentInLocalScope;var initScopeMetadata=utils.initScopeMetadata;var SUPER_PROTO_IDENT_PREFIX='____SuperProtoOf';var _anonClassUUIDCounter=0;var _mungedSymbolMaps={};function resetSymbols(){_anonClassUUIDCounter=0;_mungedSymbolMaps={};}
function _generateAnonymousClassName(state){var mungeNamespace=state.mungeNamespace||'';return'____Class'+mungeNamespace+base62.encode(_anonClassUUIDCounter++);}
function _getMungedName(identName,state){var mungeNamespace=state.mungeNamespace;var shouldMinify=state.g.opts.minify;if(shouldMinify){if(!_mungedSymbolMaps[mungeNamespace]){_mungedSymbolMaps[mungeNamespace]={symbolMap:{},identUUIDCounter:0};}
var symbolMap=_mungedSymbolMaps[mungeNamespace].symbolMap;if(!symbolMap[identName]){symbolMap[identName]=base62.encode(_mungedSymbolMaps[mungeNamespace].identUUIDCounter++);}
identName=symbolMap[identName];}
return'$'+mungeNamespace+identName;}
function _getSuperClassInfo(node,state){var ret={name:null,expression:null};if(node.superClass){if(node.superClass.type===Syntax.Identifier){ret.name=node.superClass.name;}else{ ret.name=_generateAnonymousClassName(state);ret.expression=state.g.source.substring(node.superClass.range[0],node.superClass.range[1]);}}
return ret;}
function _isConstructorMethod(classElement){return classElement.type===Syntax.MethodDefinition&&classElement.key.type===Syntax.Identifier&&classElement.key.name==='constructor';}
function _shouldMungeIdentifier(node,state){return(!!state.methodFuncNode&&!utils.getDocblock(state).hasOwnProperty('preventMunge')&&/^_(?!_)/.test(node.name));}
function visitClassMethod(traverse,node,path,state){if(!state.g.opts.es5&&(node.kind==='get'||node.kind==='set')){throw new Error('This transform does not support '+node.kind+'ter methods for ES6 '+'classes. (line: '+node.loc.start.line+', col: '+
node.loc.start.column+')');}
state=utils.updateState(state,{methodNode:node});utils.catchup(node.range[0],state);path.unshift(node);traverse(node.value,path,state);path.shift();return false;}
visitClassMethod.test=function(node,path,state){return node.type===Syntax.MethodDefinition;};function visitClassFunctionExpression(traverse,node,path,state){var methodNode=path[0];var isGetter=methodNode.kind==='get';var isSetter=methodNode.kind==='set';state=utils.updateState(state,{methodFuncNode:node});if(methodNode.key.name==='constructor'){utils.append('function '+state.className,state);}else{var methodAccessorComputed=false;var methodAccessor;var prototypeOrStatic=methodNode["static"]?'':'.prototype';var objectAccessor=state.className+prototypeOrStatic;if(methodNode.key.type===Syntax.Identifier){methodAccessor=methodNode.key.name;if(_shouldMungeIdentifier(methodNode.key,state)){methodAccessor=_getMungedName(methodAccessor,state);}
if(isGetter||isSetter){methodAccessor=JSON.stringify(methodAccessor);}else if(reservedWordsHelper.isReservedWord(methodAccessor)){methodAccessorComputed=true;methodAccessor=JSON.stringify(methodAccessor);}}else if(methodNode.key.type===Syntax.Literal){methodAccessor=JSON.stringify(methodNode.key.value);methodAccessorComputed=true;}
if(isSetter||isGetter){utils.append('Object.defineProperty('+
objectAccessor+','+
methodAccessor+','+'{configurable:true,'+
methodNode.kind+':function',state);}else{if(state.g.opts.es3){if(methodAccessorComputed){methodAccessor='['+methodAccessor+']';}else{methodAccessor='.'+methodAccessor;}
utils.append(objectAccessor+
methodAccessor+'=function'+(node.generator?'*':''),state);}else{if(!methodAccessorComputed){methodAccessor=JSON.stringify(methodAccessor);}
utils.append('Object.defineProperty('+
objectAccessor+','+
methodAccessor+','+'{writable:true,configurable:true,'+'value:function'+(node.generator?'*':''),state);}}}
utils.move(methodNode.key.range[1],state);utils.append('(',state);var params=node.params;if(params.length>0){utils.catchupNewlines(params[0].range[0],state);for(var i=0;i<params.length;i++){utils.catchup(node.params[i].range[0],state);path.unshift(node);traverse(params[i],path,state);path.shift();}}
var closingParenPosition=utils.getNextSyntacticCharOffset(')',state);utils.catchupWhiteSpace(closingParenPosition,state);var openingBracketPosition=utils.getNextSyntacticCharOffset('{',state);utils.catchup(openingBracketPosition+1,state);if(!state.scopeIsStrict){utils.append('"use strict";',state);state=utils.updateState(state,{scopeIsStrict:true});}
utils.move(node.body.range[0]+'{'.length,state);path.unshift(node);traverse(node.body,path,state);path.shift();utils.catchup(node.body.range[1],state);if(methodNode.key.name!=='constructor'){if(isGetter||isSetter||!state.g.opts.es3){utils.append('})',state);}
utils.append(';',state);}
return false;}
visitClassFunctionExpression.test=function(node,path,state){return node.type===Syntax.FunctionExpression&&path[0].type===Syntax.MethodDefinition;};function visitClassMethodParam(traverse,node,path,state){var paramName=node.name;if(_shouldMungeIdentifier(node,state)){paramName=_getMungedName(node.name,state);}
utils.append(paramName,state);utils.move(node.range[1],state);}
visitClassMethodParam.test=function(node,path,state){if(!path[0]||!path[1]){return;}
var parentFuncExpr=path[0];var parentClassMethod=path[1];return parentFuncExpr.type===Syntax.FunctionExpression&&parentClassMethod.type===Syntax.MethodDefinition&&node.type===Syntax.Identifier;};function _renderClassBody(traverse,node,path,state){var className=state.className;var superClass=state.superClass;

if(superClass.name){


if(superClass.expression!==null){utils.append('var '+superClass.name+'='+superClass.expression+';',state);}
var keyName=superClass.name+'____Key';var keyNameDeclarator='';if(!utils.identWithinLexicalScope(keyName,state)){keyNameDeclarator='var ';declareIdentInLocalScope(keyName,initScopeMetadata(node),state);}
utils.append('for('+keyNameDeclarator+keyName+' in '+superClass.name+'){'+'if('+superClass.name+'.hasOwnProperty('+keyName+')){'+
className+'['+keyName+']='+
superClass.name+'['+keyName+'];'+'}'+'}',state);var superProtoIdentStr=SUPER_PROTO_IDENT_PREFIX+superClass.name;if(!utils.identWithinLexicalScope(superProtoIdentStr,state)){utils.append('var '+superProtoIdentStr+'='+superClass.name+'===null?'+'null:'+superClass.name+'.prototype;',state);declareIdentInLocalScope(superProtoIdentStr,initScopeMetadata(node),state);}
utils.append(className+'.prototype=Object.create('+superProtoIdentStr+');',state);utils.append(className+'.prototype.constructor='+className+';',state);utils.append(className+'.__superConstructor__='+superClass.name+';',state);}

if(!node.body.body.filter(_isConstructorMethod).pop()){utils.append('function '+className+'(){',state);if(!state.scopeIsStrict){utils.append('"use strict";',state);}
if(superClass.name){utils.append('if('+superClass.name+'!==null){'+
superClass.name+'.apply(this,arguments);}',state);}
utils.append('}',state);}
utils.move(node.body.range[0]+'{'.length,state);traverse(node.body,path,state);utils.catchupWhiteSpace(node.range[1],state);}
function visitClassDeclaration(traverse,node,path,state){var className=node.id.name;var superClass=_getSuperClassInfo(node,state);state=utils.updateState(state,{mungeNamespace:className,className:className,superClass:superClass});_renderClassBody(traverse,node,path,state);return false;}
visitClassDeclaration.test=function(node,path,state){return node.type===Syntax.ClassDeclaration;};function visitClassExpression(traverse,node,path,state){var className=node.id&&node.id.name||_generateAnonymousClassName(state);var superClass=_getSuperClassInfo(node,state);utils.append('(function(){',state);state=utils.updateState(state,{mungeNamespace:className,className:className,superClass:superClass});_renderClassBody(traverse,node,path,state);utils.append('return '+className+';})()',state);return false;}
visitClassExpression.test=function(node,path,state){return node.type===Syntax.ClassExpression;};function visitPrivateIdentifier(traverse,node,path,state){utils.append(_getMungedName(node.name,state),state);utils.move(node.range[1],state);}
visitPrivateIdentifier.test=function(node,path,state){if(node.type===Syntax.Identifier&&_shouldMungeIdentifier(node,state)){
if(path[0].type===Syntax.MemberExpression&&path[0].object!==node&&path[0].computed===false){return true;}
 
if(utils.identWithinLexicalScope(node.name,state,state.methodFuncNode)){return true;}

if(path[0].type===Syntax.Property&&path[1].type===Syntax.ObjectExpression){return true;} 
if(path[0].type===Syntax.FunctionExpression||path[0].type===Syntax.FunctionDeclaration||path[0].type===Syntax.ArrowFunctionExpression){for(var i=0;i<path[0].params.length;i++){if(path[0].params[i]===node){return true;}}}}
return false;};function visitSuperCallExpression(traverse,node,path,state){var superClassName=state.superClass.name;if(node.callee.type===Syntax.Identifier){if(_isConstructorMethod(state.methodNode)){utils.append(superClassName+'.call(',state);}else{var protoProp=SUPER_PROTO_IDENT_PREFIX+superClassName;if(state.methodNode.key.type===Syntax.Identifier){protoProp+='.'+state.methodNode.key.name;}else if(state.methodNode.key.type===Syntax.Literal){protoProp+='['+JSON.stringify(state.methodNode.key.value)+']';}
utils.append(protoProp+".call(",state);}
utils.move(node.callee.range[1],state);}else if(node.callee.type===Syntax.MemberExpression){utils.append(SUPER_PROTO_IDENT_PREFIX+superClassName,state);utils.move(node.callee.object.range[1],state);if(node.callee.computed){utils.catchup(node.callee.property.range[1]+']'.length,state);}else{ utils.append('.'+node.callee.property.name,state);}
utils.append('.call(',state);utils.move(node.callee.range[1],state);}
utils.append('this',state);if(node.arguments.length>0){utils.append(',',state);utils.catchupWhiteSpace(node.arguments[0].range[0],state);traverse(node.arguments,path,state);}
utils.catchupWhiteSpace(node.range[1],state);utils.append(')',state);return false;}
visitSuperCallExpression.test=function(node,path,state){if(state.superClass&&node.type===Syntax.CallExpression){var callee=node.callee;if(callee.type===Syntax.Identifier&&callee.name==='super'||callee.type==Syntax.MemberExpression&&callee.object.name==='super'){return true;}}
return false;};function visitSuperMemberExpression(traverse,node,path,state){var superClassName=state.superClass.name;utils.append(SUPER_PROTO_IDENT_PREFIX+superClassName,state);utils.move(node.object.range[1],state);}
visitSuperMemberExpression.test=function(node,path,state){return state.superClass&&node.type===Syntax.MemberExpression&&node.object.type===Syntax.Identifier&&node.object.name==='super';};exports.resetSymbols=resetSymbols;exports.visitorList=[visitClassDeclaration,visitClassExpression,visitClassFunctionExpression,visitClassMethod,visitClassMethodParam,visitPrivateIdentifier,visitSuperCallExpression,visitSuperMemberExpression];},{"../src/utils":23,"./reserved-words-helper":34,"base62":10,"esprima-fb":9}],27:[function(_dereq_,module,exports){var Syntax=_dereq_('esprima-fb').Syntax;var utils=_dereq_('../src/utils');var reservedWordsHelper=_dereq_('./reserved-words-helper');var restParamVisitors=_dereq_('./es6-rest-param-visitors');var restPropertyHelpers=_dereq_('./es7-rest-property-helpers');function visitStructuredVariable(traverse,node,path,state){utils.append(utils.getTempVar(state.localScope.tempVarIndex)+'=',state);utils.catchupWhiteSpace(node.init.range[0],state);traverse(node.init,path,state);utils.catchup(node.init.range[1],state);utils.append(','+getDestructuredComponents(node.id,state),state);state.localScope.tempVarIndex++;return false;}
visitStructuredVariable.test=function(node,path,state){return node.type===Syntax.VariableDeclarator&&isStructuredPattern(node.id);};function isStructuredPattern(node){return node.type===Syntax.ObjectPattern||node.type===Syntax.ArrayPattern;}

function getDestructuredComponents(node,state){var tmpIndex=state.localScope.tempVarIndex;var components=[];var patternItems=getPatternItems(node);for(var idx=0;idx<patternItems.length;idx++){var item=patternItems[idx];if(!item){continue;}
if(item.type===Syntax.SpreadElement){
components.push(item.argument.name+'=Array.prototype.slice.call('+
utils.getTempVar(tmpIndex)+','+idx+')');continue;}
if(item.type===Syntax.SpreadProperty){var restExpression=restPropertyHelpers.renderRestExpression(utils.getTempVar(tmpIndex),patternItems);components.push(item.argument.name+'='+restExpression);continue;}

var accessor=getPatternItemAccessor(node,item,tmpIndex,idx);var value=getPatternItemValue(node,item);if(value.type===Syntax.Identifier){components.push(value.name+'='+accessor);}else{components.push(utils.getTempVar(++state.localScope.tempVarIndex)+'='+accessor+','+getDestructuredComponents(value,state));}}
return components.join(',');}
function getPatternItems(node){return node.properties||node.elements;}
function getPatternItemAccessor(node,patternItem,tmpIndex,idx){var tmpName=utils.getTempVar(tmpIndex);if(node.type===Syntax.ObjectPattern){if(reservedWordsHelper.isReservedWord(patternItem.key.name)){return tmpName+'["'+patternItem.key.name+'"]';}else if(patternItem.key.type===Syntax.Literal){return tmpName+'['+JSON.stringify(patternItem.key.value)+']';}else if(patternItem.key.type===Syntax.Identifier){return tmpName+'.'+patternItem.key.name;}}else if(node.type===Syntax.ArrayPattern){return tmpName+'['+idx+']';}}
function getPatternItemValue(node,patternItem){return node.type===Syntax.ObjectPattern?patternItem.value:patternItem;}
function visitStructuredAssignment(traverse,node,path,state){var exprNode=node.expression;utils.append('var '+utils.getTempVar(state.localScope.tempVarIndex)+'=',state);utils.catchupWhiteSpace(exprNode.right.range[0],state);traverse(exprNode.right,path,state);utils.catchup(exprNode.right.range[1],state);utils.append(';'+getDestructuredComponents(exprNode.left,state)+';',state);utils.catchupWhiteSpace(node.range[1],state);state.localScope.tempVarIndex++;return false;}
visitStructuredAssignment.test=function(node,path,state){

return node.type===Syntax.ExpressionStatement&&node.expression.type===Syntax.AssignmentExpression&&isStructuredPattern(node.expression.left);};
function visitStructuredParameter(traverse,node,path,state){utils.append(utils.getTempVar(getParamIndex(node,path)),state);utils.catchupWhiteSpace(node.range[1],state);return true;}
function getParamIndex(paramNode,path){var funcNode=path[0];var tmpIndex=0;for(var k=0;k<funcNode.params.length;k++){var param=funcNode.params[k];if(param===paramNode){break;}
if(isStructuredPattern(param)){tmpIndex++;}}
return tmpIndex;}
visitStructuredParameter.test=function(node,path,state){return isStructuredPattern(node)&&isFunctionNode(path[0]);};function isFunctionNode(node){return(node.type==Syntax.FunctionDeclaration||node.type==Syntax.FunctionExpression||node.type==Syntax.MethodDefinition||node.type==Syntax.ArrowFunctionExpression);}
function visitFunctionBodyForStructuredParameter(traverse,node,path,state){var funcNode=path[0];utils.catchup(funcNode.body.range[0]+1,state);renderDestructuredComponents(funcNode,state);if(funcNode.rest){utils.append(restParamVisitors.renderRestParamSetup(funcNode,state),state);}
return true;}
function renderDestructuredComponents(funcNode,state){var destructuredComponents=[];for(var k=0;k<funcNode.params.length;k++){var param=funcNode.params[k];if(isStructuredPattern(param)){destructuredComponents.push(getDestructuredComponents(param,state));state.localScope.tempVarIndex++;}}
if(destructuredComponents.length){utils.append('var '+destructuredComponents.join(',')+';',state);}}
visitFunctionBodyForStructuredParameter.test=function(node,path,state){return node.type===Syntax.BlockStatement&&isFunctionNode(path[0]);};exports.visitorList=[visitStructuredVariable,visitStructuredAssignment,visitStructuredParameter,visitFunctionBodyForStructuredParameter];exports.renderDestructuredComponents=renderDestructuredComponents;},{"../src/utils":23,"./es6-rest-param-visitors":30,"./es7-rest-property-helpers":32,"./reserved-words-helper":34,"esprima-fb":9}],28:[function(_dereq_,module,exports){var Syntax=_dereq_('esprima-fb').Syntax;var utils=_dereq_('../src/utils');var reservedWordsHelper=_dereq_('./reserved-words-helper');function visitObjectConciseMethod(traverse,node,path,state){var isGenerator=node.value.generator;if(isGenerator){utils.catchupWhiteSpace(node.range[0]+1,state);}
if(node.computed){utils.catchup(node.key.range[1]+1,state);}else if(reservedWordsHelper.isReservedWord(node.key.name)){utils.catchup(node.key.range[0],state);utils.append('"',state);utils.catchup(node.key.range[1],state);utils.append('"',state);}
utils.catchup(node.key.range[1],state);utils.append(':function'+(isGenerator?'*':''),state);path.unshift(node);traverse(node.value,path,state);path.shift();return false;}
visitObjectConciseMethod.test=function(node,path,state){return node.type===Syntax.Property&&node.value.type===Syntax.FunctionExpression&&node.method===true;};exports.visitorList=[visitObjectConciseMethod];},{"../src/utils":23,"./reserved-words-helper":34,"esprima-fb":9}],29:[function(_dereq_,module,exports){var Syntax=_dereq_('esprima-fb').Syntax;var utils=_dereq_('../src/utils');function visitObjectLiteralShortNotation(traverse,node,path,state){utils.catchup(node.key.range[1],state);utils.append(':'+node.key.name,state);return false;}
visitObjectLiteralShortNotation.test=function(node,path,state){return node.type===Syntax.Property&&node.kind==='init'&&node.shorthand===true&&path[0].type!==Syntax.ObjectPattern;};exports.visitorList=[visitObjectLiteralShortNotation];},{"../src/utils":23,"esprima-fb":9}],30:[function(_dereq_,module,exports){var Syntax=_dereq_('esprima-fb').Syntax;var utils=_dereq_('../src/utils');function _nodeIsFunctionWithRestParam(node){return(node.type===Syntax.FunctionDeclaration||node.type===Syntax.FunctionExpression||node.type===Syntax.ArrowFunctionExpression)&&node.rest;}
function visitFunctionParamsWithRestParam(traverse,node,path,state){if(node.parametricType){utils.catchup(node.parametricType.range[0],state);path.unshift(node);traverse(node.parametricType,path,state);path.shift();}
if(node.params.length){path.unshift(node);traverse(node.params,path,state);path.shift();}else{utils.catchup(node.rest.range[0]-3,state);}
utils.catchupWhiteSpace(node.rest.range[1],state);path.unshift(node);traverse(node.body,path,state);path.shift();return false;}
visitFunctionParamsWithRestParam.test=function(node,path,state){return _nodeIsFunctionWithRestParam(node);};function renderRestParamSetup(functionNode,state){var idx=state.localScope.tempVarIndex++;var len=state.localScope.tempVarIndex++;return'for (var '+functionNode.rest.name+'=[],'+
utils.getTempVar(idx)+'='+functionNode.params.length+','+
utils.getTempVar(len)+'=arguments.length;'+
utils.getTempVar(idx)+'<'+utils.getTempVar(len)+';'+
utils.getTempVar(idx)+'++) '+
functionNode.rest.name+'.push(arguments['+utils.getTempVar(idx)+']);';}
function visitFunctionBodyWithRestParam(traverse,node,path,state){utils.catchup(node.range[0]+1,state);var parentNode=path[0];utils.append(renderRestParamSetup(parentNode,state),state);return true;}
visitFunctionBodyWithRestParam.test=function(node,path,state){return node.type===Syntax.BlockStatement&&_nodeIsFunctionWithRestParam(path[0]);};exports.renderRestParamSetup=renderRestParamSetup;exports.visitorList=[visitFunctionParamsWithRestParam,visitFunctionBodyWithRestParam];},{"../src/utils":23,"esprima-fb":9}],31:[function(_dereq_,module,exports){'use strict';var Syntax=_dereq_('esprima-fb').Syntax;var utils=_dereq_('../src/utils');function visitTemplateLiteral(traverse,node,path,state){var templateElements=node.quasis;utils.append('(',state);for(var ii=0;ii<templateElements.length;ii++){var templateElement=templateElements[ii];if(templateElement.value.raw!==''){utils.append(getCookedValue(templateElement),state);if(!templateElement.tail){ utils.append(' + ',state);} 
utils.move(templateElement.range[0],state);utils.catchupNewlines(templateElement.range[1],state);}else{

if(ii>0&&!templateElement.tail){ utils.append(' + ',state);}}
utils.move(templateElement.range[1],state);if(!templateElement.tail){var substitution=node.expressions[ii];if(substitution.type===Syntax.Identifier||substitution.type===Syntax.MemberExpression||substitution.type===Syntax.CallExpression){utils.catchup(substitution.range[1],state);}else{utils.append('(',state);traverse(substitution,path,state);utils.catchup(substitution.range[1],state);utils.append(')',state);}
if(templateElements[ii+1].value.cooked!==''){utils.append(' + ',state);}}}
utils.move(node.range[1],state);utils.append(')',state);return false;}
visitTemplateLiteral.test=function(node,path,state){return node.type===Syntax.TemplateLiteral;};function visitTaggedTemplateExpression(traverse,node,path,state){var template=node.quasi;var numQuasis=template.quasis.length; utils.move(node.tag.range[0],state);traverse(node.tag,path,state);utils.catchup(node.tag.range[1],state); utils.append('(function() { var siteObj = [',state);for(var ii=0;ii<numQuasis;ii++){utils.append(getCookedValue(template.quasis[ii]),state);if(ii!==numQuasis-1){utils.append(', ',state);}}
utils.append(']; siteObj.raw = [',state);for(ii=0;ii<numQuasis;ii++){utils.append(getRawValue(template.quasis[ii]),state);if(ii!==numQuasis-1){utils.append(', ',state);}}
utils.append(']; Object.freeze(siteObj.raw); Object.freeze(siteObj); return siteObj; }()',state); if(numQuasis>1){for(ii=0;ii<template.expressions.length;ii++){var expression=template.expressions[ii];utils.append(', ',state);
 utils.move(template.quasis[ii].range[0],state);utils.catchupNewlines(template.quasis[ii].range[1],state);utils.move(expression.range[0],state);traverse(expression,path,state);utils.catchup(expression.range[1],state);}}

utils.catchupNewlines(node.range[1],state);utils.append(')',state);return false;}
visitTaggedTemplateExpression.test=function(node,path,state){return node.type===Syntax.TaggedTemplateExpression;};function getCookedValue(templateElement){return JSON.stringify(templateElement.value.cooked);}
function getRawValue(templateElement){return JSON.stringify(templateElement.value.raw);}
exports.visitorList=[visitTemplateLiteral,visitTaggedTemplateExpression];},{"../src/utils":23,"esprima-fb":9}],32:[function(_dereq_,module,exports){var Syntax=_dereq_('esprima-fb').Syntax;
var restFunction='(function(source, exclusion) {'+'var rest = {};'+'var hasOwn = Object.prototype.hasOwnProperty;'+'if (source == null) {'+'throw new TypeError();'+'}'+'for (var key in source) {'+'if (hasOwn.call(source, key) && !hasOwn.call(exclusion, key)) {'+'rest[key] = source[key];'+'}'+'}'+'return rest;'+'})';function getPropertyNames(properties){var names=[];for(var i=0;i<properties.length;i++){var property=properties[i];if(property.type===Syntax.SpreadProperty){continue;}
if(property.type===Syntax.Identifier){names.push(property.name);}else{names.push(property.key.name);}}
return names;}
function getRestFunctionCall(source,exclusion){return restFunction+'('+source+','+exclusion+')';}
function getSimpleShallowCopy(accessorExpression){
return getRestFunctionCall(accessorExpression,'{}');}
function renderRestExpression(accessorExpression,excludedProperties){var excludedNames=getPropertyNames(excludedProperties);if(!excludedNames.length){return getSimpleShallowCopy(accessorExpression);}
return getRestFunctionCall(accessorExpression,'{'+excludedNames.join(':1,')+':1}');}
exports.renderRestExpression=renderRestExpression;},{"esprima-fb":9}],33:[function(_dereq_,module,exports){var Syntax=_dereq_('esprima-fb').Syntax;var utils=_dereq_('../src/utils');function visitObjectLiteralSpread(traverse,node,path,state){utils.catchup(node.range[0],state);utils.append('Object.assign({',state);utils.move(node.range[0]+1,state);var previousWasSpread=false;for(var i=0;i<node.properties.length;i++){var property=node.properties[i];if(property.type===Syntax.SpreadProperty){ if(!previousWasSpread){utils.append('}',state);}
if(i===0){
utils.append(',',state);}
utils.catchup(property.range[0],state);utils.move(property.range[0]+3,state);traverse(property.argument,path,state);utils.catchup(property.range[1],state);previousWasSpread=true;}else{utils.catchup(property.range[0],state);if(previousWasSpread){utils.append('{',state);}
traverse(property,path,state);utils.catchup(property.range[1],state);previousWasSpread=false;}}


utils.catchupWhiteSpace(node.range[1]-1,state);utils.move(node.range[1],state);if(!previousWasSpread){utils.append('}',state);}
utils.append(')',state);return false;}
visitObjectLiteralSpread.test=function(node,path,state){if(node.type!==Syntax.ObjectExpression){return false;} 
var hasAtLeastOneSpreadProperty=false;for(var i=0;i<node.properties.length;i++){var property=node.properties[i];if(property.type===Syntax.SpreadProperty){hasAtLeastOneSpreadProperty=true;}else if(property.kind!=='init'){return false;}}
return hasAtLeastOneSpreadProperty;};exports.visitorList=[visitObjectLiteralSpread];},{"../src/utils":23,"esprima-fb":9}],34:[function(_dereq_,module,exports){var KEYWORDS=['break','do','in','typeof','case','else','instanceof','var','catch','export','new','void','class','extends','return','while','const','finally','super','with','continue','for','switch','yield','debugger','function','this','default','if','throw','delete','import','try'];var FUTURE_RESERVED_WORDS=['enum','await','implements','package','protected','static','interface','private','public'];var LITERALS=['null','true','false'];var RESERVED_WORDS=[].concat(KEYWORDS,FUTURE_RESERVED_WORDS,LITERALS);var reservedWordsMap=Object.create(null);RESERVED_WORDS.forEach(function(k){reservedWordsMap[k]=true;});var ES3_FUTURE_RESERVED_WORDS=['enum','implements','package','protected','static','interface','private','public'];var ES3_RESERVED_WORDS=[].concat(KEYWORDS,ES3_FUTURE_RESERVED_WORDS,LITERALS);var es3ReservedWordsMap=Object.create(null);ES3_RESERVED_WORDS.forEach(function(k){es3ReservedWordsMap[k]=true;});exports.isReservedWord=function(word){return!!reservedWordsMap[word];};exports.isES3ReservedWord=function(word){return!!es3ReservedWordsMap[word];};},{}],35:[function(_dereq_,module,exports){var Syntax=_dereq_('esprima-fb').Syntax;var utils=_dereq_('../src/utils');var reserverdWordsHelper=_dereq_('./reserved-words-helper');function visitProperty(traverse,node,path,state){utils.catchup(node.key.range[0],state);utils.append('"',state);utils.catchup(node.key.range[1],state);utils.append('"',state);utils.catchup(node.value.range[0],state);traverse(node.value,path,state);return false;}
visitProperty.test=function(node){return node.type===Syntax.Property&&node.key.type===Syntax.Identifier&&!node.method&&!node.shorthand&&!node.computed&&reserverdWordsHelper.isES3ReservedWord(node.key.name);};function visitMemberExpression(traverse,node,path,state){traverse(node.object,path,state);utils.catchup(node.property.range[0]-1,state);utils.append('[',state);utils.catchupWhiteSpace(node.property.range[0],state);utils.append('"',state);utils.catchup(node.property.range[1],state);utils.append('"]',state);return false;}
visitMemberExpression.test=function(node){return node.type===Syntax.MemberExpression&&node.property.type===Syntax.Identifier&&reserverdWordsHelper.isES3ReservedWord(node.property.name);};exports.visitorList=[visitProperty,visitMemberExpression];},{"../src/utils":23,"./reserved-words-helper":34,"esprima-fb":9}],36:[function(_dereq_,module,exports){var esprima=_dereq_('esprima-fb');var utils=_dereq_('../src/utils');var Syntax=esprima.Syntax;function _isFunctionNode(node){return node.type===Syntax.FunctionDeclaration||node.type===Syntax.FunctionExpression||node.type===Syntax.ArrowFunctionExpression;}
function visitClassProperty(traverse,node,path,state){utils.catchup(node.range[0],state);utils.catchupWhiteOut(node.range[1],state);return false;}
visitClassProperty.test=function(node,path,state){return node.type===Syntax.ClassProperty;};function visitTypeAlias(traverse,node,path,state){utils.catchupWhiteOut(node.range[1],state);return false;}
visitTypeAlias.test=function(node,path,state){return node.type===Syntax.TypeAlias;};function visitTypeCast(traverse,node,path,state){path.unshift(node);traverse(node.expression,path,state);path.shift();utils.catchup(node.typeAnnotation.range[0],state);utils.catchupWhiteOut(node.typeAnnotation.range[1],state);return false;}
visitTypeCast.test=function(node,path,state){return node.type===Syntax.TypeCastExpression;};function visitInterfaceDeclaration(traverse,node,path,state){utils.catchupWhiteOut(node.range[1],state);return false;}
visitInterfaceDeclaration.test=function(node,path,state){return node.type===Syntax.InterfaceDeclaration;};function visitDeclare(traverse,node,path,state){utils.catchupWhiteOut(node.range[1],state);return false;}
visitDeclare.test=function(node,path,state){switch(node.type){case Syntax.DeclareVariable:case Syntax.DeclareFunction:case Syntax.DeclareClass:case Syntax.DeclareModule:return true;}
return false;};function visitFunctionParametricAnnotation(traverse,node,path,state){utils.catchup(node.range[0],state);utils.catchupWhiteOut(node.range[1],state);return false;}
visitFunctionParametricAnnotation.test=function(node,path,state){return node.type===Syntax.TypeParameterDeclaration&&path[0]&&_isFunctionNode(path[0])&&node===path[0].typeParameters;};function visitFunctionReturnAnnotation(traverse,node,path,state){utils.catchup(node.range[0],state);utils.catchupWhiteOut(node.range[1],state);return false;}
visitFunctionReturnAnnotation.test=function(node,path,state){return path[0]&&_isFunctionNode(path[0])&&node===path[0].returnType;};function visitOptionalFunctionParameterAnnotation(traverse,node,path,state){utils.catchup(node.range[0]+node.name.length,state);utils.catchupWhiteOut(node.range[1],state);return false;}
visitOptionalFunctionParameterAnnotation.test=function(node,path,state){return node.type===Syntax.Identifier&&node.optional&&path[0]&&_isFunctionNode(path[0]);};function visitTypeAnnotatedIdentifier(traverse,node,path,state){utils.catchup(node.typeAnnotation.range[0],state);utils.catchupWhiteOut(node.typeAnnotation.range[1],state);return false;}
visitTypeAnnotatedIdentifier.test=function(node,path,state){return node.type===Syntax.Identifier&&node.typeAnnotation;};function visitTypeAnnotatedObjectOrArrayPattern(traverse,node,path,state){utils.catchup(node.typeAnnotation.range[0],state);utils.catchupWhiteOut(node.typeAnnotation.range[1],state);return false;}
visitTypeAnnotatedObjectOrArrayPattern.test=function(node,path,state){var rightType=node.type===Syntax.ObjectPattern||node.type===Syntax.ArrayPattern;return rightType&&node.typeAnnotation;};function visitMethod(traverse,node,path,state){path.unshift(node);traverse(node.key,path,state);path.unshift(node.value);traverse(node.value.params,path,state);node.value.rest&&traverse(node.value.rest,path,state);node.value.returnType&&traverse(node.value.returnType,path,state);traverse(node.value.body,path,state);path.shift();path.shift();return false;}
visitMethod.test=function(node,path,state){return(node.type==="Property"&&(node.method||node.kind==="set"||node.kind==="get"))||(node.type==="MethodDefinition");};function visitImportType(traverse,node,path,state){utils.catchupWhiteOut(node.range[1],state);return false;}
visitImportType.test=function(node,path,state){return node.type==='ImportDeclaration'&&node.isType;};exports.visitorList=[visitClassProperty,visitDeclare,visitImportType,visitInterfaceDeclaration,visitFunctionParametricAnnotation,visitFunctionReturnAnnotation,visitMethod,visitOptionalFunctionParameterAnnotation,visitTypeAlias,visitTypeCast,visitTypeAnnotatedIdentifier,visitTypeAnnotatedObjectOrArrayPattern];},{"../src/utils":23,"esprima-fb":9}],37:[function(_dereq_,module,exports){'use strict';var Syntax=_dereq_('jstransform').Syntax;var utils=_dereq_('jstransform/src/utils');function renderJSXLiteral(object,isLast,state,start,end){var lines=object.value.split(/\r\n|\n|\r/);if(start){utils.append(start,state);}
var lastNonEmptyLine=0;lines.forEach(function(line,index){if(line.match(/[^ \t]/)){lastNonEmptyLine=index;}});lines.forEach(function(line,index){var isFirstLine=index===0;var isLastLine=index===lines.length-1;var isLastNonEmptyLine=index===lastNonEmptyLine; var trimmedLine=line.replace(/\t/g,' '); if(!isFirstLine){trimmedLine=trimmedLine.replace(/^[ ]+/,'');}
if(!isLastLine){trimmedLine=trimmedLine.replace(/[ ]+$/,'');}
if(!isFirstLine){utils.append(line.match(/^[ \t]*/)[0],state);}
if(trimmedLine||isLastNonEmptyLine){utils.append(JSON.stringify(trimmedLine)+
(!isLastNonEmptyLine?' + \' \' +':''),state);if(isLastNonEmptyLine){if(end){utils.append(end,state);}
if(!isLast){utils.append(', ',state);}} 
if(trimmedLine&&!isLastLine){utils.append(line.match(/[ \t]*$/)[0],state);}}
if(!isLastLine){utils.append('\n',state);}});utils.move(object.range[1],state);}
function renderJSXExpressionContainer(traverse,object,isLast,path,state){utils.move(object.range[0]+1,state);utils.catchup(object.expression.range[0],state);traverse(object.expression,path,state);if(!isLast&&object.expression.type!==Syntax.JSXEmptyExpression){utils.catchup(object.expression.range[1],state,trimLeft);utils.append(', ',state);}
utils.catchup(object.range[1]-1,state,trimLeft);utils.move(object.range[1],state);return false;}
function quoteAttrName(attr){if(!/^[a-z_$][a-z\d_$]*$/i.test(attr)){return'"'+attr+'"';}
return attr;}
function trimLeft(value){return value.replace(/^[ ]+/,'');}
exports.renderJSXExpressionContainer=renderJSXExpressionContainer;exports.renderJSXLiteral=renderJSXLiteral;exports.quoteAttrName=quoteAttrName;exports.trimLeft=trimLeft;},{"jstransform":22,"jstransform/src/utils":23}],38:[function(_dereq_,module,exports){'use strict';var Syntax=_dereq_('jstransform').Syntax;var utils=_dereq_('jstransform/src/utils');var renderJSXExpressionContainer=_dereq_('./jsx').renderJSXExpressionContainer;var renderJSXLiteral=_dereq_('./jsx').renderJSXLiteral;var quoteAttrName=_dereq_('./jsx').quoteAttrName;var trimLeft=_dereq_('./jsx').trimLeft;var reNonWhiteParen=/([^\s\(\)])/g;function stripNonWhiteParen(value){return value.replace(reNonWhiteParen,'');}
var tagConvention=/^[a-z]|\-/;function isTagName(name){return tagConvention.test(name);}
function visitReactTag(traverse,object,path,state){var openingElement=object.openingElement;var nameObject=openingElement.name;var attributesObject=openingElement.attributes;utils.catchup(openingElement.range[0],state,trimLeft);if(nameObject.type===Syntax.JSXNamespacedName&&nameObject.namespace){throw new Error('Namespace tags are not supported. ReactJSX is not XML.');} 
utils.append('React.createElement(',state);if(nameObject.type===Syntax.JSXIdentifier&&isTagName(nameObject.name)){utils.append('"'+nameObject.name+'"',state);utils.move(nameObject.range[1],state);}else{

utils.move(nameObject.range[0],state);utils.catchup(nameObject.range[1],state);}
utils.append(', ',state);var hasAttributes=attributesObject.length;var hasAtLeastOneSpreadProperty=attributesObject.some(function(attr){return attr.type===Syntax.JSXSpreadAttribute;}); if(hasAtLeastOneSpreadProperty){utils.append('React.__spread({',state);}else if(hasAttributes){utils.append('{',state);}else{utils.append('null',state);} 
var previousWasSpread=false; attributesObject.forEach(function(attr,index){var isLast=index===attributesObject.length-1;if(attr.type===Syntax.JSXSpreadAttribute){ if(!previousWasSpread){utils.append('}, ',state);}

utils.catchup(attr.range[0],state,stripNonWhiteParen);utils.move(attr.range[0]+1,state);utils.catchup(attr.argument.range[0],state,stripNonWhiteParen);traverse(attr.argument,path,state);utils.catchup(attr.argument.range[1],state);utils.catchup(attr.range[1]-1,state,stripNonWhiteParen);if(!isLast){utils.append(', ',state);}
utils.move(attr.range[1],state);previousWasSpread=true;return;} 
if(!isLast){isLast=attributesObject[index+1].type===Syntax.JSXSpreadAttribute;}
if(attr.name.namespace){throw new Error('Namespace attributes are not supported. ReactJSX is not XML.');}
var name=attr.name.name;utils.catchup(attr.range[0],state,trimLeft);if(previousWasSpread){utils.append('{',state);}
utils.append(quoteAttrName(name),state);utils.append(': ',state);if(!attr.value){state.g.buffer+='true';state.g.position=attr.name.range[1];if(!isLast){utils.append(', ',state);}}else{utils.move(attr.name.range[1],state); utils.catchupNewlines(attr.value.range[0],state);if(attr.value.type===Syntax.Literal){renderJSXLiteral(attr.value,isLast,state);}else{renderJSXExpressionContainer(traverse,attr.value,isLast,path,state);}}
utils.catchup(attr.range[1],state,trimLeft);previousWasSpread=false;});if(!openingElement.selfClosing){utils.catchup(openingElement.range[1]-1,state,trimLeft);utils.move(openingElement.range[1],state);}
if(hasAttributes&&!previousWasSpread){utils.append('}',state);}
if(hasAtLeastOneSpreadProperty){utils.append(')',state);} 
var childrenToRender=object.children.filter(function(child){return!(child.type===Syntax.Literal&&typeof child.value==='string'&&child.value.match(/^[ \t]*[\r\n][ \t\r\n]*$/));});if(childrenToRender.length>0){var lastRenderableIndex;childrenToRender.forEach(function(child,index){if(child.type!==Syntax.JSXExpressionContainer||child.expression.type!==Syntax.JSXEmptyExpression){lastRenderableIndex=index;}});if(lastRenderableIndex!==undefined){utils.append(', ',state);}
childrenToRender.forEach(function(child,index){utils.catchup(child.range[0],state,trimLeft);var isLast=index>=lastRenderableIndex;if(child.type===Syntax.Literal){renderJSXLiteral(child,isLast,state);}else if(child.type===Syntax.JSXExpressionContainer){renderJSXExpressionContainer(traverse,child,isLast,path,state);}else{traverse(child,path,state);if(!isLast){utils.append(', ',state);}}
utils.catchup(child.range[1],state,trimLeft);});}
if(openingElement.selfClosing){utils.catchup(openingElement.range[1]-2,state,trimLeft);utils.move(openingElement.range[1],state);}else{utils.catchup(object.closingElement.range[0],state,trimLeft);utils.move(object.closingElement.range[1],state);}
utils.append(')',state);return false;}
visitReactTag.test=function(object,path,state){return object.type===Syntax.JSXElement;};exports.visitorList=[visitReactTag];},{"./jsx":37,"jstransform":22,"jstransform/src/utils":23}],39:[function(_dereq_,module,exports){'use strict';var Syntax=_dereq_('jstransform').Syntax;var utils=_dereq_('jstransform/src/utils');function addDisplayName(displayName,object,state){if(object&&object.type===Syntax.CallExpression&&object.callee.type===Syntax.MemberExpression&&object.callee.object.type===Syntax.Identifier&&object.callee.object.name==='React'&&object.callee.property.type===Syntax.Identifier&&object.callee.property.name==='createClass'&&object.arguments.length===1&&object.arguments[0].type===Syntax.ObjectExpression){ var properties=object.arguments[0].properties;var safe=properties.every(function(property){var value=property.key.type===Syntax.Identifier?property.key.name:property.key.value;return value!=='displayName';});if(safe){utils.catchup(object.arguments[0].range[0]+1,state);utils.append('displayName: "'+displayName+'",',state);}}}
function visitReactDisplayName(traverse,object,path,state){var left,right;if(object.type===Syntax.AssignmentExpression){left=object.left;right=object.right;}else if(object.type===Syntax.Property){left=object.key;right=object.value;}else if(object.type===Syntax.VariableDeclarator){left=object.id;right=object.init;}
if(left&&left.type===Syntax.MemberExpression){left=left.property;}
if(left&&left.type===Syntax.Identifier){addDisplayName(left.name,right,state);}}
visitReactDisplayName.test=function(object,path,state){return(object.type===Syntax.AssignmentExpression||object.type===Syntax.Property||object.type===Syntax.VariableDeclarator);};exports.visitorList=[visitReactDisplayName];},{"jstransform":22,"jstransform/src/utils":23}],40:[function(_dereq_,module,exports){'use strict';var es6ArrowFunctions=_dereq_('jstransform/visitors/es6-arrow-function-visitors');var es6Classes=_dereq_('jstransform/visitors/es6-class-visitors');var es6Destructuring=_dereq_('jstransform/visitors/es6-destructuring-visitors');var es6ObjectConciseMethod=_dereq_('jstransform/visitors/es6-object-concise-method-visitors');var es6ObjectShortNotation=_dereq_('jstransform/visitors/es6-object-short-notation-visitors');var es6RestParameters=_dereq_('jstransform/visitors/es6-rest-param-visitors');var es6Templates=_dereq_('jstransform/visitors/es6-template-visitors');var es6CallSpread=_dereq_('jstransform/visitors/es6-call-spread-visitors');var es7SpreadProperty=_dereq_('jstransform/visitors/es7-spread-property-visitors');var react=_dereq_('./transforms/react');var reactDisplayName=_dereq_('./transforms/reactDisplayName');var reservedWords=_dereq_('jstransform/visitors/reserved-words-visitors');var transformVisitors={'es6-arrow-functions':es6ArrowFunctions.visitorList,'es6-classes':es6Classes.visitorList,'es6-destructuring':es6Destructuring.visitorList,'es6-object-concise-method':es6ObjectConciseMethod.visitorList,'es6-object-short-notation':es6ObjectShortNotation.visitorList,'es6-rest-params':es6RestParameters.visitorList,'es6-templates':es6Templates.visitorList,'es6-call-spread':es6CallSpread.visitorList,'es7-spread-property':es7SpreadProperty.visitorList,'react':react.visitorList.concat(reactDisplayName.visitorList),'reserved-words':reservedWords.visitorList};var transformSets={'harmony':['es6-arrow-functions','es6-object-concise-method','es6-object-short-notation','es6-classes','es6-rest-params','es6-templates','es6-destructuring','es6-call-spread','es7-spread-property'],'es3':['reserved-words'],'react':['react']};var transformRunOrder=['reserved-words','es6-arrow-functions','es6-object-concise-method','es6-object-short-notation','es6-classes','es6-rest-params','es6-templates','es6-destructuring','es6-call-spread','es7-spread-property','react'];function getAllVisitors(excludes){var ret=[];for(var i=0,il=transformRunOrder.length;i<il;i++){if(!excludes||excludes.indexOf(transformRunOrder[i])===-1){ret=ret.concat(transformVisitors[transformRunOrder[i]]);}}
return ret;}
function getVisitorsBySet(sets){var visitorsToInclude=sets.reduce(function(visitors,set){if(!transformSets.hasOwnProperty(set)){throw new Error('Unknown visitor set: '+set);}
transformSets[set].forEach(function(visitor){visitors[visitor]=true;});return visitors;},{});var visitorList=[];for(var i=0;i<transformRunOrder.length;i++){if(visitorsToInclude.hasOwnProperty(transformRunOrder[i])){visitorList=visitorList.concat(transformVisitors[transformRunOrder[i]]);}}
return visitorList;}
exports.getVisitorsBySet=getVisitorsBySet;exports.getAllVisitors=getAllVisitors;exports.transformVisitors=transformVisitors;},{"./transforms/react":38,"./transforms/reactDisplayName":39,"jstransform/visitors/es6-arrow-function-visitors":24,"jstransform/visitors/es6-call-spread-visitors":25,"jstransform/visitors/es6-class-visitors":26,"jstransform/visitors/es6-destructuring-visitors":27,"jstransform/visitors/es6-object-concise-method-visitors":28,"jstransform/visitors/es6-object-short-notation-visitors":29,"jstransform/visitors/es6-rest-param-visitors":30,"jstransform/visitors/es6-template-visitors":31,"jstransform/visitors/es7-spread-property-visitors":33,"jstransform/visitors/reserved-words-visitors":35}],41:[function(_dereq_,module,exports){'use strict';var Buffer=_dereq_('buffer').Buffer;function inlineSourceMap(sourceMap,sourceCode,sourceFilename){var json=sourceMap;if(typeof sourceMap.toJSON==='function'){json=sourceMap.toJSON();}
json.sources=[sourceFilename];json.sourcesContent=[sourceCode];var base64=Buffer(JSON.stringify(json)).toString('base64');return'//# sourceMappingURL=data:application/json;base64,'+base64;}
module.exports=inlineSourceMap;},{"buffer":3}]},{},[1])(1)});(function(jQuery,undefined){var stepHooks="backgroundColor borderBottomColor borderLeftColor borderRightColor borderTopColor color columnRuleColor outlineColor textDecorationColor textEmphasisColor", rplusequals=/^([\-+])=\s*(\d+\.?\d*)/,stringParsers=[{re:/rgba?\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,parse:function(execResult){return[execResult[1],execResult[2],execResult[3],execResult[4]];}},{re:/rgba?\(\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,parse:function(execResult){return[execResult[1]*2.55,execResult[2]*2.55,execResult[3]*2.55,execResult[4]];}},{ re:/#([a-f0-9]{2})([a-f0-9]{2})([a-f0-9]{2})/,parse:function(execResult){return[parseInt(execResult[1],16),parseInt(execResult[2],16),parseInt(execResult[3],16)];}},{ re:/#([a-f0-9])([a-f0-9])([a-f0-9])/,parse:function(execResult){return[parseInt(execResult[1]+execResult[1],16),parseInt(execResult[2]+execResult[2],16),parseInt(execResult[3]+execResult[3],16)];}},{re:/hsla?\(\s*(\d+(?:\.\d+)?)\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,space:"hsla",parse:function(execResult){return[execResult[1],execResult[2]/100,execResult[3]/100,execResult[4]];}}],color=jQuery.Color=function(color,green,blue,alpha){return new jQuery.Color.fn.parse(color,green,blue,alpha);},spaces={rgba:{props:{red:{idx:0,type:"byte"},green:{idx:1,type:"byte"},blue:{idx:2,type:"byte"}}},hsla:{props:{hue:{idx:0,type:"degrees"},saturation:{idx:1,type:"percent"},lightness:{idx:2,type:"percent"}}}},propTypes={"byte":{floor:true,max:255},"percent":{max:1},"degrees":{mod:360,floor:true}},support=color.support={}, supportElem=jQuery("<p>")[0], colors, each=jQuery.each;supportElem.style.cssText="background-color:rgba(1,1,1,.5)";support.rgba=supportElem.style.backgroundColor.indexOf("rgba")>-1;
each(spaces,function(spaceName,space){space.cache="_"+spaceName;space.props.alpha={idx:3,type:"percent",def:1};});function clamp(value,prop,allowEmpty){var type=propTypes[prop.type]||{};if(value==null){return(allowEmpty||!prop.def)?null:prop.def;} 
value=type.floor?~~value:parseFloat(value); if(isNaN(value)){return prop.def;}
if(type.mod){
 return(value+type.mod)%type.mod;} 
return 0>value?0:type.max<value?type.max:value;}
function stringParse(string){var inst=color(),rgba=inst._rgba=[];string=string.toLowerCase();each(stringParsers,function(i,parser){var parsed,match=parser.re.exec(string),values=match&&parser.parse(match),spaceName=parser.space||"rgba";if(values){parsed=inst[spaceName](values);
inst[spaces[spaceName].cache]=parsed[spaces[spaceName].cache];rgba=inst._rgba=parsed._rgba; return false;}}); if(rgba.length){
if(rgba.join()==="0,0,0,0"){jQuery.extend(rgba,colors.transparent);}
return inst;} 
return colors[string];}
color.fn=jQuery.extend(color.prototype,{parse:function(red,green,blue,alpha){if(red===undefined){this._rgba=[null,null,null,null];return this;}
if(red.jquery||red.nodeType){red=jQuery(red).css(green);green=undefined;}
var inst=this,type=jQuery.type(red),rgba=this._rgba=[];if(green!==undefined){red=[red,green,blue,alpha];type="array";}
if(type==="string"){return this.parse(stringParse(red)||colors._default);}
if(type==="array"){each(spaces.rgba.props,function(key,prop){rgba[prop.idx]=clamp(red[prop.idx],prop);});return this;}
if(type==="object"){if(red instanceof color){each(spaces,function(spaceName,space){if(red[space.cache]){inst[space.cache]=red[space.cache].slice();}});}else{each(spaces,function(spaceName,space){var cache=space.cache;each(space.props,function(key,prop){ if(!inst[cache]&&space.to){
 if(key==="alpha"||red[key]==null){return;}
inst[cache]=space.to(inst._rgba);} 
inst[cache][prop.idx]=clamp(red[key],prop,true);});if(inst[cache]&&jQuery.inArray(null,inst[cache].slice(0,3))<0){ inst[cache][3]=1;if(space.from){inst._rgba=space.from(inst[cache]);}}});}
return this;}},is:function(compare){var is=color(compare),same=true,inst=this;each(spaces,function(_,space){var localCache,isCache=is[space.cache];if(isCache){localCache=inst[space.cache]||space.to&&space.to(inst._rgba)||[];each(space.props,function(_,prop){if(isCache[prop.idx]!=null){same=(isCache[prop.idx]===localCache[prop.idx]);return same;}});}
return same;});return same;},_space:function(){var used=[],inst=this;each(spaces,function(spaceName,space){if(inst[space.cache]){used.push(spaceName);}});return used.pop();},transition:function(other,distance){var end=color(other),spaceName=end._space(),space=spaces[spaceName],startColor=this.alpha()===0?color("transparent"):this,start=startColor[space.cache]||space.to(startColor._rgba),result=start.slice();end=end[space.cache];each(space.props,function(key,prop){var index=prop.idx,startValue=start[index],endValue=end[index],type=propTypes[prop.type]||{}; if(endValue===null){return;} 
if(startValue===null){result[index]=endValue;}else{if(type.mod){if(endValue-startValue>type.mod/2){startValue+=type.mod;}else if(startValue-endValue>type.mod/2){startValue-=type.mod;}}
result[index]=clamp((endValue-startValue)*distance+startValue,prop);}});return this[spaceName](result);},blend:function(opaque){ if(this._rgba[3]===1){return this;}
var rgb=this._rgba.slice(),a=rgb.pop(),blend=color(opaque)._rgba;return color(jQuery.map(rgb,function(v,i){return(1-a)*blend[i]+a*v;}));},toRgbaString:function(){var prefix="rgba(",rgba=jQuery.map(this._rgba,function(v,i){return v==null?(i>2?1:0):v;});if(rgba[3]===1){rgba.pop();prefix="rgb(";}
return prefix+rgba.join()+")";},toHslaString:function(){var prefix="hsla(",hsla=jQuery.map(this.hsla(),function(v,i){if(v==null){v=i>2?1:0;} 
if(i&&i<3){v=Math.round(v*100)+"%";}
return v;});if(hsla[3]===1){hsla.pop();prefix="hsl(";}
return prefix+hsla.join()+")";},toHexString:function(includeAlpha){var rgba=this._rgba.slice(),alpha=rgba.pop();if(includeAlpha){rgba.push(~~(alpha*255));}
return"#"+jQuery.map(rgba,function(v){ v=(v||0).toString(16);return v.length===1?"0"+v:v;}).join("");},toString:function(){return this._rgba[3]===0?"transparent":this.toRgbaString();}});color.fn.parse.prototype=color.fn;function hue2rgb(p,q,h){h=(h+1)%1;if(h*6<1){return p+(q-p)*h*6;}
if(h*2<1){return q;}
if(h*3<2){return p+(q-p)*((2/3) - h) * 6;	}
return p;}
spaces.hsla.to = function ( rgba ) {	if ( rgba[ 0 ] == null || rgba[ 1 ] == null || rgba[ 2 ] == null ) {		return [ null, null, null, rgba[ 3 ] ];	}
var r = rgba[ 0 ] /255,g=rgba[1]/255,b=rgba[2]/255,a=rgba[3],max=Math.max(r,g,b),min=Math.min(r,g,b),diff=max-min,add=max+min,l=add*0.5,h,s;if(min===max){h=0;}else if(r===max){h=(60*(g-b)/diff)+360;}else if(g===max){h=(60*(b-r)/diff)+120;}else{h=(60*(r-g)/diff)+240;}
if(diff===0){s=0;}else if(l<=0.5){s=diff/add;}else{s=diff/(2-add);}
return[Math.round(h)%360,s,l,a==null?1:a];};spaces.hsla.from=function(hsla){if(hsla[0]==null||hsla[1]==null||hsla[2]==null){return[null,null,null,hsla[3]];}
var h=hsla[0]/360,s=hsla[1],l=hsla[2],a=hsla[3],q=l<=0.5?l*(1+s):l+s-l*s,p=2*l-q;return[Math.round(hue2rgb(p,q,h+(1/3))*255),Math.round(hue2rgb(p,q,h)*255),Math.round(hue2rgb(p,q,h-(1/3))*255),a];};each(spaces,function(spaceName,space){var props=space.props,cache=space.cache,to=space.to,from=space.from;color.fn[spaceName]=function(value){ if(to&&!this[cache]){this[cache]=to(this._rgba);}
if(value===undefined){return this[cache].slice();}
var ret,type=jQuery.type(value),arr=(type==="array"||type==="object")?value:arguments,local=this[cache].slice();each(props,function(key,prop){var val=arr[type==="object"?key:prop.idx];if(val==null){val=local[prop.idx];}
local[prop.idx]=clamp(val,prop);});if(from){ret=color(from(local));ret[cache]=local;return ret;}else{return color(local);}};each(props,function(key,prop){ if(color.fn[key]){return;}
color.fn[key]=function(value){var vtype=jQuery.type(value),fn=(key==="alpha"?(this._hsla?"hsla":"rgba"):spaceName),local=this[fn](),cur=local[prop.idx],match;if(vtype==="undefined"){return cur;}
if(vtype==="function"){value=value.call(this,cur);vtype=jQuery.type(value);}
if(value==null&&prop.empty){return this;}
if(vtype==="string"){match=rplusequals.exec(value);if(match){value=cur+parseFloat(match[2])*(match[1]==="+"?1:-1);}}
local[prop.idx]=value;return this[fn](local);};});});color.hook=function(hook){var hooks=hook.split(" ");each(hooks,function(i,hook){jQuery.cssHooks[hook]={set:function(elem,value){var parsed,curElem,backgroundColor="";if(value!=="transparent"&&(jQuery.type(value)!=="string"||(parsed=stringParse(value)))){value=color(parsed||value);if(!support.rgba&&value._rgba[3]!==1){curElem=hook==="backgroundColor"?elem.parentNode:elem;while((backgroundColor===""||backgroundColor==="transparent")&&curElem&&curElem.style){try{backgroundColor=jQuery.css(curElem,"backgroundColor");curElem=curElem.parentNode;}catch(e){}}
value=value.blend(backgroundColor&&backgroundColor!=="transparent"?backgroundColor:"_default");}
value=value.toRgbaString();}
try{elem.style[hook]=value;}catch(e){}}};jQuery.fx.step[hook]=function(fx){if(!fx.colorInit){fx.start=color(fx.elem,hook);fx.end=color(fx.end);fx.colorInit=true;}
jQuery.cssHooks[hook].set(fx.elem,fx.start.transition(fx.end,fx.pos));};});};color.hook(stepHooks);jQuery.cssHooks.borderColor={expand:function(value){var expanded={};each(["Top","Right","Bottom","Left"],function(i,part){expanded["border"+part+"Color"]=value;});return expanded;}};
colors=jQuery.Color.names={ aqua:"#00ffff",black:"#000000",blue:"#0000ff",fuchsia:"#ff00ff",gray:"#808080",green:"#008000",lime:"#00ff00",maroon:"#800000",navy:"#000080",olive:"#808000",purple:"#800080",red:"#ff0000",silver:"#c0c0c0",teal:"#008080",white:"#ffffff",yellow:"#ffff00", transparent:[null,null,null,0],_default:"#ffffff"};})(jQuery);jQuery.extend(jQuery.Color.names,{aliceblue:"#f0f8ff",antiquewhite:"#faebd7",aquamarine:"#7fffd4",azure:"#f0ffff",beige:"#f5f5dc",bisque:"#ffe4c4",blanchedalmond:"#ffebcd",blueviolet:"#8a2be2",brown:"#a52a2a",burlywood:"#deb887",cadetblue:"#5f9ea0",chartreuse:"#7fff00",chocolate:"#d2691e",coral:"#ff7f50",cornflowerblue:"#6495ed",cornsilk:"#fff8dc",crimson:"#dc143c",cyan:"#00ffff",darkblue:"#00008b",darkcyan:"#008b8b",darkgoldenrod:"#b8860b",darkgray:"#a9a9a9",darkgreen:"#006400",darkgrey:"#a9a9a9",darkkhaki:"#bdb76b",darkmagenta:"#8b008b",darkolivegreen:"#556b2f",darkorange:"#ff8c00",darkorchid:"#9932cc",darkred:"#8b0000",darksalmon:"#e9967a",darkseagreen:"#8fbc8f",darkslateblue:"#483d8b",darkslategray:"#2f4f4f",darkslategrey:"#2f4f4f",darkturquoise:"#00ced1",darkviolet:"#9400d3",deeppink:"#ff1493",deepskyblue:"#00bfff",dimgray:"#696969",dimgrey:"#696969",dodgerblue:"#1e90ff",firebrick:"#b22222",floralwhite:"#fffaf0",forestgreen:"#228b22",gainsboro:"#dcdcdc",ghostwhite:"#f8f8ff",gold:"#ffd700",goldenrod:"#daa520",greenyellow:"#adff2f",grey:"#808080",honeydew:"#f0fff0",hotpink:"#ff69b4",indianred:"#cd5c5c",indigo:"#4b0082",ivory:"#fffff0",khaki:"#f0e68c",lavender:"#e6e6fa",lavenderblush:"#fff0f5",lawngreen:"#7cfc00",lemonchiffon:"#fffacd",lightblue:"#add8e6",lightcoral:"#f08080",lightcyan:"#e0ffff",lightgoldenrodyellow:"#fafad2",lightgray:"#d3d3d3",lightgreen:"#90ee90",lightgrey:"#d3d3d3",lightpink:"#ffb6c1",lightsalmon:"#ffa07a",lightseagreen:"#20b2aa",lightskyblue:"#87cefa",lightslategray:"#778899",lightslategrey:"#778899",lightsteelblue:"#b0c4de",lightyellow:"#ffffe0",limegreen:"#32cd32",linen:"#faf0e6",mediumaquamarine:"#66cdaa",mediumblue:"#0000cd",mediumorchid:"#ba55d3",mediumpurple:"#9370db",mediumseagreen:"#3cb371",mediumslateblue:"#7b68ee",mediumspringgreen:"#00fa9a",mediumturquoise:"#48d1cc",mediumvioletred:"#c71585",midnightblue:"#191970",mintcream:"#f5fffa",mistyrose:"#ffe4e1",moccasin:"#ffe4b5",navajowhite:"#ffdead",oldlace:"#fdf5e6",olivedrab:"#6b8e23",orange:"#ffa500",orangered:"#ff4500",orchid:"#da70d6",palegoldenrod:"#eee8aa",palegreen:"#98fb98",paleturquoise:"#afeeee",palevioletred:"#db7093",papayawhip:"#ffefd5",peachpuff:"#ffdab9",peru:"#cd853f",pink:"#ffc0cb",plum:"#dda0dd",powderblue:"#b0e0e6",rosybrown:"#bc8f8f",royalblue:"#4169e1",saddlebrown:"#8b4513",salmon:"#fa8072",sandybrown:"#f4a460",seagreen:"#2e8b57",seashell:"#fff5ee",sienna:"#a0522d",skyblue:"#87ceeb",slateblue:"#6a5acd",slategray:"#708090",slategrey:"#708090",snow:"#fffafa",springgreen:"#00ff7f",steelblue:"#4682b4",tan:"#d2b48c",thistle:"#d8bfd8",tomato:"#ff6347",turquoise:"#40e0d0",violet:"#ee82ee",wheat:"#f5deb3",whitesmoke:"#f5f5f5",yellowgreen:"#9acd32"});;(function(){this.ResizeSensor=function(element,callback){function EventQueue(){this.q=[];this.add=function(ev){this.q.push(ev);};var i,j;this.call=function(){for(i=0,j=this.q.length;i<j;i++){this.q[i].call();}};}
function getComputedStyle(element,prop){if(element.currentStyle){return element.currentStyle[prop];}else if(window.getComputedStyle){return window.getComputedStyle(element,null).getPropertyValue(prop);}else{return element.style[prop];}}
function attachResizeEvent(element,resized){if(!element.resizedAttached){element.resizedAttached=new EventQueue();element.resizedAttached.add(resized);}else if(element.resizedAttached){element.resizedAttached.add(resized);return;}
element.resizeSensor=document.createElement('div');element.resizeSensor.className='resize-sensor';var style='position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: scroll; z-index: -1; visibility: hidden;';var styleChild='position: absolute; left: 0; top: 0;';element.resizeSensor.style.cssText=style;element.resizeSensor.innerHTML='<div class="resize-sensor-expand" style="'+style+'">'+'<div style="'+styleChild+'"></div>'+'</div>'+'<div class="resize-sensor-shrink" style="'+style+'">'+'<div style="'+styleChild+' width: 200%; height: 200%"></div>'+'</div>';element.appendChild(element.resizeSensor);if(!{fixed:1,absolute:1}[getComputedStyle(element,'position')]){element.style.position='relative';}
var expand=element.resizeSensor.childNodes[0];var expandChild=expand.childNodes[0];var shrink=element.resizeSensor.childNodes[1];var shrinkChild=shrink.childNodes[0];var lastWidth,lastHeight;var reset=function(){expandChild.style.width=expand.offsetWidth+10+'px';expandChild.style.height=expand.offsetHeight+10+'px';expand.scrollLeft=expand.scrollWidth;expand.scrollTop=expand.scrollHeight;shrink.scrollLeft=shrink.scrollWidth;shrink.scrollTop=shrink.scrollHeight;lastWidth=element.offsetWidth;lastHeight=element.offsetHeight;};reset();var changed=function(){if(element.resizedAttached){element.resizedAttached.call();}};var addEvent=function(el,name,cb){if(el.attachEvent){el.attachEvent('on'+name,cb);}else{el.addEventListener(name,cb);}};addEvent(expand,'scroll',function(){if(element.offsetWidth>lastWidth||element.offsetHeight>lastHeight){changed();}
reset();});addEvent(shrink,'scroll',function(){if(element.offsetWidth<lastWidth||element.offsetHeight<lastHeight){changed();}
reset();});}
if("[object Array]"===Object.prototype.toString.call(element)||('undefined'!==typeof jQuery&&element instanceof jQuery)
||('undefined'!==typeof Elements&&element instanceof Elements)
){var i=0,j=element.length;for(;i<j;i++){attachResizeEvent(element[i],callback);}}else{attachResizeEvent(element,callback);}
this.detach=function(){ResizeSensor.detach(element);};};this.ResizeSensor.detach=function(element){if(element.resizeSensor){element.removeChild(element.resizeSensor);delete element.resizeSensor;delete element.resizedAttached;}};})();;(function(){var ElementQueries=this.ElementQueries=function(){this.withTracking=false;var elements=[];function getEmSize(element){if(!element){element=document.documentElement;}
var fontSize=getComputedStyle(element,'fontSize');return parseFloat(fontSize)||16;}
function convertToPx(element,value){var units=value.replace(/[0-9]*/,'');value=parseFloat(value);switch(units){case"px":return value;case"em":return value*getEmSize(element);case"rem":return value*getEmSize();
 case"vw":return value*document.documentElement.clientWidth/100;case"vh":return value*document.documentElement.clientHeight/100;case"vmin":case"vmax":var vw=document.documentElement.clientWidth/100;var vh=document.documentElement.clientHeight/100;var chooser=Math[units==="vmin"?"min":"max"];return value*chooser(vw,vh);default:return value;
}}
function SetupInformation(element){this.element=element;this.options={};var key,option,width=0,height=0,value,actualValue,attrValues,attrValue,attrName;this.addOption=function(option){var idx=[option.mode,option.property,option.value].join(',');this.options[idx]=option;};var attributes=['min-width','min-height','max-width','max-height'];this.call=function(){ width=this.element.offsetWidth;height=this.element.offsetHeight;attrValues={};for(key in this.options){if(!this.options.hasOwnProperty(key)){continue;}
option=this.options[key];value=convertToPx(this.element,option.value);actualValue=option.property=='width'?width:height;attrName=option.mode+'-'+option.property;attrValue='';if(option.mode=='min'&&actualValue>=value){attrValue+=option.value;}
if(option.mode=='max'&&actualValue<=value){attrValue+=option.value;}
if(!attrValues[attrName])attrValues[attrName]='';if(attrValue&&-1===(' '+attrValues[attrName]+' ').indexOf(' '+attrValue+' ')){attrValues[attrName]+=' '+attrValue;}}
for(var k in attributes){if(attrValues[attributes[k]]){this.element.setAttribute(attributes[k],attrValues[attributes[k]].substr(1));}else{this.element.removeAttribute(attributes[k]);}}};}
function setupElement(element,options){if(element.elementQueriesSetupInformation){element.elementQueriesSetupInformation.addOption(options);}else{element.elementQueriesSetupInformation=new SetupInformation(element);element.elementQueriesSetupInformation.addOption(options);element.elementQueriesSensor=new ResizeSensor(element,function(){element.elementQueriesSetupInformation.call();});}
element.elementQueriesSetupInformation.call();if(this.withTracking){elements.push(element);}}
function queueQuery(selector,mode,property,value){var query;if(document.querySelectorAll)query=document.querySelectorAll.bind(document);if(!query&&'undefined'!==typeof $$)query=$$;if(!query&&'undefined'!==typeof jQuery)query=jQuery;if(!query){throw'No document.querySelectorAll, jQuery or Mootools\'s $$ found.';}
var elements=query(selector);for(var i=0,j=elements.length;i<j;i++){setupElement(elements[i],{mode:mode,property:property,value:value});}}
var regex=/,?([^,\n]*)\[[\s\t]*(min|max)-(width|height)[\s\t]*[~$\^]?=[\s\t]*"([^"]*)"[\s\t]*]([^\n\s\{]*)/mgi;function extractQuery(css){var match;css=css.replace(/'/g,'"');while(null!==(match=regex.exec(css))){if(5<match.length){queueQuery(match[1]||match[5],match[2],match[3],match[4]);}}}
function readRules(rules){var selector='';if(!rules){return;}
if('string'===typeof rules){rules=rules.toLowerCase();if(-1!==rules.indexOf('min-width')||-1!==rules.indexOf('max-width')){extractQuery(rules);}}else{for(var i=0,j=rules.length;i<j;i++){if(1===rules[i].type){selector=rules[i].selectorText||rules[i].cssText;if(-1!==selector.indexOf('min-height')||-1!==selector.indexOf('max-height')){extractQuery(selector);}else if(-1!==selector.indexOf('min-width')||-1!==selector.indexOf('max-width')){extractQuery(selector);}}else if(4===rules[i].type){readRules(rules[i].cssRules||rules[i].rules);}}}}
this.init=function(withTracking){this.withTracking=withTracking;for(var i=0,j=document.styleSheets.length;i<j;i++){readRules(document.styleSheets[i].cssText||document.styleSheets[i].cssRules||document.styleSheets[i].rules);}};this.update=function(withTracking){this.withTracking=withTracking;this.init();};this.detach=function(){if(!this.withTracking){throw'withTracking is not enabled. We can not detach elements since we don not store it.'+'Use ElementQueries.withTracking = true; before domready.';}
var element;while(element=elements.pop()){ElementQueries.detach(element);}
elements=[];};};ElementQueries.update=function(withTracking){ElementQueries.instance.update(withTracking);};ElementQueries.detach=function(element){if(element.elementQueriesSetupInformation){element.elementQueriesSensor.detach();delete element.elementQueriesSetupInformation;delete element.elementQueriesSensor;console.log('detached');}else{console.log('detached already',element);}};ElementQueries.withTracking=false;ElementQueries.init=function(){if(!ElementQueries.instance){ElementQueries.instance=new ElementQueries();}
ElementQueries.instance.init(ElementQueries.withTracking);};var domLoaded=function(callback){if(document.addEventListener){document.addEventListener('DOMContentLoaded',callback,false);}
if(/KHTML|WebKit|iCab/i.test(navigator.userAgent)){var DOMLoadTimer=setInterval(function(){if(/loaded|complete/i.test(document.readyState)){callback();clearInterval(DOMLoadTimer);}},10);}
window.onload=callback;};if(window.addEventListener){window.addEventListener('load',ElementQueries.init,false);}else{window.attachEvent('onload',ElementQueries.init);}
domLoaded(ElementQueries.init);})();Date.$VERSION=1.02;Date.LZ=function(x){return(x<0||x>9?"":"0")+x};Date.monthNames=new Array('January','February','March','April','May','June','July','August','September','October','November','December');Date.monthAbbreviations=new Array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');Date.dayNames=new Array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');Date.dayAbbreviations=new Array('Sun','Mon','Tue','Wed','Thu','Fri','Sat');Date.preferAmericanFormat=true;if(!Date.prototype.getFullYear){Date.prototype.getFullYear=function(){var yy=this.getYear();return(yy<1900?yy+1900:yy);};}
Date.parseString=function(val,format){ if(typeof(format)=="undefined"||format==null||format==""){var generalFormats=new Array('y-M-d','MMM d, y','MMM d,y','y-MMM-d','d-MMM-y','MMM d','MMM-d','d-MMM');var monthFirst=new Array('M/d/y','M-d-y','M.d.y','M/d','M-d');var dateFirst=new Array('d/M/y','d-M-y','d.M.y','d/M','d-M');var checkList=new Array(generalFormats,Date.preferAmericanFormat?monthFirst:dateFirst,Date.preferAmericanFormat?dateFirst:monthFirst);for(var i=0;i<checkList.length;i++){var l=checkList[i];for(var j=0;j<l.length;j++){var d=Date.parseString(val,l[j]);if(d!=null){return d;}}}
return null;};this.isInteger=function(val){for(var i=0;i<val.length;i++){if("1234567890".indexOf(val.charAt(i))==-1){return false;}}
return true;};this.getInt=function(str,i,minlength,maxlength){for(var x=maxlength;x>=minlength;x--){var token=str.substring(i,i+x);if(token.length<minlength){return null;}
if(this.isInteger(token)){return token;}}
return null;};val=val+"";format=format+"";var i_val=0;var i_format=0;var c="";var token="";var token2="";var x,y;var year=new Date().getFullYear();var month=1;var date=1;var hh=0;var mm=0;var ss=0;var ampm="";while(i_format<format.length){ c=format.charAt(i_format);token="";while((format.charAt(i_format)==c)&&(i_format<format.length)){token+=format.charAt(i_format++);} 
if(token=="yyyy"||token=="yy"||token=="y"){if(token=="yyyy"){x=4;y=4;}
if(token=="yy"){x=2;y=2;}
if(token=="y"){x=2;y=4;}
year=this.getInt(val,i_val,x,y);if(year==null){return null;}
i_val+=year.length;if(year.length==2){if(year>70){year=1900+(year-0);}
else{year=2000+(year-0);}}}
else if(token=="MMM"||token=="NNN"){month=0;var names=(token=="MMM"?(Date.monthNames.concat(Date.monthAbbreviations)):Date.monthAbbreviations);for(var i=0;i<names.length;i++){var month_name=names[i];if(val.substring(i_val,i_val+month_name.length).toLowerCase()==month_name.toLowerCase()){month=(i%12)+1;i_val+=month_name.length;break;}}
if((month<1)||(month>12)){return null;}}
else if(token=="EE"||token=="E"){var names=(token=="EE"?Date.dayNames:Date.dayAbbreviations);for(var i=0;i<names.length;i++){var day_name=names[i];if(val.substring(i_val,i_val+day_name.length).toLowerCase()==day_name.toLowerCase()){i_val+=day_name.length;break;}}}
else if(token=="MM"||token=="M"){month=this.getInt(val,i_val,token.length,2);if(month==null||(month<1)||(month>12)){return null;}
i_val+=month.length;}
else if(token=="dd"||token=="d"){date=this.getInt(val,i_val,token.length,2);if(date==null||(date<1)||(date>31)){return null;}
i_val+=date.length;}
else if(token=="hh"||token=="h"){hh=this.getInt(val,i_val,token.length,2);if(hh==null||(hh<1)||(hh>12)){return null;}
i_val+=hh.length;}
else if(token=="HH"||token=="H"){hh=this.getInt(val,i_val,token.length,2);if(hh==null||(hh<0)||(hh>23)){return null;}
i_val+=hh.length;}
else if(token=="KK"||token=="K"){hh=this.getInt(val,i_val,token.length,2);if(hh==null||(hh<0)||(hh>11)){return null;}
i_val+=hh.length;hh++;}
else if(token=="kk"||token=="k"){hh=this.getInt(val,i_val,token.length,2);if(hh==null||(hh<1)||(hh>24)){return null;}
i_val+=hh.length;hh--;}
else if(token=="mm"||token=="m"){mm=this.getInt(val,i_val,token.length,2);if(mm==null||(mm<0)||(mm>59)){return null;}
i_val+=mm.length;}
else if(token=="ss"||token=="s"){ss=this.getInt(val,i_val,token.length,2);if(ss==null||(ss<0)||(ss>59)){return null;}
i_val+=ss.length;}
else if(token=="a"){if(val.substring(i_val,i_val+2).toLowerCase()=="am"){ampm="AM";}
else if(val.substring(i_val,i_val+2).toLowerCase()=="pm"){ampm="PM";}
else{return null;}
i_val+=2;}
else{if(val.substring(i_val,i_val+token.length)!=token){return null;}
else{i_val+=token.length;}}} 
if(i_val!=val.length){return null;}
if(month==2){ if(((year%4==0)&&(year%100!=0))||(year%400==0)){ if(date>29){return null;}}
else{if(date>28){return null;}}}
if((month==4)||(month==6)||(month==9)||(month==11)){if(date>30){return null;}} 
if(hh<12&&ampm=="PM"){hh=hh-0+12;}
else if(hh>11&&ampm=="AM"){hh-=12;}
return new Date(year,month-1,date,hh,mm,ss);};Date.isValid=function(val,format){return(Date.parseString(val,format)!=null);};Date.prototype.isBefore=function(date2){if(date2==null){return false;}
return(this.getTime()<date2.getTime());};Date.prototype.isAfter=function(date2){if(date2==null){return false;}
return(this.getTime()>date2.getTime());};Date.prototype.equals=function(date2){if(date2==null){return false;}
return(this.getTime()==date2.getTime());};Date.prototype.equalsIgnoreTime=function(date2){if(date2==null){return false;}
var d1=new Date(this.getTime()).clearTime();var d2=new Date(date2.getTime()).clearTime();return(d1.getTime()==d2.getTime());};Date.prototype.format=function(format){format=format+"";var result="";var i_format=0;var c="";var token="";var y=this.getYear()+"";var M=this.getMonth()+1;var d=this.getDate();var E=this.getDay();var H=this.getHours();var m=this.getMinutes();var s=this.getSeconds();var yyyy,yy,MMM,MM,dd,hh,h,mm,ss,ampm,HH,H,KK,K,kk,k; var value=new Object();if(y.length<4){y=""+(+y+1900);}
value["y"]=""+y;value["yyyy"]=y;value["yy"]=y.substring(2,4);value["M"]=M;value["MM"]=Date.LZ(M);value["MMM"]=Date.monthNames[M-1];value["NNN"]=Date.monthAbbreviations[M-1];value["d"]=d;value["dd"]=Date.LZ(d);value["E"]=Date.dayAbbreviations[E];value["EE"]=Date.dayNames[E];value["H"]=H;value["HH"]=Date.LZ(H);if(H==0){value["h"]=12;}
else if(H>12){value["h"]=H-12;}
else{value["h"]=H;}
value["hh"]=Date.LZ(value["h"]);value["K"]=value["h"]-1;value["k"]=value["H"]+1;value["KK"]=Date.LZ(value["K"]);value["kk"]=Date.LZ(value["k"]);if(H>11){value["a"]="PM";}
else{value["a"]="AM";}
value["m"]=m;value["mm"]=Date.LZ(m);value["s"]=s;value["ss"]=Date.LZ(s);while(i_format<format.length){c=format.charAt(i_format);token="";while((format.charAt(i_format)==c)&&(i_format<format.length)){token+=format.charAt(i_format++);}
if(typeof(value[token])!="undefined"){result=result+value[token];}
else{result=result+token;}}
return result;};Date.prototype.getDayName=function(){return Date.dayNames[this.getDay()];};Date.prototype.getDayAbbreviation=function(){return Date.dayAbbreviations[this.getDay()];};Date.prototype.getMonthName=function(){return Date.monthNames[this.getMonth()];};Date.prototype.getMonthAbbreviation=function(){return Date.monthAbbreviations[this.getMonth()];};Date.prototype.clearTime=function(){this.setHours(0);this.setMinutes(0);this.setSeconds(0);this.setMilliseconds(0);return this;};Date.prototype.add=function(interval,number){if(typeof(interval)=="undefined"||interval==null||typeof(number)=="undefined"||number==null){return this;}
number=+number;if(interval=='y'){ this.setFullYear(this.getFullYear()+number);}
else if(interval=='M'){ this.setMonth(this.getMonth()+number);}
else if(interval=='d'){ this.setDate(this.getDate()+number);}
else if(interval=='w'){ var step=(number>0)?1:-1;while(number!=0){this.add('d',step);while(this.getDay()==0||this.getDay()==6){this.add('d',step);}
number-=step;}}
else if(interval=='h'){ this.setHours(this.getHours()+number);}
else if(interval=='m'){ this.setMinutes(this.getMinutes()+number);}
else if(interval=='s'){ this.setSeconds(this.getSeconds()+number);}
return this;};var saveAs=saveAs

||(typeof navigator!=="undefined"&&navigator.msSaveOrOpenBlob&&navigator.msSaveOrOpenBlob.bind(navigator))
||(function(view){"use strict"; if(typeof navigator!=="undefined"&&/MSIE [1-9]\./.test(navigator.userAgent)){return;}
var
doc=view.document

,get_URL=function(){return view.URL||view.webkitURL||view;},save_link=doc.createElementNS("http://www.w3.org/1999/xhtml","a"),can_use_save_link="download"in save_link,click=function(node){var event=doc.createEvent("MouseEvents");event.initMouseEvent("click",true,false,view,0,0,0,0,0,false,false,false,false,0,null);node.dispatchEvent(event);},webkit_req_fs=view.webkitRequestFileSystem,req_fs=view.requestFileSystem||webkit_req_fs||view.mozRequestFileSystem,throw_outside=function(ex){(view.setImmediate||view.setTimeout)(function(){throw ex;},0);},force_saveable_type="application/octet-stream",fs_min_size=0



,arbitrary_revoke_timeout=500
,revoke=function(file){var revoker=function(){if(typeof file==="string"){ get_URL().revokeObjectURL(file);}else{ file.remove();}};if(view.chrome){revoker();}else{setTimeout(revoker,arbitrary_revoke_timeout);}},dispatch=function(filesaver,event_types,event){event_types=[].concat(event_types);var i=event_types.length;while(i--){var listener=filesaver["on"+event_types[i]];if(typeof listener==="function"){try{listener.call(filesaver,event||filesaver);}catch(ex){throw_outside(ex);}}}},FileSaver=function(blob,name){ var
filesaver=this,type=blob.type,blob_changed=false,object_url,target_view,dispatch_all=function(){dispatch(filesaver,"writestart progress write writeend".split(" "));}
,fs_error=function(){ if(blob_changed||!object_url){object_url=get_URL().createObjectURL(blob);}
if(target_view){target_view.location.href=object_url;}else{var new_tab=view.open(object_url,"_blank");if(new_tab==undefined&&typeof safari!=="undefined"){ view.location.href=object_url}}
filesaver.readyState=filesaver.DONE;dispatch_all();revoke(object_url);},abortable=function(func){return function(){if(filesaver.readyState!==filesaver.DONE){return func.apply(this,arguments);}};},create_if_not_found={create:true,exclusive:false},slice;filesaver.readyState=filesaver.INIT;if(!name){name="download";}
if(can_use_save_link){object_url=get_URL().createObjectURL(blob);save_link.href=object_url;save_link.download=name;click(save_link);filesaver.readyState=filesaver.DONE;dispatch_all();revoke(object_url);return;} 
if(/^\s*(?:text\/(?:plain|xml)|application\/xml|\S*\/\S*\+xml)\s*;.*charset\s*=\s*utf-8/i.test(blob.type)){blob=new Blob(["\ufeff",blob],{type:blob.type});}


 
if(view.chrome&&type&&type!==force_saveable_type){slice=blob.slice||blob.webkitSlice;blob=slice.call(blob,0,blob.size,force_saveable_type);blob_changed=true;}
 
if(webkit_req_fs&&name!=="download"){name+=".download";}
if(type===force_saveable_type||webkit_req_fs){target_view=view;}
if(!req_fs){fs_error();return;}
fs_min_size+=blob.size;req_fs(view.TEMPORARY,fs_min_size,abortable(function(fs){fs.root.getDirectory("saved",create_if_not_found,abortable(function(dir){var save=function(){dir.getFile(name,create_if_not_found,abortable(function(file){file.createWriter(abortable(function(writer){writer.onwriteend=function(event){target_view.location.href=file.toURL();filesaver.readyState=filesaver.DONE;dispatch(filesaver,"writeend",event);revoke(file);};writer.onerror=function(){var error=writer.error;if(error.code!==error.ABORT_ERR){fs_error();}};"writestart progress write abort".split(" ").forEach(function(event){writer["on"+event]=filesaver["on"+event];});writer.write(blob);filesaver.abort=function(){writer.abort();filesaver.readyState=filesaver.DONE;};filesaver.readyState=filesaver.WRITING;}),fs_error);}),fs_error);};dir.getFile(name,{create:false},abortable(function(file){ file.remove();save();}),abortable(function(ex){if(ex.code===ex.NOT_FOUND_ERR){save();}else{fs_error();}}));}),fs_error);}),fs_error);},FS_proto=FileSaver.prototype,saveAs=function(blob,name){return new FileSaver(blob,name);};FS_proto.abort=function(){var filesaver=this;filesaver.readyState=filesaver.DONE;dispatch(filesaver,"abort");};FS_proto.readyState=FS_proto.INIT=0;FS_proto.WRITING=1;FS_proto.DONE=2;FS_proto.error=FS_proto.onwritestart=FS_proto.onprogress=FS_proto.onwrite=FS_proto.onabort=FS_proto.onerror=FS_proto.onwriteend=null;return saveAs;}(typeof self!=="undefined"&&self||typeof window!=="undefined"&&window||this.content));

if(typeof module!=="undefined"&&module.exports){module.exports.saveAs=saveAs;}else if((typeof define!=="undefined"&&define!==null)&&(define.amd!=null)){define([],function(){return saveAs;});}



var displayNone={display:"none"};var transparentBackground={background:"transparent"}
jQuery.fn.doOnce=function(func){this.length&&func.apply(this);return this;}
var ELLIPSIS='...';String.prototype.truncate=function(n){var useWordBoundary=true;var tooLong=this.length>n;var s_=tooLong?this.substr(0,n-1):this;s_=useWordBoundary&&tooLong?s_.substr(0,s_.lastIndexOf(' ')):s_;var finalString=tooLong?_cleanEllipsis(s_+ELLIPSIS):s_;return String(finalString);};Storage.prototype.setObject=function(key,value){this.setItem(key,JSON.stringify(value));}
Storage.prototype.getObject=function(key){var value=this.getItem(key);return value&&JSON.parse(value);}
function _cleanEllipsis(string){var i=string.lastIndexOf(ELLIPSIS);if(string[i-1].match(/[,;:?!\/\.]/)){return string.substr(0,i-1)+ELLIPSIS;}
return string;}
util={convertDateToString:function(dateObj){var mdy=_getMonthDayYearStrArray(dateObj);return mdy[0]+'/'+mdy[1]+'/'+mdy[2];},getFilename:function(){var mdy=_getMonthDayYearStrArray(new Date());var today=mdy[0]+'-'+mdy[1]+'-'+mdy[2];return'my_memories '+today+'.txt';},findById:function(id,list){for(var i=0;i<list.length;++i){if(list[i].id==id)
return list[i];}
return null;}, setConfirmUnload:function(on,msgFn){if(msgFn)
window.onbeforeunload=(on)?msgFn:null;else
window.onbeforeunload=(on)?_unloadMessage:null;},splitIntoLines:function(contents){return contents.split(/\n/);}}
function _unloadMessage(){return"Just checking:";}
function _getMonthDayYearStrArray(dateObj){var today=new Date();var dd=today.getDate();var mm=today.getMonth()+1;var yyyy=today.getFullYear();var TWO_DIGITS=false;if(TWO_DIGITS){if(dd<10)
dd='0'+dd;if(mm<10)
mm='0'+mm;}
return[mm,dd,yyyy];}
function escapeHtml(text){var map={'&':'&amp;','<':'&lt;','>':'&gt;','"':'&quot;',"'":'&#039;'};return text.replace(/[&<>"']/g,function(m){return map[m];});}
 
function unescapeHtml(safe){return safe.replace(/&amp;/g,"&").replace(/&lt;/g,"<").replace(/&gt;/g,">").replace(/&quot;/g,"\"").replace(/&#039;/g,"'");}


global={Index:{memoryObjects:[],earliestDate:null,latestDate:null,datesToMemoryCount:{},},Action:{EDIT:'Edit',INDEX:'Index',NEW:'New'},View:{STREAM:'Stream'},ViewList:['Stream'],FormAction:{UPDATE:'Update',CREATE:'Create'},FormWritingMode:{NORMAL:'Normal',RAMBLING:'Rambling'},FormRamblingButtonText:{RAMBLING:'Thought-spilling mode',NORMAL:'Normal mode'},DateStringExamples:"3/15/2015, 12/25/2015",PortalType:{RETURN_FROM_FORM:'Return from form',CUSTOM:'Custom'},}
global.Structure={buildMemory:function(mid,newContent,newDateStr,newDate,newIsExactDate,newNote){return{id:mid,content:newContent,dateStr:newDateStr,date:newDate,isExactDate:newIsExactDate,note:newNote}},buildMemoryNoDate:function(mid,newContent,newDateStr,newIsExactDate,newNote){return{id:mid,content:newContent,dateStr:newDateStr,date:null,isExactDate:newIsExactDate,note:newNote}},getBlankMemory:function(){return{  id:global.Index.memoryObjects.length+1,content:"",date:null,dateStr:"",isExactDate:true,note:"",}},getMemoryShapeRequired:function(){return React.PropTypes.shape({id:React.PropTypes.number,content:React.PropTypes.string,date:React.PropTypes.object,dateStr:React.PropTypes.string,isExactDate:React.PropTypes.bool,note:React.PropTypes.string}).isRequired}}

var switch_SKIP_TO_EDITING=false;var switch_SKIP_UPLOAD_INTERACTION=false
var switch_WARN_ON_EXIT=true;var switch_PRINT_MEMORIES=false;$(document).ready(function(){if(!document.getElementById('about'))
return;$("#trigger-advanced").click(function(){$("#advanced-info").toggle();});});var Index=global.Index;var Structure=global.Structure;var PRINT=false;function _buildMemory(dateStr){return Structure.buildMemory(null,null,dateStr,Date.parseString(dateStr),null,null);}
function initIndexFromText(contents){if(PRINT){console.log('--initIndex--');}



var lines=util.splitIntoLines(contents);var dateLinePattern=/^(\d\d?\/\d\d?\/\d\d\d\d)\b(~*)(.*)\b/g;var dateLineAndContents=[];var onContent=false;for(var i=0;i<lines.length;++i){var entry=lines[i].trim();if(entry.match(dateLinePattern)){dateLineAndContents.push(entry);onContent=false;}else{if(onContent){var last=dateLineAndContents[dateLineAndContents.length-1];dateLineAndContents[dateLineAndContents.length-1]=[last,entry].join('\n');}else{dateLineAndContents.push(entry);onContent=true;}}}
if(dateLineAndContents.length%2!=0){console.error("One to one mapping from date to content not satisfied.");}
if(PRINT){console.log('dateLineAndContents:');console.log(dateLineAndContents);}
var memPieces=[];for(var i=0;i<dateLineAndContents.length;i+=2){var dateLinePattern=/\b(\d\d?\/\d\d?\/\d\d\d\d)(~*)(.*)/g;var match=dateLinePattern.exec(dateLineAndContents[i]);if(!match){console.error("No match for "+dateLineAndContents[i]);}
match.shift();for(var j=0;j<match.length;++j){memPieces.push(match[j]);}
memPieces.push(dateLineAndContents[i+1])
dateLinePattern=null;}
if(PRINT){console.log('memPieces:');console.log(memPieces);for(var i=0;i<memPieces.length;++i){console.log(i+1+': '+memPieces[i]);}}
var MemoryPart={DATE:0,DATE_ACCURACY:1,NOTE:2,CONTENT:3};var memoryObjects=[];var earliestDate=null;var latestDate=null;var datesToMemoryCount={};var memPart=MemoryPart.DATE;var currentMem=null;if(memPieces.length%4!=0){console.error("Memories text is not of correct format. Length = "+memPieces.length);}
for(var i=0;i<memPieces.length;++i){var memText=memPieces[i].trim();if(memPart==MemoryPart.DATE){if(currentMem){memoryObjects.push(currentMem);}
var dateStr=memText;currentMem=_buildMemory(dateStr);if(!earliestDate||earliestDate.isAfter(currentMem.date))
earliestDate=currentMem.date;if(!latestDate||latestDate.isBefore(currentMem.date))
latestDate=currentMem.date;if(currentMem.date in datesToMemoryCount)
datesToMemoryCount[currentMem.dateStr]+=1;else
datesToMemoryCount[currentMem.dateStr]=0;}else if(memPart==MemoryPart.DATE_ACCURACY){if(memText)
currentMem.isExactDate=false;else
currentMem.isExactDate=true;}else if(memPart==MemoryPart.NOTE){if(memText)
currentMem.note=memText;}else if(memPart==MemoryPart.CONTENT){currentMem.content=memText;}else{console.error("indexMemories: Invalid memory part.");}
memPart+=1;memPart=memPart%4;}
memoryObjects.push(currentMem);for(var i=0;i<memoryObjects.length;++i){memoryObjects[i].id=i+1;}
Index.memoryObjects=memoryObjects;Index.earliestDate=earliestDate;Index.latestDate=latestDate;Index.datesToMemoryCount=datesToMemoryCount;}

function initIndexDirectly(memArray){Index.memoryObjects=memArray;Index.earliestDate=null;Index.latestDate=null;Index.datesToMemoryCount=null;}
var Index=global.Index;function downloadMemories(){console.log('Downloading memories');var text=getMemoriesText();var blob=new Blob([text],{type:"text/plain;charset=utf-8;",});saveAs(blob,util.getFilename());}
function getMemoriesText(){var text='';for(var i=0;i<Index.memoryObjects.length;++i){var mem=Index.memoryObjects[i];console.log(mem);var dateLine=mem.dateStr;if(!mem.isExactDate)
dateLine+='~';if(mem.note)
dateLine+=' '+mem.note;text+=dateLine+'\n'+mem.content+'\n';if(i!=Index.memoryObjects.length-1){text+='\n';}}
return text;}
function _collectMemoryFileAndDisplay(theFile,setMemoriesFn){return function(e){var contents=e.target.result;processMemoryFile(theFile.name,contents,setMemoriesFn);var outputDiv=document.createElement('div');outputDiv.innerHTML=['<span>',escape(theFile.name),'</span>'].join('');document.getElementById('list').insertBefore(outputDiv,null);}
function processMemoryFile(fName,fContent,setMemoriesFn){console.log('Got file: '+fName);initIndexFromText(fContent);var arrayOfMemories=Index.memoryObjects;setMemoriesFn(arrayOfMemories);showMemoryPlaygroundAndHideUpload();}}
function handleDragOver(evt){evt.stopPropagation();evt.preventDefault();evt.dataTransfer.dropEffect='copy';}
function handleDroppedFileSelect(evt,setMemoriesFn){evt.stopPropagation();evt.preventDefault();var files=evt.dataTransfer.files;var output=[];for(var i=0,f;f=files[i];i++){if(!f.type.match('text.*')){continue;}
var r=new FileReader();r.onload=_collectMemoryFileAndDisplay(f,setMemoriesFn);r.readAsText(f);}}
function readSingleFile(evt){var f=evt.target.files[0];if(!f){alert("Failed to load file.");}else if(!f.type.match('text.*')){alert(f.name+" is not a valid text file.");}else{var r=new FileReader();r.onload=_collectMemoryFileAndDisplay(f,evt.data.setMemoriesFn);r.readAsText(f);}}
function showMemoryPlaygroundAndHideUpload(){$("#playground").show();$("#download-area").show();$("#upload-area").hide();}
function cssGetWidthOfMemoriesArea(){return $("#memories-area").width();}
function cssGetWindowWidth(){return window.innerWidth;}
function cssGetWindowHeight(){return window.innerHeight;}
function cssCenterHoverArea(){

$("#hover-area").css('width',cssGetWidthOfMemoriesArea());new ResizeSensor(jQuery("#memories-area"),function(){$("#hover-area").css('width',cssGetWidthOfMemoriesArea());});}
function cssHoverAreaToggle(nodeId,truncatedText,setHoverFn){$(nodeId).hover(function(){setHoverFn(truncatedText);$("#hover-area").stop(true,true).fadeToggle();},function(){setHoverFn("");$("#hover-area").stop(true,true).fadeToggle();});}
function cssPrettifyDropzoneDragEnterAndLeave(){var drop=document.getElementById("dropzone");drop.addEventListener("dragenter",change,false);drop.addEventListener("dragleave",changeBack,false);drop.addEventListener("mouseout",changeBack,false);var oldBorder=drop.style.border;function change(){drop.style.border='1px dashed black';};function changeBack(){drop.style.border=oldBorder;};}



var CloudMemoryNode=React.createClass({displayName:"CloudMemoryNode",propTypes:{id:React.PropTypes.string.isRequired,content:React.PropTypes.string.isRequired,dateStr:React.PropTypes.string.isRequired,isExactDate:React.PropTypes.bool.isRequired,note:React.PropTypes.string.isRequired,setHoverText:React.PropTypes.func.isRequired,x:React.PropTypes.number,y:React.PropTypes.number},getInitialState:function(){return{x:Math.random()*cssGetWindowWidth(),y:Math.random()*cssGetWindowHeight()};},render:function(){return(React.createElement("div",{id:this.props.id,className:"memory cloud-memory"},React.createElement("a",{href:"javascript:void(0);"},this.props.content,React.createElement("br",null))))},componentDidMount:function(){var nodeId="#"+this.props.id;var truncatedText=this.props.content.truncate(140,true);var setHoverFn=this.props.setHoverText;cssHoverAreaToggle(nodeId,truncatedText,setHoverFn);console.log("name: "+this.props.content);console.log("x: "+this.state.x);console.log("y: "+this.state.y);$(nodeId).css('left',this.state.x);$(nodeId).css('top',this.state.x);$(nodeId).css('left',0);$(nodeId).css('top',0);}});var StreamMemoryNode=React.createClass({displayName:"StreamMemoryNode",propTypes:{id:React.PropTypes.string.isRequired,memory:Structure.getMemoryShapeRequired(),setHoverText:React.PropTypes.func.isRequired,launchMemoryForm:React.PropTypes.func.isRequired},render:function(){return(React.createElement("div",{id:this.props.id,className:"memory stream-memory"},React.createElement("a",{href:"javascript:void(0);",className:"my-list-group-item",onClick:this.launchMemoryForm},this.props.memory.dateStr,React.createElement("br",null),this._chosenContent(),React.createElement("br",null),React.createElement("span",{className:"note"},this.props.memory.note))))},componentDidMount:function(){var nodeId="#"+this.props.id;var truncatedText=this.props.memory.content.truncate(140);var setHoverFn=this.props.setHoverText;cssHoverAreaToggle(nodeId,truncatedText,setHoverFn);},_chosenContent:function(){var content=this.props.memory.content;if(content.length<=60)
return content;return content.truncate(180);},launchMemoryForm:function(){this.props.launchMemoryForm(this.props.memory);}});var Action=global.Action;var PortalType=global.PortalType;var Portal=React.createClass({displayName:"Portal",propTypes:{msg:React.PropTypes.string,icon:React.PropTypes.string,type:React.PropTypes.string.isRequired,

action:React.PropTypes.string,setAction:React.PropTypes.func,detailedAction:React.PropTypes.func},render:function(){this._checkProps();var linkContent=null;if(this.props.type==PortalType.CUSTOM){linkContent=(React.createElement("button",{onClick:this._go,className:"btn btn-default","aria-label":this.props.msg},React.createElement("i",{className:this.props.icon})," ",this.props.msg));}else if(this.props.type==PortalType.RETURN_FROM_FORM){var icon="fa fa-arrow-circle-left";var msg="Discard changes";linkContent=(React.createElement("button",{onClick:this._go,className:"btn btn-default faint-button","aria-label":msg},React.createElement("i",{className:icon,"aria-hidden":"true"})," ",msg));}else{console.error("No such PortalType.");}
return(React.createElement("div",{className:"portal"},linkContent));},_checkProps:function(){switch(this.props.type){case PortalType.CUSTOM:if(!this.props.msg)
console.error("Portal warning: 'msg' should be provided.");if(!this.props.icon)
console.error("Portal warning: 'icon' should be provided.");break;case PortalType.RETURN_FROM_FORM:if(this.props.msg)
console.error("Portal warning: unused 'msg' provided.");if(this.props.icon)
console.error("Portal warning: unused 'icon' provided.");break;}},_go:function(){var usingActionSimple=this.props.action&&this.props.setAction&&!this.props.detailedAction;var usingActionDetailed=!this.props.action&&!this.props.setAction&&this.props.detailedAction;var validActionCombo=usingActionSimple||usingActionDetailed;if(!validActionCombo){console.error("Portal warning: Invalid set of actions passed to portal.");}
if(usingActionSimple){this.props.setAction(this.props.action);}else{this.props.detailedAction();}}});var Action=global.Action;var View=global.View;var SearchArea=React.createClass({displayName:"SearchArea",propTypes:{searchText:React.PropTypes.string.isRequired,handleSearch:React.PropTypes.func.isRequired},render:function(){return(React.createElement("form",{onSubmit:this.handleSubmit},React.createElement("input",{id:"search-text-field",type:"text",placeholder:"Search",ref:"searchText",defaultValue:this.props.searchText}),React.createElement("button",{type:"submit",className:"btn btn-default","aria-label":"search"},React.createElement("i",{className:"glyphicon glyphicon-search","aria-hidden":"true"}))))},handleSubmit:function(e){e.preventDefault();var searchStr=this.refs.searchText.getDOMNode().value.trim();this.props.handleSearch(searchStr);}});var ViewButton=React.createClass({displayName:"ViewButton",propTypes:{active:React.PropTypes.bool.isRequired,name:React.PropTypes.string.isRequired,setView:React.PropTypes.func.isRequired,view:React.PropTypes.string.isRequired},render:function(){var className=this.props.active?"active":null;return(React.createElement("li",{role:"presentation",className:className},React.createElement("a",{href:"javascript:void(0);",onClick:this.updateView},this.props.name)))},updateView:function(){this.props.setView(this.props.view);}});var MenuBar=React.createClass({displayName:"MenuBar",propTypes:{handleSearch:React.PropTypes.func.isRequired,searchText:React.PropTypes.string.isRequired,setView:React.PropTypes.func.isRequired,view:React.PropTypes.string.isRequired,viewList:React.PropTypes.array.isRequired},render:function(){var currViewName=this.props.view;var viewButtons=this.props.viewList.map(function(val,i){return(React.createElement(ViewButton,{key:val,active:val===currViewName,name:val,index:i,view:this.props.view,setView:this.props.setView}))},this);return(React.createElement("div",{id:"menu-bar"},React.createElement(SearchArea,{searchText:this.props.searchText,handleSearch:this.props.handleSearch})));
}});var FormAction=global.FormAction;var FormRamblingButtonText=global.FormRamblingButtonText;var FormWritingMode=global.FormWritingMode;var Structure=global.Structure;var MemoryForm=React.createClass({displayName:"MemoryForm",propTypes:{memory:Structure.getMemoryShapeRequired(),formAction:React.PropTypes.string.isRequired,updateMemory:React.PropTypes.func,createMemory:React.PropTypes.func},getInitialState:function(){var state=this.props.memory;state.writingMode=FormWritingMode.NORMAL;return state;},render:function(){
return(React.createElement("form",{id:"memory-form",onSubmit:this.handleSubmit},React.createElement("div",{id:"error-msg"}),React.createElement("div",{id:"main-form-area"},React.createElement("input",{type:"text",id:"memory-date-area",className:"faint-border form-space",ref:"dateStr",placeholder:"When? Default: today",value:this.state.dateStr,onChange:this._handleDateStrChange}),React.createElement("div",{id:"memory-exact-date-area-container"},React.createElement("input",{type:"checkbox",id:"memory-exact-date-area",ref:"isExactDate",placeholder:"SUP",checked:this.state.isExactDate,onChange:this._handleIsExactDateChange}),React.createElement("span",{className:"faint-text"}," Exact?")),React.createElement("br",null),React.createElement("br",null),React.createElement("textarea",{id:"memory-content-area",className:"faint-border form-space",ref:"content",placeholder:"Write for yourself",value:this.state.content,onChange:this._handleContentChange}),React.createElement("br",null),React.createElement("br",null),React.createElement("button",{id:"memory-submit-button",type:"submit",className:"btn btn-default faint-button","aria-label":"submit"},React.createElement("i",{className:"glyphicon glyphicon-check","aria-hidden":"true"})," Done"),React.createElement("input",{type:"text",id:"memory-note-area",className:"faint-border form-space",ref:"note",placeholder:"Notes, hashtags",value:this.state.note,onChange:this._handleNoteChange}),React.createElement("br",null))))},componentDidMount:function(){$("#main-form-area").css('width',$("#memory-content-area").css('width'));var margin=parseInt($("#memory-note-area").css('margin-left'));$("#memory-note-area").css('width',$("#memory-content-area").width()-$("#memory-date-area").width()-margin+'px');$("#memory-content-area").focus();},_flashError:function(errors){console.log('errors:');console.log(errors);for(var e_idx in errors){var e=errors[e_idx];var field=$("#"+e.html_id);var oldColor=field.css('background');field.animate({backgroundColor:'gold'});field.animate({backgroundColor:oldColor},'slow').delay(300);if(e.msg){$("#error-msg").append('<div class="alert alert-warning alert-dismissible">'+e.msg+'</div>');}}},_handleContentChange:function(event){this.setState({content:event.target.value});},_handleDateStrChange:function(event){this.setState({dateStr:event.target.value});},_handleNoteChange:function(event){this.setState({note:event.target.value});},_handleIsExactDateChange:function(event){this.setState({isExactDate:event.target.checked});},
handleSubmit:function(e){e.preventDefault();var newContent=this.refs.content.getDOMNode().value.trim();var newDateStr=this.refs.dateStr.getDOMNode().value.trim();var newNote=this.refs.note.getDOMNode().value.trim();var newIsExactDate=this.refs.isExactDate.getDOMNode().checked;var newMem=Structure.buildMemoryNoDate(this.props.memory.id,newContent,newDateStr,newIsExactDate,newNote);var errors=this._validateForm(newMem);if(errors.length==0){if(this.props.formAction==FormAction.UPDATE){console.log("Calling updateMemory with id "+this.props.memory.id);this.props.updateMemory(newMem);}else if(this.props.formAction==FormAction.CREATE){console.log("Calling createMemory with id "+this.props.memory.id);this.props.createMemory(newMem);}else{console.error("No such form action.");}}else{console.log("Error in memory form.");this._flashError(errors)}},_validateForm:function(mem){$("#error-msg").html('');var errors=[];_validateContent(mem.content,errors);_validateDate(mem,errors);return errors;function _validateContent(content,errors){var dateLinePattern=/^(\d\d?\/\d\d?\/\d\d\d\d)\b(~*)(.*)\b/g;var lines=util.splitIntoLines(content);if(content.trim().length==0){var err_obj={html_id:'memory-content-area',msg:"Memory should have content. That's the whole point of writing!"}
errors.push(err_obj);return;}
for(line_idx in lines){var line=lines[line_idx];if(line.match(dateLinePattern)){var err_obj={html_id:'memory-content-area',msg:"Content should not contain a date like '1/1/2000' on a new line - otherwise it conflicts with your downloaded text file!"}
errors.push(err_obj);}}}
function _validateDate(mem,errors){if(!mem.dateStr){mem.date=new Date();mem.dateStr=util.convertDateToString(mem.date);return;}
var dateLinePattern=/^(\d\d?\/\d\d?\/\d\d\d\d)\b(~*)(.*)\b/g;if(!mem.dateStr.match(dateLinePattern)){var err_obj={html_id:'memory-date-area',msg:"Sorry, I can't comprehend the date. You can enter things like this: "+global.DateStringExamples}
errors.push(err_obj);}else{mem.date=Date.parseString(mem.dateStr);}}}});var Action=global.Action;var FormAction=global.FormAction;var PortalType=global.PortalType;var Structure=global.Structure;var View=global.View;var MemoriesArea=React.createClass({displayName:"MemoriesArea",propTypes:{allMemories:React.PropTypes.array.isRequired,action:React.PropTypes.string.isRequired,formMemory:Structure.getMemoryShapeRequired(),launchNewMemoryForm:React.PropTypes.func.isRequired,launchEditMemoryForm:React.PropTypes.func.isRequired,updateMemory:React.PropTypes.func.isRequired,createMemory:React.PropTypes.func.isRequired,setAction:React.PropTypes.func.isRequired,shownMemories:React.PropTypes.array.isRequired,view:React.PropTypes.string.isRequired},getInitialState:function(){return{hoverText:""};},render:function(){if(switch_PRINT_MEMORIES){console.log('Shown memories from playground: ');console.log(this.props.shownMemories);console.log('All memories from playground: ');console.log(this.props.allMemories);}
if(this.props.action==Action.INDEX&&this.props.shownMemories.length==0){return(React.createElement("div",{id:"memories-area",className:"centered"},"No memories to show!",React.createElement("br",null),React.createElement("br",null),React.createElement(Portal,{type:PortalType.CUSTOM,icon:"fa fa-plus",msg:"Create one",detailedAction:this.props.launchNewMemoryForm})));}
if(this.props.action==Action.INDEX){var memoryNodes=this._createMemoryNodesOfType(View.STREAM);var classValues="my-list-group";
return(React.createElement("div",{id:"memories-area"},React.createElement("div",{id:"hover-area",style:displayNone},React.createElement("div",{id:"hover-area-text"},this.state.hoverText)),React.createElement("div",{id:"memories-area",className:classValues},memoryNodes)));}else if(this.props.action==Action.EDIT){return(React.createElement("div",{id:"memories-area"},React.createElement(Portal,{type:PortalType.RETURN_FROM_FORM,action:Action.INDEX,setAction:this.props.setAction}),React.createElement("br",null),React.createElement(MemoryForm,{memory:this.props.formMemory,formAction:FormAction.UPDATE,updateMemory:this.props.updateMemory})));}else if(this.props.action==Action.NEW){return(React.createElement("div",{id:"memories-area"},React.createElement(Portal,{type:PortalType.RETURN_FROM_FORM,action:Action.INDEX,setAction:this.props.setAction}),React.createElement("br",null),React.createElement(MemoryForm,{memory:this.props.formMemory,formAction:FormAction.CREATE,createMemory:this.props.createMemory})));}else{console.error('No such action.');}},componentDidMount:function(){cssCenterHoverArea();},_createMemoryNodesOfType:function(viewName){var chooseMemoryNode=function(view,generalProps){if(view==View.STREAM){return(React.createElement(StreamMemoryNode,React.__spread({},generalProps,{key:generalProps.id})));}else{console.log("No such view in chooseMemoryNode.");}};return this.props.shownMemories.map(function(memory){var props={};


 props.id=memory.id+"-memory";var validateDateStr=false; props.memory=Structure.buildMemoryNoDate(memory.id,memory.content,memory.dateStr,memory.isExactDate,memory.note);props.setHoverText=this.setHoverText;props.launchMemoryForm=this.props.launchEditMemoryForm;return chooseMemoryNode(this.props.view,props);},this);},setHoverText:function(text){this.setState({hoverText:text});},});var Action=global.Action;var DownloadArea=React.createClass({displayName:"DownloadArea",propTypes:{},render:function(){return(React.createElement("div",{id:"download-area"},React.createElement("button",{onClick:this._download,type:"button",className:"btn btn-default","aria-label":"download"},React.createElement("i",{className:"glyphicon glyphicon-download-alt","aria-hidden":"true"})," Download")));},_download:function(){downloadMemories();}});var UploadArea=React.createClass({displayName:"UploadArea",propTypes:{
setMemories:React.PropTypes.func.isRequired,launchBlankSlate:React.PropTypes.func.isRequired},render:function(){return(React.createElement("div",{id:"upload-area"},React.createElement("div",{id:"clean-start",className:"big-button well well-lg"},React.createElement("h1",null,"New ",React.createElement("br",null),React.createElement("br",null),React.createElement("i",{className:"glyphicon glyphicon-pencil"}))),React.createElement("input",{type:"file",id:"file-input",style:displayNone}),React.createElement("div",{id:"dropzone",className:"big-button well well-lg"},React.createElement("h1",null,"Existing ",React.createElement("br",null),React.createElement("small",null,"Drag & Drop, or Click Here"),React.createElement("br",null),React.createElement("br",null),React.createElement("i",{className:"glyphicon glyphicon-cloud-upload"}))),React.createElement("output",{id:"list"})))},componentDidMount:function(){if(window.File&&window.FileReader&&window.FileList&&window.Blob){if(switch_SKIP_UPLOAD_INTERACTION)
showMemoryPlaygroundAndHideUpload();cssPrettifyDropzoneDragEnterAndLeave();$("#clean-start").click(this.props.launchBlankSlate);var dropzone=$('#dropzone');var domDropzone=dropzone[0];domDropzone.addEventListener('dragover',handleDragOver,false);domDropzone.addEventListener('drop',(function(evt){handleDroppedFileSelect(evt,this.props.setMemories);}).bind(this),false);$('#file-input').bind('change',{setMemoriesFn:this.props.setMemories
},readSingleFile);dropzone.click(function(evt){$('#file-input').click();});}else{alert('The File APIs are not fully supported by your browser.');}}});var Action=global.Action;var Structure=global.Structure;var View=global.View;var defaultMemories=[{id:1,content:'I felt something. Something great.',dateStr:'11/20/2013',date:null,isExactDate:true,note:'feeling'},{id:2,content:'I ran to a great place.',dateStr:'10/20/2013',date:null,isExactDate:true,note:'exercise'},{id:3,content:'Went through a bunch of my old music from a decade ago! So many notes, so many sheets.',dateStr:'12/20/2013',date:null,isExactDate:true,note:'piano'},];for(var m_idx in defaultMemories){var mem=defaultMemories[m_idx];mem.date=Date.parseString(mem.dateStr);}
var Playground=React.createClass({displayName:"Playground",getInitialState:function(){var initVars={allMemories:[],shownMemories:[],searchText:"",action:Action.INDEX,view:View.STREAM,formMemory:Structure.getBlankMemory()};if(switch_SKIP_TO_EDITING){initVars.action=Action.EDIT;}
if(switch_SKIP_UPLOAD_INTERACTION){initIndexDirectly(defaultMemories);initVars.allMemories=Index.memoryObjects;initVars.shownMemories=this._defaultShownMemories(initVars.allMemories);}
return initVars;},render:function(){var quickControls=null;var menu=null;if(this.state.action==Action.INDEX){quickControls=(React.createElement("div",{id:"quick-controls"},React.createElement(DownloadArea,null),React.createElement(Portal,{type:PortalType.CUSTOM,icon:"fa fa-plus",msg:"Create",detailedAction:this.launchNewMemoryForm})));menu=(React.createElement(MenuBar,{searchText:this.state.searchText,action:this.state.action,view:this.state.view,viewList:global.ViewList,handleSearch:this.handleSearch,setView:this.setView}));}
return(React.createElement("div",{id:"main"},React.createElement("div",{id:"playground"},quickControls,menu,React.createElement("hr",null),React.createElement(MemoriesArea,{allMemories:this.state.allMemories,action:this.state.action,launchNewMemoryForm:this.launchNewMemoryForm,launchEditMemoryForm:this.launchEditMemoryForm,formMemory:this.state.formMemory,createMemory:this.createMemory,updateMemory:this.updateMemory,shownMemories:this.state.shownMemories,setAction:this.setAction,view:this.state.view})),React.createElement(UploadArea,{setMemories:this.setMemories,launchBlankSlate:this.launchBlankSlate})));},componentDidMount:function(){if(switch_SKIP_UPLOAD_INTERACTION)return;$("#playground").hide();},_defaultShownMemories:function(mems){var limit=30;if(mems.length>limit){return mems.slice(0,limit);}else{return mems;}},_doSearch:function(searchStr){var mems=[];for(key in this.state.allMemories){var mem=this.state.allMemories[key];if(mem.content.toLowerCase().indexOf(searchStr)>-1||mem.note&&mem.note.toLowerCase().indexOf(searchStr)>-1){mems.push(mem);}}
return mems;},handleSearch:function(searchStr){var newMemoriesToShow=this._doSearch(searchStr.toLowerCase());this.setState({searchText:searchStr,shownMemories:newMemoriesToShow});},launchBlankSlate:function(){console.log('launching blank slate');showMemoryPlaygroundAndHideUpload();this.launchNewMemoryForm();},launchNewMemoryForm:function(){console.log('launching new form');var mem=Structure.getBlankMemory();this.setState({action:Action.NEW,formMemory:mem});},launchEditMemoryForm:function(mem){console.log('launching edit form');console.log(mem);if(!mem){console.error('launchMemoryForm: given memory is null');}
this.setState({action:Action.EDIT,formMemory:mem});},setAction:function(newAction){this.setState({action:newAction});},setMemories:function(mems){this.setState({allMemories:mems,shownMemories:this._defaultShownMemories(mems)});if(!switch_WARN_ON_EXIT)return;var queryUserOnExit=Index.memoryObjects.length>0?true:false;util.setConfirmUnload(queryUserOnExit,function(){return"Some memories are here.";});},setView:function(newView){this.setState({view:newView});},createMemory:function(newMem){console.log("Calling create memory with new mem: ");console.log(newMem);var mid=newMem.id;var newContent=newMem.content;var newDateStr=newMem.dateStr;var newDate=newMem.date;var newIsExactDate=newMem.isExactDate;var newNote=newMem.note;var dbMemory=Structure.buildMemory(mid,newContent,newDateStr,newDate,newIsExactDate,newNote);var mems=Index.memoryObjects;mems.push(dbMemory);this.setMemories(mems);this.setState({action:Action.INDEX});},updateMemory:function(newMem){console.log("Calling update memory with new mem: ");console.log(newMem);var memID=newMem.id;var dbMemory=util.findById(memID,Index.memoryObjects);dbMemory.content=newMem.content;dbMemory.dateStr=newMem.dateStr;dbMemory.date=newMem.date;dbMemory.note=newMem.note;dbMemory.isExactDate=newMem.isExactDate;var mems=Index.memoryObjects;this.setMemories(mems);this.setState({action:Action.INDEX});}});$(document).ready(function(){var containerID='playground-container';if(document.getElementById(containerID)){React.render(React.createElement(Playground,null),document.getElementById(containerID));}});
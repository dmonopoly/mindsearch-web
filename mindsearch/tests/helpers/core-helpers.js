var CoreHelpers = {
  // @param {assertion} bool The thing to assert.
  // @param {options} Hash various data with these possible keys: 'msg'
	assert: function(assertion, options) {
		if (assertion) return;
		console.trace();
		if (options.msg)
			throw new Error(options.msg);
		else
			throw new Error('Assertion failed');
	},
	runTest: function(fn) {
		fn();
		console.log('Success: ' + _fnName(fn) + ' passed');
	},
}

function _fnName(fun) {
  var ret = fun.toString();
  ret = ret.substr('function '.length);
  ret = ret.substr(0, ret.indexOf('('));
  return ret;
}

var Core = CoreHelpers;
var Index = global.Index;

function initIndexFromTextGeneralTest() {
	console.log('Testing initIndexFromText');

	console.log('Set 1: Normal memories -------- ');
	initIndexFromText(set1);
	Core.assert(Index.memoryObjects.length == 2, { msg: 'Length of memoryObjects is incorrect.' });
	Core.assert(Index.memoryObjects[0].isExactDate, { msg: 'isExactDate is incorrect for memory 0.' });
	Core.assert(Index.memoryObjects[1].isExactDate, { msg: 'isExactDate is incorrect for memory 1.' });
	Core.assert(!Index.memoryObjects[0].note, { msg: 'note is incorrect for memory 0.' });
	Core.assert(!Index.memoryObjects[1].note, { msg: 'note is incorrect for memory 1.' });
	console.log('End Set 1 -------- ');

	console.log('Set 2: Memories that are inexact -------- ');
	initIndexFromText(set2);
	Core.assert(Index.memoryObjects.length == 2, { msg: 'Length of memoryObjects is incorrect.' });
	Core.assert(!Index.memoryObjects[0].isExactDate, { msg: 'isExactDate is incorrect for memory 0.' });
	Core.assert(!Index.memoryObjects[1].isExactDate, { msg: 'isExactDate is incorrect for memory 1.' });

	Core.assert(!Index.memoryObjects[0].note, { msg: 'note is incorrect for memory 0.' });
	Core.assert(!Index.memoryObjects[1].note, { msg: 'note is incorrect for memory 1.' });
	console.log('End Set 2 -------- ');

	console.log('Set 3: Memories that have notes -------- ');
	initIndexFromText(set3);
	Core.assert(Index.memoryObjects.length == 2, { msg: 'Length of memoryObjects is incorrect.' });
	Core.assert(Index.memoryObjects[0].isExactDate, { msg: 'isExactDate is incorrect for memory 0.' });
	Core.assert(Index.memoryObjects[1].isExactDate, { msg: 'isExactDate is incorrect for memory 1.' });

	Core.assert(Index.memoryObjects[0].note, { msg: 'note is incorrect for memory 0.' });
	Core.assert(Index.memoryObjects[1].note, { msg: 'note is incorrect for memory 1.' });
	console.log('End Set 3 -------- ');

	console.log('Set 4: Memories that have notes and are inexact -------- ');
	initIndexFromText(set4);
	Core.assert(Index.memoryObjects.length == 2, { msg: 'Length of memoryObjects is incorrect.' });
	Core.assert(!Index.memoryObjects[0].isExactDate, { msg: 'isExactDate is incorrect for memory 0.' });
	Core.assert(!Index.memoryObjects[1].isExactDate, { msg: 'isExactDate is incorrect for memory 1.' });

	Core.assert(Index.memoryObjects[0].note, { msg: 'note is incorrect for memory 0.' });
	Core.assert(Index.memoryObjects[1].note, { msg: 'note is incorrect for memory 1.' });
	console.log('End Set 4 -------- ');
	return true;
}

function initIndexFromTextFormatTest() {
	console.log('Testing initIndexFromTextFormatTest');

	initIndexFromText(set5);

	// The memory that has 2 line breaks in a row should preserve these new lines
	// in the parsed memory.
	var correctStr = 'I wonder if thotdrop/mindsearch/all this journaling is just a hobby like music\n\nYeah...\n\n';
	console.log('Actual:');
	console.log(Index.memoryObjects[1].content);
	console.log('Expected:');
	console.log(correctStr);
	Core.assert(Index.memoryObjects[1].content.trim() == correctStr.trim(), { msg: 'content is not correct' });
}

Core.runTest(initIndexFromTextGeneralTest);
Core.runTest(initIndexFromTextFormatTest);

# Mindsearch

Search inside yourself. Exploring the power of writing.

## Motivation

Issues with existing note services:

- \*Lack of freedom to play around with data
- \*Need better search functionality to interact memories
- Visualization is not good enough
- Can't easily modify thoughts, group them into bottles like I want to
- Enter notes with dates of a long time ago
- Free writing interface that emphasizes personal exploration in writing

## Q&A

*Why would I need an account if the service just gives me my writing in a text file and saves nothing?
Why signup?*

- Saved settings
- Plugins, more customizability
- Updates on site features
- Premium features
- Sharing capabilities

(not implemented)

*What about editing on mobile?*

- Mobile method of creating and editing... just changing a text file is cheap. but flexible, b/c you could put it anywhere. but then compiling it all together is a hassle.
copy paste, drag a bunch of files. is that okay? Yeah.

The true power is the visualization, reminders, and flexibility to edit and explore on your own!

*How am I empowered to interact with my memories as I please?*

- Thotdrop provides a default means of interacting with your memories, like Cloud, Stream.
- This is the essence, but there could be plugins/scripts for more functionality, like the thots-in-a-bottle tool (not implemented)
- It's all just javascript interacting with text.

In the plugins section of your account, you can add your javascript plugin and css stylings.

If you want, you can also share your plugin with others!

## Quickstart

First, set your app's secret key as an environment variable. For example, add the following to .bashrc or .bash_profile:

    export MINDSEARCH_SECRET = 'something-really-secret'

Then run the following commands to bootstrap your environment.

    git clone https://github.com/dmonopoly/mindsearch
    pip install -r requirements/dev.txt
    cd mindsearch
    python manage.py db init
    python manage.py db migrate
    python manage.py db upgrade
    python manage.py server



## Deployment

### Set environment variables
In your production environment, make sure the `MINDSEARCH_ENV` environment variable is set to `prod`.
This is checked in manage.py.

### JSX precompilation

Make sure JSX is compiled properly.
https://facebook.github.io/react/docs/tooling-integration.html#productionizing-precompiled-jsx

Assuming playground is the only folder with JSX:

*From project root:*

    jsx mindsearch/static/js/playground/ mindsearch/static/final/built-jsx/playground

To constantly update changes, you can pass `--watch` / `-w`:


## Shell

To open the interactive shell, run ::

    python manage.py shell

By default, you will have access to ``app``, ``db``, and the ``User`` model.


## Running Tests

To run all tests, run ::

    python manage.py test


## Migrations

Whenever a database migration needs to be made. Run the following commmands:

    python manage.py db migrate

This will generate a new migration script. Then run:

    python manage.py db upgrade

To apply the migration.

For a full migration command reference, run ``python manage.py db --help``.

## UI
For those icons: http://fortawesome.github.io/Font-Awesome/icons/#web-application

## Small Issues
